<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:47:53pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Category extends Model
{

    public $fillable = ['name','parent_id'];

    //public $timestamps = false;
    
    //protected $table = 'categories';

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }

	
}
