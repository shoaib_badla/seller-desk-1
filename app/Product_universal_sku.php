<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product_universal_sku.
 *
 * @author  The scaffold-interface created at 2017-01-16 12:43:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Product_universal_sku extends Model
{
	
	
    protected $table = 'product_universal_skus';

	
}
