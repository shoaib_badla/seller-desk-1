<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Attributes_datum.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:17:13pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attributes_datum extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'attributes_data';

	
}
