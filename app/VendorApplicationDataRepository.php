<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Config,DB,Auth;

class VendorApplicationDataRepository extends EloquentRepositoryAbstract {
 
 /*
    public function __construct()
    {
        $this->Database = new Vendor_info();
 
        $this->visibleColumns = array('firstname','lastname','vendor_email','action_column');
 
        $this->orderBy = array(array('id', 'asc'));
    }*/
	
	
  /**
   * Calculate the number of rows. It's used for paging the result.
   *
   * @param    array $filters
   *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
   *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
   *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
   *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
   *  The 'data' key will contain the string searched by the user.
   * @return  integer
   *  Total number of rows
   */
  public function getTotalNumberOfRows(array $filters = array())
  {
      $vendor_info =  Vendor_info::select('id');
	  
	   $vendor_info->leftJoin('form_of_organization', function ($join) {
            $join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
        });
		 $vendor_info->leftJoin('city', function ($join) {
            $join->on('vendor_infos.city', '=', 'city.id');
        });
		 $vendor_info->leftJoin('province_tbl', function ($join) {
            $join->on('vendor_infos.province', '=', 'province_tbl.id');
        });
	
	
	  if(!empty($filters)){
		  foreach($filters as $filtersRow){
			  
			  $field 	= $filtersRow['field'];
			  $op 		= $filtersRow['op'];
			  $data 	= $filtersRow['data'];
			  
			   //SKIP FILTER IF DATA ALL
			  if($data=='all'){
				  continue;
			  }
			  
			  //FORM OF ORGANIZATION
			  if($field=='form_of_organization'){
				  $vendor_info->where('form_of_organization.label', $op,$data);
			  }elseif($field=='province'){
				  $vendor_info->where('province_tbl.name', $op,$data);
			  }elseif($field=='city'){
				  $vendor_info->where('city.name', $op,$data);
			  }elseif($field=='is_vendor_approved'){
				  
				  if($data == 4){
					   $vendor_info->whereIn('vendor_infos.is_vendor_approved', array(4,5));
				  }elseif($data == 2){
					   $vendor_info->whereIn('vendor_infos.is_vendor_approved', array(2,3));
				  }else{
					   $vendor_info->where($field, $op,$data);
				  }
				 
			  }else{
				  $vendor_info->where($field, $op,$data);
			  }
			  
		  }
	  }
	  
	  //GET LOGIN USER ASSIGNED APPLICATION LIST
	  $vendor_info->where('vendor_manager_assigned', '=', Auth::user()->id);
	  
       //FILTER APPROVED DATA
	 // $vendor_info->where('status', '=', Config::get('constants.vendorAcceptedStatus'));
	  $vendor_info->whereIn('status', array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')));
	 
	  return $vendor_info->count();
	  
  }
 
  /**
   * Get the rows data to be shown in the grid.
   *
   * @param    integer $limit
   *  Number of rows to be shown into the grid
   * @param    integer $offset
   *  Start position
   * @param    string $orderBy
   *  Column name to order by.
   * @param    array $sord
   *  Sorting order
   * @param    array $filters
   *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
   *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
   *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
   *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
   *  The 'data' key will contain the string searched by the user.
   * @return  array
   *  An array of array, each array will have the data of a row.
   *  Example: array(array("column1" => "1-1", "column2" => "1-2"), array("column1" => "2-1", "column2" => "2-2"))
   */
  public function getRows($limit, $offset, $orderBy = null, $sord = null, array $filters = array(),$nodeId = null, $nodeLevel = null,$exporting)
  {
	  $vendor_infos = Vendor_info::select('vendor_infos.id','vendor_infos.vendor_name','vendor_infos.vendor_email','vendor_infos.cell_phone','vendor_infos.is_vendor_approved','vendor_infos.is_vendor_finance_approved','vendor_infos.is_vendor_ops_approved',DB::Raw('form_of_organization.label AS form_of_organization'),'vendor_infos.ntn_number',DB::Raw('province_tbl.name AS province'),DB::Raw('city.name AS city'),'status');
	
		 $vendor_infos->leftJoin('form_of_organization', function ($join) {
            $join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
        });
		 $vendor_infos->leftJoin('city', function ($join) {
            $join->on('vendor_infos.city', '=', 'city.id');
        });
		 $vendor_infos->leftJoin('province_tbl', function ($join) {
            $join->on('vendor_infos.province', '=', 'province_tbl.id');
        });

	    if(!empty($filters)){
		  foreach($filters as $filtersRow){
			 
			  $field 	= $filtersRow['field'];
			  $op 		= $filtersRow['op'];
			  $data 	= $filtersRow['data'];
			  
			  
			   //SKIP FILTER IF DATA ALL
			  if($data=='all'){
				  continue;
			  }
			  
			  //FORM OF ORGANIZATION
			  if($field=='form_of_organization'){
				  $vendor_infos->where('form_of_organization.label', $op,$data);
			  }elseif($field=='province'){
				  $vendor_infos->where('province_tbl.name', $op,$data);
			  }elseif($field=='city'){
				  $vendor_infos->where('city.name', $op,$data);
			  }elseif($field=='is_vendor_approved'){
				  
				  if($data == 4){
					   $vendor_infos->whereIn('vendor_infos.is_vendor_approved', array(4,5));
				  }elseif($data == 2){
					   $vendor_infos->whereIn('vendor_infos.is_vendor_approved', array(2,3));
				  }else{
					   $vendor_infos->where($field, $op,$data);
				  }
				 
			  }else{
				  $vendor_infos->where($field, $op,$data);
			  }
			  
		  }
	  }
	  
	  if(!empty($orderBy) && !empty($sord)){
		$vendor_infos->orderBy($orderBy, $sord);
	  }
	  //FILTER APPROVED DATA
	  //$vendor_infos->where('status', '=', Config::get('constants.vendorAcceptedStatus'));
	  $vendor_infos->whereIn('status', array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')));
	  
	    //GET LOGIN USER ASSIGNED APPLICATION LIST
	  $vendor_infos->where('vendor_manager_assigned', '=', Auth::user()->id);
	  
	   $vendor_infos->offset($offset);
	   $vendor_infos->limit(15);
		$vendorData = $vendor_infos->get();
	  
	  $data = array();
	  if($vendorData->count() > 0){
		  $sno = 1;
		  foreach($vendorData as $vendor_infosRow){
			  
			  $updateUrl = url("vendor_info/edit/".$vendor_infosRow->id);
			  
			  //SET ACTION COLOUMN
			 // $actionColoumn = "<a href='javascript:void(0)'  class='viewVendor' vendor-id='".$vendor_infosRow->id."'><small class='label bg-purple'><i class='fa fa-clock-o'></i>View</small></a> | <a href='javascript:void(0)'  class='approvedVendor' vendor-id='".$vendor_infosRow->id."'><small class='label label-success'><i class='fa fa-clock-o'></i>Approved</small></a> | <a href='javascript:void(0)' class='rejectedVendor' vendor-id='".$vendor_infosRow->id."'><small class='label label-danger'><i class='fa fa-clock-o'></i>Rejected</small></a> " ;
			  
			  //IF VENDOR NOT SUBMITTED INFO YET
			  	if($vendor_infosRow->is_vendor_approved==0){
					 // SET ACTION COLOUMN
					//$actionColoumn = "<a href='javascript:void(0)'  class='viewVendor' vendor-id='".$vendor_infosRow->id."'><small class='label bg-purple'><i class='fa fa-clock-o'></i>View</small></a> | <a href='javascript:void(0)' class='rejectedVendor' vendor-id='".$vendor_infosRow->id."'><small class='label label-danger'><i class='fa fa-clock-o'></i>Rejected</small></a> " ;
				}
				
				
				$actionColoumn	=	"<a href='javascript:void(0)'  class='viewVendor' vendor-id='".$vendor_infosRow->id."'><small class='label bg-purple'><i class='fa fa-clock-o'></i>View</small></a>";
			  //VENDOR APPLICATION STATUS
			  $vendorApplicationStatus = "-";
			  //IF STATE NEW
				if($vendor_infosRow->is_vendor_approved==0){
					 //return view('vendor/welcome');
					$vendorApplicationStatus = "<span class='label label-info'>Pending Vendor Detail</span>";
				}
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_approved==1){
					$vendorApplicationStatus = "<span class='label label-success'>Vendor Onboard</span>";
				}
			
				
				 //IF STATE  Waiting
				if(in_array($vendor_infosRow->is_vendor_approved,array(2,3))){
					//return redirect('marginwaiting');
					$vendorApplicationStatus = "<span class='label label-info'>Vendor Detail Pending for Review</span>";
				}
				
				 //IF STATE  REJECTED
				if(in_array($vendor_infosRow->is_vendor_approved,array(4,5))){
					$vendorApplicationStatus = "<span class='label label-warning'>Vendor Detail send for Correction</span>";
				}
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_approved==6){
					$vendorApplicationStatus = "<span class='label label-info'>Send for Other's Depart Review </span>";
				}
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_approved==7){
					$vendorApplicationStatus = "<span class='label label-success'>Application Reviewed By Other Depart's</span>";
				}
			  
			  /**
			  * VENDOR APPLICATION FINANCE STATUS
			  */
			    //VENDOR APPLICATION STATUS
			  $vendorApplicationFinanceStatus = "<span class='label label-info'>N/A</span>";
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_finance_approved==1){
					$vendorApplicationFinanceStatus = "<span class='label label-success'>Approved</span>";
				}
			
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_finance_approved==2){
					//return redirect('marginwaiting');
					$vendorApplicationFinanceStatus = "<span class='label label-danger'>Rejected</span>";
				}
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_finance_approved==3){
					//return redirect('marginwaiting');
					$vendorApplicationFinanceStatus = "<span class='label label-info'>Pending</span>";
				}
				
			  /**
			  * VENDOR APPLICATION OPS STATUS
			  */
			    //VENDOR APPLICATION STATUS
			  $vendorApplicationOpsStatus = "<span class='label label-info'>N/A</span>";
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_ops_approved==1){
					$vendorApplicationOpsStatus = "<span class='label label-success'>Approved</span>";
				}
			
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_ops_approved==2){
					//return redirect('marginwaiting');
					$vendorApplicationOpsStatus = "<span class='label label-danger'>Rejected</span>";
				}
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_ops_approved==3){
					//return redirect('marginwaiting');
					$vendorApplicationOpsStatus = "<span class='label label-info'>Pending</span>";
				}
			  
			  /**
			  * SET ORDER STATUS
			  */
			  $vendorStatus = !empty($vendor_infosRow->status)?$vendor_infosRow->status:'-';
			   if($vendorStatus == Config::get('constants.vendorNewStatus')) {
				   $vendorStatus	= ucfirst($vendorStatus);
				   $vendorStatus = "<span class='label label-info'>$vendorStatus</span>";
			   }
			   
			   if($vendorStatus == Config::get('constants.vendorApprovedStatus')) {
				  $vendorStatus	= ucfirst($vendorStatus);
				  $vendorStatus = "<span class='label label-success'>$vendorStatus</span>";
			   }
			   
			    if($vendorStatus == Config::get('constants.vendorRejectedStatus')) {
					$vendorStatus	= ucfirst($vendorStatus);
				   $vendorStatus = "<span class='label label-danger'>$vendorStatus</span>";
			   }
			   
			   
			   
			  
			  $data[] = array(
					'id'							=>	$sno,
					'status'						=>	$vendorStatus,
					'is_vendor_approved'			=>	$vendorApplicationStatus,
					'is_vendor_finance_approved'	=>	$vendorApplicationFinanceStatus,
					'is_vendor_ops_approved'		=>	$vendorApplicationOpsStatus,
					'vendor_name'					=>	$vendor_infosRow->vendor_name,
					'vendor_email'					=>	$vendor_infosRow->vendor_email,
					'cell_phone'					=>	$vendor_infosRow->cell_phone,
					'form_of_organization'			=>	$vendor_infosRow->form_of_organization,
					'ntn_number'					=>	$vendor_infosRow->ntn_number,
					'province'						=>	$vendor_infosRow->province,
					'city'							=>	$vendor_infosRow->city,
					'action_column'					=>	$actionColoumn
			  );
			  
			  $sno++;
		  }
	  }else{
		  $data[] = array(
					'id'							=>	'',
					'is_vendor_approved'			=>	'No Record Found',
					'is_vendor_finance_approved'	=>	'',
					'is_vendor_ops_approved'		=>	'',
					'status'						=>	'',
					'vendor_name'					=>	'',
					'vendor_email'					=>	'',
					'cell_phone'					=>	'',
					'form_of_organization'			=>	'',
					'ntn_number'					=>	'',
					'province'						=>	'',
					'city'							=>	'',
					'action_column'					=>	''
			  );
	  }

	  return $data;
	  /*
      return array(
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => 'Limit->'.$limit." Offset->$offset"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  );*/
				 
  }
}
