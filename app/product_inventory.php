<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Product_Inventory extends Model
{

    protected $table = 'product_inventory';

}
