<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Brand.
 *
 * @author  The scaffold-interface created at 2017-01-23 10:03:51am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class BusinessModel extends Model
{
	
	
    protected $table = 'business_model';

	
}
