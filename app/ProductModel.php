<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use File;
use DB;

class ProductModel extends Model{

    // add and update product data
    public function productdataadd($data){

        //not in this phase
        //$this->addproductuniversalsku($data);
            $productid = $this->addmainproduct($data);
            $this->addproductinventory($data,$productid);
            $this->addproductcategories($data,$productid);
            $this->addproductimages($data,$productid);
            $this->addproductverianceatt($data,$productid);
            $this->addproductconfigurable($data,$productid);
            $this->changestatuspendingqc($productid);
            if ($productid !== null){
                return "Record Has been Added Succesfully";
            }


        // add product data end
    }
    public function productdataupdate($data){


        //not in this phase
        //$this->addproductuniversalsku($data);

            $this->updatemainproduct($data,$data["id"]);
            $this->updateproductinventory($data,$data["id"]);
            $this->updateproductcategories($data,$data["id"]);
            $rules= $this->getallcheckrules();
            $this->updateproductverianceatt($data,$data["id"],$rules);

            //$this->updateproductimage($data,$data["id"]);
            $this->resetapprovalqc($data["id"]);
            $this->resetapprovalvm($data["id"]);
            return "Record Has been Updated Succesfully";

        // add product data end
    }

    // add the universal sku
    private function addproductuniversalsku($data){
        // add universal sku start
        $uni_sku=Product_universal_sku::where('universal_sku','=',$data['universal_sku'])->get();
        if ($uni_sku->count() < 1)
        {

//            add the universal sku if it's not exist
            $pus = new Product_universal_sku();
            $pus->universal_sku = $data['universal_sku'];
            $pus->product_count = intval($pus->product_count) + 1;
            $pus->created_at = date("Y-m-d H:i:s");
            $pus->save();
            $universal_sku = $pus->id;

        }
        else
        {
            $universal_sku = $uni_sku[0]->id;

        }
    }

    // add the main product data
    private function addmainproduct($data){

        $pro = new Product();
        $pro->attribute_set_id = $data["attribute_sets"];
        $pro->sku = $data['sku'];
        $pro->mode_of_fullfillment = $data['mode_of_fullfillment'];
        $pro->vendor_id = $data['vendor'];
        $pro->universal_sku_id = 0;//$pro->product_count + 1;
        $pro->created_at = date("Y-m-d H:i:s");
        $pro->save();
        return $pro->id;
    }

    // add the inventory
    private function addproductinventory($data,$productid){
        // add inventory data start
        $productinv = new Product_Inventory();
        $productinv->product_id = $productid;
        $productinv->dropship_qty =$data['qty_dropship'];
        $productinv->stocking_qty = $data['qty_stocking'];
        $productinv->save();
        // add inventory data end
    }

    //add product different attribute data
    private function addproductverianceatt($data,$productid){
        foreach ($data as $key => $value )
        {

            if($key != "_token" && $key != "universal_sku" && $key != "sku" && $key != "qty_dropship" && $key != "qty_stocking" && $key != "vendor" && $key != "categories" && $key != "attribute_sets")
            {

                if ($value != "" || $value != null){

                    if(!is_int($key))
                    {
                        $key = $this->getattributeid($key);
                    }
                    if($key != "")
                    {
                        $entry = new Attributes_datum();
                        $entry->attribute_id = $key;
                        if(is_array($value)){
                            $entry->value = $value[0];
                        }
                        else{
                            $entry->value = $value;
                        }

                        $entry->product_id = $productid;
                        $entry->created_at = date("Y-m-d H:i:s");
                        $entry->save();
                    }

                }
            }
        }
    }

    // add the product images
    private function addproductimages($data,$productid){
        // add product images start
        $temimagefiles = Temimage_files::where('token', '=', $data['_token'])->get();
        if ($temimagefiles->count() > 0)
        {
            foreach ( $temimagefiles as $temimagefile) {
                $product_media = new Product_media();
                $product_media->product_id = $productid;
                $product_media->parent_id =$temimagefile->parent_id;
                $product_media->lft = $temimagefile->lft;
                $product_media->rgt = $temimagefile->rgt;
                $product_media->depth = $temimagefile->depth;
                $product_media->type = $temimagefile->type;
                $product_media->has_thumbnail = $temimagefile->has_thumbnail;
                $product_media->meta = $temimagefile->meta;
                $product_media->filename = $temimagefile->filename;

                $product_media->alias = $temimagefile->alias;
                $product_media->public_resource_url = $temimagefile->public_resource_url;
                if(isset($data[$temimagefile->id."imagetype"])){
                    foreach ($data[$temimagefile->id."imagetype"] as $imagetype)
                    {
                        $imagetypedata = explode('|',$imagetype);
                        if ($imagetypedata[0]= $temimagefile->id)
                        {
                            $product_media->imagetype = $imagetypedata[1];
                        }

                    }
                }
                else
                {
                    $product_media->imagetype = "thumbnail";
                }

                $product_media->save();
            }
            Temimage_files::where('token', '=', $data['_token'])->delete();
        }
        // add product images end
    }

    //add product categories
    private function addproductcategories($data,$productid){
        // add category product start
        $cat = new Categories_product();
        $cat->cat_id = $data['categories'];
        $cat->product_id = $productid;
        $cat->save();
        // add category product end
    }

    //add product configurable option
    private function addproductconfigurable($data,$productid){
        // add category product start
        $configurableopt = new Product_configurable_option();
        $count = 0;
        foreach ($data["optionalattr"] as $opt)
        {
            if( $data["optionalattr"][$count] != null )
            {
                $configurableopt->attribute_id = $data["optionalattr"][$count];
                $configurableopt->attribute_optid = $data["description"][$count];
                $configurableopt->qty = $data["qty"][$count];
                $configurableopt->price = $data["price"][$count];
                $configurableopt->status = $data["status"][$count];
                $configurableopt->cost = $data["cost"][$count];
                $configurableopt->product_id = $productid;
                $configurableopt->created_at = date("Y-m-d H:i:s");
                $configurableopt->save();
            }

            $count++;
        }
        // add category product end
    }

    // update the master product
    private function updateproductconfigurable($data,$id){

        $count = 0;
        $configurableopt = Product_configurable_option::where('id','=',$id)->first();
        $configurableopt->attribute_id = $data["optionalattr"][$count];
        $configurableopt->attribute_optid = $data['description'][$count];
        $configurableopt->qty = $data['qty'][$count];
        $configurableopt->price = $data['price'][$count];
        $configurableopt->status = $data["status"][$count];
        $configurableopt->cost = $data["cost"][$count];
        $configurableopt->product_id = $data["product_id"];
        $configurableopt->updated_at = date("Y-m-d H:i:s");
        $configurableopt->save();
    }

    // update the master product
    private function updatemainproduct($data,$id){

        $pro = Product::where('id','=',$id)->first();
        $pro->attribute_set_id = $data["attribute_sets"];
        $pro->sku = $data['sku'];
        $pro->mode_of_fullfillment = $data['mode_of_fullfillment'];
        $pro->vendor_id = $data['vendor'];
        $pro->universal_sku_id = 0;//$pro->product_count + 1;
        $pro->updated_at = date("Y-m-d H:i:s");
        $pro->save();
    }

    // update inventory data
    private function updateproductinventory($data,$productid){
        // add inventory data start

        $productinv = Product_Inventory::where('product_id', '=', $productid)->first();
        if($productinv === null)
        {
            $this->addproductinventory($data,$productid);
        }
        else
        {
            $productinv->dropship_qty =$data['qty_dropship'];
            $productinv->stocking_qty = $data['qty_stocking'];
            $productinv->save();
        }

        // add inventory data end
    }

    // update product category data
    private function updateproductcategories($data,$productid){
        // add category product start
        $cat = Categories_product::where('product_id','=', $productid)->first();
        if($cat === null)
        {
            $this->addproductcategories($data,$productid);
        }
        else
        {
            $cat->cat_id = $data['categories'];
            $cat->save();
        }

        // add category product end
    }

    //update the different attribute data
    private function updateproductverianceatt($data,$productid,$rules){
        $approval = 0;
        foreach ($data as $key => $value )
        {

            if($key != "_token" && $key != "universal_sku" && $key != "sku" && $key != "qty_dropship" && $key != "qty_stocking" && $key != "vendor" && $key != "categories" && $key != "attribute_sets")
            {
                if ($value != ""){
                    if(!is_int($key))
                    {
                        $key = $this->getattributeid($key);
                    }
                    if($key != "") {
                        $entry = Attributes_datum::where([
                            ['product_id', '=', $productid],
                            ['attribute_id', '=', $key]
                        ])->first();
                        if($entry === null)
                        {
                            $this->addproductverianceatt($data,$productid);
                        }
                        else {
                            if ($entry->value != $value)
                            {
                                foreach ($rules as $rule) {
                                    if ($rule->attribute_id = $entry->attribute_id) {

                                        if ($rule->ruleinpercent != null || $rule->ruleinpercent != 0) {
                                            $approval += 0;
                                        } else {
                                            $ruleval = ($entry->value * $rule->ruleinpercent) / 100;
                                            $addvalue = floatval($entry->value) + floatval($ruleval);
                                            $reducevalue = floatval($entry->value) - floatval($ruleval);
                                            if ($addvalue >= $value && $reducevalue <= $value) {
                                                $approval += 0;
                                            }
                                        }
                                    }
                                    else{
                                        $approval += 1;
                                    }

                                }
                                if(is_array($value)){
                                    $entry->value = $value[0];
                                }
                                else{
                                    $entry->value = $value;
                                }
                                $entry->updated_at = date("Y-m-d H:i:s");
                                $entry->save();
                            }
                        }

                    }
                }
            }
        }
        return $approval;
    }

    //update the product image data
    private function updateproductimage($data,$productid){

        Product_media::where('product_id','=',$productid)->delete();
        $imgarr = array("image","small_image","thumbnail");

        foreach ($imgarr as $imgty)
        {


            $filename = new file($data[$imgty]);
            $oldfilename = $data["image"];
            $oldfilepath = public_path()."/new";
            $list = File::directories($oldfilepath);
            $oldfilepathimg = $list[0]."/";
            $oldfile = $list[0]."/".$data[$imgty];
            $newfilename = date('Ymdhis').$data[$imgty];
            $Storagepath = public_path()."/uploads"."/";
            $newfile = $Storagepath.$newfilename;

            File::copy($oldfilepathimg.$data[$imgty], $newfile);
            //File::move($oldfile, $newfile);

            $mimeType= File::mimeType($oldfile);
            $fileSize = File::size($oldfile);
            $fileExt = File::extension($oldfile);
            /*
             * create thumbnail if needed
             */


            $disk = Storage::disk(config('laradrop.disk'));

            $fileData['has_thumbnail'] = 0;

            if ($fileSize <= ( (int) config('laradrop.max_thumbnail_size') * 1000000) && in_array($mimeType, ["image/jpg", "image/jpeg", "image/png", "image/gif"])) {

                $thumbDims = config('laradrop.thumb_dimensions');
                $img = Image::make($newfile);
                $img->resize($thumbDims['width'], $thumbDims['height']);
                $img->save($Storagepath . '/_thumb_' . $newfilename);

                // move thumbnail to final location
                //$disk->put('_thumb_' . $newfilename, fopen($Storagepath . '/_thumb_' . $newfilename, 'r+'));
                //File::delete($Storagepath . '/_thumb_' . $newfilename);
                $fileData['has_thumbnail'] = 1;

            }

            /*
             * move uploaded file to final location
             */
//            $disk->put($newfilename, fopen($Storagepath . '/' . $newfilename, 'r+'));


            /*
             * save in db
             */

            $fileData['filename'] = $newfilename;
            $fileData['alias'] = $oldfilename;
            $fileData['public_resource_url'] = config('laradrop.disk_public_url') . '/' . $newfilename;
            $fileData['type'] = $fileExt;
            $fileData['parent_id'] = null;
            $fileData['lft'] = 1;
            $fileData['rgt'] = 2;
            $fileData['depth'] = 0;

            if(Input::get('pid') > 0) {
                $fileData['parent_id'] = null;
            }

            $meta = $disk->getDriver()->getAdapter()->getMetaData($newfilename);



            $meta['disk'] = config('laradrop.disk');

            $fileData['meta'] = json_encode($meta);
            $fileData['token'] ="";

            //$file = $this->file->create($fileData);
            $productmedia = new Product_media();
            $productmedia->parent_id = $fileData['parent_id'];
            $productmedia->lft = $fileData['lft'];
            $productmedia->rgt = $fileData['rgt'];
            $productmedia->depth = $fileData['depth'];
            $productmedia->type = $fileData['type'];
            $productmedia->has_thumbnail = $fileData['has_thumbnail'];
            $productmedia->meta = $fileData['meta'];
            $productmedia->filename = $fileData['filename'];

            $productmedia->alias = $fileData['alias'];
            $productmedia->public_resource_url = $fileData['public_resource_url'];
            $productmedia->imagetype = $imgty;
            // $productmedia->token = $fileData['token'];

            $productmedia->product_id = $productid;

            $productmedia->save();

        }

    }

    //get the attribute id by name
    private function getattributeid($attributename){

        $Attr =  Attribute::where('code','=',$attributename)->first();
        if ($Attr === null){
            return "";
        }
        else
        {
            return $Attr->id;
        }
    }

    //get the rule checks
    private function getallcheckrules(){

        return Bypasschecks::all();

    }

    // get the existing vendor sku by vendor id and sku
    public function getvendorsku($vendorid,$sku){

        $product = Product::where([
            ['vendor_id', '=', $vendorid],
            ['sku', '=', $sku]
        ])->first();

        if($product === null){

            return "";
        }
        else
        {

            return $product->id;
        }
    }

    // get l3 categories
    public function getl3categories(){

        $allCategories = Category::join('categories as c3',function($join)
        {
            $join->on('categories.parent_id', '=', 'c3.sf_catid');
            $join->on('c3.level','=',DB::raw(3));
        })->join('categories as c2',function($join)
        {
            $join->on('c3.parent_id', '=', 'c2.sf_catid');
            $join->on('c2.level','=',DB::raw(2));
        })->select('categories.id', 'categories.sf_catid', 'categories.name', 'c3.sf_catid as sf_catidl3','c3.name as l3catname','c3.parent_id as l3parentid', 'c2.sf_catid as sf_catidl2','c2.name as l2catname', 'c2.parent_id as l2parentid' )->get();
        $l1categories = array();
        $l2categories = array();
        $l3categories = array();
        foreach ($allCategories as $category)
        {

            $l1categories = array_prepend($l1categories, $category->l2catname, $category->sf_catidl2);
            $l2categories = array_prepend($l2categories,[$category->l3catname,$category->sf_catidl2 ], $category->sf_catidl3);
            $l3categories = array_prepend($l3categories,[$category->name , $category->sf_catidl3 ], $category->id);

        }

        return ['l1categories'=>$l1categories, 'l2categories'=>$l2categories,'l3categories'=>$l3categories];
    }

    // get l3 categories
    public function getalll3categories(){

        return Category::join('categories as c3',function($join)
        {
            $join->on('categories.parent_id', '=', 'c3.sf_catid');
            $join->on('c3.level','=',DB::raw(3));
        })->join('categories as c2',function($join)
        {
            $join->on('c3.parent_id', '=', 'c2.sf_catid');
            $join->on('c2.level','=',DB::raw(2));
        })->select('categories.id', 'categories.sf_catid', 'categories.name', 'c3.sf_catid as sf_catidl3','c3.name as l3catname','c3.parent_id as l3parentid', 'c2.sf_catid as sf_catidl2','c2.name as l2catname', 'c2.parent_id as l2parentid' )->get();

    }

    //get all attribute selected categories for csv
    public function getAttributeSetFieldName($allcategories){

        $fieldName = array();
        //GET ATTRIBUTES BY ATTRIBUTESET
        $attr_map = Attributes_mapping::join('categories_attributes', 'categories_attributes.attribitute_set_id', '=', 'attributes_mapping.attribute_set_id')
            ->join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
            ->distinct('attributes.code')
            ->whereIn('cat_id', $allcategories)->get();


        if(!empty($attr_map)){
            foreach($attr_map as $attr_mapData){
                $fieldName[]	=	$attr_mapData->code;
            }
        }

        return $fieldName;

    }

    //get attribute set id and name by category id
    public function getattributesbycatid($categoryId)
    {
        return Categories_attribute::leftJoin('attribute_sets', 'attribute_sets.id', '=', 'categories_attributes.attribitute_set_id')
            ->select('attribute_sets.id', 'attribute_sets.name')
            ->where('cat_id', '=', $categoryId)->get();
    }

    //get the list of all vendors
    public function allvendorlist(){
        return Vendor_info::select('vendor_name AS name','id')->get();
    }

    //get all the fullfillment option
    public function allfullfillmentopt(){
        return Fullfillment_option::where('id','=','3')->select('id','name')->get();
    }

    //get the existing product data
    public function getproductdata($id){
        return Product::join('categories_products', 'categories_products.product_id', '=', 'product.id')
            ->join('categories', 'categories.id', '=', 'categories_products.cat_id')
            ->join('product_inventory', 'product_inventory.product_id', '=', 'product.id')
            ->select('product.id','categories.name', 'product.attribute_set_id','product.universal_sku_id','product.sku','categories.id as categories_id', 'product.mode_of_fullfillment','product_inventory.dropship_qty','product_inventory.stocking_qty')
            ->where('product.id', '=',$id)->get();
    }

    //get product attribute data
    public function getproductattributedata($id,$prodata){
        return Attributes_mapping::join('attributes', 'attributes.sf_attributeid', '=', 'attributes_mapping.attribute_id')
            ->leftjoin('attributes_data',function($join) use ($id)
            {
                $join->on('attributes.id', '=', 'attributes_data.attribute_id');
                $join->on('attributes_data.product_id','=',DB::raw("'".$id."'"));
            })
            ->select('attributes.frontend_type','attributes.additional_info','attributes.name','attributes.id as attr_id','attributes_data.attribute_id', 'attributes_data.value')
            ->where([['attributes_mapping.attribute_set_id', '=',$prodata[0]->attribute_set_id],
                ['attributes.name','!=','']])->get();
    }

    //get the attribute options
    public function getattributeoption($attr_ids){
        return Attribute_option_value::whereIn('attribute_id', $attr_ids)->get();
    }

    //get product images by product id
    public function getproductimages($productid){
        return Product_media::where('product_id','=',$productid)->get();
    }

    //change the status pending approval for qc and vm
    public function changestatuspendingqc($productid){
        $data = new Product_approve_reject();
        $data->product_id = $productid;
        $data->pending_app_qc = 1;
        $data->pending_app_vm = 1;

        $data->save();
    }

    //change the status approval for qc
    public function changestatusapproveqc($productid,$comment,$userid){
        $data = Product_approve_reject::where("product_id","=",$productid)->first();
        $data->type_qc = "approve";
        $data->comments_qc = $comment;
        $data->app_reject_by_qc = $userid;
        $data->pending_app_qc = 0;
        $data->create_at_qc = date("Y-m-d H:i:s");
        $data->save();
    }

    //change the status reject for qc
    public function changestatusrejectqc($productid,$comment,$userid){
        $data = Product_approve_reject::where("product_id","=",$productid)->first();
        $data->product_id = $productid;
        $data->type_qc = "reject";
        $data->comments_qc = $comment;
        $data->app_reject_by_qc = $userid;
        $data->pending_app_qc = 0;
        $data->create_at_qc = date("Y-m-d H:i:s");
        $data->save();
    }

    //change the status approval for vm
    public function changestatusapprovevm($productid,$comment,$userid){
        $data = Product_approve_reject::where("product_id","=",$productid)->first();
        $data->product_id = $productid;
        $data->type_vm = "approve";
        $data->comments_vm = $comment;
        $data->app_reject_by_vm = $userid;
        $data->pending_app_vm = 0;
        $data->create_at_vm = date("Y-m-d H:i:s");
        $data->save();
    }

    //change the status reject for vm
    public function changestatusrejectvm($productid,$comment,$userid){
        $data = Product_approve_reject::where("product_id","=",$productid)->first();
        $data->product_id = $productid;
        $data->type_vm = "reject";
        $data->comments_vm = $comment;
        $data->app_reject_by_vm = $userid;
        $data->pending_app_vm = 0;
        $data->create_at_vm = date("Y-m-d H:i:s");
        $data->save();
    }

    //reset the approval of qc
    public function resetapprovalqc($productid){
        $data = Product_approve_reject::where('product_id','=',$productid)->first();
        $data->pending_app_qc = 1;
        $data->type_qc = null;
        $data->comments_qc = null;
        $data->app_reject_by_qc = null;
        $data->create_at_qc = null;
        $data->update_at_qc = date("Y-m-d H:i:s");
        $data->save();
    }

    //reset the approval of vm
    public function resetapprovalvm($productid){
        $data = Product_approve_reject::where('product_id','=',$productid)->first();
        $data->pending_app_vm = 1;
        $data->type_vm = null;
        $data->comments_vm = null;
        $data->app_reject_by_vm = null;
        $data->create_at_vm = null;
        $data->update_at_vm = date("Y-m-d H:i:s");
        $data->save();
    }

    //push to store front
    public function pushstorefront($productid){
        $data = new Push_storefront();
        $data->product_id = $productid;
        $data->type = 1;
        $data->status = null;
        $data->create_at = date("Y-m-d H:i:s");
        $data->save();
    }

    //approval check
    public function approvalcheck($productid){
    $data = Product_approve_reject::where([
        ['product_approve_reject.product_id', '=',$productid],
        ['product_approve_reject.type_qc', '=','approve'],
        ['product_approve_reject.type_vm','=','approve']
    ])->first();

    if($data === null)
    {
        return false;
    }else{
        return true;
    }
}

    //get all the configurable product
    public function getconfigurablepro(){
        return Attribute::where('attributes.is_configurable', '=',1)->get();
    }

    //get all the configurable product option
    public function getconfigurableproopt($id){
        return Attribute_option_value::wherein('attribute_option_values.attribute_id',$id)->get();
    }

    public function getconfigurableprodata($productid){
            return Product_configurable_option::where('product_id', '=',$productid)->get();
    }

    public function addmagento(){
        define('MAGENTO_API_URL_V1_LOCAL','http://park.yayvo.com/index.php/api/?wsdl');
        define('MAGENTO_API_URL_V2_LOCAL','http://park.yayvo.com/index.php/api/v2_soap/?wsdl=1');
        define('MAGENTO_API_V2_USERNAME_LOCAL','arshad');
        define('MAGENTO_API_V2_PASSWORD_LOCAL','123456');
        $id= 48;
        //join('product_universal_skus', 'product_universal_skus.id', '=', 'product.universal_sku_id')  'product_universal_skus.universal_sku'
        $prodata = Product::join('categories_products', 'categories_products.product_id', '=', 'product.id')
            ->join('categories', 'categories.id', '=', 'categories_products.cat_id')
            ->join('product_inventory', 'product_inventory.product_id', '=', 'product.id')
            ->select('product.id','categories.name', 'product.attribute_set_id','product.universal_sku_id','product.sku','categories.id as categories_id', 'product.mode_of_fullfillment','product_inventory.dropship_qty','product_inventory.stocking_qty')
            ->where('product.id', '=',$id)->get();
        $insertdata = array();
        $maininfo = array();
        foreach($prodata as $pdata)
        {

            foreach ($pdata->getattributes() as $key=>$value)
            {
                //sku
                //CategoryIds
                //AttributeSetId
                //TypeId
                //Qty
                //mode_of_fullfillment
                if($key ==  "id")
                {
                    $insertdata = array_prepend($insertdata,$value,"sku");
                }else if($key ==  "mode_of_fullfillment"){
                    $insertdata = array_prepend($insertdata,$value,$key);
                }else if($key ==  "attribute_set_id"){
                    $insertdata = array_prepend($insertdata,$value,$key);
                }else if($key ==  "categories_id"){
                    $maininfo = array_prepend($maininfo,$value,$key);
                }else if($key ==  "dropship_qty"){
                    $qty =intval($pdata->getattributes()["dropship_qty"])+intval($pdata->getattributes()["stocking_qty"]);
                    $maininfo = array_prepend($maininfo,$qty,"Qty");
                }


            }

            $atributedata = Attributes_mapping::join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
                ->leftjoin('attributes_data',function($join) use ($id)
                {
                    $join->on('attributes_mapping.attribute_id', '=', 'attributes_data.attribute_id');
                    $join->on('attributes_data.product_id','=',DB::raw("'".$id."'"));
                })
                ->select('attributes.type','attributes.additional_info','attributes.code','attributes.name','attributes.id as attr_id','attributes_data.attribute_id', 'attributes_data.value')
                ->where([['attributes_mapping.attribute_set_id', '=',$prodata[0]->attribute_set_id],
                    ['attributes.name','!=','']])->get();

            //attributes_data.product_id','=', $id
            $allvendors = $this->allvendorlist();

            $allCategories = Category::where('level', '=', 3)->select('name','id')->get();

            $fullfillmentoption = Fullfillment_option::select('id','name')->get();


            //$attr_id = array();
            foreach ( $atributedata as $attribute) {
                $insertdata = array_prepend($insertdata,$attribute->value,$attribute->code);
                //$attr_id = array_prepend($attr_id, $attribute->attr_id);
            }
            echo "<br>";
            echo "<br>";
            echo "<br>";
            print_r($insertdata);
            echo "<br>";
            echo "<br>";
            echo "<br>";
            print_r($maininfo);

        }

        $productOptiontoSave= ["attributedata"=> $insertdata, "maininfo" => $maininfo];
        // $attr_option_value =  Attribute_option_value::whereIn('attribute_id', $attr_id)->get();
        $files = Product_media::where('product_id','=',$id)->get();

        //$productOptiontoSave  = "test";
        $client_v1 = new SoapClient(MAGENTO_API_URL_V1_LOCAL);
        $session_id_v1 = $client_v1->login(MAGENTO_API_V2_USERNAME_LOCAL, MAGENTO_API_V2_PASSWORD_LOCAL);

        //magento extended api v1 call for product info start
        $responseCatalogProductInfo=$client_v1->call($session_id_v1, 'catalog_category.saveSellerCenterProduct', array($productOptiontoSave));
        //magento extended api v1 call for product info start end

        echo "<br/>responce<br/>";
        print_r($responseCatalogProductInfo);
        echo "<br/><br/>";
        exit();
        echo "--------test";
        exit();
    }
}
?>