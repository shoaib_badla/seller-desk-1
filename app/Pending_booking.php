<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Categories_product.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:49:24pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Pending_booking extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'pending_bookings';

	
}
