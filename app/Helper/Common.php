<?php

namespace App\Helper;
use Auth;
use Config;
use App\Permission\Models\Role;
use App\Vendor_info, App\Rejection_reason;

class Common {

	static public function modeOfFullfillment(){
	   return array('Stocking','CrossDock');
	} 
	
	
	static public  function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	static public function sendEmail($to,$subject,$body,$cc = ""){
		
		$from = "supoort@sellerdesk.com";
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= "From: ".$from . "\r\n" ;
		
		if(!empty($cc)){
			$headers.= "CC: ".$cc;
		}
		

		mail($to,$subject,$body,$headers);
	}

	
	//GET LOGIN VENDOR ID
	static public function getLoginVendorId(){
		
		//GET LOGIN USER ID
		$loginEmail = Auth::user()->email;
		
		$userRole = \App\User::findOrfail(Auth::user()->id);
		//CHECK USER IS VENDOR
		$vendorRole	=	Config::get('constants.vendorRole');
		
		$vendor_id = 0;
		if($userRole->hasRole($vendorRole) ){
			
			$vendor_info	=	Vendor_info::where('vendor_email',$loginEmail)->whereIn('status',array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')))
				->select('id')
                ->first();
				
			
				$vendor_id = isset($vendor_info->id)?$vendor_info->id:0;;
				
			
		}
		
		return $vendor_id;
	}
	
	/*
  * CHECK LOGIN VENDOR MARGIN STATE
  *
  
	0 -> New Application , 
	1-> Application Approved,
	2-> Application send for Approval, 
	3 -> Application send for negotiation ,
	4-> Application Send for Correction  ,
	5 ->  Application Send for Correction Again
	6 ->  Application send to other depart for review
	7 ->  Application reviewd by other depart
  */
 static  public function checkLoginVendorApplicationState(){
	  
	  $marginState = "-";

	   /**
	   * CHECK LOGIN VENDOR MARGIN STATE
	   */
	   //GET LOGIN USER ID
		$loginEmail = Auth::user()->email;
		
		$userRole = \App\User::findOrfail(Auth::user()->id);
		//CHECK USER IS VENDOR
		$vendorRole	=	Config::get('constants.vendorRole');
		if($userRole->hasRole($vendorRole) ){

			$vendor_info	=	Vendor_info::where('vendor_email',$loginEmail)->whereIn('status',array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')))
				->select('is_vendor_approved')
                ->first();
				
			if(!empty($vendor_info)){
				
				
				$marginState = isset($vendor_info->is_vendor_approved)?$vendor_info->is_vendor_approved:0;
					
				/*if($vendor_info->is_margin_approved==0){
					//$marginState = "new";
				}
				
				if($vendor_info->is_margin_approved==1){
					//$marginState = "approved";
				}
				
				if($vendor_info->is_margin_approved==2){
					//$marginState = "pending_approval";
				}
				
				if($vendor_info->is_margin_approved==3){
					//$marginState = "send_for_negotiation";
				}
				
				if($vendor_info->is_margin_approved==4){
					//$marginState = "negotiation_reject";
				}
				
				if($vendor_info->is_margin_approved==5){
					//$marginState = "proposedMarginReject";
				}
				*/
			}	
			
		}
		
		return $marginState;
  }
  
  
  static public function redirectOnBasisOfMarginStatus(){
	    //CHECK MARGIN STATE
		$checkLoginVendorApplicationState = Common::checkLoginVendorApplicationState();
		
			
	  //IF STATE  NEW
		if($checkLoginVendorApplicationState == 0){
			
			return redirect('vendormargin');
			echo "mak";exit();
		
		}
	
		 //IF STATE  NEW
		if($checkLoginVendorApplicationState == 2){
			return redirect('marginwaiting');
		}
  }
//user wise permission
    static public function userwisepermission($userid, $permission){

      $permission_exist = 0;
        $user = \App\User::findOrfail($userid);
        $role = isset($user->roles)?$user->roles:array();
        //$role->hasPermissionTo($permission);
        foreach ($role->pluck('id') as $per)
        {

            $ro = Role::findOrFail($per);

            if ($ro->hasPermissionRoleWise($permission,$per) == true)
            {
                $permission_exist = 1;
            }

        }

        return $permission_exist;

    }
  
  static public function wordWrapContent($Message, $length = 10){
	    $MessageW = wordwrap($Message, $length, "\n", true);
		$MessageW = htmlentities($MessageW);
		
		return $MessageW;
  }
  
  /**
  * GET REJECTION REASON LIST
  */
  
  static public function getRejectionList($department = ""){
	  
	  $reasonList = Rejection_reason::select('reason');
	  
	  if($department!=""){
		  $reasonList->where('department','=',$department);
	  }
	  
	  $reasonList	=	$reasonList->get();
	  
	  $reasonListArr = array();
	  if(!empty($reasonList)){
		  foreach($reasonList as $reasonListRow){
			  $reasonListArr[]	=	$reasonListRow->reason;
		  }
	  }
	  
	  return $reasonListArr;
  }
  
  
  /**
  * GET PAYMENT CYC
  */
  static public function getPaymentCycle(){
	  //return array(15,7,10,21,31);
	  return array(15);
  }
  
 /**
 * CHECK CURRENT RUNNING ON LINUX OR WINDOW
 */
 static public function isWindowEnvironment(){
	 
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		return true;
	}
	
	return false;
 }
}


