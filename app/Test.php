<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Test.
 *
 * @author  The scaffold-interface created at 2017-01-03 06:46:41am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Test extends Model
{
	
	
    protected $table = 'tests';

	
}
