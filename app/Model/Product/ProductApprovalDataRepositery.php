<?php
namespace App\Model\Product;
use App\Product;
use App\Product_approve_reject;
use Illuminate\Database\Eloquent\Model;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use App\Helper\Common;
use Auth;
use DB;

class ProductApprovalDataRepositery extends EloquentRepositoryAbstract {

    /*
       public function __construct()
       {
           $this->Database = new Vendor_info();

           $this->visibleColumns = array('firstname','lastname','vendor_email','action_column');

           $this->orderBy = array(array('id', 'asc'));
       }*/


    /**
     * Calculate the number of rows. It's used for paging the result.
     *
     * @param    array $filters
     *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
     *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
     *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
     *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
     *  The 'data' key will contain the string searched by the user.
     * @return  integer
     *  Total number of rows
     */
    public function getTotalNumberOfRows(array $filters = array())
    {
        $product_info =  Product::select('id');

        if(!empty($filters)){
            foreach($filters as $filtersRow){

                $field 	= $filtersRow['field'];
                $op 	= $filtersRow['op'];
                $data 	= $filtersRow['data'];

                $product_info->where($field, $op,$data);
            }
        }

        return $product_info->count();

    }

    /**
     * Get the rows data to be shown in the grid.
     *
     * @param    integer $limit
     *  Number of rows to be shown into the grid
     * @param    integer $offset
     *  Start position
     * @param    string $orderBy
     *  Column name to order by.
     * @param    array $sord
     *  Sorting order
     * @param    array $filters
     *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
     *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
     *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
     *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
     *  The 'data' key will contain the string searched by the user.
     * @return  array
     *  An array of array, each array will have the data of a row.
     *  Example: array(array("column1" => "1-1", "column2" => "1-2"), array("column1" => "2-1", "column2" => "2-2"))
     */
    public function getRows($limit, $offset, $orderBy = null, $sord = null, array $filters = array(),$nodeId = null, $nodeLevel = null,$exporting)
    {
        $product_infos = Product_approve_reject::join('product', 'product_approve_reject.product_id' ,'=', 'product.id')
            ->leftjoin('attributes_data', 'product.id', '=', 'attributes_data.product_id')
            ->join('attributes',function($join){
                $join->on('attributes.code' ,'=',DB::raw("'name'") );
                $join->on('attributes.id' ,'=', 'attributes_data.attribute_id');} )
            ->join('vendor_infos', 'vendor_infos.id' ,'=', 'product.vendor_id')
            ->join('product_inventory', 'product_inventory.product_id','=', 'product.id')
            ->join('categories_products', 'categories_products.product_id','=', 'product.id')
            ->join('categories', 'categories.id','=', 'categories_products.cat_id')
            ->join('attribute_sets', 'attribute_sets.id','=', 'product.attribute_set_id')
            ->select('product_approve_reject.pending_app_qc','product_approve_reject.pending_app_vm','product.id',DB::raw('attribute_sets.name as attributeset_name'),DB::raw('categories.name as category_name'),DB::raw('attributes_data.value as name'), 'product.sku','vendor_infos.vendor_name as vendor_name',DB::raw('IFNULL(product_inventory.dropship_qty,0) as qty_dropship'),DB::raw('IFNULL(product_inventory.stocking_qty,0) AS qty_stocking'), DB::raw('product.created_at as created_date'),'product.updated_at  as update_date' );
            if (Common::userwisepermission(Auth::user()->id, "QC Approval") == 1) {
                $product_infos->where('product_approve_reject.type_qc', 'approve');
            }
            else if (Common::userwisepermission(Auth::user()->id, "VM Approval") == 1) {
                $product_infos->where('product_approve_reject.type_vm', 'approve');
            }
            else
            {
                $product_infos->where('product_approve_reject.type_qc', 'approve');
                $product_infos->where('product_approve_reject.type_vm', 'approve');
            }

        if(!empty($filters)){
            foreach($filters as $filtersRow){

                $field 	= $filtersRow['field'];
                $op 		= $filtersRow['op'];
                $data 	= $filtersRow['data'];

                $product_infos->where($field, $op,$data);
            }
        }

        if(!empty($orderBy) && !empty($sord)){
            $product_infos->orderBy($orderBy, $sord);
        }

        $product_infos->offset($offset);

        $product_infos->limit($limit);
        $vendorData = $product_infos->get();

        $data = array();
        if(!empty($vendorData)){
            $sno = 1;

            foreach($vendorData as $product_infosRow){

                $updateUrl = url("product/edit/".$product_infosRow->id);
                $viewUrl = url("products/approvalview/".$product_infosRow->id);
                $data[] = array(
                    'id'					=>	$sno,
                    'name'				    =>	$product_infosRow->name,
                    'sku'				    =>	$product_infosRow->sku,
                    'vendor_name'			=>	$product_infosRow->vendor_name,
                    'qty_dropship'          =>	$product_infosRow->qty_dropship,
                    'qty_stocking'          =>	$product_infosRow->qty_stocking,
                    'category_name'         =>	$product_infosRow->category_name,
                    'attributeset_name'     =>	$product_infosRow->attributeset_name,
                    'created_at'			=>	$product_infosRow->created_date,
                    'updated_at'			=>	$product_infosRow->update_date
                );

                $sno++;
            }
        }

        return $data;


    }
}
