<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Attribute_option_value.
 *
 * @author  The scaffold-interface created at 2017-02-08 11:48:12am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attribute_option_value extends Model
{
	
	
    protected $table = 'attribute_option_values';

	
}
