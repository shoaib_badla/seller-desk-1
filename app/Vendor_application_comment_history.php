<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vendor_info.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vendor_application_comment_history extends Model
{
	
	
    protected $table = 'vendor_application_comment_history';

	
}
