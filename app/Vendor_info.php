<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Config;
/**
 * Class Vendor_info.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vendor_info extends Model
{
	
	
    protected $table = 'vendor_infos';

	static public function getUserListByRole($roleName){
		
		
		
		$vendorManagerList = DB::table('users')->leftJoin('user_has_roles', 'user_has_roles.user_id', '=', 'users.id')
		->select('users.id','users.name')
		->where('user_has_roles.role_id', '=', DB::raw("(SELECT id FROM roles WHERE `name` = '$roleName')"))
		->get();
		
		$vendorManagerListArr = array();
		
		if(!empty($vendorManagerList)){
			foreach($vendorManagerList as $vendorManagerListRow){
				$vendorManagerListArr[$vendorManagerListRow->id] = $vendorManagerListRow->name;
			}
		}
		
		return $vendorManagerListArr;
	}
}
