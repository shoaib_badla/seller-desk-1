<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Attribute_set.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:09:56pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attribute_set extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'attribute_sets';

	
}
