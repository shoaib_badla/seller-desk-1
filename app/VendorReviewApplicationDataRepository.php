<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;
use Config,DB,Auth;

class VendorReviewApplicationDataRepository extends EloquentRepositoryAbstract {
 
 /*
    public function __construct()
    {
        $this->Database = new Vendor_info();
 
        $this->visibleColumns = array('firstname','lastname','vendor_email','action_column');
 
        $this->orderBy = array(array('id', 'asc'));
    }*/
	
	
  /**
   * Calculate the number of rows. It's used for paging the result.
   *
   * @param    array $filters
   *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
   *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
   *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
   *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
   *  The 'data' key will contain the string searched by the user.
   * @return  integer
   *  Total number of rows
   */
  public function getTotalNumberOfRows(array $filters = array())
  {
	  //GET USER ROLE INFO
		$userRole = \App\User::findOrfail(Auth::user()->id);

		$financeRole			=	Config::get('constants.financeRole');
		$operationRole			=	Config::get('constants.operationRole');
		
      $vendor_info =  Vendor_info::select('id');
	  
	   $vendor_info->leftJoin('form_of_organization', function ($join) {
            $join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
        });
		 $vendor_info->leftJoin('city', function ($join) {
            $join->on('vendor_infos.city', '=', 'city.id');
        });
		 $vendor_info->leftJoin('province_tbl', function ($join) {
            $join->on('vendor_infos.province', '=', 'province_tbl.id');
        });
	
	
	  if(!empty($filters)){
		  foreach($filters as $filtersRow){
			  
			  $field 	= $filtersRow['field'];
			  $op 		= $filtersRow['op'];
			  $data 	= $filtersRow['data'];
			  
			  
			  if($data=='all'){
				  continue;
			  }
			  
			  //FORM OF ORGANIZATION
			  if($field=='form_of_organization'){
				  $vendor_info->where('form_of_organization.label', $op,$data);
			  }elseif($field=='province'){
				  $vendor_info->where('province_tbl.name', $op,$data);
			  }elseif($field=='city'){
				  $vendor_info->where('city.name', $op,$data);
			  }elseif($field=='status'){
				  //APPLY FILTER ON APP STATUS BY DEPART WISE
				  if($userRole->hasRole($financeRole)){
					$vendor_info->where('vendor_infos.is_vendor_finance_approved', $op,$data);
				  }elseif($userRole->hasRole($operationRole)){
					$vendor_info->where('vendor_infos.is_vendor_ops_approved', $op,$data);
				  }
			  }elseif($field=='app_status'){
				  //APPLY FILTER ON APP STATUS BY DEPART WISE
				  if($data != 1){
					$vendor_info->where('vendor_infos.is_vendor_approved', '<>',1);
				  }else{
					$vendor_info->where('vendor_infos.is_vendor_approved', $op,$data);
				  }
			  }else{
				  $vendor_info->where($field, $op,$data);
			  }
			  
		  }
	  }
	  
	  /*
	  * GET LIST BY OF APPLICATION WHICH IS PROCESSED FOR OTHER DEPART REVIEW
	  */

	 // $vendor_info->where('is_vendor_approved', '=', 6);
	  $vendor_info->whereIn('is_vendor_approved', array(6,7,1));
	  
       //FILTER APPROVED DATA
	  //$vendor_info->where('status', '=', Config::get('constants.vendorAcceptedStatus'));
	   $vendor_info->whereIn('status', array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')));
	 
	  return $vendor_info->count();
	  
  }
 
  /**
   * Get the rows data to be shown in the grid.
   *
   * @param    integer $limit
   *  Number of rows to be shown into the grid
   * @param    integer $offset
   *  Start position
   * @param    string $orderBy
   *  Column name to order by.
   * @param    array $sord
   *  Sorting order
   * @param    array $filters
   *  An array of filters, example: array(array('field'=>'column index/name 1','op'=>'operator','data'=>'searched string column 1'), array('field'=>'column index/name 2','op'=>'operator','data'=>'searched string column 2'))
   *  The 'field' key will contain the 'index' column property if is set, otherwise the 'name' column property.
   *  The 'op' key will contain one of the following operators: '=', '<', '>', '<=', '>=', '<>', '!=','like', 'not like', 'is in', 'is not in'.
   *  when the 'operator' is 'like' the 'data' already contains the '%' character in the appropiate position.
   *  The 'data' key will contain the string searched by the user.
   * @return  array
   *  An array of array, each array will have the data of a row.
   *  Example: array(array("column1" => "1-1", "column2" => "1-2"), array("column1" => "2-1", "column2" => "2-2"))
   */
  public function getRows($limit, $offset, $orderBy = null, $sord = null, array $filters = array(),$nodeId = null, $nodeLevel = null,$exporting)
  {
	  
		//GET USER ROLE INFO
		$userRole = \App\User::findOrfail(Auth::user()->id);

		$financeRole			=	Config::get('constants.financeRole');
		$operationRole			=	Config::get('constants.operationRole');
				
	  $vendor_infos = Vendor_info::select('vendor_infos.id','vendor_infos.vendor_name','vendor_infos.vendor_email','vendor_infos.cell_phone','vendor_infos.is_vendor_approved','vendor_infos.is_vendor_finance_approved','vendor_infos.is_vendor_ops_approved',DB::Raw('form_of_organization.label AS form_of_organization'),'vendor_infos.ntn_number',DB::Raw('province_tbl.name AS province'),DB::Raw('city.name AS city'),'status');
	
		 $vendor_infos->leftJoin('form_of_organization', function ($join) {
            $join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
        });
		 $vendor_infos->leftJoin('city', function ($join) {
            $join->on('vendor_infos.city', '=', 'city.id');
        });
		 $vendor_infos->leftJoin('province_tbl', function ($join) {
            $join->on('vendor_infos.province', '=', 'province_tbl.id');
        });

	    if(!empty($filters)){
		  foreach($filters as $filtersRow){
			 
			  $field 	= $filtersRow['field'];
			  $op 		= $filtersRow['op'];
			  $data 	= $filtersRow['data'];
			  
			  if($data=='all'){
				  continue;
			  }
			  
			  //FORM OF ORGANIZATION
			  if($field=='form_of_organization'){
				  $vendor_infos->where('form_of_organization.label', $op,$data);
			  }elseif($field=='province'){
				  $vendor_infos->where('province_tbl.name', $op,$data);
			  }elseif($field=='city'){
				  $vendor_infos->where('city.name', $op,$data);
			  }elseif($field=='status'){
				  //APPLY FILTER ON APP STATUS BY DEPART WISE
				  if($userRole->hasRole($financeRole)){
					$vendor_infos->where('vendor_infos.is_vendor_finance_approved', $op,$data);
				  }elseif($userRole->hasRole($operationRole)){
					$vendor_infos->where('vendor_infos.is_vendor_ops_approved', $op,$data);
				  }
			  }elseif($field=='app_status'){
				  //APPLY FILTER ON APP STATUS BY DEPART WISE
				  if($data != 1){
					$vendor_infos->where('vendor_infos.is_vendor_approved', '<>',1);
				  }else{
					$vendor_infos->where('vendor_infos.is_vendor_approved', $op,$data);
				  }
			  }else{
				  $vendor_infos->where($field, $op,$data);
			  }
			  
		  }
	  }
	  
	  if(!empty($orderBy) && !empty($sord)){
		$vendor_infos->orderBy($orderBy, $sord);
	  }
	  //FILTER APPROVED DATA
	  //$vendor_infos->where('status', '=', Config::get('constants.vendorAcceptedStatus'));
	  $vendor_infos->whereIn('status', array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')));
	  
	  /*
	  * GET LIST BY OF APPLICATION WHICH IS PROCESSED FOR OTHER DEPART REVIEW
	  */

	  $vendor_infos->whereIn('is_vendor_approved', array(6,7,1));
	  
	   $vendor_infos->offset($offset);
	   $vendor_infos->limit(15);
		$vendorData = $vendor_infos->get();
	  
	  $data = array();
	  if($vendorData->count() > 0){
		  $sno = 1;
		  foreach($vendorData as $vendor_infosRow){
			  
			  
				
				$actionColoumn	=	"<a href='javascript:void(0)'  class='viewVendor' vendor-id='".$vendor_infosRow->id."'><small class='label bg-purple'><i class='fa fa-clock-o'></i>View</small></a>";
			  //VENDOR APPLICATION STATUS
		
			  /**
			  * VENDOR APPLICATION FINANCE STATUS
			  */
			    //VENDOR APPLICATION STATUS
			  $vendorApplicationFinanceStatus = "<span class='label label-info'>N/A</span>";
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_finance_approved==1){
					$vendorApplicationFinanceStatus = "<span class='label label-success'>Approved</span>";
				}
			
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_finance_approved==2){
					//return redirect('marginwaiting');
					$vendorApplicationFinanceStatus = "<span class='label label-danger'>Rejected</span>";
				}
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_finance_approved==3){
					//return redirect('marginwaiting');
					$vendorApplicationFinanceStatus = "<span class='label label-info'>Pending</span>";
				}
				
			  /**
			  * VENDOR APPLICATION OPS STATUS
			  */
			    //VENDOR APPLICATION STATUS
			  $vendorApplicationOpsStatus = "<span class='label label-info'>N/A</span>";
				
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_ops_approved==1){
					$vendorApplicationOpsStatus = "<span class='label label-success'>Approved</span>";
				}
			
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_ops_approved==2){
					//return redirect('marginwaiting');
					$vendorApplicationOpsStatus = "<span class='label label-danger'>Rejected</span>";
				}
				
				 //IF STATE  Waiting
				if($vendor_infosRow->is_vendor_ops_approved==3){
					//return redirect('marginwaiting');
					$vendorApplicationOpsStatus = "<span class='label label-info'>Pending</span>";
				}
				
				
				
				
				
				//SET FINANCE DEPART STATUS
				$applicationStatus	=	"-";
				if($userRole->hasRole($financeRole)){
					$applicationStatus	=	$vendorApplicationFinanceStatus;
				}
				//SET OPERATION DEPART STATUS
				if($userRole->hasRole($operationRole)){
					$applicationStatus	=	$vendorApplicationOpsStatus;
				}
		
				//
				 $vendorApplicationStatus = "<span class='label label-info'>On Hold</span>";
				//IF STATE APPROVED
				if($vendor_infosRow->is_vendor_approved==1){
					$vendorApplicationStatus = "<span class='label label-success'>Vendor Onboard</span>";
				}
		
			  
			  $data[] = array(
					'id'							=>	$sno,
					'status'						=>	$applicationStatus,
					'app_status'					=>	$vendorApplicationStatus,
					'vendor_name'					=>	$vendor_infosRow->vendor_name,
					'vendor_email'					=>	$vendor_infosRow->vendor_email,
					'cell_phone'					=>	$vendor_infosRow->cell_phone,
					'form_of_organization'			=>	$vendor_infosRow->form_of_organization,
					'ntn_number'					=>	$vendor_infosRow->ntn_number,
					'province'						=>	$vendor_infosRow->province,
					'city'							=>	$vendor_infosRow->city,
					'action_column'					=>	$actionColoumn
			  );
			  
			  $sno++;
		  }
	  }else{
		  $data[] = array(
					'id'							=>	'',
					'status'						=>	'No Record Found',
					'app_status'					=>	'',
					'vendor_name'					=>	'',
					'vendor_email'					=>	'',
					'cell_phone'					=>	'',
					'form_of_organization'			=>	'',
					'ntn_number'					=>	'',
					'province'						=>	'',
					'city'							=>	'',
					'action_column'					=>	''
			  );
	  }

	  return $data;
	  /*
      return array(
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => 'Limit->'.$limit." Offset->$offset"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  array("firstname" => "1-1", "lastname" => "1-2", "vendor_email" => "1-3", "action_column" => "1-4"),
                  );*/
				 
  }
}
