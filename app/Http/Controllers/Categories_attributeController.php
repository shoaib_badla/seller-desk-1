<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories_attribute;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Categories_attributeController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:48:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Categories_attributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - categories_attribute';
        $categories_attributes = Categories_attribute::paginate(6);
        return view('categories_attribute.index',compact('categories_attributes','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - categories_attribute';
        
        return view('categories_attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories_attribute = new Categories_attribute();

        
        $categories_attribute->cat_id = $request->cat_id;

        
        $categories_attribute->attribitute_set_id = $request->attribitute_set_id;

        
        
        $categories_attribute->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new categories_attribute has been created !!']);

        return redirect('categories_attribute');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - categories_attribute';

        if($request->ajax())
        {
            return URL::to('categories_attribute/'.$id);
        }

        $categories_attribute = Categories_attribute::findOrfail($id);
        return view('categories_attribute.show',compact('title','categories_attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - categories_attribute';
        if($request->ajax())
        {
            return URL::to('categories_attribute/'. $id . '/edit');
        }

        
        $categories_attribute = Categories_attribute::findOrfail($id);
        return view('categories_attribute.edit',compact('title','categories_attribute'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $categories_attribute = Categories_attribute::findOrfail($id);
    	
        $categories_attribute->cat_id = $request->cat_id;
        
        $categories_attribute->attribitute_set_id = $request->attribitute_set_id;
        
        
        $categories_attribute->save();

        return redirect('categories_attribute');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/categories_attribute/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$categories_attribute = Categories_attribute::findOrfail($id);
     	$categories_attribute->delete();
        return URL::to('categories_attribute');
    }
}
