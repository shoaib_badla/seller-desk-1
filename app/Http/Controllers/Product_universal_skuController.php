<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product_universal_sku;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Product_universal_skuController.
 *
 * @author  The scaffold-interface created at 2017-01-16 12:43:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Product_universal_skuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - product_universal_sku';
        $product_universal_skus = Product_universal_sku::paginate(6);
        return view('product_universal_sku.index',compact('product_universal_skus','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - product_universal_sku';
        
        return view('product_universal_sku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_universal_sku = new Product_universal_sku();

        
        $product_universal_sku->universal_sku = $request->universal_sku;

        
        $product_universal_sku->product_count = $request->product_count;

        
        $product_universal_sku->created_at = $request->created_at;

        
        $product_universal_sku->updated_at = $request->updated_at;

        
        
        $product_universal_sku->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new product_universal_sku has been created !!']);

        return redirect('product_universal_sku');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - product_universal_sku';

        if($request->ajax())
        {
            return URL::to('product_universal_sku/'.$id);
        }

        $product_universal_sku = Product_universal_sku::findOrfail($id);
        return view('product_universal_sku.show',compact('title','product_universal_sku'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - product_universal_sku';
        if($request->ajax())
        {
            return URL::to('product_universal_sku/'. $id . '/edit');
        }

        
        $product_universal_sku = Product_universal_sku::findOrfail($id);
        return view('product_universal_sku.edit',compact('title','product_universal_sku'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $product_universal_sku = Product_universal_sku::findOrfail($id);
    	
        $product_universal_sku->universal_sku = $request->universal_sku;
        
        $product_universal_sku->product_count = $request->product_count;
        
        $product_universal_sku->created_at = $request->created_at;
        
        $product_universal_sku->updated_at = $request->updated_at;
        
        
        $product_universal_sku->save();

        return redirect('product_universal_sku');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/product_universal_sku/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$product_universal_sku = Product_universal_sku::findOrfail($id);
     	$product_universal_sku->delete();
        return URL::to('product_universal_sku');
    }
}
