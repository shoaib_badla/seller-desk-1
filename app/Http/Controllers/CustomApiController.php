<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Pending_booking;
use DB;
use Auth;
use \App\User;
use \App\Vendor_info;
use Config;
use \App\Country;
use \App\Brand;
use App\Helper\Common;

class CustomApiController extends Controller
{
     protected $respose;
 
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
	
	
	//GET API TOKEN
	public function getApiToken(Request $request){
		
		if ($request->isMethod('post')) {
			$postData 	= $request->all();
			
			$email 	= $request->input('email');
			$password 	= $request->input('password');
			
			//AUTHENTICATE USER CREDNTIAL
			if (Auth::attempt(array('email' => $email, 'password' => $password))){
				
				//GET USER ID
				$user_id = User::where('email',$email)->select('id')->first();
				$user_id = isset($user_id->id)?$user_id->id:0;
				//CHECK USER HAS API ROLE
				$user = User::findOrfail($user_id);
		
				//IF ROLE NOT ASSIGN ALREADY
				$roleName	=	Config::get('constants.apiRole');
				if($user->hasRole($roleName)){
					
					//API TOKEN
					$apiToken =  $this->getRandomString(40);
					 
					//UPDATE GENERATED TOKEN
  				    $user = User::find($user_id);
					$user->api_token = $apiToken;
					$user->save();
					
					return $apiToken;
				}else {        
					return "Wrong Credentials.";
				}
			}
			else {        
				return "Wrong Credentials";
			}
			
		}
	}
	
	
	 public function dataoffload(Request $request)
    {
		
		 if ($request->isMethod('post')) {
			 
				$postData 	= $request->input('data');
				
				$postData	=	json_decode($postData);
				//GET POSTED Data
				$order_created_at		=	isset($postData->order_created_at)?$postData->order_created_at:'';
				$shipping_name			=	isset($postData->shipping_name)?$postData->shipping_name:'';
				$shipping_address		=	isset($postData->shipping_address)?$postData->shipping_address:'';
				$shipping_phone			=	isset($postData->shipping_phone)?$postData->shipping_phone:'';
				$status					=	isset($postData->status)?$postData->status:'';
				$customer_email			=	isset($postData->customer_email)?$postData->customer_email:'';
				$destination_city		=	isset($postData->destination_city)?$postData->destination_city:'';
				$pieces					=	isset($postData->pieces)?$postData->pieces:'';
				$weight					=	isset($postData->weight)?$postData->weight:'';
				$grand_total			=	isset($postData->grand_total)?$postData->grand_total:'';
				$cod_amount				=	isset($postData->cod_amount)?$postData->cod_amount:'';
				$order_number			=	isset($postData->order_number)?$postData->order_number:'';
				$sku					=	isset($postData->sku)?$postData->sku:'';
				$vendor_city			=	isset($postData->vendor_city)?$postData->vendor_city:'';
				$product_type			=	isset($postData->product_type)?$postData->product_type:'';
				$custom_option			=	isset($postData->custom_option)?$postData->custom_option:'';
				$ship_win_ammount		=	isset($postData->ship_win_ammount)?$postData->ship_win_ammount:'';
				$internal_credit_amount	=	isset($postData->internal_credit_amount)?$postData->internal_credit_amount:'';
				$discount_amount		=	isset($postData->discount_amount)?$postData->discount_amount:'';
				$payment_type			=	isset($postData->payment_type)?$postData->payment_type:'';
				$item_amount			=	isset($postData->item_amount)?$postData->item_amount:'';
				$base_grand_total		=	isset($postData->base_grand_total)?$postData->base_grand_total:'';
				$billing_address		=	isset($postData->billing_address)?$postData->billing_address:'';
				$base_refunded_total	=	isset($postData->base_refunded_total)?$postData->base_refunded_total:'';
				$vendor_name			=	isset($postData->vendor_name)?$postData->vendor_name:'';
				$shipping_charges		=	isset($postData->shipping_charges)?$postData->shipping_charges:'';
				$delivery_date			=	isset($postData->delivery_date)?$postData->delivery_date:'';
				$commision_code			=	isset($postData->commision_code)?$postData->commision_code:'';
				$purchase_from			=	isset($postData->purchase_from)?$postData->purchase_from:'';
				$cn_number				=	isset($postData->cn_number)?$postData->cn_number:'';
				$mode_of_fulfillment	=	isset($postData->mode_of_fulfillment)?$postData->mode_of_fulfillment:'';
				$mode_of_buying			=	isset($postData->mode_of_buying)?$postData->mode_of_buying:'';
				$return_val				=	isset($postData->return_val)?$postData->return_val:'';
				$exchange				=	isset($postData->exchange)?$postData->exchange:'';
				$itemstatus				=	isset($postData->itemstatus)?$postData->itemstatus:'';
				$product_name			=	isset($postData->product_name)?$postData->product_name:'';
				
				
				
				//CHECK RECORD ALREADY EXIST OR NOT
				$recordAlreadyExsist = Pending_booking::where([
					['order_number', '=', $order_number],
					['sku', '=', $sku]
				])
				->first();
				
				if ($recordAlreadyExsist === null) {
				
					$pending_booking 	=	new Pending_booking;
					
					
					$pending_booking->order_created_at			=	$order_created_at;
					$pending_booking->shipping_name				=	$shipping_name;
					$pending_booking->shipping_address			=	$shipping_address;
					$pending_booking->shipping_phone			=	$shipping_phone;
					$pending_booking->Status					=	$status;
					$pending_booking->customer_email			=	$customer_email;
					$pending_booking->destination_city			=	$destination_city;
					$pending_booking->pieces					=	$pieces;
					$pending_booking->weight					=	$weight;
					$pending_booking->grand_total				=	$grand_total;
					$pending_booking->cod_amount				=	$cod_amount;
					$pending_booking->order_number				=	$order_number;
					$pending_booking->sku						=	$sku;
					$pending_booking->vendor_city				=	$vendor_city;
					$pending_booking->product_type				=	$product_type;
					$pending_booking->custom_option				=	$custom_option;
					$pending_booking->ship_win_ammount			=	$ship_win_ammount;
					$pending_booking->internal_credit_amount	=	$internal_credit_amount;
					$pending_booking->discount_amount			=	$discount_amount;
					$pending_booking->payment_type				=	$payment_type;
					$pending_booking->item_amount				=	$item_amount;
					$pending_booking->base_grand_total			=	$base_grand_total;
					$pending_booking->billing_address			=	$billing_address;
					$pending_booking->base_refunded_total		=	$base_refunded_total;
					$pending_booking->vendor_name				=	$vendor_name;
					$pending_booking->shipping_charges			=	$shipping_charges;
					$pending_booking->delivery_date				=	$delivery_date;
					$pending_booking->commision_code			=	$commision_code;
					$pending_booking->purchase_from				=	$purchase_from;
					$pending_booking->cn_number					=	$cn_number;
					$pending_booking->mode_of_fulfillment		=	$mode_of_fulfillment;
					$pending_booking->mode_of_buying			=	$mode_of_buying;
					$pending_booking->return_val				=	$return_val;
					$pending_booking->exchange					=	$exchange;
					$pending_booking->itemstatus				=	$itemstatus;
					$pending_booking->product_name				=	$product_name;
					
					
					$pending_booking->save();
					
					
					//UPDATE PENDING BOOKING
				
					 DB::update('update pending_bookings set Status = ? where order_number = ?',[$status,$order_number]);
					 return $this->response->withArray(array('success'=>true,'message'=>'Dataoffloaded successfully'));
				}else{
					 return $this->response->errorInternalError('Record already exsist');
				}
				
		 }else{
			   return $this->response->errorInternalError('Could not offload data because request should be post.');
		 }
		
    }
	
	//GENERATE RANDOM STRING
	function getRandomString($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$string = '';

		for ($i = 0; $i < $length; $i++) {
			$string .= $characters[mt_rand(0, strlen($characters) - 1)];
		}

		return $string;
	}
	
	
	//GET CITY LIST
	public function citylist(Request $request){
		/**
		* GET CITY LIST
		*/
		$countryId = 1;
		$cityList = app('App\Http\Controllers\Vendor_infoController')->getCityListByCountryId($countryId);//$this->getCityListByCountryId($countryId);
		return json_encode($cityList);
	}
	
	
		//GET CITY LIST
	public function countrylist(Request $request){
	
		$countryList	=	 Country::pluck('name','id')->all();
		return json_encode($countryList);
	}
	
	//GET CATEGORY LIST
	public function categorylist(){
		
		//GET L3 Categories
		$categoryList	=	app('App\Http\Controllers\Vendor_infoController')->getCatgeoryListByLevel(3);
		return json_encode($categoryList);
	}
	
	//GET BRAND LIST
	public function brandlist(){
				//GET Brand LIST
		$brandList	=	 Brand::pluck('name','id')->all();
		return json_encode($brandList);
	}
	
	
	//GET BRAND LIST
	public function modeoffullfilement(){
				//GET Brand LIST
		$modeOfFullfillment = Common::modeOfFullfillment();
		return json_encode($modeOfFullfillment);
	}
	
	
	//GET VENDOR EXSIST OR NOT
	public function checkvendoralreadyexsist(Request $request){
				
				
		$userExsist = false;
	

			
		$vendor_email = $request->vendor_email;
			
		//CHECK VENDOR EXSIST IN USER TABLE
		$userExsistArr = User::where('email', $vendor_email)->first();
		
		if(!empty($userExsistArr)){
			$userExsist = true;
		}
		
		//CHECK USER FROM VENDOR INFO TABLE
		$vendorInfoExsist = Vendor_info::where('vendor_email', $vendor_email)->first();
		if(!empty($vendorInfoExsist)){
			$userExsist = true;
		}
			
		if($userExsist){
			return json_encode('exsist');
		}else{
			return json_encode('not exsist');
		}
	}
}
