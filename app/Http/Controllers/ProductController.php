<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Attributes_mapping;
use App\Categories_attribute;
use App\Product_media;
use App\Product_universal_sku;
use App\Category;
use App\Product;
use App\Categories_product;
use App\Attribute_set;
use App\Temimage_files;
use App\Http\Requests;
use App\Vendor_info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use Redirect;
use Session;
use DB;
use App\Model\Product\ProductDataRepository;
use App\Model\Product\ApprovalPendingDataRepositery;
use App\Model\Product\ProductApprovalDataRepositery;
use App\Model\Product\ProductRejectedDataRepositery;
use Mgallegos\LaravelJqgrid\Facades\GridEncoder;
use Intervention\Image\ImageManagerStatic as Image;
use Jasekz\Laradrop\Services\File as FileService;
use Jasekz\Laradrop\Events\FileWasUploaded;
use Exception, File, Storage;

use App\Attribute_option_value;
use ZipArchive;
use App\Fullfillment_option;

use App\Model\Product\StoreFrontPushDataRepositery;
use App\Product_authorities;
use App\Product_approve_reject;
use Auth;
use SoapClient;
use App\Helper\Common;
use App\ProductModel;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(FileService $file)
    {
        $this->middleware('auth');
        $this->file = $file;
        $this->productmodel = new ProductModel();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('product.index');
    }
    public function approvalpending()
    {
        return view('product.approvalpending');
    }
    
    public function approval()
    {
        return view('product.approval');
    }

    public function productreject()
    {
        return view('product.productreject');
    }
    public function storefrontpush()
    {
        return view('product.storefrontpush');
    }

    //product create form view
    public function create()
    {
        $configurableattrs=$this->productmodel->getconfigurablepro();


         //   $allvendors = Auth::user()->id;

            $allvendors = $this->productmodel->allvendorlist();



        $fullfillmentoption = $this->productmodel->allfullfillmentopt();
        //$allCategories = json_decode($allCategories);
        return view('product.create', compact("configurableattrs","allvendors","fullfillmentoption"));
    }

    //render the product grid data
    public function productgriddata()
    {
        GridEncoder::encodeRequestedData(new ProductDataRepository(), Input::all());
    }

    //render the pending approval grid data
    public function approvalpendinggriddata()
    {
        GridEncoder::encodeRequestedData(new ApprovalPendingDataRepositery(), Input::all());
    }

    // render the approval grid data
    public function productaprovalgriddata()
    {
        GridEncoder::encodeRequestedData(new ProductApprovalDataRepositery(), Input::all());
    }

    // render the rejection grid data
    public function productrejectedgriddata()
    {
        //StoreFrontPushDataRepositery
        //$users = \App\User::all();
        GridEncoder::encodeRequestedData(new ProductRejectedDataRepositery(), Input::all());
        //return view('users.index', compact('users'));
    }

    // render the data the are in que to push in store front
    public function storefrontpushgriddata()
    {
        //$users = \App\User::all();
        GridEncoder::encodeRequestedData(new StoreFrontPushDataRepositery(), Input::all());
        //return view('users.index', compact('users'));
    }

    public function update($id)
    {
        $prodata = $this->productmodel->getproductdata($id);

        $atributedata = $this->productmodel->getproductattributedata($id,$prodata);

        //attributes_data.product_id','=', $id
        $allvendors = $this->productmodel->allvendorlist();

        $allCategories = $this->productmodel->getl3categories();

        $fullfillmentoption = $this->productmodel->allfullfillmentopt();

        $configurableattrs=$this->productmodel->getconfigurablepro();

        $confiattr = array();
        foreach ( $configurableattrs as $attr) {
            $confiattr = array_prepend($confiattr, $attr->id);
        }

        $configurabledataopt=$this->productmodel->getconfigurableproopt($confiattr);

        $configurabledata=$this->productmodel->getconfigurableprodata($id);

        $attr_id = array();
        foreach ( $atributedata as $attribute) {
            $attr_id = array_prepend($attr_id, $attribute->attr_id);
        }

        $attr_option_value =  $this->productmodel->getattributeoption($attr_id);

        return view('product.edit', compact("prodata","configurabledataopt","configurabledata","configurableattrs","atributedata","allCategories","allvendors","fullfillmentoption","id","attr_option_value"));
    }

    public function view($id)
    {
        //join('product_universal_skus', 'product_universal_skus.id', '=', 'product.universal_sku_id')  'product_universal_skus.universal_sku'
        $prodata = Product::join('categories_products', 'categories_products.product_id', '=', 'product.id')
            ->join('categories', 'categories.id', '=', 'categories_products.cat_id')
            ->join('product_inventory', 'product_inventory.product_id', '=', 'product.id')
            ->select('product.id','categories.name', 'product.attribute_set_id','product.universal_sku_id','product.sku','categories.id as categories_id', 'product.mode_of_fullfillment','product_inventory.dropship_qty','product_inventory.stocking_qty')
            ->where('product.id', '=',$id)->get();

        $atributedata = Attributes_mapping::join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
            ->leftjoin('attributes_data',function($join) use ($id)
            {
                $join->on('attributes_mapping.attribute_id', '=', 'attributes_data.attribute_id');
                $join->on('attributes_data.product_id','=',DB::raw("'".$id."'"));
            })
            ->select('attributes.type','attributes.additional_info','attributes.name','attributes.id as attr_id','attributes_data.attribute_id', 'attributes_data.value')
            ->where([['attributes_mapping.attribute_set_id', '=',$prodata[0]->attribute_set_id],
                ['attributes.name','!=','']])->get();

        //attributes_data.product_id','=', $id
        $allvendors = $this->productmodel->allvendorlist();

        $allCategories = Category::where('level', '=', 3)->select('name','id')->get();

        $fullfillmentoption = Fullfillment_option::select('id','name')->get();


        $attr_id = array();
        foreach ( $atributedata as $attribute) {
            $attr_id = array_prepend($attr_id, $attribute->attr_id);
        }

        $attr_option_value = $this->productmodel->getattributeoption($attr_id);
        $files = Product_media::where('product_id','=',$id)->get();

        return view('product.view', compact("prodata","atributedata","allCategories","allvendors","fullfillmentoption","id","attr_option_value","files"));
    }
    public function approvalview($id,$type)
    {
        //join('product_universal_skus', 'product_universal_skus.id', '=', 'product.universal_sku_id')  'product_universal_skus.universal_sku'
        $prodata = Product::join('categories_products', 'categories_products.product_id', '=', 'product.id')
            ->join('categories', 'categories.id', '=', 'categories_products.cat_id')
            ->join('product_inventory', 'product_inventory.product_id', '=', 'product.id')
            ->select('product.id','categories.name', 'product.attribute_set_id','product.universal_sku_id','product.sku','categories.id as categories_id', 'product.mode_of_fullfillment','product_inventory.dropship_qty','product_inventory.stocking_qty')
            ->where('product.id', '=',$id)->get();

        $atributedata = Attributes_mapping::join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
            ->leftjoin('attributes_data',function($join) use ($id)
            {
                $join->on('attributes_mapping.attribute_id', '=', 'attributes_data.attribute_id');
                $join->on('attributes_data.product_id','=',DB::raw("'".$id."'"));
            })
            ->select('attributes.type','attributes.additional_info','attributes.name','attributes.id as attr_id','attributes_data.attribute_id', 'attributes_data.value')
            ->where([['attributes_mapping.attribute_set_id', '=',$prodata[0]->attribute_set_id],
                ['attributes.name','!=','']])->get();

        //attributes_data.product_id','=', $id
        $allvendors = $this->productmodel->allvendorlist();

        $allCategories = Category::where('level', '=', 3)->select('name','id')->get();

        $fullfillmentoption = Fullfillment_option::select('id','name')->get();


        $attr_id = array();
        foreach ( $atributedata as $attribute) {
            $attr_id = array_prepend($attr_id, $attribute->attr_id);
        }

        $attr_option_value =  $this->productmodel->getattributeoption($attr_id);

        $files = $this->productmodel->getproductimages($id);

        $productauths =Product_authorities::select('id','name')->get();
        return view('product.approvalview', compact("prodata","type","atributedata","allCategories","allvendors","fullfillmentoption","id","attr_option_value","files","productauths"));
    }
    public function backtoapproval($id)
    {
        return view('product.backtoapproval',compact('id'));
    }
    public function revertapproval(Request $request)
    {
        $data = $request->input();
        $this->productmodel->resetapprovalqc($data["id"]);
        $this->productmodel->resetapprovalvm($data["id"]);
        return Redirect::to('products/storefrontpush');
    }
    public function pushtomagento($id)
    {
        return view('product.pushtomagento',compact('id'));
    }
    public function rejectview($id)
    {
        //join('product_universal_skus', 'product_universal_skus.id', '=', 'product.universal_sku_id')  'product_universal_skus.universal_sku'
        $prodata = Product::join('categories_products', 'categories_products.product_id', '=', 'product.id')
            ->join('categories', 'categories.id', '=', 'categories_products.cat_id')
            ->join('product_inventory', 'product_inventory.product_id', '=', 'product.id')
            ->select('product.id','categories.name', 'product.attribute_set_id','product.universal_sku_id','product.sku','categories.id as categories_id', 'product.mode_of_fullfillment','product_inventory.dropship_qty','product_inventory.stocking_qty')
            ->where('product.id', '=',$id)->get();

        $atributedata = Attributes_mapping::join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
            ->leftjoin('attributes_data',function($join) use ($id)
            {
                $join->on('attributes_mapping.attribute_id', '=', 'attributes_data.attribute_id');
                $join->on('attributes_data.product_id','=',DB::raw("'".$id."'"));
            })
            ->select('attributes.type','attributes.additional_info','attributes.name','attributes.id as attr_id','attributes_data.attribute_id', 'attributes_data.value')
            ->where([['attributes_mapping.attribute_set_id', '=',$prodata[0]->attribute_set_id],
                ['attributes.name','!=','']])->get();

        //attributes_data.product_id','=', $id
        $allvendors = Vendor_info::select(DB::raw('CONCAT(firstname,"-",lastname) AS name'),'id')->get();

        $allCategories = Category::where('level', '=', 3)->select('name','id')->get();

        $fullfillmentoption = Fullfillment_option::select('id','name')->get();


        $attr_id = array();
        foreach ( $atributedata as $attribute) {
            $attr_id = array_prepend($attr_id, $attribute->attr_id);
        }

        $attr_option_value =  Attribute_option_value::whereIn('attribute_id', $attr_id)->get();
        $files = Product_media::where('product_id','=',$id)->get();

        return view('product.rejectview', compact("prodata","atributedata","allCategories","allvendors","fullfillmentoption","id","attr_option_value","files"));
    }
    public function store_approvedreject(Request $request){


        $Productapprovereject = new Product_approve_reject();
        $Productapprovereject->product_id = $request->product_id;
        $Productapprovereject->comments = $request->approval_comment;
        $Productapprovereject->approve_type = $request->type_name;
        if($request->type_name == "approve") {
            $Productapprovereject->approval_by = Auth::user()->id;
        }
        else
        {
            $Productapprovereject->reject_by = Auth::user()->id;
        }

        $Productapprovereject->role_id = $request->role_id;
        $Productapprovereject->save();
        return Redirect::to('product');
    }

    // add and update product data from form
    public function add_productdata(Request $request) {

        // getting all of the post data
        $data = $request->input();

        $checkdup = $this->productmodel->getvendorsku($data["vendor"],$data["sku"]);

        if($checkdup != "")
        {
            $request->session()->flash('alert-danger', 'This sku already Exist ');
            return redirect('product/create');

        }
        else
        {
            if (!array_key_exists('id', $data)) {
                $data = array_add($data, 'id', '');
            }
            $this->productmodel->productdataadd($data);

            return Redirect::to('product');
        }
    }

    public function update_productdata(Request $request) {

        // getting all of the post data
        $data = $request->input();

            if (!array_key_exists('id', $data)) {
                $data = array_add($data, 'id', '');
            }
            $this->productmodel->productdataupdate($data);

            return Redirect::to('product');

    }

    public function getimageview($parentId, $token =null, $id = null)
    {
        try {
            $out = [];

            //if($this->count() && $parentId > 0) {
            //    $files = Temimage_files::where('id', '=', $parentId)->first()->immediateDescendants()->get();
            //} else if($this->count()) {
            if($id ==  null)
            {
                $files = Temimage_files::where('token','=',$token)->get();
            }
            else
            {
                $files = Product_media::where('product_id','=',$id)->get();
            }

            //}

            if(isset($files)) {

                foreach($files as $file) {

                    if( $file->has_thumbnail && config('laradrop.disk_public_url')) {

                        $publicResourceUrlSegments = explode('/', $file->public_resource_url);
                        $publicResourceUrlSegments[count($publicResourceUrlSegments) - 1] = '_thumb_' . $publicResourceUrlSegments[count($publicResourceUrlSegments) - 1];
                        $file->filename = implode('/', $publicResourceUrlSegments);

                    } else {
                        $file->filename = config('laradrop.default_thumbnail_url');
                    }

                    //$file->numChildren = $file->children()->count();

                    if($file->type == 'folder') {
                        array_unshift($out, $file);
                    } else {
                        $out[] = $file;
                    }
                }
            }

            return $out;
        }

        catch (Exception $e) {
            throw $e;
        }
    }

    public function show($id = null)
    {

        $token = csrf_token();

        try {

            $files = $this->getimageview(Input::get('pid'), $token);

            return response()->json([
                'status' => 'success',
                'data' => $files,
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
    }
    public function store(Request $request)
    {

        try {
            $token = $request->input('_token');
            //echo $name." - test2";
            //exit();
            if (! $request->hasFile('file')) {
                throw new Exception(trans('err.fileNotProvided'));
            }

            if( ! $request->file('file')->isValid()) {
                throw new Exception(trans('err.invalidFile'));
            }

            /*
             * move file to temp location
             */
            $fileExt = Input::file('file')->getClientOriginalExtension();
            $fileName = str_replace('.' . $fileExt, '', Input::file('file')->getClientOriginalName()) . '-' . date('Ymdhis');
            $mimeType = $request->file('file')->getMimeType();
            $tmpStorage = storage_path();
            $movedFileName = $fileName . '.' . $fileExt;
            $fileSize = Input::file('file')->getSize();

            if($fileSize > ( (int) config('laradrop.max_upload_size') * 1000000) ) {
                throw new Exception(trans('err.invalidFileSize'));
            }

            $request->file('file')->move($tmpStorage, $movedFileName);

            $disk = Storage::disk(config('laradrop.disk'));

            /*
             * create thumbnail if needed
             */
            $fileData['has_thumbnail'] = 0;
            if ($fileSize <= ( (int) config('laradrop.max_thumbnail_size') * 1000000) && in_array($mimeType, ["image/jpg", "image/jpeg", "image/png", "image/gif"])) {

                $thumbDims = config('laradrop.thumb_dimensions');
                $img = Image::make($tmpStorage . '/' . $movedFileName);
                $img->resize($thumbDims['width'], $thumbDims['height']);
                $img->save($tmpStorage . '/_thumb_' . $movedFileName);

                // move thumbnail to final location
                $disk->put('_thumb_' . $movedFileName, fopen($tmpStorage . '/_thumb_' . $movedFileName, 'r+'));
                File::delete($tmpStorage . '/_thumb_' . $movedFileName);
                $fileData['has_thumbnail'] = 1;

            }

            /*
             * move uploaded file to final location
             */
            $disk->put($movedFileName, fopen($tmpStorage . '/' . $movedFileName, 'r+'));
            File::delete($tmpStorage . '/' . $movedFileName);

            /*
             * save in db
             */
            $fileData['filename'] = $movedFileName;
            $fileData['alias'] = Input::file('file')->getClientOriginalName();
            $fileData['public_resource_url'] = config('laradrop.disk_public_url') . '/' . $movedFileName;
            $fileData['type'] = $fileExt;
            $fileData['parent_id'] = null;
            $fileData['lft'] = 1;
            $fileData['rgt'] = 2;
            $fileData['depth'] = 0;
            if(Input::get('pid') > 0) {
                $fileData['parent_id'] = Input::get('pid');
            }
            $meta = $disk->getDriver()->getAdapter()->getMetaData($movedFileName);
            $meta['disk'] = config('laradrop.disk');
            $fileData['meta'] = json_encode($meta);
            $fileData['token'] =$token;
            //$file = $this->file->create($fileData);
            $productmedia = new Temimage_files();
            $productmedia->parent_id = $fileData['parent_id'];
            $productmedia->lft = $fileData['lft'];
            $productmedia->rgt = $fileData['rgt'];
            $productmedia->depth = $fileData['depth'];
            $productmedia->type = $fileData['type'];
            $productmedia->has_thumbnail = $fileData['has_thumbnail'];
            $productmedia->meta = $fileData['meta'];
            $productmedia->filename = $fileData['filename'];

            $productmedia->alias = $fileData['alias'];
            $productmedia->public_resource_url = $fileData['public_resource_url'];
            $productmedia->token = $fileData['token'];

            $productmedia->save();
            $file=$fileData;
            //echo $token;
            /*
             * fire 'file uploaded' event
             */
            event(new FileWasUploaded([
                'file'     => $file,
                'postData' => Input::all()
            ]));

            return $file;

        }

        catch (Exception $e) {

            // delete the file(s)
            if( isset($disk) && $disk) {
                $disk->delete($movedFileName);
                $disk->delete('_thumb_' . $movedFileName);
            }

            return $this->handleError($e);
        }
    }
    public function updatestore(Request $request,$id)
    {

        try {
            $token = $request->input('_token');
            //echo $name." - test2";
            //exit();
            if (! $request->hasFile('file')) {
                throw new Exception(trans('err.fileNotProvided'));
            }

            if( ! $request->file('file')->isValid()) {
                throw new Exception(trans('err.invalidFile'));
            }

            /*
             * move file to temp location
             */
            $fileExt = Input::file('file')->getClientOriginalExtension();
            $fileName = str_replace('.' . $fileExt, '', Input::file('file')->getClientOriginalName()) . '-' . date('Ymdhis');
            $mimeType = $request->file('file')->getMimeType();
            $tmpStorage = storage_path();
            $movedFileName = $fileName . '.' . $fileExt;
            $fileSize = Input::file('file')->getSize();

            if($fileSize > ( (int) config('laradrop.max_upload_size') * 1000000) ) {
                throw new Exception(trans('err.invalidFileSize'));
            }

            $request->file('file')->move($tmpStorage, $movedFileName);

            $disk = Storage::disk(config('laradrop.disk'));

            /*
             * create thumbnail if needed
             */
            $fileData['has_thumbnail'] = 0;
            if ($fileSize <= ( (int) config('laradrop.max_thumbnail_size') * 1000000) && in_array($mimeType, ["image/jpg", "image/jpeg", "image/png", "image/gif"])) {

                $thumbDims = config('laradrop.thumb_dimensions');
                $img = Image::make($tmpStorage . '/' . $movedFileName);
                $img->resize($thumbDims['width'], $thumbDims['height']);
                $img->save($tmpStorage . '/_thumb_' . $movedFileName);

                // move thumbnail to final location
                $disk->put('_thumb_' . $movedFileName, fopen($tmpStorage . '/_thumb_' . $movedFileName, 'r+'));
                File::delete($tmpStorage . '/_thumb_' . $movedFileName);
                $fileData['has_thumbnail'] = 1;

            }

            /*
             * move uploaded file to final location
             */
            $disk->put($movedFileName, fopen($tmpStorage . '/' . $movedFileName, 'r+'));
            File::delete($tmpStorage . '/' . $movedFileName);

            /*
             * save in db
             */
            $fileData['filename'] = $movedFileName;
            $fileData['alias'] = Input::file('file')->getClientOriginalName();
            $fileData['public_resource_url'] = config('laradrop.disk_public_url') . '/' . $movedFileName;
            $fileData['type'] = $fileExt;
            $fileData['parent_id'] = null;
            $fileData['lft'] = 1;
            $fileData['rgt'] = 2;
            $fileData['depth'] = 0;
            if(Input::get('pid') > 0) {
                $fileData['parent_id'] = Input::get('pid');
            }
            $meta = $disk->getDriver()->getAdapter()->getMetaData($movedFileName);
            $meta['disk'] = config('laradrop.disk');
            $fileData['meta'] = json_encode($meta);
            $fileData['token'] =$token;


            //$file = $this->file->create($fileData);
            $productmedia = new Product_media();
            $productmedia->parent_id = $fileData['parent_id'];
            $productmedia->lft = $fileData['lft'];
            $productmedia->rgt = $fileData['rgt'];
            $productmedia->depth = $fileData['depth'];
            $productmedia->type = $fileData['type'];
            $productmedia->has_thumbnail = $fileData['has_thumbnail'];
            $productmedia->meta = $fileData['meta'];
            $productmedia->filename = $fileData['filename'];

            $productmedia->alias = $fileData['alias'];
            $productmedia->public_resource_url = $fileData['public_resource_url'];
           // $productmedia->token = $fileData['token'];

            $productmedia->product_id = $id;

            $productmedia->save();
            $file=$fileData;
            //echo $token;
            /*
             * fire 'file uploaded' event
             */
            event(new FileWasUploaded([
                'file'     => $file,
                'postData' => Input::all()
            ]));

            return $file;

        }

        catch (Exception $e) {

            // delete the file(s)
            if( isset($disk) && $disk) {
                $disk->delete($movedFileName);
                $disk->delete('_thumb_' . $movedFileName);
            }

            return $this->handleError($e);
        }
    }
    /**
     * Return html containers
     *
     * @return JsonResponse
     */
    public function getContainers()
    {
        return response()->json([
            'status' => 'success',
            'data' => [
                'main' => view('product.mainContainer')->render(),
                'preview' => view('product.previewContainer')->render(),
                'file' => view('product.fileContainer')->render(),
            ]
        ]);
    }

    /**
     * Create a folder
     *
     * @return JsonResponse
     */
    public function imagecreate()
    {
        try {
            $fileData['alias'] = Input::get('filename') ? Input::get('filename') : date('m.d.Y - G:i:s');
            $fileData['type'] = 'folder';
            if(Input::get('pid') > 0) {
                $fileData['parent_id'] = Input::get('pid');
            }

            $this->file->create($fileData);

            return response()->json([
                'status' => 'success'
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
    }
    /**
     * Delete the resource
     *
     * @param $id
     * @return JsonResponse
     */
    public function imagedestroy($id)
    {

        try {


            $productmedia = Temimage_files::find($id);

            $data = json_decode($productmedia->meta);
            $file ='public/uploads/'.$data->path;
            $filethumnail = 'public/uploads/'.'_thumb_'.$data->path;
            $productmedia->delete();
            File::delete($file);
            File::delete($filethumnail);
            //$this->file->destroy($id);

            return response()->json([
                'status' => 'success'
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
    }

    public function updateimagedestroy($id)
    {

        try {


            $productmedia = Product_media::find($id);

            $data = json_decode($productmedia->meta);
            $file ='public/uploads/'.$data->path;
            $filethumnail = 'public/uploads/'.'_thumb_'.$data->path;
            $productmedia->delete();
            File::delete($file);
            File::delete($filethumnail);
            //$this->file->destroy($id);

            return response()->json([
                'status' => 'success'
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
    }
    /**
     * Move file
     *
     * @return JsonResponse
     */
    public function move(){

        try {
            $this->file->move(Input::get('draggedId'), Input::get('droppedId'));

            return response()->json([
                'status' => 'success'
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
    }


    /**
     * Update filename
     *
     * @param int $id
     * @return JsonResponse
     */
    public function imageupdate($id){
        $token = csrf_token();

        try {

            $files = $this->getimageview(Input::get('pid'), $token, $id);

            return response()->json([
                'status' => 'success',
                'data' => $files,
            ]);
        }

        catch (Exception $e) {
            return $this->handleError($e);
        }
//        try {
//            $file = $this->file->find($id);
//            $file->filename = Input::get('filename');
//            $file->save();
//
//            return response()->json([
//                'status' => 'success'
//            ]);
//        }
//
//        catch (Exception $e) {
//            return $this->handleError($e);
//        }
    }

    /**
     * Error handler for this controller
     *
     * @param Exception $e
     * @return JsonResponse
     */
    private function handleError(Exception $e)
    {
        return response()->json([
            'msg' => $e->getMessage()
        ], 401);
    }

    public function multiple_upload() {
        // getting all of the post data
        $files = Input::file('images');
        // Making counting of uploaded images
        $file_count = count($files);
        // start count how many uploaded
        $uploadcount = 0;

        foreach ($files as $file) {
            $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
                $destinationPath = 'uploads'; // upload folder in public directory
                $filename = $file->getClientOriginalName();
                $upload_success = $file->move($destinationPath, $filename);
                $uploadcount ++;

                // save into database
                $extension = $file->getClientOriginalExtension();
                $entry = new Product_media();
                $entry->mime = $file->getClientMimeType();
                $entry->original_filename = $filename;
                $entry->filename = $file->getFilename().'.'.$extension;
                $entry->save();
            }
        }
        if($uploadcount == $file_count){
            Session::flash('success', 'Upload successfully');
            return Redirect::to('upload');
        } else {
            return Redirect::to('upload')->withInput()->withErrors($validator);
        }
    }

    public function autoComplete(Request $request) {
        $query = $request->get('term','');

        $products=Product_universal_sku::where('universal_sku','LIKE','%'.$query.'%')->get();

        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->universal_sku,'id'=>$product->id);
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
	
	/**
	* UPLOAD BULK PRODUCT START
	*/
	
	public function bulkproduct(){
		
		//GET LIST OF CATEGORY
//		$categories = Category::pluck('name', 'id');
        $allcategories=$this->productmodel->getalll3categories();
		$responce = array();
		return view('product.bulkupload', compact('responce','allcategories'));
	}
	public function validatorules($feild){
	    //print_r($feild);
        $arrdata = array();
	    foreach($feild as $fei)
        {
         if ($fei->type == "text" || $fei->type == "textarea" || $fei->type == "select" )
         {
             if($fei->required == 1){
                 $arrdata = array_add($arrdata, $fei->code, "required");
             }
             else
             {
                 continue;
             }

         }
         if($fei->type == "price")
         {
             if($fei->required == 1) {
                 $type = "required|numeric|min:1";
             }
             else
             {
                 $type = "numeric|min:1";
             }
             $arrdata = array_add($arrdata, $fei->code, $type);
         }
         else{
             if($fei->required == 1) {
                 $arrdata = array_add($arrdata, $fei->code,  "required|".$fei->type);
             }
             else
             {
                 $arrdata = array_add($arrdata, $fei->code, $fei->type);
             }
         }
            $arrdata = array_add($arrdata, 'categories', 'required|numeric');
            //echo $fei->code." ".$fei->type." ". $fei->required."<br/>";
        }
//print_r($arrdata);
         return $arrdata;
    }
    public function checkreqcolumn($required_feilds, $fileColumn){
	    $error = array();
        foreach($required_feilds as $column)
        {
            if(!in_array($column,$fileColumn))
            {
                $error[]= $column." Column dose exist";
            }

        }
        return $error;
    }
	private function getproductcategory($productid){
        $cat_pro = Categories_product::where('product_id','product_id',$productid);
        if($cat_pro !== null)
        {
            return $cat_pro->cat_id;
        }
        else
        {
            return "";
        }

    }
    public function bulkproductSubmit(Request $request){


		//$categoryId		= $request->get('categories');
		//$attributeSetId = $request->get('attribute_sets');


		/**
		* VALIDATE FORM INPUT START
		*/
		//$this->validate($request, [
		//		'categories' => 'required',
		//		'attribute_sets' => 'required',
		//		'product_csv' => 'required'
		//	],

		//	$messages = [
		//		'required'    => ':attribute is required.',
		//	]
		//);
        $extension = "";
        $extensionimage= "";
        if(!$request->file('product_csv') == "") {
            $file = $request->file('product_csv');
            $extension = $file->getClientOriginalExtension();
            $tmp_pathName = $file->getPathName();
        }

        if(!$request->file('product_image') == "") {
            $fileimage = $request->file('product_image');
            $extensionimage = $fileimage->getClientOriginalExtension();
            $tmp_pathNameimage = $fileimage->getPathName();

            $fileExt = $fileimage->getClientOriginalExtension();
            $fileName = str_replace('.' . $extensionimage, '', $fileimage->getClientOriginalName()) . '-' . date('Ymdhis');
            $mimeType = $fileimage->getMimeType();
            $tmpStorage = public_path()."\\new";
            $movedFileName = $fileName . '.' . $fileExt;
            $fileSize = $fileimage->getSize();

            if($fileSize > ( (int) config('laradrop.max_upload_size') * 1000000) ) {
                throw new Exception(trans('err.invalidFileSize'));
            }

            $fileimage->move($tmpStorage, $movedFileName);


            $zip = new ZipArchive;
            //$res = $zip->open($tmp_pathNameimage);
            if ($zip->open($tmpStorage."\\".$movedFileName) === TRUE) {

                $zip->extractTo($tmpStorage);
                $zip->close();
                //echo 'ok';
            } else {
                ///echo 'failed';
            }
            File::delete($tmpStorage."\\".$movedFileName);

        }

		//ONLY ALLOWED CSV FILE
		if($extension!='csv'){
			return Redirect::back()->withErrors(['Only CSV File can be uploaded (For Product Data).']);
		}
        if($extensionimage!='zip'){
            return Redirect::back()->withErrors(['Only Zip File can be uploaded (For Product Image).']);
        }
		/**
		* VALIDATE FORM INPUT END
		*/

		/**
		* READ FILE START
		*/

		///Changes::updateOrCreate
		$row = 1;
		$fileData 	= array();
		$fileColumn = array();
        $required_feilds = array();
        $arraycheck = array();
        $arraydata = array();
        $csvdata= array();
		if (($handle = fopen($tmp_pathName, "r")) !== FALSE) {
		  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

			if($row==1){

				$fileColumn = $data;
				$required_feilds = array('sku','vendor','attribute_sets');

                $responce = $this->checkreqcolumn($required_feilds, $fileColumn);
                $count=0;
                foreach ($fileColumn as $fileColumndata)
                {
                    $arraycheck = array_add($arraycheck, $count, $fileColumndata);
                    $count++;
                }

                if(sizeof($responce) > 0)
                {
                    return view('product.bulkupload', compact('responce'));
                }

                //return Redirect::to('/product/bulkproduct', compact('responce'));

			}else{
				$fileData[] = $data;
                $columnwisedata = array();
                $count = 0;

                foreach ($data as $datatest)
                {
                    $columnwisedata = array_add($columnwisedata, $arraycheck[$count], $datatest);
                    $count++;
                }

                if (!array_key_exists('mode_of_fullfillment', $columnwisedata)) {
                    $columnwisedata = array_add($columnwisedata, 'mode_of_fullfillment', '');
                }
                if (!array_key_exists('qty_dropship', $columnwisedata)) {
                    $columnwisedata = array_add($columnwisedata, 'qty_dropship', 0);
                }
                if (!array_key_exists('qty_stocking', $columnwisedata)) {
                    $columnwisedata = array_add($columnwisedata, 'qty_stocking', 0);
                }
                if (!array_key_exists('categories', $columnwisedata)) {
                    $columnwisedata = array_add($columnwisedata, 'categories', '');
                }

                $columnwisedata = array_add($columnwisedata, 'id', '');
                $csvdata = array_add($csvdata,$row,$columnwisedata);
			}
			$row++;
		  }
		  fclose($handle);
		}
        $countar= 2;
		foreach ($csvdata as $datas)
        {

            $attaribute_set_name = Attribute_set::where('name', '=', $datas["attribute_sets"])->first();
            $getAttributeSetFieldName	=	$this->getAttributeSetFieldNameall($attaribute_set_name->id);


            $product = $this->getProduct($datas["sku"]);

            if($product == "")
            {
                $csvdata[$countar]["id"] ="";
                $csvdata[$countar]["mode_of_fullfillment"] =null;
                $csvdata[$countar]["qty_dropship"] =0;
                $csvdata[$countar]["qty_stocking"] =0;
            }
            else
            {
                $csvdata[$countar]["id"] =$product->id;
                $csvdata[$countar]["mode_of_fullfillment"] =$product->mode_of_fullfillment;
                $csvdata[$countar]["qty_dropship"] =$product->qty_dropship;
                $csvdata[$countar]["qty_stocking"] =$product->qty_stocking;
                if(!array_key_exists('categories', $columnwisedata))
                {
                    $csvdata[$countar]["categories"] =$this->getproductcategory($product->id);
                }


            }


            $csvdata[$countar]["attribute_sets"] =$attaribute_set_name->id;
            //$this->validatorules($getAttributeSetFieldName);
            $validator = Validator::make($csvdata[$countar],$this->validatorules($getAttributeSetFieldName));
            $errors = $validator->errors()->getMessages();
            $resdata = "";
            if(sizeof($errors) > 0)
            {
                foreach($errors as $error)
                {
                    $resdata .= $error[0]." ";
                    $responce[] =  "Row# ".$countar.": ".$resdata;

                }
            }
            else{

                $responce[] =  "Row# ".$countar.": ".$this->productdata($csvdata[$countar]);
            }


            $countar++;
        }
        if(sizeof($responce) > 0)
        {
            return view('product.bulkupload', compact('responce'));
        }
        //print_r($csvdata);

		/**
		* READ FILE END
		*/

		/**
		* VALIDATE FILE COLUMN ON BASIS OF SELECTED ATTRIBUTE SET - START
		*/
        $attributeSetId = 1;
		 $getAttributeSetFieldName	=	$this->getAttributeSetFieldName($attributeSetId);

		 $fileColumnCount			=	count($fileColumn);
		 $attributeSetColumnCount	=	count($getAttributeSetFieldName);

		 //CHECK COLOUMN COUNT
		 if($fileColumnCount!=$attributeSetColumnCount){
			 return Redirect::back()->withErrors(['Invalid File Format column count not matched. Please check sample file before upload']);
		 }

		 //CHECK COLUMN NAME
		 $columnNameMatched = true;

		 if(!empty($fileColumn)){
			 foreach($fileColumn as $fileColumnRow){

				 //COLUMN EXSIST IN ATTRIBUTE ARRAY
				 if(!in_array($fileColumnRow,$getAttributeSetFieldName)){
					  $columnNameMatched = false;
					  break;
				 }
			 }
		 }


		 if(!$columnNameMatched){
			 return Redirect::back()->withErrors(['Invalid File Format column name not matched. Please check sample file before upload']);
		 }



		/**
		*  VALIDATE FILE COLUMN - END
		*/

		echo "Success";



	}


	//GET LIST OF ATTRIBUTE CATEGORY WISE
	public function attributeListByCategory(Request $request){
		
		$attr_map = array();
		if ($request->isMethod('post')) {
			$categoryId = $request->get('categoryId');
			
			if(is_numeric($categoryId)){
                $attr_map = $this->productmodel->getattributesbycatid($categoryId);
			}
		}
		return $attr_map;
		
	}

    //GET LIST OF ATTRIBUTE by Attribute set
    public function attributeListByattributeset(Request $request){

        $attributes = array();
         if ($request->isMethod('post')) {
            $attribute_sets = $request->get('attribute_sets');

            if(is_numeric($attribute_sets)){
                //GET ATTRIBUTE LIST
                    $attributes = Attributes_mapping::join('attributes', 'attributes.sf_attributeid', '=', 'attributes_mapping.attribute_id')
                    ->where('attribute_set_id', '=', $attribute_sets)->get();
                //$attr_map = Categories_attribute::where('cat_id', '=', $categoryId)->get();
            }

            $data=array();
            $attr_id = array();
            foreach ( $attributes as $attribute) {
                $data[]=array('value'=>$attribute->name,'id'=>$attribute->id, 'code'=>$attribute->code,'additional_info' => $attribute->additional_info, 'type' => $attribute->frontend_type, 'required' => $attribute->required);
                $attr_id = array_prepend($attr_id, $attribute->sf_attributeid);
            }

             $attr_option_value =  Attribute_option_value::whereIn('attribute_id', $attr_id)->get();

        }

        if(count($data))
            return ["attr_data"=>$data, "attr_option"=> $attr_option_value];
        else
            return ['value'=>'No Result Found','id'=>''];
        //return view('product.getATtributeList', compact('attr_map'));

    }

    public function attributeListByattributesetupdate(Request $request){

        $attributes = array();
        $productid = $request->get('id');
        if ($request->isMethod('post')) {
            $attribute_sets = $request->get('attribute_sets');

            if(is_numeric($attribute_sets)){
                //GET ATTRIBUTE LIST
                $attributes = Attributes_mapping::join('attributes', 'attributes.sf_attributeid', '=', 'attributes_mapping.attribute_id')
                    ->leftjoin('attributes_data',function($join) use ($productid)
                    {
                        $join->on('attributes.id', '=', 'attributes_data.attribute_id');
                        $join->on('attributes_data.product_id','=',DB::raw("'".$productid."'"));
                    })->select('attributes.name','attributes.id','attributes.code','attributes.additional_info','attributes.frontend_type','attributes.required','attributes_data.value')
                    ->where('attribute_set_id', '=', $attribute_sets)->get();
                //$attr_map = Categories_attribute::where('cat_id', '=', $categoryId)->get();
            }

            $data=array();
            $attr_id = array();
            foreach ( $attributes as $attribute) {
                $data[]=array('datavalue'=>$attribute->value, 'value'=>$attribute->name,'id'=>$attribute->id, 'code'=>$attribute->code,'additional_info' => $attribute->additional_info, 'type' => $attribute->frontend_type, 'required' => $attribute->required);
                $attr_id = array_prepend($attr_id, $attribute->sf_attributeid);
            }

            $attr_option_value =  Attribute_option_value::whereIn('attribute_id', $attr_id)->get();

        }

        if(count($data))
            return ["attr_data"=>$data, "attr_option"=> $attr_option_value];
        else
            return ['value'=>'No Result Found','id'=>''];
        //return view('product.getATtributeList', compact('attr_map'));

    }
    //GET LIST OF ATTRIBUTE by Attribute set
    public function getalll3categories(Request $request){

            return $this->productmodel->getl3categories();
        //return view('product.getATtributeList', compact('attr_map'));

    }

	//GET SAMPLE CSV
	public function sampleCSVDownload(Request $request){


        $allcategories = $request->get('categories');
//        print_r($allcategories);

		$getAttributeSetFieldName	=	array();
		if(!empty($allcategories)){


            $getAttributeSetFieldName	=	$this->productmodel->getAttributeSetFieldName($allcategories);
		}

        //if record found
		if(!empty($getAttributeSetFieldName)){
			$csv = "sku,vendor,mode_of_fullfillment,qty_dropship,categories".implode(",",$getAttributeSetFieldName);
		}else{
			$csv = "No Record Found";
		}
		
		
		  header('Content-Disposition: attachment; filename="export.csv"');
		  header("Cache-control: private");
		  header("Content-type: application/force-download");
		  header("Content-transfer-encoding: binary\n");

		  echo $csv;

		  exit;
	}
	
	//GET ONLY REQUIRED FIELD FOR CSV
	public function getAttributeSetFieldName($attributeSetId){
		
		$fieldName = array();
		//GET ATTRIBUTES BY ATTRIBUTESET
		 $attr_map = Attributes_mapping::join('categories_attributes', 'categories_attributes.attribitute_set_id', '=', 'attributes_mapping.attribute_set_id')
         ->join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
		 ->select('distinct attributes.code')
		->whereIn('cat_id', $attributeSetId)->get();


		if(!empty($attr_map)){
			foreach($attr_map as $attr_mapData){
				$fieldName[]	=	$attr_mapData->code;
			}
		}

		return $fieldName;
			
	}

    public function getAttributeSetFieldNameall($attributeSetId){

        //GET ATTRIBUTES BY ATTRIBUTESET
        $attr_map = Attributes_mapping::join('attributes', 'attributes.id', '=', 'attributes_mapping.attribute_id')
            ->select('attributes.code' , 'attributes.required', 'attributes.type')
            ->where([
                ['attribute_set_id', '=', $attributeSetId],
                ['type', '!=', '']
            ])->get();

            return $attr_map;

    }

    public function getProduct($sku){

        //GET ATTRIBUTES BY ATTRIBUTESET
        $product_id = Product::where('sku','=',$sku)->first();

        if ($product_id !== null) {
            return $product_id;
        }
        else
        {
            return "";
        }

    }
	/**
	* UPLOAD BULK PRODUCT END
	*/

    public function addapprovereject(Request $request){

        $productid = $request->get('product_id');
        $comment = $request->get('comment');
        $userid = Auth::user()->id;
        if ($request->get('type')=="approve")
        {
            if($request->get('role_id')=="1"){
                $this->productmodel->changestatusapproveqc($productid,$comment,$userid);
                $request->session()->flash('success', 'Product Approved by Quality Control ');


            }
            else {
                $this->productmodel->changestatusapprovevm($productid,$comment,$userid);
                $request->session()->flash('success', 'Product Approved by Vendor Manager ');
            }
        }
        else{
            if($request->get('role_id')=="1"){
                $this->productmodel->changestatusrejectqc($productid,$comment,$userid);
                $request->session()->flash('success', 'Product Rejected by Quality Control ');

            }
            else {
                $this->productmodel->changestatusrejectvm($productid,$comment,$userid);
                $request->session()->flash('success', 'Product Rejected by Vendor Manager ');
            }
        }
        if($this->productmodel->approvalcheck($productid))
        {
            $this->productmodel->pushstorefront($productid);
        }
        return Redirect::to('products/approvalpending');
//        $attr_map = array();
//        if ($request->isMethod('post')) {
//            $categoryId = $request->get('categoryId');
//
//            if(is_numeric($categoryId)){
//                $attr_map = $this->productmodel->getattributesbycatid($categoryId);
//            }
//        }


    }

    public function getconfigurableopt(Request $request){
        return $this->productmodel->getconfigurableproopt($request->get('attr_id'));

    }

}