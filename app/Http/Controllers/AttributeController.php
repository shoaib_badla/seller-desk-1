<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attribute;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class AttributeController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:11:55pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - attribute';
        $attributes = Attribute::paginate(6);
        return view('attribute.index',compact('attributes','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - attribute';
        
        return view('attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = new Attribute();

        
        $attribute->attribute_set_id = $request->attribute_set_id;

        
        $attribute->code = $request->code;

        
        $attribute->name = $request->name;

        
        $attribute->type = $request->type;

        
        $attribute->default_value = $request->default_value;

        
        $attribute->created_at = $request->created_at;

        
        $attribute->updated_at = $request->updated_at;

        
        $attribute->premission_role_id = $request->premission_role_id;

        
        
        $attribute->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new attribute has been created !!']);

        return redirect('attribute');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - attribute';

        if($request->ajax())
        {
            return URL::to('attribute/'.$id);
        }

        $attribute = Attribute::findOrfail($id);
        return view('attribute.show',compact('title','attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - attribute';
        if($request->ajax())
        {
            return URL::to('attribute/'. $id . '/edit');
        }

        
        $attribute = Attribute::findOrfail($id);
        return view('attribute.edit',compact('title','attribute'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $attribute = Attribute::findOrfail($id);
    	
        $attribute->attribute_set_id = $request->attribute_set_id;
        
        $attribute->code = $request->code;
        
        $attribute->name = $request->name;
        
        $attribute->type = $request->type;
        
        $attribute->default_value = $request->default_value;
        
        $attribute->created_at = $request->created_at;
        
        $attribute->updated_at = $request->updated_at;
        
        $attribute->premission_role_id = $request->premission_role_id;
        
        
        $attribute->save();

        return redirect('attribute');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/attribute/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$attribute = Attribute::findOrfail($id);
     	$attribute->delete();
        return URL::to('attribute');
    }
}
