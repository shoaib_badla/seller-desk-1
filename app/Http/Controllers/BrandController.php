<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class BrandController.
 *
 * @author  The scaffold-interface created at 2017-01-23 10:03:52am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - brand';
        $brands = Brand::paginate(6);
        return view('brand.index',compact('brands','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - brand';
        
        return view('brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = new Brand();

        
        $brand->name = $request->name;

        
        $brand->description = $request->description;

        
        
        $brand->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new brand has been created !!']);

        return redirect('brand');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - brand';

        if($request->ajax())
        {
            return URL::to('brand/'.$id);
        }

        $brand = Brand::findOrfail($id);
        return view('brand.show',compact('title','brand'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - brand';
        if($request->ajax())
        {
            return URL::to('brand/'. $id . '/edit');
        }

        
        $brand = Brand::findOrfail($id);
        return view('brand.edit',compact('title','brand'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $brand = Brand::findOrfail($id);
    	
        $brand->name = $request->name;
        
        $brand->description = $request->description;
        
        
        $brand->save();

        return redirect('brand');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/brand/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$brand = Brand::findOrfail($id);
     	$brand->delete();
        return URL::to('brand');
    }
}
