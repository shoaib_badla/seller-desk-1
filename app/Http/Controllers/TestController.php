<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Test;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class TestController.
 *
 * @author  The scaffold-interface created at 2017-01-03 06:46:41am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - test';
        $tests = Test::paginate(6);
        return view('test.index',compact('tests','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - test';
        
        return view('test.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = new Test();

        
        $test->firstname = $request->firstname;

        
        
        $test->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new test has been created !!']);

        return redirect('test');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - test';

        if($request->ajax())
        {
            return URL::to('test/'.$id);
        }

        $test = Test::findOrfail($id);
        return view('test.show',compact('title','test'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - test';
        if($request->ajax())
        {
            return URL::to('test/'. $id . '/edit');
        }

        
        $test = Test::findOrfail($id);
        return view('test.edit',compact('title','test'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $test = Test::findOrfail($id);
    	
        $test->firstname = $request->firstname;
        
        
        $test->save();

        return redirect('test');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/test/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$test = Test::findOrfail($id);
     	$test->delete();
        return URL::to('test');
    }
}
