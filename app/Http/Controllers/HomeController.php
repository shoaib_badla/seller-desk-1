<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Config;
use App\Vendor_info;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
		
		//GET LOGIN USER ID
		$loginEmail = Auth::user()->email;
		
		$userRole = \App\User::findOrfail(Auth::user()->id);
		//CHECK USER IS VENDOR
		$vendorRole	=	Config::get('constants.vendorRole');
		if($userRole->hasRole($vendorRole) ){
			
			$vendor_info	=	Vendor_info::where('vendor_email',$loginEmail)->whereIn('status',array(Config::get('constants.vendorAcceptedStatus'),Config::get('constants.vendorApprovedStatus')))
				->select('is_vendor_approved')
                ->first();
				
			if(!empty($vendor_info)){
				
				//IF STATE NEW
				if($vendor_info->is_vendor_approved==0){
					 return view('vendor/welcome');
				}
				
				//IF STATE APPROVED
				if($vendor_info->is_vendor_approved==1){
					 return redirect('vendor/dashboard');
				}
			
				
				 //IF STATE  Waiting
				if(in_array($vendor_info->is_vendor_approved,array(2,3,6,7))){
					return redirect('applicationpending');
				}
				
				 //IF STATE  REJECTED
				if(in_array($vendor_info->is_vendor_approved,array(4,5))){
					return redirect('applicationcorrection');
				}
			}	
			
		}
		
        return view('home');
    }
}