<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Common;
use App\Vendor_info;
use App\City;
use App\Country;
use App\Brand;
use App\Category;
use App\User;
use App\Vendor_signing_contract;
use App\VendorDataRepository, App\VendorApplicationDataRepository, App\VendorReviewApplicationDataRepository;
use App\Vendor_pickup_address;
use App\Vendor_product_category;
use App\Vendor_brand;
use Amranidev\Ajaxis\Ajaxis;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Mgallegos\LaravelJqgrid\Facades\GridEncoder;
use URL,JsValidator,Validator,Config,Auth,DB,Route,SoapClient;
use App\UserHasRoles;
/**
 * Class Vendor_infoController.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vendor_infoController extends Controller
{
	
	  /**
     * validation rules in a property in 
     * the controller to reuse the rules.
     */
    protected $validationRules=[
              'vendor_name' => 'required|max:255',
                'cnic' => 'required|alpha_dash|max:13',
                'vendor_email' => 'required|email|max:255',
                'poc_email' => 'required|email|max:255',
                'poc_name' => 'required|max:255',
                //'vendor_manager_assigned' => 'required',
                'form_of_organization' => 'required|numeric',
                'poc_contact' => 'required|alpha_dash|max:10',
                'country' => 'required|numeric',
                'province' => 'required|numeric',
                'city' => 'required|numeric',
                'cell_phone' => 'required|alpha_dash|max:10',
                'ntn1' => 'required|alpha_dash|max:7',
                'ntn2' => 'required|alpha_dash|max:1',
                'office_phone' =>'required|numeric',
                'company_address' => 'required',
				
    ];
	
	    
	

	protected $errorMessagePickupAddress,$errorMessagepickup_city,$errorMessagewarehouse_reporting_ffc,$errorMessagewarehouse_reporting_poc,$errorMessagewarehouse_timming,$errorMessageproduct_category,$errorMessageproposed_commission,$form_of_organizationErr,$logoErr,$signing_contractErr,$vendorAleadyExsist = false;
   
	public function __construct()
	 {
		 //GET CURRENT ACTION
		 list(, $action) = explode('@', Route::getCurrentRoute()->getActionName());
		 
		 if($action!="addVendorFromYayvo"){
			 $this->middleware('auth');
		 }
		  
	 }

   /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    { 
        $title = 'Index - vendor_info';
        $vendor_infos = Vendor_info::paginate(6);
		
	
		//GET REJECTION LIST FOR Vendor
		$getRejectionList	=	Common::getRejectionList();
		return view('vendor.index',compact('vendor_infos','title','getRejectionList'));
		
		
    }
	
	public function vendordata()
    {
        //$users = \App\User::all();
        GridEncoder::encodeRequestedData(new VendorDataRepository(), Input::all());
        //return view('users.index', compact('users'));
    }




   
	
	/**
	* SEND VENDOR REGISTRATION EMAIL
	*/
	public function sendVendorRegistrationEmail($to,$userName,$randomPassword){
		
		//SEND USER CREATION EMAIL
		$subject =	"Welcome to seller desk";
		$cc = Config::get('constants.cc_email_address');
		
		$url =  url('/');
		
		$body	= <<<HTML
				Welcome $userName,
					Your Login credential are as follows:
						Portal URL : $url
						Email Address : $to
						Password : $randomPassword
HTML;
		Common::sendEmail($to,$subject,$body,$cc);
	}	
	
	/**
	* SEND VENDOR REGISTRATION EMAIL
	*/
	public function sendVendorApplicationAssignedEmail($to){
		
		//SEND USER CREATION EMAIL
		$subject =	"Vendor Application is assigned for review";
		$cc = Config::get('constants.cc_email_address');
		
		$url =  url('/');
		
		$body	= <<<HTML
				Dear Concern,
				Vendor Application is assigned for review
HTML;
		Common::sendEmail($to,$subject,$body,$cc);
	}
	
	/**
	* SEND VENDOR EMAIL ABOUT HIS REQESUT IS PENDING
	*/
	public function sendVendorEmailAboutPendingFormRequest($to,$userName){
		
		//SEND USER CREATION EMAIL
		$subject =	"Yayvo Vendor Registration Approval Pending";
		$cc = Config::get('constants.cc_email_address');
		
		$body	= <<<HTML
				Welcome $userName,
					Your information is submitted successfully & Waiting for Approval!
					
				Thanks
HTML;
		Common::sendEmail($to,$subject,$body,$cc);
	}
	
	/**
	* SEND VENDOR APPLICTION REJECTION EMAIL
	*/
	public function sendVendorApplicationRejectionEmail($to,$userName ,$lead_rejection_reason){
		//SEND USER CREATION EMAIL
		$subject =	"Yayvo Vendor Application Rejected";
		$cc = Config::get('constants.cc_email_address');
		
		
		$rejectionReason	=	"";
		$lead_rejection_reasonArr	=	explode(",",$lead_rejection_reason);
		
		if(!empty($lead_rejection_reasonArr)){
			foreach($lead_rejection_reasonArr as $lead_rejection_reasonROw){
				$rejectionReason	.=	" - ".$lead_rejection_reasonROw."<br/>";
			}
		}
		
		$body	= <<<HTML
				Welcome $userName,
					Your application for vendor registration is rejected! Below are reasons
					$rejectionReason
				Thanks
HTML;
		Common::sendEmail($to,$subject,$body,$cc);
	}
	
	
	/**
	* Check vendor already exsist
	*/
	public function checkVendorAlradyExsist(Request $request){
		
		$userExsist = false;
		
		if ($request->isMethod('post')) {
			$vendor_email = $request->vendor_email;
			$vendor_id = isset($request->vendor_id)?$request->vendor_id:"";
			
			//if(empty($vendor_id)){
				//CHECK VENDOR EXSIST IN USER TABLE
				$userExsistArr = User::where('email', $vendor_email)->where('is_active', 1)->first();
				
				if(!empty($userExsistArr)){
					$userExsist = true;
				}
				
				//CHECK USER FROM VENDOR INFO TABLE
				$vendorInfoExsist = Vendor_info::where('vendor_email', $vendor_email)->where('status', '<>', Config::get('constants.vendorRejectedStatus'))->first();
				if(!empty($vendorInfoExsist)){
					$userExsist = true;
				}
			//}else{
				//CHECK USER FROM VENDOR INFO TABLE
				//$vendorInfoExsist = Vendor_info::where('vendor_email', $vendor_email)->where('id','!=', $vendor_id)->first();
				
				//if(!empty($vendorInfoExsist)){
				//	$userExsist = true;
				//}
			//}
			
		}
		
		
		if($userExsist){
			return 'exsist';
		}else{
			return 'not exsist';
		}
	}
	
	
	/**
	* ADD VENDOR FORM MAGENTO
	*/
	public function addVendorFromYayvo(Request $request){
		
		$magentoUrl	=	Config::get('constants.magentoUrl');
		$validator = Validator::make($request->all(),$this->validationRules);

		//CHECK VENDOR ALREADY EXSIST
		$checkVendorAlradyExsist = $this->checkVendorAlradyExsist($request);
		if($checkVendorAlradyExsist == 'exsist'){
			$this->vendorAleadyExsist = true;
		}
		
		$validator->after(function ($validator) {
			
			//CHECK VENDOR ALREADY EXSIST
			if($this->vendorAleadyExsist){
				$this->addValidationErrorMessage($validator,array('Vendor Already Exsist with same email address'),'vendor_email');
			}
			
		});
		
	

		
		if ($validator->fails()){
			echo "Required field Missing <hr/>";
			echo "<pre>";print_r($validator->errors()->all());exit();
			//return redirect('vendor_info/login')->withErrors($validator)->withInput();
			// return redirect($magentoUrl.'/vendor_form/index/index');
		}
		

		/**
		* SAVE VENDOR INFO START
		*/
		//echo "<pre>";print_r($request->all());exit();
				
		$mytime 		= Carbon::now();
		$currentTime	= $mytime->toDateTimeString();
		
        $vendor_info = new Vendor_info();
        
        $vendor_info->vendor_name	 					= $request->vendor_name;
        $vendor_info->vendor_email	 					= $request->vendor_email;
        $vendor_info->office_phone	 					= $request->office_phone;
        $vendor_info->poc_name	 						= $request->poc_name;
        $vendor_info->poc_contact	 					= $request->poc_contact;
        $vendor_info->ntn_number	 					= $request->ntn1.'-'.$request->ntn2;
        $vendor_info->cnic	 							= $request->cnic;
        $vendor_info->cell_phone	 					= $request->cell_phone;
        $vendor_info->form_of_organization	 			= $request->form_of_organization;
        $vendor_info->poc_email	 						= $request->poc_email;
        $vendor_info->country	 						= $request->country;
        $vendor_info->province	 						= $request->province;
        $vendor_info->city	 							= $request->city;
        $vendor_info->detailed_added_by	 				= 0;
        $vendor_info->company_address	 				= $request->company_address;
		$vendor_info->created_at						= $currentTime;
		$vendor_info->updated_at						= $currentTime;
  
        $vendor_info->save();
		
		/**
		* SAVE VENDOR INFO END
		*/
		
		//GET NEW ADDED VENDOR ID
		$newVendorId 	=	$vendor_info->id;
		
		/**
		* ADDING Vendor Brand INFO - START
		*/
		
			Vendor_brand::where('vendor_id',$newVendorId)->delete();
	
			$vendBrand			= $request->brand;
			
			if(!empty($vendBrand)){
					
				foreach($vendBrand as $vendBrandRow){
				
					//SAVING RECORD
					$vendor_brand = new Vendor_brand();	
				
					$vendor_brand->vendor_id			=	$newVendorId;
					$vendor_brand->brand_id				=	$vendBrandRow;
					$vendor_brand->created_at			=	$currentTime;
					$vendor_brand->updated_at			=	$currentTime;
					
					$vendor_brand->save();
					
				}
			}
		/**
		* ADDING Vendor Brand INFO - END
		*/
		
		$userName = $vendor_info->vendor_name;
		$this->sendVendorEmailAboutPendingFormRequest($request->vendor_email,$userName);
		
		
		
        return redirect($magentoUrl.'/vendor_form/index/success');
	}
	
	
	/**
	* VENDOR APPROVED - START
	*/
	
	public function approvedVendor(Request $request){
		
		 if($request->ajax()){
			 
			$id  = $request->vendor_id;
			$title = 'Approved - Vendor';
			/*if($request->ajax())
			{
				return URL::to('vendor_info/'. $id . '/edit');
			}*/

					
			//VENDOR DATA
			$vendor_info = Vendor_info::findOrfail($id);
			
			if(empty($vendor_info)){
				die('Vendor info not exsist!');
			}
			
			//GET VENDOR MANAGER LIST
			$vendorManagerList = Vendor_info::getUserListByRole(Config::get('constants.vendorManagerRole'));
			
			//GET CATEGORY MANAGER LIST
			$categoryManagerList = Vendor_info::getUserListByRole(Config::get('constants.categoryManagerRole'));
			
			//GET LOGIN MANAGER FIRST FOR ASSIGNED Vendor
			//
			//MANAGER LIST
		//	$managerList = array_merge($managerList,$categoryManagerList);
			
			$data = array('vendorManagerList'=>$vendorManagerList,'categoryManagerList'=>$categoryManagerList,'vendorId'=>$id);
			
		   
			return view('vendor.approved',$data);
		}
	}
	
	//APPROVED VENDOR FORM SUBMIT
	public function approvedVendorFormSubmit(Request $request){
		
		$validator = Validator::make($request->all(),[
                'vendormanagerAssigned' => 'required'
				
    ]);


		if ($validator->fails()){
		
			return redirect('vendor_info')
                        ->withErrors($validator)
                        ->withInput();
		}
		/**
		* SAVE VENDOR INFO START
		*/
		$vendor_id 				= $request->vendor_id;
		$vendormanagerAssigned  = $request->vendormanagerAssigned;
		
		/**
		* GET ASSIGNED VM/HOV/CM EMAIL ADDRESSS
		*/
		$assignedCatVmEmail = User::select('email')->where('id', $vendormanagerAssigned)->first();
		$assignedCatVmEmail	= isset($assignedCatVmEmail->email)?$assignedCatVmEmail->email:'';
		
		$mytime 		= Carbon::now();
		$currentTime	= $mytime->toDateTimeString();
		
        $vendor_info = Vendor_info::findOrfail($vendor_id);
        $vendor_info->accepted_by	 				= Auth::user()->id;
        $vendor_info->vendor_manager_assigned	 	= $vendormanagerAssigned;
        $vendor_info->accepted_at	 				= $currentTime;
        $vendor_info->status	 					= Config::get('constants.vendorAcceptedStatus');
		
        $vendor_info->save();
		
		
		
		/**
		* CREATE NEW USER FOR VENDOR
		*/
		
		//CHECK VENDOR EXSIST IN USER TABLE
		$userExsistArr = User::where('email', $vendor_info->vendor_email)->where('is_active', 1)->get();
		//USERINFO NOT FOUND
		if($userExsistArr->count() == 0){
		
			//GET PASSWORD
			$randomPassword	=	Common::randomPassword();
			$hashRandomPassword	=	bcrypt($randomPassword);
			//ACTIVE VENDOR AGAIN & RESET PASSOWRD IF USER ALREADY EXSIST & REJECTED BEFORE
			$vendorEmail	=  $vendor_info->vendor_email;
			$userExsist 	=  User::select('id')->where('email', $vendor_info->vendor_email)->get();
			
			if($userExsist->count() > 0){
				
				$userData	=	$userExsist->first();
				
				//GET USER ID
				$userId	=	$userData->id;
			
				//Auth::HasRoles->where('user_id','=','');
				//ACTIVE & UPDATE PASSWORD
				User::where('email', $vendorEmail)->update(['is_active' => 1,'password'=>$hashRandomPassword]);
			}else{
				//CREATE USER
				$userRegistered =  User::create([
					'name' =>  $vendor_info->vendor_name,
					'email' => $vendor_info->vendor_email,
					'password' => $hashRandomPassword,
				]);
				
				//GET USER ID
				$userId = $userRegistered->id;
			}
			
			//ASSIGN VENDOR ROLE ON REGISTARTION
			$userRole = User::findOrfail($userId);
			
			//IF ROLE NOT ASSIGN ALREADY
			$roleName	=	Config::get('constants.vendorRole');
			if(!$userRole->hasRole($roleName)){
				$userRole->assignRole($roleName);
			}
			
			//SEND EMAIL
			$userName = $vendor_info->vendor_name;
			$this->sendVendorRegistrationEmail($vendor_info->vendor_email,$userName,$randomPassword);
			$this->sendVendorApplicationAssignedEmail($assignedCatVmEmail);
			
		}
		
        return redirect('vendor_info')->withSuccess('Vendor Request Accepted Successfully');
		
	}
	
	/**
	* REJECT VENDOR
	*/
	public function rejecteVendor(Request $request){
		
		 if($request->ajax()){
			 
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;
				$lead_rejection_reason  = urldecode($request->lead_rejection_reason);

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus')){
					$vendor_info->rejected_by	 				= Auth::user()->id;
					$vendor_info->rejection_reason			 	= $lead_rejection_reason;
					$vendor_info->rejected_at	 				= $currentTime;
					$vendor_info->status	 					= Config::get('constants.vendorRejectedStatus');

					$vendor_info->save();
					
					//DISABLED USER IF EXSIST FOR SAME EMAIL ADDRESSS
					$vendorEmail	=	$vendor_info->vendor_email;
					User::where('email', $vendorEmail)->update(['is_active' => 0]);
					
					//DELETE ASSIGN ROLE OF Vendor
					$userExsist 	=  User::select('id')->where('email', $vendorEmail)->get();
					if($userExsist->count()>0){
						
						
						$userData	=	$userExsist->first();
						
						//GET USER ID
						$user_id	=	$userData->id;
				
						UserHasRoles::where('user_id','=',$user_id)->delete();
					}
					
					
					
					$this->sendVendorApplicationRejectionEmail($vendorEmail,$vendor_info->vendor_name,$lead_rejection_reason);
				}
				
				
				
				
				echo "success";
				exit();
		 }
		 
		 
	}
	
	/**
	* VEIW VENDOR BASIC INFO
	*/
	public function veiwBasicVendorInfo(Request $request){

		if($request->ajax())
        {
           
		   $id  = $request->vendor_id;
			$title = 'Show - vendor_info';

			//VENDOR DATA
			$vendor_info = Vendor_info::findOrfail($id);
			
			if(empty($vendor_info)){
				die('Vendor info not exsist!');
			}
			
			//GET VENDOR OFFERED BRANDS
			$vendor_brand 				= Vendor_brand::where('vendor_id', $id)->select('brand_id')->get();
			
			$vendorBrandId = array();
			if(!empty($vendor_brand)){
				foreach($vendor_brand as $vendor_brandRow){
					$vendorBrandId[] = $vendor_brandRow->brand_id;
				}
			}
			
			//GET BRAND NAME
			$brandList = Brand::select('name')->whereIn('id',$vendorBrandId)->get();
			$vendorBrandId = array();
			if(!empty($brandList)){
				foreach($brandList as $brandListRow){
					$vendorBrandId[] = $brandListRow->name;
				}
			}
			
			$vendorBrandId	=	!empty($vendorBrandId)?implode(" , ",$vendorBrandId):"N/A";
			
			 //CHECK MARGIN STATE
			$checkLoginVendorApplicationState = $vendor_info->is_vendor_approved;
		
			//GET ALL REJECTION REASON
			$getRejectionList	=	Common::getRejectionList();
					
			//GET USER ROLE INFO
			$userRole = \App\User::findOrfail(Auth::user()->id);
			
			$financeRole			=	Config::get('constants.financeRole');
			$operationRole			=	Config::get('constants.operationRole');
			
			//GET FINANCE DEPART REASON
			if($userRole->hasRole($financeRole)){
				$getRejectionList	=	Common::getRejectionList($financeRole);
			}
			//GET OPERATION DEPART REASON
			if($userRole->hasRole($operationRole)){
				$getRejectionList	=	Common::getRejectionList($operationRole);
			}
		
			//IF STATE APPROVED
			if($checkLoginVendorApplicationState < 1){
				
				/**
				* LOAD VENDOR BASIC INFO
				*/
			 
				$vendor_info = Vendor_info::select(
													'vendor_infos.id',
													'vendor_infos.vendor_name',
													'vendor_infos.vendor_email',
													'vendor_infos.cnic',
													'vendor_infos.cell_phone',
													'vendor_infos.office_phone',
													'vendor_infos.poc_name',
													'vendor_infos.poc_email',
													'vendor_infos.poc_contact',
													'vendor_infos.ntn_number',
													'vendor_infos.company_address',
													'vendor_infos.vendor_manager_assigned',
													'vendor_infos.is_vendor_approved',
													DB::Raw('users.name AS assignedManager'),
													DB::Raw('form_of_organization.label AS form_of_organization'),
													DB::Raw('province_tbl.name AS province'),
													DB::Raw('city.name AS city'),
													DB::Raw('country.name AS country'),
													'status'
												);
				
				 $vendor_info->leftJoin('form_of_organization', function ($join) {
					$join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
				});
				 $vendor_info->leftJoin('city', function ($join) {
					$join->on('vendor_infos.city', '=', 'city.id');
				});
				 $vendor_info->leftJoin('province_tbl', function ($join) {
					$join->on('vendor_infos.province', '=', 'province_tbl.id');
				});
				 $vendor_info->leftJoin('country', function ($join) {
					$join->on('vendor_infos.country', '=', 'country.id');
				});
				 $vendor_info->leftJoin('users', function ($join) {
					$join->on('vendor_infos.vendor_manager_assigned', '=', 'users.id');
				});
				
				$vendor_info->where('vendor_infos.id','=',$id);
				$vendor_info	=	$vendor_info->first();
				
				$data = array('brandList'=>$vendorBrandId,'vendor_info'=>$vendor_info,'getRejectionList'=>$getRejectionList);
			
			
			
				return view('vendor.basicInfo',$data);
			}else{
				//SHOW VENDOR DETAILED INFO
				//GET LOGIN VENDOR DATA DETAIL
				$getLoginVendorDataDetail	=	app('App\Http\Controllers\VendorController')->getLoginVendorDataDetail($id);
				
				//CHECK USER ROLE
				$userRole = User::findOrfail(Auth::user()->id);
				
				$financeRole			=	Config::get('constants.financeRole');
				$operationRole			=	Config::get('constants.operationRole');
				
				$is_financeUser 	=	false;
				$is_OpsUser 	=	false;
				//Display REJECTION & APPROVED BUTTON FOR FINANCE &OPS
				if($userRole->hasRole($financeRole)){
					$is_financeUser 	=	true;
				}
				if($userRole->hasRole($operationRole)){
					$is_OpsUser 	=	true;
				}
				
				$data = array('brandList'=>$vendorBrandId,'vendor_info'=>$getLoginVendorDataDetail['vendorBasicInfo'],'vendBankInfoArr'=>$getLoginVendorDataDetail['vendBankInfoArr'],'VendorPickupAddressArr'=>$getLoginVendorDataDetail['VendorPickupAddressArr'],'vendorReturnAddressArr'=>$getLoginVendorDataDetail['vendorReturnAddressArr'],'vendorMargin'=>$getLoginVendorDataDetail['vendorMargin'],'requestFrom'=>'modal','getRejectionList'=>$getRejectionList,'is_financeUser'=>$is_financeUser,'is_OpsUser'=>$is_OpsUser);
		
				return view('vendor.infomrationpendingmodal',$data);
			}

			
			
        }
    }
	
	
	
   /**
     * Display a listing vendor application assigned person
     */
    public function vendorApplicationRequest()
    { 
        $title = 'Index - vendor_info';
		
		//GET REJECTION LIST FOR Vendor
		$getRejectionList	=	Common::getRejectionList();
	//	GridEncoder::encodeRequestedData(new ExampleRepository(), Input::all());
        return view('vendor.vendor_assigned_apllication',compact('title','getRejectionList'));
    }
	
	public function vendorApplicationRequestData()
    {
        //$users = \App\User::all();
        GridEncoder::encodeRequestedData(new VendorApplicationDataRepository(), Input::all());
        //return view('users.index', compact('users'));
    }
	
	
	   /**
     * Display a listing vendor application REVIEW TO OTHER DEPARTMENT
     */
    public function vendorApplicationReview()
    { 
        $title = 'Index - vendor_info';
		

		
	//	GridEncoder::encodeRequestedData(new ExampleRepository(), Input::all());
        return view('vendor.vendor_review_application',compact('title'));
    }
	
	public function vendorApplicationReviewData()
    {
        //$users = \App\User::all();
        GridEncoder::encodeRequestedData(new VendorReviewApplicationDataRepository(), Input::all());
        //return view('users.index', compact('users'));
    }
	
	
	/**
	* REJECT VENDOR FROM FINANCE
	*/
	public function rejectedFromFinanceDepart(Request $request){
		
		 if($request->ajax()){
			 
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;
				$lead_rejection_reason  = urldecode($request->lead_rejection_reason);

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED && STATUS IS PENDING FROM DEAPRT
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus') && $vendor_info->is_vendor_finance_approved == 3){
					
					//ADD COMMENT HISTORY AND USER ID WITH ROLE
					app('App\Http\Controllers\VendorController')->addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.applicationRejectedFromFinance'),$lead_rejection_reason);
					
					//UPDATE VENDOR STATUS IF OPS DEPART GIVE FEEDBACK ABOUT APPLICATION
					if($vendor_info->is_vendor_ops_approved!=3){
						$vendor_info->is_vendor_approved	= 7; //FEEDBACK RECIEVED
					}
					
					$vendor_info->is_vendor_finance_approved	= 2;  //REJECTED
					$vendor_info->save();
					
				}
				
				echo "success";
				exit();
		 }
		 
		 
	}
	
	/**
	* REJECT VENDOR FROM OPS
	*/
	public function rejectedFromOpsDepart(Request $request){
		
		 if($request->ajax()){
			 
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;
				$lead_rejection_reason  = urldecode($request->lead_rejection_reason);

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED && STATUS IS PENDING FROM DEAPRT
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus') && $vendor_info->is_vendor_ops_approved == 3){
					
					//ADD COMMENT HISTORY AND USER ID WITH ROLE
					app('App\Http\Controllers\VendorController')->addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.applicationRejectedFromOps'),$lead_rejection_reason);
					
					//UPDATE VENDOR STATUS IF FINANCE DEPART GIVE FEEDBACK ABOUT APPLICATION
					if($vendor_info->is_vendor_finance_approved !=3){
						$vendor_info->is_vendor_approved	= 7; //FEEDBACK RECIEVED
					}
					
					$vendor_info->is_vendor_ops_approved	= 2; //REJECTED
					$vendor_info->save();
					
				}
				
				echo "success";
				exit();
		 }
		 
		 
	}
	
	/**
	* APPROVED VENDOR FROM FINANCE
	*/
	public function approvedFromFinanceDepart(Request $request){
		
		 if($request->ajax()){
			 
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED && STATUS IS PENDING FROM DEAPRT
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus') && $vendor_info->is_vendor_finance_approved == 3){
					
					//ADD COMMENT HISTORY AND USER ID WITH ROLE
					app('App\Http\Controllers\VendorController')->addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.applicationApprovedFromFinance'),'Vendor Application Approved from Finance');
					
					//UPDATE VENDOR STATUS IF OPS DEPART GIVE FEEDBACK ABOUT APPLICATION
					if($vendor_info->is_vendor_ops_approved!=3){
						$vendor_info->is_vendor_approved	= 7; //FEEDBACK RECIEVED
					}
					
					$vendor_info->is_vendor_finance_approved	= 1;  //APPROVED
					$vendor_info->save();
					
				}
				
				echo "success";
				exit();
		 }
		 
		 
	}
	
	/**
	* REJECT VENDOR FROM OPS
	*/
	public function approvedFromOpsDepart(Request $request){
		
		 if($request->ajax()){
			 
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;
				$lead_rejection_reason  = urldecode($request->lead_rejection_reason);

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED && STATUS IS PENDING FROM DEAPRT
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus') && $vendor_info->is_vendor_ops_approved == 3){
					
					//ADD COMMENT HISTORY AND USER ID WITH ROLE
					app('App\Http\Controllers\VendorController')->addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.applicationApprovedFromOps'),'Vendor Application Approved from Ops');
					
					//UPDATE VENDOR STATUS IF OPS DEPART GIVE FEEDBACK ABOUT APPLICATION
					if($vendor_info->is_vendor_finance_approved!=3){
						$vendor_info->is_vendor_approved	= 7; //FEEDBACK RECIEVED
					}
					
					$vendor_info->is_vendor_ops_approved	= 1; //REJECTED
					$vendor_info->save();
					
				}
				
				echo "success";
				exit();
		 }
		 
		 
	}
	
	/**
	* VENDOR FINAL APPROVED - START
	*/
	
	public function vendorFinalApproval(Request $request){
		
		 if($request->ajax()){
			 
			$id  = $request->vendor_id;
			$title = 'Approved - Vendor';
			
					
			//VENDOR DATA
			$vendor_info = Vendor_info::findOrfail($id);
			
			if(empty($vendor_info)){
				die('Vendor info not exsist!');
			}
			
			
			$data = array('vendorId'=>$id);
			
		   
			return view('vendor.vendorFinalApproval',$data);
		}
	}
	
	//APPROVED VENDOR FORM SUBMIT
	public function vendorFinalApprovalSubmit(Request $request){
	
		$validator = Validator::make($request->all(),[
				 'signing_contract' => 'required|max:20000', //a required, max 10000kb, doc or docx file
				
		]);

		//VALIDATE FORMAT
		if( $request->hasFile('signing_contract')){ 
			$file = $request->file('signing_contract');
			$extension 	 = strtolower($file->getClientOriginalExtension());
			$tmp_pathName = $file->getPathName();

			//ONLY ALLOWED PDF FILE
			if($extension!='pdf'){
				$this->signing_contractErr = array("The form of organization must be a file of type: pdf");
			}
		}

		$validator->after(function ($validator) {
			
			
			$this->addValidationErrorMessage($validator,$this->signing_contractErr,'signing_contract');
	
			
			
		});
		if ($validator->fails()){
		
			return redirect('vendor_application_request')
                        ->withErrors($validator)
                        ->withInput();
		}
		
		
		//APPROVED Vendor
		//UPLOAD FORM OF ORGANIZATION
		$signing_contractOrginal = "";
		$signing_contractUploaded = "";
		if( $request->hasFile('signing_contract')){ 
			$file = $request->file('signing_contract');
			$signing_contractOrginal = $file->getClientOriginalName(); 
			$signing_contractUploaded = $this->upload_files('uploads/sigining_contract',$file);
		}
	
		/**
		* ADD VENDOR TO MAGENTO
		*/
		$vendor_id 		= 	$request->vendor_id;
			
		//ADD RECORD TO MAGENTO SELLERCENTER VENDOR OPTION FIRST
		$magentoApiUrl = Config::get('constants.magentoApiUrl');
		$magentoApiUsername = Config::get('constants.magentoApiUsername');
		$magentoApiPassword = Config::get('constants.magentoApiPassword');
		
		
		$mytime 		= Carbon::now();
		$currentTime	= $mytime->toDateTimeString();
		
		//UPDATE VENDOR STATUS
		$vendor_info 						= Vendor_info::findOrfail($vendor_id);
		
		$vendorFullname = $vendor_info->vendor_name;
		
		$vendorOptiontoSave = $vendorFullname;
		$client_v1 = new SoapClient($magentoApiUrl);
		$session_id_v1 = $client_v1->login($magentoApiUsername, $magentoApiPassword);
		$magentoVendorId = $client_v1->call($session_id_v1, 'catalog_category.saveSellerCenterVendor',$vendorOptiontoSave);
		
		//IF VENDOR NAME UPLOADED IN MAGENTO
		if(is_numeric($magentoVendorId)){
			
			$vendor_info->is_vendor_approved	= 1;
			$vendor_info->magento_id			= $magentoVendorId;
			$vendor_info->approved_by			= Auth::user()->id;
			$vendor_info->approved_at			= $currentTime;
			$vendor_info->status	 			= Config::get('constants.vendorApprovedStatus');
			$vendor_info->save();
			
			
			
			//ADD SIGNING CONTRACT INFO
			$vendor_signing_contract = new Vendor_signing_contract();
			
			$vendor_signing_contract->vendor_id	 				= $vendor_id;
			$vendor_signing_contract->orginal_name	 			= $signing_contractOrginal;
			$vendor_signing_contract->uploaded_name	 			= $signing_contractUploaded;
			$vendor_signing_contract->uploaded_by	 			= Auth::user()->id;
			$vendor_signing_contract->created_at	 			= $currentTime;
			
			$vendor_signing_contract->save();
		}
		
        return redirect('vendor_application_request')->withSuccess('Vendor Application Approved Successfully');
		
	}
	
	//ADD VALIDATION ERROR MESSAGE
	public function addValidationErrorMessage($validator,$errorMessageArrr,$fieldName){
		
		if(!empty($errorMessageArrr)){
			
			foreach($errorMessageArrr as $errorMessageRow){
				$validator->errors()->add($fieldName, $errorMessageRow);
			}
		}
	}
	/**
	* Uppload File
	*/
	public function upload_files($destinationPath,$file) {    
          
		$filename = $file->getClientOriginalName();  
		$extension = $file->getClientOriginalExtension();  

		$filename = md5(date('c'))."-".uniqid().".".$extension;
		$upload_success = $file->move($destinationPath, $filename);   

		return $filename;
	}
}
