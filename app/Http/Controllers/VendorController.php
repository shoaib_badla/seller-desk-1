<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Common;
use URL;
use Carbon\Carbon;
use Config;
use Auth;
use Illuminate\Support\Facades\Input;
use Mgallegos\LaravelJqgrid\Facades\GridEncoder;
use DB;
use App\Vendor_margin;
use SoapClient,JsValidator,Validator,Route,Response;
use App\Vendor_info, App\City, App\Country, App\Category, App\Brand, App\Vendor_pickup_address,App\Vendor_product_category, App\Form_of_organization, App\Vendor_brand,App\Vendor_return_address,App\Vendor_bank_info,App\UserHasRoles;

/**
 * Class Vendor_infoController.
 *

 
 
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorController extends Controller
{
	  /**
     * validation rules in a property in 
     * the controller to reuse the rules.
     */
    protected $validationRules=[
              'shop_online_name' => 'required|max:100',
              'company_owner_name' => 'required|max:80',
                'authorize_person_cnic' => 'alpha_dash|max:13|min:13',
                'company_website' => 'required|url|max:255',
                'payment_cycle' => 'required',
                'acceptance_of_return' => 'required',
                'offer_exchange' => 'required',
                'withholding_tax_dposited_by' => 'required',
                'internation_shipping' => 'required',
                'business_model' => 'required',
                'company_individual_registered_address' => 'required',
                'return_address' => 'required',
                'return_province' => 'required',
                'return_city' => 'required',
                'bank_name' => 'max:150|min:3',
                'bank_account_title' => 'max:150|min:3',
                'bank_account_number' => 'max:40|min:8',
                'strn' => 'max:20|min:3',
                'authorize_person_name' => 'max:150|min:3',
   			    'vendor_name' => 'required|max:255',
                'cnic' => 'required|alpha_dash|max:13|min:13',
                'poc_email' => 'required|email|max:255',
                'poc_name' => 'required|max:255',
                //'vendor_manager_assigned' => 'required',
                'form_of_organization' => 'required|numeric',
                'poc_contact' => 'required|alpha_dash|max:10',
                'country' => 'required|numeric',
                'province' => 'required|numeric',
                'city' => 'required|numeric',
                'cell_phone' => 'required|alpha_dash|max:10',
                'ntn1' => 'required|alpha_dash|max:7',
                'ntn2' => 'required|alpha_dash|max:1',
                'office_phone' =>'required|numeric',
                'company_address' => 'required',
              
    ];
	
	protected $errorMessagePickupAddress,$errorMessagepickup_city,$errorMessagepickup_province,$errorMessageproduct_category,$errorMessageproposed_commission,$vendorAleadyExsist = false;
   
	
	public function __construct()
	 {
	   //GET CURRENT ACTION
		 list(, $action) = explode('@', Route::getCurrentRoute()->getActionName());
		
		 if($action!="getListOfMarginByVendorId"){
			 $this->middleware('auth');
		 }
	 }


	 /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
		
		// $validator = JsValidator::make($this->validationRules);
		
		 //CHECK MARGIN STATE
		$checkLoginVendorApplicationState = Common::checkLoginVendorApplicationState();
	
		//IF STATE APPROVED
		if($checkLoginVendorApplicationState==1){
			 return redirect('vendor/dashboard');
		}
	
		 //IF STATE  Waiting
		if(in_array($checkLoginVendorApplicationState,array(2,3,6,7))){
			return redirect('applicationpending');
		}
	
		 //IF STATE  REJECTED
		if(in_array($checkLoginVendorApplicationState,array(4,5))){
			return redirect('applicationcorrection');
		}
		
		
	    $title = 'Vendor Information Detail';
        
		/**
		* GET CITY LIST
		*/
		$countryId = 1;
		$cityList = $this->getCityListByCountryId($countryId);
		
		//GET COUNTRY LIST
		$countryList	=	 Country::pluck('name','id')->all();
		
		//GET L1 Categories
		$categoryList	=	$this->getCatgeoryListByLevel(2);
		
		//GET Brand LIST
		$brandList	=	 Brand::pluck('name','id')->all();
		
		//FORM OF RGANIZATION LIST
		$form_of_organization	=	 Form_of_organization::pluck('label','id')->all();
		
		//GET Payment Cycle List
		$payment_cycle = Common::getPaymentCycle();
		
		//GET Province LIST
		$provinceList	=	 \App\Province::pluck('name','id')->all();
		
				
		//GET Tax Depositer List
		$getWithholdingTaxDespoiterList = \App\WithHoldingTaxDepositer::pluck('label','id')->all();		
		//GET Business Model
		$business_model = \App\BusinessModel::pluck('label','id')->all();
		
		//GET LOGIN VENDOR ID
		$loginVendorId = Common::getLoginVendorId();
		
		if($loginVendorId == 0){
			die("You're not allowed to access this URL.");
		}
				
		//GET VENDOR INFO
		$vendor_info = Vendor_info::findOrfail($loginVendorId);

		//GET VENDOR BRAND BY
		$vendorBrand = DB::table('vendor_brand')->where('vendor_id', $loginVendorId)->pluck('brand_id')->all();
	
		
		$data = array('cityList'=>$cityList,'countryList'=>$countryList,'categoryList'=>$categoryList,'brandList'=>$brandList,'payment_cycle'=>$payment_cycle,'withholding_tax_dposited_by'=>$getWithholdingTaxDespoiterList,'business_model'=>$business_model,'provinceList'=>$provinceList,'form_of_organization'=>$form_of_organization,'vendor_info'=>$vendor_info,'vendorBrand'=>$vendorBrand);
		
        return view('vendor.create',$data);
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		$validator = Validator::make($request->all(),$this->validationRules);

        //VALIDATE DYNAMIC GENEARTED FIELD
		$this->errorMessagePickupAddress 				= $this->validateDynamicGeneratedField($request->pickup_address, 'Pickup Address',$validator);
		$this->errorMessagepickup_city 					= $this->validateDynamicGeneratedField($request->pickup_city, 'Pickup City',$validator);
		$this->errorMessagepickup_province				= $this->validateDynamicGeneratedField($request->pickup_province, 'Pickup Province',$validator);
		
		//NO L3 SELECTED & NOT AGREE FOR ALL OTHER CATGEORY
		if(empty($request->categoryId) && $request->agree_for_all_other_catgeory != 'on'){
			$this->errorMessageproduct_category  = array("L3 Category is Required.");
		}
		if(empty($request->proposed_margin) && $request->agree_for_all_other_catgeory != 'on'){
			$this->errorMessageproposed_commission  = array("L3 Category Proposed Margin/Commission is Required.");
		}

		
		$validator->after(function ($validator) {
			
			
			$this->addValidationErrorMessage($validator,$this->errorMessagePickupAddress,'pickup_address');
			$this->addValidationErrorMessage($validator,$this->errorMessagepickup_city,'pickup_city');
			$this->addValidationErrorMessage($validator,$this->errorMessagepickup_province,'pickup_province');
			$this->addValidationErrorMessage($validator,$this->errorMessageproduct_category,'l1_product_category');
			$this->addValidationErrorMessage($validator,$this->errorMessageproposed_commission,'l1_product_category');
			
			
		});
		
		

		
		if ($validator->fails()){
			return redirect('vendordetail')
                        ->withErrors($validator)
                        ->withInput();
		}
		
		//GET LOGIN VENDOR ID
		$loginVendorId = Common::getLoginVendorId();
		
		
		
		
		/**
		* SAVE VENDOR INFO START
		*/
		
				
		$mytime 		= Carbon::now();
		$currentTime	= $mytime->toDateTimeString();
		
		if($loginVendorId == 0){
			die("You're not allowed to access this URL.");
		}
		
        $vendor_info = Vendor_info::findOrfail($loginVendorId);
        
		$vendor_info->vendor_name	 							= $request->vendor_name;
        $vendor_info->office_phone	 							= $request->office_phone;
        $vendor_info->poc_name	 								= $request->poc_name;
        $vendor_info->poc_contact	 							= $request->poc_contact;
        $vendor_info->ntn_number	 							= $request->ntn1.'-'.$request->ntn2;
        $vendor_info->cnic	 									= $request->cnic;
        $vendor_info->cell_phone	 							= $request->cell_phone;
        $vendor_info->form_of_organization	 					= $request->form_of_organization;
        $vendor_info->poc_email	 								= $request->poc_email;
        $vendor_info->country	 								= $request->country;
        $vendor_info->province	 								= $request->province;
        $vendor_info->city	 									= $request->city;
        $vendor_info->company_address	 						= $request->company_address;
        $vendor_info->shop_online_name	 						= $request->shop_online_name;
        $vendor_info->authorize_person_name 					= $request->authorize_person_name;
        $vendor_info->company_website 							= $request->company_website;
        $vendor_info->acceptance_of_return 						= $request->acceptance_of_return;
        $vendor_info->acceptance_of_return_range 				= $request->acceptance_of_return_range;
        $vendor_info->withholding_tax_dposited_by 				= $request->withholding_tax_dposited_by;
        $vendor_info->business_model 							= $request->business_model;
        $vendor_info->pickup_day								= 'N/A';//$request->pickup_day;
        $vendor_info->company_owner_name 						= $request->company_owner_name;
        $vendor_info->authorize_person_cnic 					= $request->authorize_person_cnic;
        $vendor_info->payment_cycle 							= $request->payment_cycle;
        $vendor_info->offer_exchange 							= $request->offer_exchange;
        $vendor_info->internation_shipping 						= $request->internation_shipping;
        $vendor_info->is_vendor_approved					 	= ($vendor_info->is_vendor_approved==4)?3:2;
		$vendor_info->is_vendor_ops_approved					=	0;
		$vendor_info->is_vendor_finance_approved				=	0;
        $vendor_info->company_individual_registered_address 	= $request->company_individual_registered_address;
        $vendor_info->agree_for_all_other_catgeory 				= ($request->agree_for_all_other_catgeory=='on')?1:0;
        $vendor_info->detailed_added_by							= Auth::user()->id;
		$vendor_info->updated_at								= $currentTime;
  
        $vendor_info->save();
	
		/**
		* SAVE VENDOR INFO END
		*/
		
		
		//ADDING VENDOR INFO (Pickup address,brand,categoyr etc)
		$this->addingVendorAdditionInfo($loginVendorId,$request,$currentTime);
		
	
        return redirect('thankyou');
    }
	


	
	//GET CITY LIST BY COUNTRY ID
	public function getCityListByCountryId($countryId){
		$city = City::where('country_id',$countryId)
				->select('id','name')
                ->get();
		
		$cityList =	array();
		if(!empty($city)){
			foreach($city as $cityRow){
				$cityList[$cityRow->id]	=	$cityRow->name;
			}
		}
		return $cityList;
	}
	
	/**
	* GET CHILD CATGEORY LIST
	*/
	public function getChildCategory(Request $request){
		
		$categoryList =	array();
		
		if($request->category_id){ 
			
			$category_id = $request->category_id;
			
			$category = Category::where('parent_id',$category_id)
			->select('id','name','margin','sf_catid')
			->get();
	
			
			if(!empty($category)){
				foreach($category as $categoryRow){
					$categoryList[$categoryRow->sf_catid][]	=	$categoryRow->name;
					$categoryList[$categoryRow->sf_catid][]	=	$categoryRow->margin;
					$categoryList[$categoryRow->sf_catid][]	=	$categoryRow->id;
				}
			}
				
		
		}
		return json_encode($categoryList);
	}
	
	//GET LEVEL 3 CATEGORY LIST
	public function getCatgeoryListByLevel($level){
		$category = Category::where('level',$level)
				->select('sf_catid','name')
                ->get();
		
		$categoryList =	array();
		if(!empty($category)){
			foreach($category as $categoryRow){
				$categoryList[$categoryRow->sf_catid]	=	$categoryRow->name;
			}
		}
		return $categoryList;
	}

	/**
	* VALIDATE FIELD WHICH IS DYNAMICALLY GENERATED
	*/
	public function validateDynamicGeneratedField($fieldValue,$fieldLabel,$validator = array()){
		
		$errorMessage = array();
		if(!empty($fieldValue)){
			$loop = 1;
			foreach($fieldValue as $fieldValueRow){
				if(trim(empty($fieldValueRow))){
					$errorMessage[] = $loop." - ".$fieldLabel." is required.";
				}
				$loop++;
			}
		}
		  
		return $errorMessage;
	}
	//ADD VALIDATION ERROR MESSAGE
	public function addValidationErrorMessage($validator,$errorMessageArrr,$fieldName){
		
		if(!empty($errorMessageArrr)){
			
			foreach($errorMessageArrr as $errorMessageRow){
				$validator->errors()->add($fieldName, $errorMessageRow);
			}
		}
	}
	
	/**
	* Uppload File
	*/
	public function upload_files($destinationPath,$file) {    
          
		$filename = $file->getClientOriginalName();  
		$extension = $file->getClientOriginalExtension();  

		$filename = md5(date('c'))."-".uniqid().".".$extension;
		$upload_success = $file->move($destinationPath, $filename);   

		return $filename;
	}
	
	/**
	* ADDING VENDOR ADDITIONAL INFO
	*/
	public function addingVendorAdditionInfo($newVendorId,$request,$currentTime){
		
		/**
		* ADDING PICKUP ADDRESSS INFO - START
		*/
		Vendor_pickup_address::where('vendor_id',$newVendorId)->delete();
			
		
		$vendPickupAddress 				= $request->pickup_address;
		$vendPickupCity					= $request->pickup_city;
		$vendPickupProvince		 		= $request->pickup_province;
		
		$venPickLoop = 0;

		
		if(!empty($vendPickupAddress)){
			

				
			foreach($vendPickupAddress as $vendPickupAddressRow){
			
				$vendPickupCityRow				=	isset($vendPickupCity[$venPickLoop])?$vendPickupCity[$venPickLoop]:"";
				$vendPickupProvinceRow			=	isset($vendPickupProvince[$venPickLoop])?$vendPickupProvince[$venPickLoop]:"";
				
				//SAVING RECORD
				$vendor_pickup_address = new Vendor_pickup_address();	
			
				$vendor_pickup_address->vendor_id				=	$newVendorId;
				$vendor_pickup_address->pickup_address			=	$vendPickupAddressRow;
				$vendor_pickup_address->pickup_city				=	$vendPickupCityRow;
				$vendor_pickup_address->pickup_province			=	$vendPickupProvinceRow;
				$vendor_pickup_address->created_at				=	$currentTime;
				$vendor_pickup_address->updated_at				=	$currentTime;
				
				$vendor_pickup_address->save();
				
				$venPickLoop++;
			}
		}
		
		/**
		* ADDING PICKUP ADDRESSS INFO - END
		*/
		
		/**
		* ADDING Product Catgeory INFO - START
		*/
		Vendor_product_category::where('vendor_id',$newVendorId)->delete();
				
	
		$vendproduct_category			= $request->categoryId;
		$proposed_commission			= $request->proposed_margin;
		
		$venCatLoop = 0;

		
		if(!empty($vendproduct_category)){
			
			
				
			foreach($vendproduct_category as $vendproduct_categoryRow){
			
				$proposed_commissionRow				=	isset($proposed_commission[$venCatLoop])?$proposed_commission[$venCatLoop]:"";
				
				//SAVING RECORD
				$vendor_product_category = new Vendor_product_category();	
			
				$vendor_product_category->vendor_id				=	$newVendorId;
				$vendor_product_category->category_id			=	$vendproduct_categoryRow;
				$vendor_product_category->proposed_commission	=	$proposed_commissionRow;
				$vendor_product_category->priority				=	1;
				$vendor_product_category->created_at			=	$currentTime;
				$vendor_product_category->updated_at			=	$currentTime;
				
				$vendor_product_category->save();
				
				$venCatLoop++;
			}
			
			
			//CHECK VENDOR IS AGREE FOR ALL OTHER Category
			/*
			if($request->agree_for_all_other_catgeory=='on'){
				//GET ALL OTHER L3 CATEGORY
				$getL3CatgeoryOnly	=	$this->getL3CatgeoryOnly($request->categoryId);
				
				//CREATE BULK INERT FOR OTHER Category
				$l3OtherCategoryData	=	array();
				if($getL3CatgeoryOnly->count()>0){
					foreach($getL3CatgeoryOnly as $getL3CatgeoryOnlyRow){
						$l3OtherCategoryData[]	=	array('vendor_id'=>$newVendorId,'category_id'=>$getL3CatgeoryOnlyRow->category_id,'proposed_commission'=>$getL3CatgeoryOnlyRow->margin,'priority'=>2,'created_at'=>$currentTime,'updated_at'=>$currentTime);
					}
				}
				
				Vendor_product_category::insert($l3OtherCategoryData);
				
			}*/
		}
			
			
		/**
		* ADDING Product Catgeory INFO - END
		*/
		
		
		/**
		* ADDING Vendor Brand INFO - START
		*/
		
			Vendor_brand::where('vendor_id',$newVendorId)->delete();
	
			$vendBrand			= $request->brand;
			
			if(!empty($vendBrand)){
					
				foreach($vendBrand as $vendBrandRow){
				
					//SAVING RECORD
					$vendor_brand = new Vendor_brand();	
				
					$vendor_brand->vendor_id			=	$newVendorId;
					$vendor_brand->brand_id				=	$vendBrandRow;
					$vendor_brand->created_at			=	$currentTime;
					$vendor_brand->updated_at			=	$currentTime;
					
					$vendor_brand->save();
					
				}
			}
		/**
		* ADDING Vendor Brand INFO - END
		*/
		
		/**
		* ADDING Vendor Return Address INFO - START
		*/
		
			Vendor_return_address::where('vendor_id',$newVendorId)->delete();
	
			
		//	if(!empty($vendBrand)){
			//		
				//foreach($vendBrand as $vendBrandRow){
				
					//SAVING RECORD
					$vendor_return_address = new Vendor_return_address();	
				
					$vendor_return_address->vendor_id			=	$newVendorId;
					$vendor_return_address->return_address		=	$request->return_address;
					$vendor_return_address->return_city			=	$request->return_city;
					$vendor_return_address->return_province		=	$request->return_province;
					$vendor_return_address->created_at			=	$currentTime;
					$vendor_return_address->updated_at			=	$currentTime;
					
					$vendor_return_address->save();
					
			//	}
			//}
		/**
		* ADDING  Vendor Return Address INFO - END
		*/
		
		
				
		/**
		* ADDING Vendor BANK INFO - START
		*/
		
			Vendor_bank_info::where('vendor_id',$newVendorId)->delete();
	
			
		//	if(!empty($vendBrand)){
			//		
				//foreach($vendBrand as $vendBrandRow){
				
					//SAVING RECORD
					$vendor_bank_info = new Vendor_bank_info();	
				
					$vendor_bank_info->vendor_id				=	$newVendorId;
					$vendor_bank_info->bank_name				=	$request->bank_name;
					$vendor_bank_info->bank_account_number		=	$request->bank_account_number;
					$vendor_bank_info->bank_account_title		=	$request->bank_account_title;
					$vendor_bank_info->strn						=	$request->strn;
					$vendor_bank_info->created_at				=	$currentTime;
					$vendor_bank_info->updated_at				=	$currentTime;
					
					$vendor_bank_info->save();
					
			//	}
			//}
		/**
		* ADDING  Vendor BANK INFO - END
		*/
		
		
	}
	
	

	/**
	* GET ALL OTHER CATEGORY LIST FOR SATNDARD MARGIN
	*/
	public function viewAllOtherCategory(Request $request){
		
		$categoryList =	array();
		$getL3CatgeoryWithParent = array();
		
		if($request->category_id){ 
			
			$category_id = $request->category_id;
			$category_id = explode(",",$category_id);
			
			if(!empty($category_id)){
				$getL3CatgeoryWithParent	=	$this->getL3CatgeoryWithParent($category_id);
				
			}
	
			/*
			$category = Category::where('parent_id',$category_id)
			->select('id','name','margin')
			->get();
	
			*/
				
				
				
	
		}
		  return view('vendor.viewCategoryList',compact('getL3CatgeoryWithParent'));
	}
	
	//GET L3 CATEGORY WITH l1 & l2
	public function getL3CatgeoryWithParent($notInCat = ''){

		$categoryList	=	Category::select(
								DB::Raw("IFNULL(tempcat3.name,'N/A') AS L1_Category"),
								DB::Raw("IFNULL(tempcat2.name,'N/A') AS L2_Category"),
								"categories.name AS L3_Category",
								"categories.id AS category_id",
								"categories.margin AS margin"
					)
				->leftJoin('categories AS tempcat2', function ($join) {
							$join->on('categories.parent_id', '=', 'tempcat2.sf_catid')
						 ->where('tempcat2.level', '=', 3);
				})
				->leftJoin('categories AS tempcat3', function ($join) {
							$join->on('tempcat2.parent_id', '=', 'tempcat3.sf_catid')
						 ->where('tempcat3.level', '=', 2);
				})
				->where('categories.level', '=', 4);
		
		if(!empty($notInCat)){
			$categoryList->whereNotIn('categories.id', $notInCat);
		}
		
		$categoryList	=	$categoryList->get();
		return $categoryList;
	}
	
		//GET L3 CATEGORY WITH l1 & l2
	public function getL3CatgeoryOnly($notInCat = ''){
		

		$categoryList	=	Category::select(
								"categories.name AS L3_Category",
								"categories.id AS category_id",
								"categories.margin AS margin"
					)
				->where('categories.level', '=', 4);
		
		if(!empty($notInCat)){
			$categoryList->whereNotIn('categories.id', $notInCat);
		}
		
		$categoryList	=	$categoryList->get();
		return $categoryList;
	}
	
	/**
	* PENDING APPLICATION SCREEN
	*/
	public function applicationPendingScreen(){
		
		 //CHECK MARGIN STATE
		$checkLoginVendorApplicationState = Common::checkLoginVendorApplicationState();
		 //IF STATE  REJECTED
		if(in_array($checkLoginVendorApplicationState,array(4,5))){
			return redirect('applicationcorrection');
		}
		
		//GET LOGIN VENDOR ID
		$loginVendorId = Common::getLoginVendorId();
		
		if($loginVendorId == 0){
			die("You're not allowed to access this URL.");
		}

		//VENDOR DATA
		$vendor_info = Vendor_info::findOrfail($loginVendorId);

		if(empty($vendor_info)){
			die('Vendor info not exsist!');
		}

		//GET LOGIN VENDOR DATA DETAIL
		$getLoginVendorDataDetail	=	$this->getLoginVendorDataDetail($loginVendorId);

		//GET VENDOR OFFERED BRANDS
		$vendor_brand 				= Vendor_brand::where('vendor_id', $loginVendorId)->select('brand_id')->get();

		$vendorBrandId = array();
		if(!empty($vendor_brand)){
			foreach($vendor_brand as $vendor_brandRow){
				$vendorBrandId[] = $vendor_brandRow->brand_id;
			}
		}

		//GET BRAND NAME
		$brandList = Brand::select('name')->whereIn('id',$vendorBrandId)->get();
		$vendorBrandId = array();
		if(!empty($brandList)){
			foreach($brandList as $brandListRow){
				$vendorBrandId[] = $brandListRow->name;
			}
		}

		$vendorBrandId	=	!empty($vendorBrandId)?implode(" , ",$vendorBrandId):"N/A";

		$data = array('brandList'=>$vendorBrandId,'vendor_info'=>$getLoginVendorDataDetail['vendorBasicInfo'],'vendBankInfoArr'=>$getLoginVendorDataDetail['vendBankInfoArr'],'VendorPickupAddressArr'=>$getLoginVendorDataDetail['VendorPickupAddressArr'],'vendorReturnAddressArr'=>$getLoginVendorDataDetail['vendorReturnAddressArr'],'vendorMargin'=>$getLoginVendorDataDetail['vendorMargin']);
		
		return view('vendor.infomrationpending',$data);
	}
	
	public function getLoginVendorDataDetail($loginVendorId){


			
			/**
			* LOAD VENDOR BASIC INFO
			*/
		 
			$vendor_info = Vendor_info::select(
												'vendor_infos.id',
												'vendor_infos.vendor_name',
												'vendor_infos.vendor_email',
												'vendor_infos.cnic',
												'vendor_infos.cell_phone',
												'vendor_infos.office_phone',
												'vendor_infos.poc_name',
												'vendor_infos.poc_email',
												'vendor_infos.poc_contact',
												'vendor_infos.ntn_number',
												'vendor_infos.company_address',
												'vendor_infos.shop_online_name',
												'vendor_infos.company_owner_name',
												'vendor_infos.authorize_person_name',
												'vendor_infos.authorize_person_cnic',
												'vendor_infos.company_website',
												'vendor_infos.payment_cycle',
												'vendor_infos.acceptance_of_return',
												'vendor_infos.acceptance_of_return_range',
												'vendor_infos.offer_exchange',
												'vendor_infos.is_vendor_approved',
												'vendor_infos.is_vendor_ops_approved',
												'vendor_infos.is_vendor_finance_approved',
												'vendor_infos.withholding_tax_dposited_by AS withholding_tax_dposited_by_id',
												DB::Raw('withholding_tax_depositer.label AS withholding_tax_dposited_by'),
												'vendor_infos.internation_shipping',
												'vendor_infos.vendor_manager_assigned',
												'vendor_infos.business_model AS business_model_id',
												DB::Raw('business_model.label AS business_model'),
												'vendor_infos.company_individual_registered_address',
												'vendor_infos.pickup_day',
												DB::Raw('users.name AS assignedManager'),
												'vendor_infos.form_of_organization AS form_of_organization_id',
												DB::Raw('form_of_organization.label AS form_of_organization'),
												'vendor_infos.province AS province_id',
												DB::Raw('province_tbl.name AS province'),
												'vendor_infos.city AS city_id',
												DB::Raw('city.name AS city'),
												'vendor_infos.country AS country_id',
												DB::Raw('country.name AS country'),
												'vendor_infos.agree_for_all_other_catgeory',
												'status'
											);
			
			 $vendor_info->leftJoin('form_of_organization', function ($join) {
				$join->on('vendor_infos.form_of_organization', '=', 'form_of_organization.id');
			});
			 $vendor_info->leftJoin('city', function ($join) {
				$join->on('vendor_infos.city', '=', 'city.id');
			});
			 $vendor_info->leftJoin('province_tbl', function ($join) {
				$join->on('vendor_infos.province', '=', 'province_tbl.id');
			});
			 $vendor_info->leftJoin('country', function ($join) {
				$join->on('vendor_infos.country', '=', 'country.id');
			});
			 $vendor_info->leftJoin('users', function ($join) {
				$join->on('vendor_infos.vendor_manager_assigned', '=', 'users.id');
			});
			 $vendor_info->leftJoin('withholding_tax_depositer', function ($join) {
				$join->on('vendor_infos.withholding_tax_dposited_by', '=', 'withholding_tax_depositer.id');
			});
			$vendor_info->leftJoin('business_model', function ($join) {
				$join->on('vendor_infos.business_model', '=', 'business_model.id');
			});
			
			$vendor_info->where('vendor_infos.id','=',$loginVendorId);
			
			$vendorBasicInfo 	=	$vendor_info->first();

			/**
			* GET VENDOR FINANCIAL INFO
			*/
			$vendorBankInfo = Vendor_bank_info::where('vendor_id',$loginVendorId)->get();
			
			$vendBankInfoArr	=	array();
			if($vendorBankInfo->count()>0){
				
				$loop = 0;
				foreach($vendorBankInfo as $vendorBankInfoRow){
					
					$vendBankInfoArr[$loop]['bank_name'] 			= !empty(trim($vendorBankInfoRow->bank_name))?$vendorBankInfoRow->bank_name:'';
					$vendBankInfoArr[$loop]['bank_account_number'] 	= !empty(trim($vendorBankInfoRow->bank_account_number))?$vendorBankInfoRow->bank_account_number:'';
					$vendBankInfoArr[$loop]['bank_account_title'] 	= !empty(trim($vendorBankInfoRow->bank_account_title))?$vendorBankInfoRow->bank_account_title:'';
					$vendBankInfoArr[$loop]['strn'] 				= !empty(trim($vendorBankInfoRow->strn))?$vendorBankInfoRow->strn:'';
					
					$loop++;
				}
			}
			
			
			/**
			* GET VENDOR PICKUP ADDRESS
			*/
			$VendorPickupAddressInfo = Vendor_pickup_address::where('vendor_id',$loginVendorId);
			$VendorPickupAddressInfo->select('vendor_pickup_address.pickup_address','vendor_pickup_address.pickup_city AS pickup_city_id','vendor_pickup_address.pickup_province AS pickup_province_id',DB::Raw('province_tbl.name AS province'),
												DB::Raw('city.name AS city'));
			 $VendorPickupAddressInfo->leftJoin('city', function ($join) {
				$join->on('vendor_pickup_address.pickup_city', '=', 'city.id');
			});
			 $VendorPickupAddressInfo->leftJoin('province_tbl', function ($join) {
				$join->on('vendor_pickup_address.pickup_province', '=', 'province_tbl.id');
			});
			$VendorPickupAddressInfo	=	$VendorPickupAddressInfo->get();
			
			$VendorPickupAddressArr	=	array();
			if($VendorPickupAddressInfo->count()>0){
				
				$loop = 0;
				foreach($VendorPickupAddressInfo as $VendorPickupAddressInfoRow){
					
					$VendorPickupAddressArr[$loop]['pickup_address'] 				= !empty(trim($VendorPickupAddressInfoRow->pickup_address))?$VendorPickupAddressInfoRow->pickup_address:'';
					$VendorPickupAddressArr[$loop]['pickup_city'] 				= !empty(trim($VendorPickupAddressInfoRow->city))?$VendorPickupAddressInfoRow->city:'';
					$VendorPickupAddressArr[$loop]['pickup_province'] 				= !empty(trim($VendorPickupAddressInfoRow->province))?$VendorPickupAddressInfoRow->province:'';
					$VendorPickupAddressArr[$loop]['pickup_city_id'] 				= !empty(trim($VendorPickupAddressInfoRow->pickup_city_id))?$VendorPickupAddressInfoRow->pickup_city_id:'';
					$VendorPickupAddressArr[$loop]['pickup_province_id'] 				= !empty(trim($VendorPickupAddressInfoRow->pickup_province_id))?$VendorPickupAddressInfoRow->pickup_province_id:'';
					
					$loop++;
				}
			}
			
			/**
			* GET VENDOR RETURN ADDRESS
			*/
			$vendorReturnAddressInfo = Vendor_return_address::where('vendor_id',$loginVendorId);
			$vendorReturnAddressInfo->select('vendor_return_address.return_address','vendor_return_address.return_city AS return_city_id','vendor_return_address.return_province AS return_province_id',DB::Raw('province_tbl.name AS province'),
												DB::Raw('city.name AS city'));
			 $vendorReturnAddressInfo->leftJoin('city', function ($join) {
				$join->on('vendor_return_address.return_city', '=', 'city.id');
			});
			 $vendorReturnAddressInfo->leftJoin('province_tbl', function ($join) {
				$join->on('vendor_return_address.return_province', '=', 'province_tbl.id');
			});
			$vendorReturnAddressInfo	=	$vendorReturnAddressInfo->get();
			
			$vendorReturnAddressArr	=	array();
			if($vendorReturnAddressInfo->count()>0){
				
				$loop = 0;
				foreach($vendorReturnAddressInfo as $vendorReturnAddressInfoRow){
					
					$vendorReturnAddressArr[$loop]['return_address'] 				= !empty(trim($vendorReturnAddressInfoRow->return_address))?$vendorReturnAddressInfoRow->return_address:'';
					$vendorReturnAddressArr[$loop]['return_city'] 				= !empty(trim($vendorReturnAddressInfoRow->city))?$vendorReturnAddressInfoRow->city:'';
					$vendorReturnAddressArr[$loop]['return_province'] 				= !empty(trim($vendorReturnAddressInfoRow->province))?$vendorReturnAddressInfoRow->province:'';
					$vendorReturnAddressArr[$loop]['return_city_id'] 				= !empty(trim($vendorReturnAddressInfoRow->return_city_id))?$vendorReturnAddressInfoRow->return_city_id:'';
					$vendorReturnAddressArr[$loop]['return_province_id'] 				= !empty(trim($vendorReturnAddressInfoRow->return_province_id))?$vendorReturnAddressInfoRow->return_province_id:'';
					
					$loop++;
				}
			}
			
			/**
			* GET VENDOR SELETCED Catgeory Margin
			*/
			
			$vendorMargin = $this->getVendorSelectedMargin($loginVendorId);
			
	
		 return array('vendorBasicInfo'=>$vendorBasicInfo,'vendBankInfoArr'=>$vendBankInfoArr,'VendorPickupAddressArr'=>$VendorPickupAddressArr,'vendorReturnAddressArr'=>$vendorReturnAddressArr,'vendorMargin'=>$vendorMargin);
	}
	//GET VENDOR SELECTED MARGIN
	public function getVendorSelectedMargin($vendor_id){
		
		$vendorMargin =	Vendor_product_category::select(
							DB::Raw("IFNULL(tempcat3.name,'N/A') AS L1_Category"),
							DB::Raw("IFNULL(tempcat2.name,'N/A') AS L2_Category"),
							"cat.name AS L3_Category",
							"cat.id AS category_id",
							"cat.margin AS margin",
							"vendor_product_category.proposed_commission AS proposed_commission"
				)
			->join('categories AS cat', 'vendor_product_category.category_id', '=', 'cat.id')
			->leftJoin('categories AS tempcat2', function ($join) {
						$join->on('cat.parent_id', '=', 'tempcat2.sf_catid')
					 ->where('tempcat2.level', '=', 3);
			})
			->leftJoin('categories AS tempcat3', function ($join) {
						$join->on('tempcat2.parent_id', '=', 'tempcat3.sf_catid')
					 ->where('tempcat3.level', '=', 2);
			})
			->where('vendor_product_category.priority', '=', DB::Raw('1'))
			->where('vendor_product_category.vendor_id', '=', $vendor_id)
			->where('cat.level', '=', '4')
			->get();
			
		return $vendorMargin;
	}
	
	/**
	* THANK YOU PAGE
	*/
	
	public function thankyou(){
		 return view('vendor.thankyou');
	}
	
	/**
	* SEND FOR CORRECTION VENDOR APLLICATION
	*/
	public function sendForCorrection(Request $request){
		
		 if($request->ajax()){
			
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;
				$correction_comment  = trim(urldecode($request->correction_comment));

				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus') && !empty($correction_comment)){
					
					//ADD COMMENT HISTORY
					$this-> addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.correctionRequired'),$correction_comment);
					
					
					$vendor_info->is_vendor_approved	 					= ($vendor_info->is_vendor_approved==2)?4:5; //SET 4 FOR FIRST TIME CORRECTION 5 FOR Loop
					$vendor_info->is_vendor_ops_approved		=	0;
					$vendor_info->is_vendor_finance_approved	=	0;
					$vendor_info->save();
					
					//GET VENDOR EMAIL ADDRESS
					$vendorEmail	=	$vendor_info->vendor_email;
					
					
					$this->sendVendorApplicationCorrectionEmail($vendorEmail,$vendor_info->vendor_name,$correction_comment);
				}
				
				
				
				
				echo "success";
				exit();
		 }
	}
	
	
	/**
	* ADDING APPLICATION COMMENT HISTORY
	*/
	public function addVendorApplicationCommentHistroy($vendor_id,$status,$comment){
		
		//GET LOGIN USER ROLES
		$userRoles	=	DB::table('user_has_roles')->where('user_id', Auth::user()->id)->pluck('role_id')->all();
		$userRoles	=	implode(",",$userRoles);
		
		$commentHistory 	=	new \App\Vendor_application_comment_history();
		
		$commentHistory->vendor_id	=	$vendor_id;
		$commentHistory->status		=	$status;
		$commentHistory->comment	=	$comment;
		$commentHistory->user_id	=	Auth::user()->id;
		$commentHistory->user_role	=	$userRoles;
		
		$commentHistory->save();
	}
	
	/**
	* SEND VENDOR APPLICTION CORRECTION EMAIL
	*/
	public function sendVendorApplicationCorrectionEmail($to,$userName,$correction_comment){
		//SEND USER CREATION EMAIL
		$subject =	"Yayvo Vendor Application Correction Required";
		$cc = Config::get('constants.cc_email_address');
		
		$body	= <<<HTML
				Welcome $userName,
					Correction is required for your application of vendor registration !
					Here is Commnets for Yayvo : 
					$correction_comment
					
				Thanks
HTML;
		Common::sendEmail($to,$subject,$body,$cc);
	}
 
 
 /**
 * CORRECTION VENDOR APPLICATION SCREEN
 */
 public function applicationCorrectionScreen(){
	
		 //CHECK MARGIN STATE
		$checkLoginVendorApplicationState = Common::checkLoginVendorApplicationState();
	
		//IF STATE APPROVED
		if($checkLoginVendorApplicationState==1){
			 return redirect('vendor/dashboard');
		}

		 //IF STATE  Waiting
		if(in_array($checkLoginVendorApplicationState,array(2,3,6,7))){
			return redirect('applicationpending');
		}
		
		 
	    $title = 'Vendor Information Correction Required';
        
		/**
		* GET CITY LIST
		*/
		$countryId = 1;
		$cityList = $this->getCityListByCountryId($countryId);
		
		//GET COUNTRY LIST
		$countryList	=	 Country::pluck('name','id')->all();
		
		//GET L1 Categories
		$categoryList	=	$this->getCatgeoryListByLevel(2);
		
		//GET Brand LIST
		$brandList	=	 Brand::pluck('name','id')->all();
		
		//FORM OF RGANIZATION LIST
		$form_of_organization	=	 Form_of_organization::pluck('label','id')->all();
		
		//GET Payment Cycle List
		$payment_cycle = Common::getPaymentCycle();
		
		//GET Province LIST
		$provinceList	=	 \App\Province::pluck('name','id')->all();
		
				
		//GET Tax Depositer List
		$getWithholdingTaxDespoiterList = \App\WithHoldingTaxDepositer::pluck('label','id')->all();		
		//GET Business Model
		$business_model = \App\BusinessModel::pluck('label','id')->all();
		
		//GET LOGIN VENDOR ID
		$loginVendorId = Common::getLoginVendorId();
		
		if($loginVendorId == 0){
			die("You're not allowed to access this URL.");
		}
		//GET VENDOR INFO
		$vendor_info = Vendor_info::findOrfail($loginVendorId);

		//GET VENDOR BRAND BY
		$vendorBrand = DB::table('vendor_brand')->where('vendor_id', $loginVendorId)->pluck('brand_id')->all();
	
		
		//$data = array(,'vendor_info'=>$vendor_info,'vendorBrand'=>$vendorBrand);
		//GET LOGIN VENDOR DATA DETAIL
		$getLoginVendorDataDetail	=	$this->getLoginVendorDataDetail($loginVendorId);
		
		//GET VENDOR LAST CORRECTION COMMENT
		$commentHistory	=	$this->getLoginVendorLastCorrectionComment($loginVendorId);

		$data = array('cityList'=>$cityList,'countryList'=>$countryList,'categoryList'=>$categoryList,'brandList'=>$brandList,'payment_cycle'=>$payment_cycle,'withholding_tax_dposited_by'=>$getWithholdingTaxDespoiterList,'business_model'=>$business_model,'provinceList'=>$provinceList,'form_of_organization'=>$form_of_organization,'vendor_info'=>$getLoginVendorDataDetail['vendorBasicInfo'],'vendBankInfoArr'=>$getLoginVendorDataDetail['vendBankInfoArr'],'VendorPickupAddressArr'=>$getLoginVendorDataDetail['VendorPickupAddressArr'],'vendorReturnAddressArr'=>$getLoginVendorDataDetail['vendorReturnAddressArr'],'vendorMargin'=>$getLoginVendorDataDetail['vendorMargin'],'vendorBrand'=>$vendorBrand,'commentHistory'=>$commentHistory);
		
		return view('vendor.update',$data);
 }
 
	//GET VENDOR LAST CORRECTION COMMENT
	public function getLoginVendorLastCorrectionComment($loginVendorId){
	 
		 $commentHistory 	=	\App\Vendor_application_comment_history::select('comment')->where('vendor_id','=',$loginVendorId)->where('status','=',Config::get('constants.correctionRequired'))->latest()->first();
		 
		 $commentHistory	=	isset($commentHistory->comment)?$commentHistory->comment:'';
		 return $commentHistory;
	}
	
	/**
	* SEND APPLICATION FOR REVIEW TO OTHER DEPART
	*/	
	public function sendApplicationForReviewToOtherDepart(Request $request){
		
		 if($request->ajax()){
			
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;

				if($vendor_id == 0){
					die("You're not allowed to access this URL.");
				}
				$vendor_info = Vendor_info::findOrfail($vendor_id);
				
				//CHECK ORDER IS NOT APPROVED
				if($vendor_info->status	!= Config::get('constants.vendorApprovedStatus')){
					
					//ADD COMMENT HISTORY
					$this-> addVendorApplicationCommentHistroy($vendor_id,Config::get('constants.applicationSendOtherDeaprt'),'Send For Review to Other Depart');
					
					
					$vendor_info->is_vendor_approved	 					= 6; //Application Send To OTher Depart
					$vendor_info->is_vendor_finance_approved				= 3; //PENDING
					$vendor_info->is_vendor_ops_approved	 				= 3; //PENDING
					$vendor_info->save();

				}
				
				echo "success";
				exit();
		 }
	}
	
	/**
	* CHECK MARGIN DOWNLOADED
	*/
	public function checkMarginDownloaded(Request $request){
		
		if($request->ajax()){
			
				$mytime 		= Carbon::now();
				$currentTime	= $mytime->toDateTimeString();
		
				$vendor_id  = $request->vendor_id;

				if($vendor_id == 0){
					die("You're not allowed to access this URL.");
				}
				
				if($this->isVendorMarginDownloadStatus($vendor_id)){
					echo "success";
				}else{
					echo "failed";
				}
				
				
				exit();
		 }
	}
	
	//GET MARGIN DOWNLOAD STATUS
	public function isVendorMarginDownloadStatus($vendor_id){
		
		$vendor_info = Vendor_info::select('is_margin_downloaded')->where('id','=',$vendor_id)->first();
				
		return $vendor_info->is_margin_downloaded;
	}
	
	/**
	* DOWNLOAD AGREED MARGIN
	*/
	
	public function downloadAgreedMargin(Request $request){
		
	
		if ($request->isMethod('post')) {
			
			$vendor_id 						= 	$request->vendor_id;	
			
			$filename		=	md5(date('c')).'.pdf';
			$pathToFile		= public_path('pdf').'/'.$filename; //SAVE FILE PATH
			$inputUrl		= url('getlistofmarginmyvendor?vendor_id='.$vendor_id);
			
			//COMMAND RUN FOR WINDOW
			if(Common::isWindowEnvironment()){
				
				$wkhtmlLibPath 	= base_path('vendor/wkhtmltopdf/bin/wkhtmltopdf');
				$command	=	"$wkhtmlLibPath $inputUrl $pathToFile 2>&1";
				exec($command);
				
			}else{
				//COMMAND FOR LINUX
				$wkhtmlLibPath 	= 'wkhtmltopdf.sh';
				$command	=	"$wkhtmlLibPath $inputUrl $pathToFile 2>&1";
				system($command);
			}

			
		
			$response = Response::download($pathToFile)->deleteFileAfterSend(true);
			ob_end_clean();

			return $response;	 
			 
			//return response()->download($pathToFile,$filename,$headers)->deleteFileAfterSend(true);
			
			 
			   
		}
	}
	
	/**
	* GET LIST OF VENDOR MARGIN
	*/
	public function getListOfMarginByVendorId(Request $request){
		
		$htmlFile = "No Record Found";
		if ($request->isMethod('get')) {
			
			$vendor_id 						= 	$request->vendor_id;	
			
			$vendorMarginList	=	array();
			$vendCategoryIdArr	=	array();
			
			//LOAD VENDOR INFO
			$vendor_info 					= Vendor_info::findOrfail($vendor_id);
			$agree_for_all_other_catgeory 	= $vendor_info->agree_for_all_other_catgeory;
			
			/*
			* GET SELECTED L3 VENDOR IF ANY CATEGORY
			*/
			
			$vendorMargin = $this->getVendorSelectedMargin($vendor_id);
			
			$loop	=	0;
			if($vendorMargin->count()>0){
				
				foreach($vendorMargin as $vendorMarginRow){
					
					$vendorMarginList[$loop]['L1_Category']			=	$vendorMarginRow->L1_Category;
					$vendorMarginList[$loop]['L2_Category']			=	$vendorMarginRow->L2_Category;
					$vendorMarginList[$loop]['L3_Category']			=	$vendorMarginRow->L3_Category;
					$vendorMarginList[$loop]['category_id']			=	$vendorMarginRow->category_id;
					$vendorMarginList[$loop]['margin']				=	$vendorMarginRow->margin;
					$vendorMarginList[$loop]['proposed_commission']	=	$vendorMarginRow->proposed_commission;
					
					
					$vendCategoryIdArr[]		=	$vendorMarginRow->category_id;
					$loop++;
				}
			}
			
			
			/**
			* CHECK VENDOR IS AGREED FOR ALL OTHER CATGEORY
			*/
			if($agree_for_all_other_catgeory){
				//GET ALL OTHER CATGEORY WITH STANDARD MARGIN
				$getL3CatgeoryWithParent	=	$this->getL3CatgeoryWithParent($vendCategoryIdArr);
				
				if($getL3CatgeoryWithParent->count()>0){
				
					foreach($getL3CatgeoryWithParent as $vendorMarginRow){
						
						$loop++;
						
						$vendorMarginList[$loop]['L1_Category']			=	$vendorMarginRow->L1_Category;
						$vendorMarginList[$loop]['L2_Category']			=	$vendorMarginRow->L2_Category;
						$vendorMarginList[$loop]['L3_Category']			=	$vendorMarginRow->L3_Category;
						$vendorMarginList[$loop]['category_id']			=	$vendorMarginRow->category_id;
						$vendorMarginList[$loop]['margin']				=	$vendorMarginRow->margin;
						$vendorMarginList[$loop]['proposed_commission']	=	$vendorMarginRow->margin;
						
						
					}
				}
			}
			
			$htmlFile = view('vendor.downloadvendormargin',compact('vendorMarginList'));
			
			//SET MARGIN DOWNLOADED FLAG TRUE
			$vendor_info->is_margin_downloaded = 1;
			$vendor_info->save();
		}
		
		return $htmlFile;
	}
	
	/**
	* SHOW DASHBOARD
	*/
	public function dashboard(){
		return view('vendor.dashboard');
	}
	
}
