<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attributes_mapping;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Attributes_mappingController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:14:58pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attributes_mappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - attributes_mapping';
        $attributes_mappings = Attributes_mapping::paginate(6);
        return view('attributes_mapping.index',compact('attributes_mappings','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - attributes_mapping';
        
        return view('attributes_mapping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes_mapping = new Attributes_mapping();

        
        $attributes_mapping->attribute_set_id = $request->attribute_set_id;

        
        $attributes_mapping->attribute_id = $request->attribute_id;

        
        
        $attributes_mapping->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new attributes_mapping has been created !!']);

        return redirect('attributes_mapping');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - attributes_mapping';

        if($request->ajax())
        {
            return URL::to('attributes_mapping/'.$id);
        }

        $attributes_mapping = Attributes_mapping::findOrfail($id);
        return view('attributes_mapping.show',compact('title','attributes_mapping'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - attributes_mapping';
        if($request->ajax())
        {
            return URL::to('attributes_mapping/'. $id . '/edit');
        }

        
        $attributes_mapping = Attributes_mapping::findOrfail($id);
        return view('attributes_mapping.edit',compact('title','attributes_mapping'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $attributes_mapping = Attributes_mapping::findOrfail($id);
    	
        $attributes_mapping->attribute_set_id = $request->attribute_set_id;
        
        $attributes_mapping->attribute_id = $request->attribute_id;
        
        
        $attributes_mapping->save();

        return redirect('attributes_mapping');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/attributes_mapping/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$attributes_mapping = Attributes_mapping::findOrfail($id);
     	$attributes_mapping->delete();
        return URL::to('attributes_mapping');
    }
}
