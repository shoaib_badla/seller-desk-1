<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attributes_datum;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Attributes_datumController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:17:13pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attributes_datumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - attributes_datum';
        $attributes_datas = Attributes_datum::paginate(6);
        return view('attributes_datum.index',compact('attributes_datas','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - attributes_datum';
        
        return view('attributes_datum.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes_datum = new Attributes_datum();

        
        $attributes_datum->attribute_id = $request->attribute_id;

        
        $attributes_datum->value = $request->value;

        
        $attributes_datum->approved_by = $request->approved_by;

        
        $attributes_datum->rejected_by = $request->rejected_by;

        
        $attributes_datum->approved_at = $request->approved_at;

        
        $attributes_datum->rejected_at = $request->rejected_at;

        
        $attributes_datum->reason = $request->reason;

        
        $attributes_datum->created_at = $request->created_at;

        
        $attributes_datum->updated_at = $request->updated_at;

        
        
        $attributes_datum->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new attributes_datum has been created !!']);

        return redirect('attributes_datum');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - attributes_datum';

        if($request->ajax())
        {
            return URL::to('attributes_datum/'.$id);
        }

        $attributes_datum = Attributes_datum::findOrfail($id);
        return view('attributes_datum.show',compact('title','attributes_datum'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - attributes_datum';
        if($request->ajax())
        {
            return URL::to('attributes_datum/'. $id . '/edit');
        }

        
        $attributes_datum = Attributes_datum::findOrfail($id);
        return view('attributes_datum.edit',compact('title','attributes_datum'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $attributes_datum = Attributes_datum::findOrfail($id);
    	
        $attributes_datum->attribute_id = $request->attribute_id;
        
        $attributes_datum->value = $request->value;
        
        $attributes_datum->approved_by = $request->approved_by;
        
        $attributes_datum->rejected_by = $request->rejected_by;
        
        $attributes_datum->approved_at = $request->approved_at;
        
        $attributes_datum->rejected_at = $request->rejected_at;
        
        $attributes_datum->reason = $request->reason;
        
        $attributes_datum->created_at = $request->created_at;
        
        $attributes_datum->updated_at = $request->updated_at;
        
        
        $attributes_datum->save();

        return redirect('attributes_datum');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/attributes_datum/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$attributes_datum = Attributes_datum::findOrfail($id);
     	$attributes_datum->delete();
        return URL::to('attributes_datum');
    }
}
