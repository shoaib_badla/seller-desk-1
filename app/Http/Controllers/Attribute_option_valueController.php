<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attribute_option_value;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Attribute_option_valueController.
 *
 * @author  The scaffold-interface created at 2017-02-08 11:48:13am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attribute_option_valueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - attribute_option_value';
        $attribute_option_values = Attribute_option_value::paginate(6);
        return view('attribute_option_value.index',compact('attribute_option_values','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - attribute_option_value';
        
        return view('attribute_option_value.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute_option_value = new Attribute_option_value();

        
        $attribute_option_value->attribute_id = $request->attribute_id;

        
        $attribute_option_value->option_id_yayvo = $request->option_id_yayvo;

        
        $attribute_option_value->option_value = $request->option_value;

        
        
        $attribute_option_value->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new attribute_option_value has been created !!']);

        return redirect('attribute_option_value');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - attribute_option_value';

        if($request->ajax())
        {
            return URL::to('attribute_option_value/'.$id);
        }

        $attribute_option_value = Attribute_option_value::findOrfail($id);
        return view('attribute_option_value.show',compact('title','attribute_option_value'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - attribute_option_value';
        if($request->ajax())
        {
            return URL::to('attribute_option_value/'. $id . '/edit');
        }

        
        $attribute_option_value = Attribute_option_value::findOrfail($id);
        return view('attribute_option_value.edit',compact('title','attribute_option_value'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $attribute_option_value = Attribute_option_value::findOrfail($id);
    	
        $attribute_option_value->attribute_id = $request->attribute_id;
        
        $attribute_option_value->option_id_yayvo = $request->option_id_yayvo;
        
        $attribute_option_value->option_value = $request->option_value;
        
        
        $attribute_option_value->save();

        return redirect('attribute_option_value');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/attribute_option_value/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$attribute_option_value = Attribute_option_value::findOrfail($id);
     	$attribute_option_value->delete();
        return URL::to('attribute_option_value');
    }
}
