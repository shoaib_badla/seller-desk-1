<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attribute_set;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Attribute_setController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:09:57pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attribute_setController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - attribute_set';
        $attribute_sets = Attribute_set::paginate(6);
        return view('attribute_set.index',compact('attribute_sets','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - attribute_set';
        
        return view('attribute_set.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute_set = new Attribute_set();

        
        $attribute_set->name = $request->name;

        
        $attribute_set->created_at = $request->created_at;

        
        $attribute_set->updated_at = $request->updated_at;

        
        $attribute_set->user_id = $request->user_id;

        
        
        $attribute_set->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new attribute_set has been created !!']);

        return redirect('attribute_set');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - attribute_set';

        if($request->ajax())
        {
            return URL::to('attribute_set/'.$id);
        }

        $attribute_set = Attribute_set::findOrfail($id);
        return view('attribute_set.show',compact('title','attribute_set'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - attribute_set';
        if($request->ajax())
        {
            return URL::to('attribute_set/'. $id . '/edit');
        }

        
        $attribute_set = Attribute_set::findOrfail($id);
        return view('attribute_set.edit',compact('title','attribute_set'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $attribute_set = Attribute_set::findOrfail($id);
    	
        $attribute_set->name = $request->name;
        
        $attribute_set->created_at = $request->created_at;
        
        $attribute_set->updated_at = $request->updated_at;
        
        $attribute_set->user_id = $request->user_id;
        
        
        $attribute_set->save();

        return redirect('attribute_set');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/attribute_set/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$attribute_set = Attribute_set::findOrfail($id);
     	$attribute_set->delete();
        return URL::to('attribute_set');
    }
}
