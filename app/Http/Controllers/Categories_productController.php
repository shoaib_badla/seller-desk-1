<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories_product;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class Categories_productController.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:49:24pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Categories_productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - categories_product';
        $categories_products = Categories_product::paginate(6);
        return view('categories_product.index',compact('categories_products','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - categories_product';
        
        return view('categories_product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories_product = new Categories_product();

        
        $categories_product->cat_id = $request->cat_id;

        
        $categories_product->product_id = $request->product_id;

        
        
        $categories_product->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new categories_product has been created !!']);

        return redirect('categories_product');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - categories_product';

        if($request->ajax())
        {
            return URL::to('categories_product/'.$id);
        }

        $categories_product = Categories_product::findOrfail($id);
        return view('categories_product.show',compact('title','categories_product'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - categories_product';
        if($request->ajax())
        {
            return URL::to('categories_product/'. $id . '/edit');
        }

        
        $categories_product = Categories_product::findOrfail($id);
        return view('categories_product.edit',compact('title','categories_product'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $categories_product = Categories_product::findOrfail($id);
    	
        $categories_product->cat_id = $request->cat_id;
        
        $categories_product->product_id = $request->product_id;
        
        
        $categories_product->save();

        return redirect('categories_product');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/categories_product/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$categories_product = Categories_product::findOrfail($id);
     	$categories_product->delete();
        return URL::to('categories_product');
    }
}
