<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Helper\Common;
use App\Permission\Models\Role;
use App\Permission\Models\Permission;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {

        if (Auth::guest()) {
            return redirect('/login');
        }
//
//        if (! $request->user()->hasRole($role)) {
//            return redirect('/login');
//            //abort(403);
//        }


        if (Common::userwisepermission($request->user()->id,$permission) != 1) {
            return redirect('/login');
//            abort(403);
        }

        return $next($request);
    }
}
