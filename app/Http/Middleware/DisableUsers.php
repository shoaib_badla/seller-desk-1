<?php

namespace App\Http\Middleware;
use Illuminate\Support\MessageBag;
use Closure,Auth,Session;

class DisableUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        // You might want to create a method on your model to
        // prevent direct access to the `logout` property. Something
        // like `markedForLogout()` maybe.
		if (Auth::check()){
		   if (!Auth::User()->is_active) {
					Auth::logout();
					//Session::set('userDisabled', 'Your account is deactivated!');
					//return redirect('/');
					 return redirect('login')->with('info',  'Your account is deactivated!');
			}
		}

		
        return $next($request);
    }
}
