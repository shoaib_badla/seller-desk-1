<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Categories_attribute.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:48:50pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Categories_attribute extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'categories_attributes';

	
}
