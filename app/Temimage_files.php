<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:47:53pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Temimage_files extends Model
{

    protected $table = 'temimage_files';

}