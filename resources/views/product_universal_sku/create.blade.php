@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create product_universal_sku
    </h1>
    <form method = 'get' action = '{!!url("product_universal_sku")!!}'>
        <button class = 'btn blue'>product_universal_sku Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("product_universal_sku")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="universal_sku" name = "universal_sku" type="text" class="validate">
            <label for="universal_sku">universal_sku</label>
        </div>
        <div class="input-field col s6">
            <input id="product_count" name = "product_count" type="text" class="validate">
            <label for="product_count">product_count</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate">
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate">
            <label for="updated_at">updated_at</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection