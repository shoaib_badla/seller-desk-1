@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show product_universal_sku
    </h1>
    <form method = 'get' action = '{!!url("product_universal_sku")!!}'>
        <button class = 'btn blue'>product_universal_sku Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>universal_sku : </i></b>
                </td>
                <td>{!!$product_universal_sku->universal_sku!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>product_count : </i></b>
                </td>
                <td>{!!$product_universal_sku->product_count!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$product_universal_sku->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$product_universal_sku->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection