@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        product_universal_sku Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("product_universal_sku")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New product_universal_sku</button>
        </form>
    </div>
    <table>
        <thead>
            <th>universal_sku</th>
            <th>product_count</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($product_universal_skus as $product_universal_sku) 
            <tr>
                <td>{!!$product_universal_sku->universal_sku!!}</td>
                <td>{!!$product_universal_sku->product_count!!}</td>
                <td>{!!$product_universal_sku->created_at!!}</td>
                <td>{!!$product_universal_sku->updated_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/product_universal_sku/{!!$product_universal_sku->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/product_universal_sku/{!!$product_universal_sku->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/product_universal_sku/{!!$product_universal_sku->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $product_universal_skus->render() !!}

</div>
@endsection