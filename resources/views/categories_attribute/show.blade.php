@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show categories_attribute
    </h1>
    <form method = 'get' action = '{!!url("categories_attribute")!!}'>
        <button class = 'btn blue'>categories_attribute Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>cat_id : </i></b>
                </td>
                <td>{!!$categories_attribute->cat_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>attribitute_set_id : </i></b>
                </td>
                <td>{!!$categories_attribute->attribitute_set_id!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection