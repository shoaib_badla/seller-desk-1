@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create categories_attribute
    </h1>
    <form method = 'get' action = '{!!url("categories_attribute")!!}'>
        <button class = 'btn blue'>categories_attribute Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("categories_attribute")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="cat_id" name = "cat_id" type="text" class="validate">
            <label for="cat_id">cat_id</label>
        </div>
        <div class="input-field col s6">
            <input id="attribitute_set_id" name = "attribitute_set_id" type="text" class="validate">
            <label for="attribitute_set_id">attribitute_set_id</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection