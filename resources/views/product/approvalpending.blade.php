@extends('layouts.app')
@section('title','Approval Pending')
@section('content')

    <div class = 'container'>

        <div class="row">



            <div class="col-md-11">
                <h1 style="display: inline-block; color: #a81d21;">
                    Pending for Approval
                </h1>

            </div>
            <div class="col-md-11">
                <div class="flash-message">

                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))

                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div> <!-- end .flash-message -->
            </div>
            <div class="col-md-11">
                <?php
                echo GridRender::setGridId("ProductListView")
                    ->enableFilterToolbar()
                    ->setGridOption('url',URL::to('/approval-pending-data'))
                    ->setGridOption('rowNum', 15)
                    ->setGridOption('sortname','id')
                    ->setGridOption('viewrecords',false)
                    ->setGridOption('caption','Product Listing ')
                    ->setGridOption('width', 1100)
                    ->setGridOption('height', 350)
                    ->setGridOption('rowList', [15, 25, 50])
                    ->setGridOption('pager', "jqGridPager")
                    ->setGridOption('shrinkToFit', false)
                    ->setFilterToolbarOptions(array('autosearch'=>true))
                    ->setGridOption('postData', array('_token' => Session::token()))
                    //->setGridEvent('gridComplete', 'gridCompleteEvent') //gridCompleteEvent must be previously declared as a javascript function.
                    ->addColumn(array('index'=>'Id', 'index'=>'id','width'=>20,'search'=>false))
                    ->addColumn(array('label'=>'Product Name','index'=>'name', 'width'=>100))
                    ->addColumn(array('label'=>'Sku','index'=>'sku', 'width'=>100))
                    ->addColumn(array('label'=>'Vendor Name','index'=>'vendor_name', 'width'=>100))
                    ->addColumn(array('label'=>'Qty Dropship','index'=>'qty_dropship', 'width'=>100))
                    ->addColumn(array('label'=>'Category Name','index'=>'category_name', 'width'=>100))
                    ->addColumn(array('label'=>'Attributeset Name','index'=>'attributeset_name', 'width'=>100))
                    ->addColumn(array('label'=>'Created at','index'=>'created_at', 'width'=>100))
                    ->addColumn(array('label'=>'Updated at','index'=>'updated_at', 'width'=>100))
                    ->addColumn(array('label'=>'Action','index'=>'action_column', 'width'=>150,'search'=>false))
                    ->renderGrid()
                ?>
            </div>
        </div>
    </div>
    <script>
        function pendingapprovedreject(url, product_id){
            //SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
            //jQuery('.ajax-loader-region').show();

            //var vendorId = jQuery(this).attr('vendor-id');
            jQuery.ajax({
                url: url,
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(result){
                    jQuery('.custom-modal-view').modal('toggle');
                    jQuery('.custom-modal-dialog').addClass('modal-lg');
                    jQuery('.custom-modal-title').html('Product Approval');//SET TITLE
                    jQuery('.custom-modal-body').html(result);//SET BODY

                    //SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
                    jQuery('.ajax-loader-region').hide();
                }
            });
            return false;
        }
        function pendingapprovedreject(url, product_id, title){
            //SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
            //jQuery('.ajax-loader-region').show();
if (title == "approve")
{
    title = "Product Approve";
}
else
{
    title = "Product Reject";
}
            //var vendorId = jQuery(this).attr('vendor-id');
            jQuery.ajax({
                url: url,
                type: "get",
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                success: function(result){
                    jQuery('.custom-modal-view').modal('toggle');
                    jQuery('.custom-modal-dialog').addClass('modal-lg');
                    jQuery('.custom-modal-title').html(title);//SET TITLE
                    jQuery('.custom-modal-body').html(result);//SET BODY

                    //SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
                    jQuery('.ajax-loader-region').hide();
                }
            });
            return false;
        }
    </script>
@endsection