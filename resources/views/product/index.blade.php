@extends('layouts.app')
@section('title','Index')
@section('content')

<div class = 'container'>

    <div class="row">
        <div class="col-md-11">
            <h1 style="display: inline-block; color: #a81d21;">
                Product
            </h1>
            <div style="float: right; padding-top: 18px;">
                {!! Form::open(array('url'=>'product/create','method'=>'POST', 'files'=>true)) !!}
                {!! Form::submit('Add Product', array('class'=>'btn btn-lg btn-primary col-md-2', 'style'=> 'width: 100%;')) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-11">
        <?php
        echo GridRender::setGridId("ProductListView")
            ->enableFilterToolbar()
            ->setGridOption('url',URL::to('/product-grid-data'))
            ->setGridOption('rowNum', 5)
            ->setGridOption('sortname','id')
            ->setGridOption('viewrecords',false)
            ->setGridOption('caption','Product Listing ')
            ->setGridOption('autowidth', true)
            ->setGridOption('height', 'auto')
            ->setGridOption('rowList', [5,10, 15, 25, 50])
            ->setGridOption('pager', "jqGridPager")
            ->setGridOption('shrinkToFit', false)
            ->setFilterToolbarOptions(array('autosearch'=>true))
            ->setGridOption('postData', array('_token' => Session::token()))
            //->setGridEvent('gridComplete', 'gridCompleteEvent') //gridCompleteEvent must be previously declared as a javascript function.
            ->addColumn(array('index'=>'Id', 'index'=>'id','width'=>20,'search'=>false))
            ->addColumn(array('label'=>'Product Name','index'=>'name', 'width'=>100))
            ->addColumn(array('label'=>'Product Image','index'=>'product_image', 'width'=>75))
            ->addColumn(array('label'=>'Sku','index'=>'sku','width'=>100))
            ->addColumn(array('label'=>'Vendor Name','index'=>'vendor_name','width'=>100))
            ->addColumn(array('label'=>'Qty','index'=>'qty_dropship','width'=>75))
            ->addColumn(array('label'=>'Category Name','index'=>'category_name'))
            ->addColumn(array('label'=>'Attributeset Name','index'=>'attributeset_name','width'=>100))
            ->addColumn(array('label'=>'Created at','index'=>'created_at','width'=>90))
            ->addColumn(array('label'=>'Updated at','index'=>'updated_at','width'=>90))
            ->addColumn(array('label'=>'Action','index'=>'action_column','search'=>false))
            ->renderGrid()
        ?>
        </div>
    </div>
</div>

@endsection