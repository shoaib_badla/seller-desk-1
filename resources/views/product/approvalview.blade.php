@extends('layouts.ajax')
@section('title','Approval')
@section('content')

    <section class = 'content'>

        <div class="row">

            {!! Form::open(array('url'=>'products/addapprovereject','method'=>'POST', 'files'=>true)) !!}
                <input type="hidden" id="product_id" name="product_id" value="{{$id}}">

            <div class="col-md-9" >
                    @foreach ($productauths as $productauth)
                        @if($productauth->name == $type)
                            <input type="hidden" id="role_id" name="role_id" value="{{$productauth->id}}">
                        @elseif($productauth->name == $type)
                            <input type="hidden" id="role_id" name="role_id" value="{{$productauth->id}}">
                        @endif
                    @endforeach
            </div>
            <div class="col-md-3">
                <select name="type" id="type" style="width: 100%;">
                    <option value="approve">Select..</option>
                    <option value="approve">Approve</option>
                    <option value="reject">Reject</option>
                </select>
            </div>
            <div class="col-md-9">
            <textarea rows="4"  class="" style="width: 100%;" id="comment" name="comment" placeholder="Comments" ></textarea>
            </div>
            <div class="col-md-3">
                <input type="submit" class="btn btn-lg btn-primary col-md-12 submit-product" style="float: right; margin-top: 20px;" type="button" value="Submit Comment">
            </div>

            {!! Form::close() !!}

            <div class="col-md-12">

                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Product Information</a></li>
                            <li><a href="#tab2primary" data-toggle="tab">Additional Info</a></li>
                            <li><a href="#tab3primary" data-toggle="tab">Image</a></li>

                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="tab-content">

                            <div class="tab-pane fade in active" id="tab1primary">
                                @foreach ($prodata as $prod)
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <td>
                                                Sku
                                            </td>
                                            <td>
                                                {{$prod->sku}}
                                            </td>
                                            <td>
                                                Vendor
                                            </td>
                                            <td>
                                                {{$prod->vendor_id}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Category
                                            </td>
                                            <td>
                                                {{$prod->categories_id}}
                                            </td>
                                            <td>
                                                Mode of Fullfillment
                                            </td>
                                            <td>
                                                {{$prod->mode_of_fullfillment}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Qty
                                            </td>
                                            <td>
                                                {{$prod->dropship_qty}}
                                            </td>
                                            <td>
                                                Attribute Sets
                                            </td>
                                            <td>
                                                {{$prod->attribute_set_id}}
                                            </td>
                                        </tr>
                                        <?php $count= 0 ?>

                                        @foreach ($atributedata as $attridata)
                                            @if($attridata->additional_info == 0)
                                                <?php $mod = $count % 2; ?>
                                                @if($mod == 0)
                                                    <tr>
                                                        @endif
                                                        <td>
                                                            {{$attridata->name}}
                                                        </td>
                                                        <td>
                                                            {{$attridata->value}}
                                                        </td>
                                                        @if($mod == 1)
                                                    </tr>
                                                @endif
                                                <?php $count++ ?>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>

                                @endforeach
                            </div>
                            <div class="tab-pane fade" id="tab2primary">

                                <div class="tab2attributearea" >
                                    <table class="table table-striped">
                                        <tbody>
                                        <?php $count= 0 ?>

                                        @foreach ($atributedata as $attridata)
                                            @if($attridata->additional_info == 1)
                                                <?php $mod = $count % 2; ?>
                                                @if($mod == 0)
                                                    <tr>
                                                        @endif
                                                        <td>
                                                            {{$attridata->name}}
                                                        </td>
                                                        <td>
                                                            {{$attridata->value}}
                                                        </td>
                                                        @if($mod == 1)
                                                    </tr>
                                                @endif
                                                <?php $count++ ?>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tab3primary">
                                <div class="col-md-12">
                                    @foreach($files as $file)
                                        <img src="{{url("public/uploads/".$file->filename)}}" width="150" height="150" />
                                    @endforeach
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section>
    <script>

    </script>
@endsection

