@extends('layouts.app')
@section('title','Product Update')
@section('content')

    <div class = 'container'>

        <div class="row">

            {!! Form::open(array('url'=>'products/updatedata','method'=>'POST', 'files'=>true)) !!}

            <div class="col-md-11">
                <h1 style="display: inline-block; color: #a81d21;">
                    Product
                </h1>
                {!! Form::submit('Submit', array('class'=>'btn btn-lg btn-primary col-md-2', 'style'=>'float: right; margin-top: 20px;')) !!}</div>
            <div class="col-md-11">

                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Product Information</a></li>
                            <li><a href="#tab2primary" data-toggle="tab">Additional Info</a></li>
                            <li><a href="#tab3primary" data-toggle="tab">Image</a></li>
                            <li><a href="#tab4primary" data-toggle="tab">configurable option</a></li>

                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="tab-content">

                            <div class="tab-pane fade in active" id="tab1primary">
                                @foreach ($prodata as $prod)
                                    <input type="hidden" class="form-control " value="{{$prod->id}}" id="id" name="id" >
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L1'); ?> </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="categoryl1" class="chosen-select form-control selectrequired" name="categories">
                                                    <option value="">Select Categories</option>

                                                </select>

                                            </div>
                                            <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L2'); ?> </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="categoryl2" class="chosen-select form-control selectrequired" name="categories">
                                                    <option value="">Select Categories</option>
                                                </select>

                                            </div>
                                            <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L3'); ?> </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="categories" class="chosen-select form-control selectrequired" name="categories">
                                                    <option value="">Select Categories</option>

                                                </select>

                                            </div>
                                            <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12">Sku </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <input type="text" readonly class="form-control sku" name="sku" id="sku" aria-describedby="{{$prod->sku}}Help" value="{{$prod->sku}}" placeholder="Enter {{$prod->sku}}">
                                                </div>

                                        </div>
                                    </div>


                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> {!! Form::label('Vendor ') !!} </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="vendor" class="chosen-select form-control" name="vendor">
                                                    @foreach ($allvendors as $allvendor)
                                                        @if($prod->vendor_id == $allvendor->id)
                                                                <option value="{{$allvendor->id}}" selected="selected">{{$allvendor->name}}</option>
                                                            @else
                                                                <option value="{{$allvendor->id}}" selected="selected">{{$allvendor->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>


                                @endforeach


                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group <?php echo e($errors->has('id') ? 'has-error' : ''); ?>">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Mode of Fullfillment '); ?> </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="mode_of_fullfillment" class="chosen-select form-control selectrequired" name="mode_of_fullfillment">
                                                    <option value="">Select Fullfillment</option>
                                                    @foreach ($fullfillmentoption as $fullfillmentopt)
                                                        @if($prod->mode_of_fullfillment == $fullfillmentopt->id)
                                                            <option value="{{$fullfillmentopt->id}}" selected="selected">{{$fullfillmentopt->name}}</option>
                                                        @else
                                                            <option value="{{$fullfillmentopt->id}}">{{$fullfillmentopt->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="hidden" placeholder="Qty Stocking" class="form-control "  id="qty_stocking" name="qty_stocking" value="{{$prod->stocking_qty}}"  >
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group">
                                            <div class="col-xs-12 col-sm-12 col-md-12"><?php echo Form::label('Qty '); ?></div>
                                            <div class="col-xs-12 col-sm-12 col-md-12"><input type="number" placeholder="Qty" class="form-control numberrequired" id="qty_dropship" value="{{$prod->dropship_qty}}" name="qty_dropship" ></div>
                                        </div>
                                    </div>

                                    <input type="hidden" class="form-control attribute_sets" name="attribute_sets" id="attribute_sets" aria-describedby="attribute_sets_Help" placeholder="Enter attribute_sets" value = "{{$prod->attribute_set_id}}">
                                <div class="tab1attributearea" >
                                    @foreach ($atributedata as $attridata)
                                        @if($attridata->additional_info == 0)
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <div class="col-xs-12 col-sm-12 col-md-12"><label for="Input{{$attridata->attribute_id}}">{{$attridata->name}}</label></div>
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    @if($attridata->frontend_type == "select")
                                                        <select class="chosen-select form-control {{$attridata->attribute_id}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->attribute_id}}Help" >
                                                                @foreach($attr_option_value as $attr_option_val)
                                                                    @if($attr_option_val->attribute_id == $attridata->attribute_id)
                                                                        @if($attr_option_val->option_id_yayvo == $attridata->value)
                                                                            <option value="{{$attr_option_val->option_id_yayvo}}" selected="selected">{{$attr_option_val->option_value}}</option>
                                                                        @else
                                                                            <option value="{{$attr_option_val->option_id_yayvo}}">{{$attr_option_val->option_value}}</option>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                        </select>
                                                    @elseif($attridata->frontend_type == "textarea")
                                                            @if($attridata->required == 1)
                                                                <textarea rows="4" class="form-control {{$attridata->code}} {{$attridata->frontend_type}}required" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->value}}Help" placeholder="Enter {{$attridata->value}}">{{$attridata->value}}</textarea>
                                                            @else
                                                                <textarea rows="4" class="form-control {{$attridata->code}} {{$attridata->frontend_type}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->value}}Help" placeholder="Enter {{$attridata->value}}">{{$attridata->value}}</textarea>
                                                            @endif
                                                    @elseif($attridata->frontend_type == "checkbox" || $attridata->frontend_type == "boolean")
                                                                <label class="switch">
                                                                    @if($attridata->required == 1)
                                                                        <input required="required" type="{{$attridata->frontend_type}}" class="{{$attridata->code}}  {{$attridata->frontend_type}}required" name="{{$attridata->id}}" id="{{$attridata->id}}" aria-describedby="{{$attridata->value}}Help" placeholder="Enter {{$attridata->value}}"  value="{{$attridata->value}}">
                                                                    @else
                                                                        <input type="{{$attridata->frontend_type}}" class="{{$attridata->code}} {{$attridata->frontend_type}}" name="{{$attridata->id}}" id="{{$attridata->id}}" aria-describedby="{{$attridata->value}}Help" placeholder="Enter {{$attridata->value}}" value="{{$attridata->value}}">
                                                                    @endif
                                                                    <div class="slider round"></div>
                                                                    </label>

                                                    @elseif($attridata->frontend_type == "price")
                                                        <input type="number" class="form-control {{$attridata->attribute_id}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->attribute_id}}Help" value="{{$attridata->value}}" placeholder="Enter {{$attridata->attribute_id}}">
                                                    @else

                                                    <input type="{{$attridata->frontend_type}}" class="form-control {{$attridata->attribute_id}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->attribute_id}}Help" value="{{$attridata->value}}" placeholder="Enter {{$attridata->attribute_id}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>


                            </div>
                            <div class="tab-pane fade" id="tab2primary">

                                <div class="tab2attributearea" >
                                    @foreach ($atributedata as $attridata)
                                        @if($attridata->additional_info == 1)
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="form-group">
                                                    <div class="col-xs-12 col-sm-12 col-md-12"><label for="Input{{$attridata->attribute_id}}">{{$attridata->name}}</label></div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                                        @if($attridata->type == "select")
                                                            <select class="chosen-select form-control {{$attridata->attribute_id}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->attribute_id}}Help" >
                                                            </select>
                                                            @else
                                                            <input type="{{$attridata->type}}" class="form-control {{$attridata->attribute_id}}" name="{{$attridata->attribute_id}}" id="{{$attridata->attribute_id}}" aria-describedby="{{$attridata->attribute_id}}Help" value="{{$attridata->value}}" placeholder="Enter {{$attridata->attribute_id}}">
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tab3primary">
                                <div class="laradrop"
                                     laradrop-csrf-token="<?php echo e(csrf_token()); ?>" > </div>
                            </div>
                            <div class="tab-pane fade" id="tab4primary" >

                                    <div class="row option-attribute-inner1 option-attribute-inner">

                                        <div class="box-header with-border">
                                            <div class="box-tools pull-right" style="display: block;">
                                                <button type="button" class="btn btn-box-tool addoptionattribute" style=""><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-box-tool removeoptionattribute" style="display: none;"><i class="fa fa-remove"></i></button>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Description">Option Attribute</label>
                                            <select id="optionalattr[]" name="optionalattr[]" class="optionalattr form-control validate">
                                                <option value="">Select Attribute</option>
                                                @foreach ($configurableattrs as $configurableattr)
                                                    <option value="{{$configurableattr->id}}">{{$configurableattr->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Description">Description</label>
                                            <select id="description[]" name="description[]" class="description form-control validate">
                                                <option value="">Select option</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="qty">Qty</label>
                                            <input type="number" id="qty[]" name="qty[]" class="qty form-control validate">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="price">Price</label>
                                            <input type="number" id="price[]" name="price[]" type="text" class="price form-control validate">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Status">Status</label>
                                            <select id="status[]" name="status[]" class="status form-control validate">
                                                <option value="1">Enabled</option>
                                                <option value="2">Disabled</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Cost">Cost</label>
                                            <input type="number" id="cost[]" name="cost[]" class="cost form-control validate">

                                        </div>
                                    </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script>
        var categoryid = '{{$prod->categories_id}}';

        var allcategories;
        function updatecatdropdownl1(){

            $('#categoryl1').find("option").remove();
            $('#categoryl1').append('<option value="">Select L1 Categories</option>');
            for(var item in allcategories["l1categories"])
            {
                $('#categoryl1').append('<option value="'+item+'">'+allcategories["l1categories"][ item ]+'</option>');
            }

            $('#categoryl1').trigger("chosen:updated");
        }
        function updatecatdropdownl2(){
            var id = $('#categoryl1').val();
            $('#categoryl2').find("option").remove();
            $('#categoryl2').append('<option value="">Select L2 Categories</option>');
            for(var item in allcategories["l2categories"])
            {
                if (allcategories["l2categories"][ item ][1] == id) {
                    $('#categoryl2').append('<option value="' + item + '">' + allcategories["l2categories"][item][0] + '</option>');
                }
            }

            $('#categoryl2').trigger("chosen:updated");
        }
        function updatecatdropdownl3(){

            var id = $('#categoryl2').val();
            $('#categories').find("option").remove();
            $('#categories').append('<option value="">Select L3 Categories</option>');
            for(var item in allcategories["l3categories"])
            {
                if (allcategories["l3categories"][ item ][1] == id)
                {
                    $('#categories').append('<option value="'+item+'">'+allcategories["l3categories"][ item ][0]+'</option>');
                }

            }

            $('#categories').trigger("chosen:updated");
        }

        function fillconfigurableopt(obj){
            jQuery.ajax({
                url: "<?php echo url('/products/configurableopt');?>",
                type:"post",
                dataType: "json",
                data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "attr_id": jQuery(obj).val(),
                },
                success: function(data) {

                    $(obj).parent().parent().find(".description").find(".option").remove();
                    $(obj).parent().parent().find(".description").find(".option").append('<option value="">Select option</option>');
                    for(var a=0; a<data.length; a++)
                    {
                        $(obj).parent().parent().find(".description").append('<option value="'+data[a].option_id_yayvo+'">'+data[a].option_value +'</option>');
                    }

                }
            });

        }

        function setselectedcategory(){
            var l1cat = "";
            var l2cat = "";

            for(var item in allcategories["l3categories"]) {
                if(item == categoryid){
                    l2cat = allcategories["l3categories"][ item ][1];
                    break;
                }
            }
            for(var item in allcategories["l2categories"]) {
                if(item == l2cat){
                    l1cat = allcategories["l2categories"][ item ][1];
                    break;
                }
            }
            $("#categoryl1").val(l1cat);
            $("#categoryl1").change();
            $('#categoryl1').trigger("chosen:updated");

            $("#categoryl2").val(l2cat);
            $("#categoryl2").change();
            $('#categoryl2').trigger("chosen:updated");

            $("#categories").val(categoryid);
            $('#categories').trigger("chosen:updated");
        }

        function showHideCSVRegion(){

            var attributeSetVal = jQuery.trim(jQuery('#attribute_sets').val());
            var categoryId 		= jQuery.trim(jQuery('#categories').val());

            var downloadURL = "<?php echo url('/product/sampleCSVDownload');?>/"+attributeSetVal+"/"+categoryId;

            if(attributeSetVal != "" && categoryId != ""){
                jQuery('.csvUploadField>.help-block').html("Click <a href='"+downloadURL+"' target='_blank'>here</a> for Sample CSV");
                jQuery('.csvUploadField').show();
            }else{
                jQuery('.csvUploadField').hide();
            }
        }

        jQuery(document).ready(function() {
            jQuery('#categoryl1').on('change',function(){
                updatecatdropdownl2();
            });
            jQuery('#categoryl2').on('change',function(){
                updatecatdropdownl3();
            });

            jQuery.ajax({
                url: "<?php echo url('/products/getalll3categories');?>",
                type:"post",
                dataType: "json",
                data: {"_token": "<?php echo e(csrf_token()); ?>",},
                success: function(data) {
                    allcategories =data;
                    updatecatdropdownl1();
                    setselectedcategory();

                }
            });

            jQuery('#categories').on('change',function(){
                getattributes();
            });

            function getattributes(){
                if (jQuery('#categories').val() == ""){
                    return false;
                }
                var categoryId = jQuery.trim(jQuery('#categories').val());

                showHideCSVRegion();

                //GET ASSOCIATED ATTRIBUTE Sets
                jQuery.ajax({
                    url: "<?php echo url('/product/attributeListByCategory');?>",
                    type: "post",
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>",
                        "categoryId": categoryId
                    },
                    success: function(result){
                        //UPDATE ATTRIBUTES VALUE
                        for(var i = 0; i < result.length; i++)
                        {
                            jQuery('#attribute_sets').val(result[i].id);
                            attributelist(result[i].id);
                        }

                    }
                });
            }
// attribute by category
            //jQuery('#attribute_sets').on('change',
            function  attributelist(attributesets){

                //var attributesets = jQuery.trim(jQuery(this).val());

                //GET ASSOCIATED ATTRIBUTE Sets
                jQuery.ajax({
                    url: "<?php echo url('/product/attributeListByattributesetupdate');?>",
                    type: "post",
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>",
                        "attribute_sets": attributesets,
                        "id": '{{$id}}'
                    },
                    success: function(result){
                        //UPDATE ATTRIBUTES VALUE
                        var attr_data = result["attr_data"];
                        var attr_option = result["attr_option"];
                        jQuery(".tab1attributearea").html("");
                        jQuery(".tab2attributearea").html("");
                        for(var i=0; i < attr_data.length; i++)
                        {

                                if (attr_data[i].value != "")
                                {
                                    var type = "";
                                    if(attr_data[i].type == "price")
                                    {
                                        type = "number";
                                    }
                                    else if(attr_data[i].type == "boolean")
                                    {
                                        type = "checkbox";
                                    }
                                    else
                                    {
                                        type =  attr_data[i].type;
                                    }

                                    var html = '';
                                    html += '<div class="col-xs-12 col-sm-6 col-md-4">';
                                    html +=  '<div class="form-group">';


                                    if (type == "checkbox")
                                    {
                                        html += '<div class="col-xs-12 col-sm-12 col-md-12" style="min-height: 18px;" ></div>';
                                        html += '<div class="col-xs-12 col-sm-12 col-md-12" "><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label>';
                                        html += '<label class="switch">'
                                        if(attr_data[i].required == 1) {
                                            html += '<input required="required" type="' + type + '" class="' + attr_data[i].code + ' ' + attr_data[i].type + 'required" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                        }
                                        else
                                        {
                                            html += '<input type="' + type + '" class="' + attr_data[i].code + ' ' + attr_data[i].type + '" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                        }
                                        html += '<div class="slider round"></div>'
                                        html += '</label>'
                                        html += '</div>'
                                    }
                                    else if (type == "select")
                                    {
                                        html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                        html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                        if(attr_data[i].required == 1) {

                                            html += '<select required="required" class="chosen-select form-control ' + attr_data[i].code + ' ' + attr_data[i].type + 'required" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';

                                            if(getValueByKey( attr_data[i].id, attr_option));
                                            {
                                                html +=  getoptionvalue(attr_data[i].id, attr_option);

                                            }

                                            html += '</select>';
                                        }
                                        else
                                        {
                                            html += '<select class="form-control ' + attr_data[i].code + ' ' + attr_data[i].type + '" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                            html += '</select>';
                                        }
                                        html += '</div>';
                                    }
                                    else if (type == "textarea")
                                    {
                                        html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                        html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                        if(attr_data[i].required == 1)
                                        {

                                            html += '<textarea rows="4" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'required" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'">'+attr_data[i].datavalue+'</textarea>';
                                        }
                                        else
                                        {
                                            html += '<textarea rows="4" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'">'+attr_data[i].datavalue+'</textarea>';
                                        }

                                        html += '</div>';

                                    }
                                    else
                                    {
                                        html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                        html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                        if(attr_data[i].required == 1)
                                        {

                                            html += '<input  type="'+ type +'" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'required" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'" value="'+attr_data[i].datavalue+'">';
                                        }
                                        else
                                        {
                                            html += '<input  type="'+ type +'" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'"  value="'+attr_data[i].datavalue+'">';
                                        }

                                        html += '</div>';

                                    }


                                    html += '</div>';
                                    html += '</div>';

                                    if (attr_data[i].additional_info == 0){



                                        jQuery(".tab1attributearea").append(html);
                                    }
                                    else
                                    {

                                        jQuery(".tab2attributearea").append(html);
                                    }
                                }


                        }
                        $("#product_data").validate({
                            errorElement: 'span',
                            errorClass: 'help-block error-help-block',

                            errorPlacement: function(error, element) {
                                if (element.parent('.input-group').length ||
                                    element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                                    error.insertAfter(element.parent());
                                    // else just place the validation message immediatly after the input
                                } else {
                                    error.insertAfter(element);
                                }
                            },
                            highlight: function(element) {
                                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
                            },
                            focusInvalid: false, // do not focus the last invalid input
                            ignore:"ui-tabs-hide"});
                        jQuery.validator.addClassRules("pricerequired", {
                            required: true,
                            number: true,
                        });
                        jQuery.validator.addClassRules("daterequired", {
                            required: true,
                        });
                        jQuery.validator.addClassRules("textrequired", {
                            required: true,
                        });
                        jQuery.validator.addClassRules("textarearequired", {
                            required: true,
                        });
                        jQuery.validator.addClassRules("selectrequired", {
                            required: true,
                        });
                        jQuery.validator.addClassRules("numberrequired", {
                            required: true,
                        });

                        jQuery( ".submit-product" ).prop( "disabled", false );
                        //jQuery('#attribute_sets').empty().append(result);
                    }
                });
            }


            jQuery('.laradrop').laradrop({
                fileHandler:"/product/updatestore/{{$id}}",
                fileSrc:"/product/imageupdate/{{$id}}",
                fileDeleteHandler: "/product/updateimagedestroy"
            });
        })



    </script>
@endsection

