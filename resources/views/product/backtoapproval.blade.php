@extends('layouts.ajax')
@section('title','Approval')
@section('content')

    <section class = 'content'>

        <div class="row">

            {!! Form::open(array('url'=>'products/revertapproval','method'=>'POST', 'files'=>true)) !!}
<input type="hidden" name="id" id="id" value="{{$id}}" />
            <div class="col-md-6">
                <div>
                    Are you sure to revert the Approval?
                </div>
            </div>
            <div class="col-md-3">
            <input type="submit" class="btn btn-lg btn-primary col-md-12 submit-product" style="float: right; margin-top: 20px;" type="button" value=" Revert it">
            </div>
            {!! Form::close() !!}

        </div>
    </section>
    <script>

    </script>
@endsection

