<?php $__env->startSection('title','create'); ?>
<?php $__env->startSection('content'); ?>

<section class = 'content'>

    <div class="row">

        <div class="flash-message">

            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
        </div> <!-- end .flash-message -->

        <?php echo Form::open(array('url'=>'products/add','method'=>'POST', 'files'=>true, 'id'=>'product_data')); ?>


        <div class="col-md-12">
            <h1 style="display: inline-block; color: #a81d21;">
                Product
            </h1>
            <input class="btn btn-lg btn-primary col-md-2 submit-product" style="float: right; margin-top: 20px;" disabled type="button" value="Submit">
        </div>
            <div class="col-md-12">

                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab">Product Information</a></li>
                            <li><a href="#tab2primary" data-toggle="tab">Additional Info</a></li>
                            <li><a href="#tab3primary" data-toggle="tab">Image</a></li>
                            <li><a href="#tab4primary" data-toggle="tab">configurable option</a></li>

                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="tab-content">

                            <div class="tab-pane fade in active" id="tab1primary">

                                <!--                                <div class="col-xs-12 col-sm-6 col-md-6">-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <div class="col-xs-12 col-sm-12 col-md-12"> --><?php //echo Form::label('Universal Sku '); ?><!-- </div>-->
                                <!--                                        <div class="col-xs-12 col-sm-12 col-md-12"> --><?php //echo Form::text('universal_sku', null, array('placeholder' => 'universal sku','class' => 'form-control','id'=>'universal_sku')); ?><!--</div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                        <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L1'); ?> </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <select id="categoryl1" class="chosen-select form-control selectrequired" name="categories">
                                                <option value="">Select Categories</option>

                                            </select>

                                        </div>
                                        <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                        <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L2'); ?> </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <select id="categoryl2" class="chosen-select form-control selectrequired" name="categories">
                                                <option value="">Select Categories</option>
                                            </select>

                                        </div>
                                        <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                        <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Category L3'); ?> </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <select id="categories" class="chosen-select form-control selectrequired" name="categories">
                                                <option value="">Select Categories</option>

                                            </select>

                                        </div>
                                        <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12 col-md-12"><?php echo Form::label('sku '); ?></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12"><?php echo Form::text('sku', null, array('placeholder' => 'sku','class' => 'form-control textrequired','id'=>'sku')); ?></div>
                                    </div>
                                 </div>
                                {{--@if($Common::userwisepermission(Auth::user()->id, "vendor") == 1)--}}
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                                            <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Vendor '); ?> </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <select id="vendor" data-placeholder="Choose a Vendor..."  class="chosen-select form-control selectrequired" name="vendor">
                                                    <option value="">Select Vendor</option>
                                                    @foreach ($allvendors as $allvendor)
                                                        <option value="{{$allvendor->id}}">{{$allvendor->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                {{--@endif--}}

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group <?php echo e($errors->has('id') ? 'has-error' : ''); ?>">
                                        <div class="col-xs-12 col-sm-12 col-md-12"> <?php echo Form::label('Mode of Fullfillment '); ?> </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <select id="mode_of_fullfillment" class="chosen-select form-control selectrequired" name="mode_of_fullfillment">
                                                @foreach ($fullfillmentoption as $fullfillmentopt)
                                                    <option value="{{$fullfillmentopt->id}}">{{$fullfillmentopt->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <input type="hidden" placeholder="Qty Stocking" class="form-control " value="0" id="qty_stocking" name="qty_stocking" >

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-12 col-md-12"><?php echo Form::label('Qty '); ?></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12"><input type="number" placeholder="Qty" class="form-control numberrequired" id="qty_dropship" name="qty_dropship" ></div>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control attribute_sets" name="attribute_sets" id="attribute_sets" aria-describedby="attribute_sets_Help" placeholder="Enter attribute_sets">

                                <div class="tab1attributearea" >

                                </div>


                            </div>
                            <div class="tab-pane fade" id="tab2primary">

                                <div class="tab2attributearea" >

                                </div>

                            </div>
                            <div class="tab-pane fade" id="tab3primary">
                                <div class="laradrop"
                                     laradrop-file-source="<?php echo e(route('product.imagesave')); ?>"
                                     laradrop-upload-handler="<?php echo e(route('product.store')); ?>"
                                     laradrop-file-delete-handler="<?php echo e(route('product.imagedestroy', 0)); ?>"
                                     laradrop-file-move-handler="<?php echo e(route('product.move')); ?>"
                                     laradrop-file-create-handler="<?php echo e(route('product.imagecreate')); ?>"
                                     laradrop-containers="<?php echo e(route('product.containers')); ?>"
                                     laradrop-csrf-token="<?php echo e(csrf_token()); ?>"> </div>
                            </div>
                            <div class="tab-pane fade" id="tab4primary" >

                                    <div class="row option-attribute-inner1 option-attribute-inner">

                                        <div class="box-header with-border">
                                            <div class="box-tools pull-right" style="display: block;">
                                                <button type="button" class="btn btn-box-tool addoptionattribute" style=""><i class="fa fa-plus"></i></button>
                                                <button type="button" class="btn btn-box-tool removeoptionattribute" style="display: none;"><i class="fa fa-remove"></i></button>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Description">Option Attribute</label>
                                            <select id="optionalattr[]" name="optionalattr[]" class="optionalattr form-control validate">
                                                <option value="">Select Attribute</option>
                                                @foreach ($configurableattrs as $configurableattr)
                                                    <option value="{{$configurableattr->id}}">{{$configurableattr->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Description">Description</label>
                                            <select id="description[]" name="description[]" class="description form-control validate">
                                                <option value="">Select option</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="qty">Qty</label>
                                            <input type="number" id="qty[]" name="qty[]" class="qty form-control validate">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="price">Price</label>
                                            <input type="number" id="price[]" name="price[]" type="text" class="price form-control validate">
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Status">Status</label>
                                            <select id="status[]" name="status[]" class="status form-control validate">
                                                <option value="1">Enabled</option>
                                                <option value="2">Disabled</option>
                                            </select>

                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="Cost">Cost</label>
                                            <input type="number" id="cost[]" name="cost[]" class="cost form-control validate">

                                        </div>
                                    </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <?php echo Form::close(); ?>


    </div>
    <script>
        var allcategories;
        function updatecatdropdownl1(){

            $('#categoryl1').find("option").remove();
            $('#categoryl1').append('<option value="">Select L1 Categories</option>');
            for(var item in allcategories["l1categories"])
            {
                $('#categoryl1').append('<option value="'+item+'">'+allcategories["l1categories"][ item ]+'</option>');
            }

            $('#categoryl1').trigger("chosen:updated");
        }
        function updatecatdropdownl2(){

            var id = $('#categoryl1').val();
            $('#categoryl2').find("option").remove();
            $('#categoryl2').append('<option value="">Select L2 Categories</option>');
            for(var item in allcategories["l2categories"])
            {
                if (allcategories["l2categories"][ item ][1] == id) {
                    $('#categoryl2').append('<option value="' + item + '">' + allcategories["l2categories"][item][0] + '</option>');
                }
            }

            $('#categoryl2').trigger("chosen:updated");
        }
        function updatecatdropdownl3(){

            var id = $('#categoryl2').val();
            $('#categories').find("option").remove();
            $('#categories').append('<option value="">Select L3 Categories</option>');
            for(var item in allcategories["l3categories"])
            {
                if (allcategories["l3categories"][ item ][1] == id)
                {
                    $('#categories').append('<option value="'+item+'">'+allcategories["l3categories"][ item ][0]+'</option>');
                }

            }

            $('#categories').trigger("chosen:updated");
        }

        function fillconfigurableopt(obj){
            jQuery.ajax({
                url: "<?php echo url('/products/configurableopt');?>",
                type:"post",
                dataType: "json",
                data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "attr_id": jQuery(obj).val(),
                },
                success: function(data) {
                    $(obj).parent().parent().find(".description").find(".option").remove();
                    $(obj).parent().parent().find(".description").find(".option").append('<option value="">Select option</option>');
                    for(var a=0; a<data.length; a++)
                    {
                        $(obj).parent().parent().find(".description").append('<option value="'+data[a].option_id_yayvo+'">'+data[a].option_value +'</option>');
                    }

                }
            });

        }
        jQuery(document).ready(function() {
            jQuery('#categoryl1').on('change',function(){
                updatecatdropdownl2();
            });
            jQuery('#categoryl2').on('change',function(){
                updatecatdropdownl3();
            });
            jQuery('.optionalattr').on('change',function(){
                fillconfigurableopt(jQuery(this));
            });
            jQuery.ajax({
                url: "<?php echo url('/products/getalll3categories');?>",
                type:"post",
                dataType: "json",
                data: {"_token": "<?php echo e(csrf_token()); ?>",},
                success: function(data) {
                    allcategories =data;
                    updatecatdropdownl1();


                }
            });

            getattributes();
            src = "<?php echo e(route('searchajax')); ?>";
            jQuery("#universal_sku").autocomplete({
                source: function(request, response) {
                    jQuery.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);

                        }
                    });
                },
                min_length: 3,

            });

            jQuery('.laradrop').laradrop({
            fileHandler:"/product/store",
                fileSrc:"/product/imagesave",
                fileDeleteHandler: "/product/imagedestroy"
        });
            $(".submit-product").click(function(){
                if($("#product_data").valid() == true)
                {
                    $("#product_data").submit();
                }
                else{
                    return false;
                }
            })

        });

        // attribute by category
        jQuery('#categories').on('change',function(){
            getattributes();
        });
        function getattributes(){
            if (jQuery('#categories').val() == ""){
                return false;
            }
            var categoryId = jQuery.trim(jQuery('#categories').val());

            showHideCSVRegion();

            //GET ASSOCIATED ATTRIBUTE Sets
            jQuery.ajax({
                url: "<?php echo url('/product/attributeListByCategory');?>",
                type: "post",
                data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "categoryId": categoryId
                },
                success: function(result){
                    //UPDATE ATTRIBUTES VALUE
                    for(var i = 0; i < result.length; i++)
                    {
                        jQuery('#attribute_sets').val(result[i].id);
                        attributelist(result[i].id);
                    }

                }
            });
        }
        // attribute by category
        //jQuery('#attribute_sets').on('change',
        function  attributelist(attributesets){

            //var attributesets = jQuery.trim(jQuery(this).val());

            //GET ASSOCIATED ATTRIBUTE Sets
            jQuery.ajax({
                url: "<?php echo url('/product/attributeListByattributeset');?>",
                type: "post",
                data: {
                    "_token": "<?php echo e(csrf_token()); ?>",
                    "attribute_sets": attributesets
                },
                success: function(result){
                    jQuery(".tab1attributearea").html("");
                    //UPDATE ATTRIBUTES VALUE
                    var attr_data = result["attr_data"];
                    var attr_option = result["attr_option"];
                    for(var i=0; i < attr_data.length; i++)
                    {
                        if (attr_data[i].value != "")
                        {
                            var type = "";
                            if(attr_data[i].type == "price")
                            {
                                type = "number";
                            }
                            else if(attr_data[i].type == "boolean")
                            {
                                type = "checkbox";
                            }
                            else
                            {
                                type =  attr_data[i].type;
                            }

                            var html = '';
                            html += '<div class="col-xs-12 col-sm-6 col-md-4">';
                            html +=  '<div class="form-group">';


                            if (type == "checkbox")
                            {
                                html += '<div class="col-xs-12 col-sm-12 col-md-12" style="min-height: 18px;" ></div>';
                                html += '<div class="col-xs-12 col-sm-12 col-md-12" "><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label>';
                                html += '<label class="switch">'
                                if(attr_data[i].required == 1) {
                                    html += '<input required="required" type="' + type + '" class="' + attr_data[i].code + ' ' + attr_data[i].type + 'required" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                }
                                else
                                {
                                    html += '<input type="' + type + '" class="' + attr_data[i].code + ' ' + attr_data[i].type + '" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                }
                                html += '<div class="slider round"></div>'
                                html += '</label>'
                                html += '</div>'
                            }
                            else if (type == "select")
                            {
                                html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                if(attr_data[i].required == 1) {

                                    html += '<select required="required" class="chosen-select form-control ' + attr_data[i].code + ' ' + attr_data[i].type + 'required" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';

                                    if(getValueByKey( attr_data[i].sf_attributeid, attr_option));
                                    {
                                        html +=  getoptionvalue(attr_data[i].sf_attributeid, attr_option);

                                    }

                                    html += '</select>';
                                }
                                else
                                {
                                    html += '<select class="form-control ' + attr_data[i].code + ' ' + attr_data[i].type + '" name="' + attr_data[i].id + '" id="' + attr_data[i].id + '" aria-describedby="' + attr_data[i].value + 'Help" placeholder="Enter ' + attr_data[i].value + '">';
                                    html += '</select>';
                                }
                                html += '</div>';
                            }
                            else if (type == "textarea")
                            {
                                html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                if(attr_data[i].required == 1)
                                {

                                    html += '<textarea rows="4" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'required" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'"></textarea>';
                                }
                                else
                                {
                                    html += '<textarea rows="4" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'"></textarea>';
                                }

                                html += '</div>';

                            }
                            else
                            {
                                html +=  '<div class="col-xs-12 col-sm-12 col-md-12"><label for="Input'+attr_data[i].value+'">'+attr_data[i].value+'</label></div>';
                                html += '<div class="col-xs-12 col-sm-12 col-md-12">';
                                if(attr_data[i].required == 1)
                                {

                                    html += '<input  type="'+ type +'" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'required" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'">';
                                }
                                else
                                {
                                    html += '<input  type="'+ type +'" class="form-control '+attr_data[i].code+' '+ attr_data[i].type +'" name="'+attr_data[i].id+'" id="'+attr_data[i].id+'" aria-describedby="'+attr_data[i].value+'Help" placeholder="Enter '+attr_data[i].value+'">';
                                }

                                html += '</div>';

                            }


                            html += '</div>';
                            html += '</div>';

                            if (attr_data[i].additional_info == 0){



                                jQuery(".tab1attributearea").append(html);
                            }
                            else
                            {

                                jQuery(".tab2attributearea").append(html);
                            }
                        }

                    }
                    $("#product_data").validate({
                        errorElement: 'span',
                        errorClass: 'help-block error-help-block',

                        errorPlacement: function(error, element) {
                            if (element.parent('.input-group').length ||
                                element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                                error.insertAfter(element.parent());
                                // else just place the validation message immediatly after the input
                            } else {
                                error.insertAfter(element);
                            }
                        },
                        highlight: function(element) {
                            $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
                        },
                        focusInvalid: false, // do not focus the last invalid input
                        ignore:"ui-tabs-hide"});
                    jQuery.validator.addClassRules("pricerequired", {
                        required: true,
                        number: true,
                    });
                    jQuery.validator.addClassRules("daterequired", {
                        required: true,
                    });
                    jQuery.validator.addClassRules("textrequired", {
                        required: true,
                    });
                    jQuery.validator.addClassRules("textarearequired", {
                        required: true,
                    });
                    jQuery.validator.addClassRules("selectrequired", {
                        required: true,
                    });
                    jQuery.validator.addClassRules("numberrequired", {
                        required: true,
                    });

                    jQuery( ".submit-product" ).prop( "disabled", false );
                    //jQuery('#attribute_sets').empty().append(result);
                }
            });
        }
        //);

        function showHideCSVRegion(){

            var attributeSetVal = jQuery.trim(jQuery('#attribute_sets').val());
            var categoryId 		= jQuery.trim(jQuery('#categories').val());

            var downloadURL = "<?php echo url('/product/sampleCSVDownload');?>/"+attributeSetVal+"/"+categoryId;

            if(attributeSetVal != "" && categoryId != ""){
                jQuery('.csvUploadField>.help-block').html("Click <a href='"+downloadURL+"' target='_blank'>here</a> for Sample CSV");
                jQuery('.csvUploadField').show();
            }else{
                jQuery('.csvUploadField').hide();
            }
        }
        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i].attribute_id == key) {
                    return data[i].attribute_id;
                }
            }

            return -1;
        }
        function getoptionvalue(key, data){
            var i, len = data.length, html;
            for(var i = 0;  i< len; i++)
            {
                if (data[i].sf_attributeid == key) {
                    html += '<option value="'+data[i].option_id_yayvo+'">'+data[i].option_value+'</option>';
                }
            }
            return html;
        }
        //start block add remove
        /**
         * FORM VALIDATION END
         */
        //jQuery( ".pickup-address-region:last-child" )
        //displayChildHideAndShowBtn();
        displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');
        displayChildHideAndShowBtn('tab5primary','category-level-region1','addCategory','removeCategory');

        //ADD NEW PICKUP ADDREE
        jQuery(document).on( 'click', '.addPickupAddress', function() {

            var pickupAddressHtml	=	jQuery('.pickup-address-region').clone(true);

            pickupAddressHtml =   pickupAddressHtml.find("input:text").val("").end();
            pickupAddressHtml =   pickupAddressHtml.find("textarea").val("").end();
            pickupAddressHtml.appendTo('#tab4primary');


            //REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
            jQuery('#tab4primary').find("div.pickup-address-region" ).first().removeClass('pickup-address-region');

            //HIDE AND SHOW BUTTON
            displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');



        });

        //REMOVE PICKUP Address
        jQuery('.removePickupAddress').on('click',function(){

            jQuery(this).closest(".pickup-address-region1").remove();
            //ADD CLASS FOR MAKING CLONE
            //REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
            jQuery('#tab4primary').find("div.pickup-address-region1" ).first().addClass('pickup-address-region');
            //HIDE AND SHOW BUTTON
            displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');
        });

        /**
         * FORM VALIDATION END
         */
        //jQuery( ".pickup-address-region:last-child" )
        //displayChildHideAndShowBtn();
        displayChildHideAndShowBtn('tab4primary','option-attribute-inner1','addoptionattribute','removeoptionattribute');


        //ADD NEW PICKUP ADDREE
        jQuery(document).on( 'click', '.addoptionattribute', function() {

            var pickupAddressHtml	=	jQuery('.option-attribute-inner').clone(true);

            pickupAddressHtml =   pickupAddressHtml.find("input:text").val("").end();
            pickupAddressHtml =   pickupAddressHtml.find("textarea").val("").end();
            pickupAddressHtml.appendTo('#tab4primary');


            //REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
            jQuery('#tab4primary').find("div.pickup-address-region" ).first().removeClass('option-attribute-inner');

            //HIDE AND SHOW BUTTON
            displayChildHideAndShowBtn('tab4primary','option-attribute-inner1','addoptionattribute','removeoptionattribute');



        });

        //REMOVE PICKUP Address
        jQuery('.removeoptionattribute').on('click',function(){

            jQuery(this).closest(".option-attribute-inner1").remove();
            //ADD CLASS FOR MAKING CLONE
            //REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
            jQuery('#tab4primary').find("div.option-attribute-inner1" ).first().addClass('option-attribute-inner');
            //HIDE AND SHOW BUTTON
            displayChildHideAndShowBtn('tab4primary','option-attribute-inner1','addoptionattribute','removeoptionattribute');
        });
        //end block add remove
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>