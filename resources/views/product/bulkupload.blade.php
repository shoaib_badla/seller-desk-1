@extends('layouts.app')
@section('title','Index')
@section('content')

<section class = 'content'>
    <h1>
        Product Bulk Upload
    </h1>
    <div class="row">
    <div class="col-md-12">
        <div class="panel with-nav-tabs panel-primary">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1primary" data-toggle="tab">Bulk Upload</a></li>
                   
                   
                </ul>
            </div>
            <div class="panel-body">
				<div class='err-region alert alert-danger' style='display:none;'></div>
				{{--@if (count($errors) > 0)--}}
						{{--<div class="alert alert-danger">--}}
							{{--<ul>--}}
								{{--@foreach ($errors->all() as $error)--}}
									{{--<li>{{ $error }}</li>--}}
								{{--@endforeach--}}
							{{--</ul>--}}
						{{--</div>--}}
					{{--@endif--}}

			

                {{--<div class="form-group">--}}
				{{----}}
					{{--<label for="">Category</label>--}}
					{{--<select name="categories" id="categories" class = "form-control">--}}
						{{--<option value="">Please Select</option>--}}
						{{--@foreach($categories as $key=>$category)--}}
						{{--<option value="{{$key}}">{{$category}}</option>--}}
						{{--@endforeach--}}
					{{--</select>--}}
                {{--</div>--}}
				{{----}}
				 {{--<div class="form-group">--}}
					{{--<label for="">Attribute Sets</label>--}}
					{{--<select name="attribute_sets" id="attribute_sets" class = "form-control">--}}
						{{--<option value="">Please select attribute sets</option>--}}
					{{--</select>--}}
                {{--</div>--}}
				{{----}}
				<div class="form-group csvUploadField" >
                  <div class="row">
                      <div class="col-md-8">
                      {!! Form::open(array('url'=>'product/bulkproductSubmit','method'=>'POST','id'=>'bluk_product_form', 'files'=>true)) !!}
                      <div class="col-md-6">
                    <label for="exampleInputFile">Uploaed Product CSV</label>
                  <input type="file" class="imagerequired" id="product_csv" name="product_csv">

                  <p class="help-block ">upload your csv here.</p>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputFile">Uploaed Product Image Zip</label>
                        <input type="file" class="imagerequired" id="product_image" name="product_image">
                        <p class="help-block ">upload your Image Zip file.</p>
                    </div>

                      {!! Form::submit('Submit', array('class'=>'btn  btn-primary ')) !!}
                      {!! Form::close() !!}

                      </div>
                      <div class="col-md-4">
                          <div class="form-group <?php echo e($errors->has('parent_id') ? 'has-error' : ''); ?>">
                              {!! Form::open(array('url'=>'/product/sampleCSVDownload','method'=>'POST','id'=>'bluk_product_csv', 'files'=>true)) !!}
                              <div class="col-xs-12 col-sm-12 col-md-12">
                                  <select id="categories" class="chosen-select form-control selectrequired" multiple name="categories">
                                      <option value="">Select L3 Category</option>
                                      @foreach ($allcategories as $allcategory)
                                          <option value="{{$allcategory->id}}">{{$allcategory->l2catname}}/{{$allcategory->l3catname}}/{{$allcategory->name}}</option>
                                      @endforeach
                                  </select>

                              </div>
                              <span class="text-danger"><?php echo e($errors->first('parent_id')); ?></span>
                          </div>
                          <p class="help-block ">Click <input type="submit" href="{{url('/product/sampleCSVDownload')}}">Here</input> to download the sample of above mention categories </p>

                          {!! Form::close() !!}
                      </div>

                  </div>
                </div>
				<div class="box-footer">
					

				</div>
                @foreach ($responce as $res)
                    @if(strpos($res,"Succesfully") !== false)
                        <p class="success-log">{{$res}}</p>
                        @else
                        <p class="error-log" >{{$res}}</p>
                    @endif


                @endforeach
				
            </div>
        </div>
    </div>
    </div>
   
   
   <script>

       $("#bluk_product_form").validate({
           errorElement: 'span',
           errorClass: 'help-block error-help-block',

           errorPlacement: function(error, element) {
               if (element.parent('.input-group').length ||
                   element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                   error.insertAfter(element.parent());
                   // else just place the validation message immediatly after the input
               } else {
                   error.insertAfter(element);
               }
           },
           highlight: function(element) {
               $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
           },
           focusInvalid: false, // do not focus the last invalid input
           ignore:"ui-tabs-hide"});
		jQuery(document).ready(function(){
            jQuery.validator.addClassRules("imagerequired", {
                required: true,
            });
		});
   </script>

</section>
@endsection