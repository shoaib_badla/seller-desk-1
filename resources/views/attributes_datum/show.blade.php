@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show attributes_datum
    </h1>
    <form method = 'get' action = '{!!url("attributes_datum")!!}'>
        <button class = 'btn blue'>attributes_datum Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>attribute_id : </i></b>
                </td>
                <td>{!!$attributes_datum->attribute_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>value : </i></b>
                </td>
                <td>{!!$attributes_datum->value!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>approved_by : </i></b>
                </td>
                <td>{!!$attributes_datum->approved_by!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>rejected_by : </i></b>
                </td>
                <td>{!!$attributes_datum->rejected_by!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>approved_at : </i></b>
                </td>
                <td>{!!$attributes_datum->approved_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>rejected_at : </i></b>
                </td>
                <td>{!!$attributes_datum->rejected_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>reason : </i></b>
                </td>
                <td>{!!$attributes_datum->reason!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$attributes_datum->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$attributes_datum->updated_at!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection