@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        attributes_datum Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("attributes_datum")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New attributes_datum</button>
        </form>
    </div>
    <table>
        <thead>
            <th>attribute_id</th>
            <th>value</th>
            <th>approved_by</th>
            <th>rejected_by</th>
            <th>approved_at</th>
            <th>rejected_at</th>
            <th>reason</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($attributes_datas as $attributes_datum) 
            <tr>
                <td>{!!$attributes_datum->attribute_id!!}</td>
                <td>{!!$attributes_datum->value!!}</td>
                <td>{!!$attributes_datum->approved_by!!}</td>
                <td>{!!$attributes_datum->rejected_by!!}</td>
                <td>{!!$attributes_datum->approved_at!!}</td>
                <td>{!!$attributes_datum->rejected_at!!}</td>
                <td>{!!$attributes_datum->reason!!}</td>
                <td>{!!$attributes_datum->created_at!!}</td>
                <td>{!!$attributes_datum->updated_at!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/attributes_datum/{!!$attributes_datum->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/attributes_datum/{!!$attributes_datum->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/attributes_datum/{!!$attributes_datum->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $attributes_datas->render() !!}

</div>
@endsection