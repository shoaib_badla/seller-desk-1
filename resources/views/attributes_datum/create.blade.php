@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create attributes_datum
    </h1>
    <form method = 'get' action = '{!!url("attributes_datum")!!}'>
        <button class = 'btn blue'>attributes_datum Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("attributes_datum")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="attribute_id" name = "attribute_id" type="text" class="validate">
            <label for="attribute_id">attribute_id</label>
        </div>
        <div class="input-field col s6">
            <input id="value" name = "value" type="text" class="validate">
            <label for="value">value</label>
        </div>
        <div class="input-field col s6">
            <input id="approved_by" name = "approved_by" type="text" class="validate">
            <label for="approved_by">approved_by</label>
        </div>
        <div class="input-field col s6">
            <input id="rejected_by" name = "rejected_by" type="text" class="validate">
            <label for="rejected_by">rejected_by</label>
        </div>
        <div class="input-field col s6">
            <input id="approved_at" name = "approved_at" type="text" class="validate">
            <label for="approved_at">approved_at</label>
        </div>
        <div class="input-field col s6">
            <input id="rejected_at" name = "rejected_at" type="text" class="validate">
            <label for="rejected_at">rejected_at</label>
        </div>
        <div class="input-field col s6">
            <input id="reason" name = "reason" type="text" class="validate">
            <label for="reason">reason</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate">
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate">
            <label for="updated_at">updated_at</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection