<!DOCTYPE html>
<html lang="en">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url('public/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('public/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('public/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('public/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('public/css/_all-skins.min.css')}}">

    <link rel="stylesheet" href="{{url('public/css/treeview.css')}}">

    <link rel="stylesheet" href="{{url('public/css/scpcustom.css')}}">

    <link href="{{url('public/css/jquery.ui.autocomplete.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{url('public/js/html5shiv.min.js')}}"></script>
    <script src="{{url('public/js/respond.min.js')}}"></script>
    <![endif]-->
	
	<!-- Compiled and minified JavaScript -->
	<script src="{{url('public/js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{url('public/js/jquery.js')}}"></script>
    <script src="{{url('public/js/jquery-ui.min.js')}}"></script>
	
	
	<script type="text/javascript" src="{{ asset('js/custom_method.js' )}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation.js' )}}"></script>
</head>
<body >

    <div class="">
        @yield('content')
    </div>

<script src="{{url('public/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/js/app.min.js')}}"></script>
<script> var baseURL = "{{ URL::to('/') }}"</script>
<script src = "{{url('public/js/AjaxisBootstrap.js') }}"></script>
 <script type="text/javascript">
  // MAKE TEXT FIELD WRITABLE ON MODAL
	$.fn.modal.Constructor.prototype.enforceFocus = function () {};
 </script>
</body>
</html>
