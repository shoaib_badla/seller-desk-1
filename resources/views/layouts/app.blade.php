<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url('public/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('public/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('public/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('public/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('public/css/_all-skins.min.css')}}">

    <link rel="stylesheet" href="{{url('public/css/treeview.css')}}">

    <link rel="stylesheet" href="{{url('public/css/scpcustom.css')}}">
    <link rel="stylesheet" href="{{url('public/css/custom_style.css')}}">

    <link href="{{url('public/css/jquery.ui.autocomplete.css')}}" rel="stylesheet">
  
	<link href="{{url('public/css/ui.jqgrid-bootstrap.css')}}" rel="stylesheet">
	<link href="{{url('public/css/ui.jqgrid-bootstrap-ui.css')}}" rel="stylesheet">
	<link href="{{url('public/css/ui.jqgrid.css')}}" rel="stylesheet">
    <link href="{{url('public/css/jquery-ui-1.10.3.custom.css')}}" rel="stylesheet">
	<link href="{{url('public/css/jquery.ui.autocomplete.css')}}" rel="stylesheet">
    <link href="{{url('public/css/chosen.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{url('public/js/html5shiv.min.js')}}"></script>
    <script src="{{url('public/js/respond.min.js')}}"></script>
    <![endif]-->
	
	
	
	
	
	<!-- Compiled and minified JavaScript -->
	<script src="{{url('public/js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{url('public/js/jquery.js')}}"></script>
    <script src="{{url('public/js/jquery-ui.min.js')}}"></script>

    <script src="{{url('public/js/enyo.dropzone.js')}}"></script>
    <script src="{{url('public/js/laradrop.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/custom_method.js' )}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jqGrid/i18n/grid.locale-en.js' )}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jqGrid/jquery.jqGrid.min.js' )}}"></script>
	
	
	<script type="text/javascript" src="{{ asset('vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation.js' )}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

	<?php
		//GET USER ROLE INFO
		$userRole = \App\User::findOrfail(Auth::user()->id);
		
		$vendorManagerRole				=	Config::get('constants.vendorManagerRole');
		$vendorRole				=	Config::get('constants.vendorRole');
		$superadminRole			=	Config::get('constants.superadminRole');
		$headOfVendor			=	Config::get('constants.headOfVendor');
		$categoryManagerRole	=	Config::get('constants.categoryManagerRole');
		$financeRole			=	Config::get('constants.financeRole');
		$operationRole			=	Config::get('constants.operationRole');
	?>
    <header class="main-header">
        <!-- Logo -->
        <a href="{{url('home')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>CP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Seller Desk</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Notification Navbar List-->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label notification-label">new</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Your notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu notification-menu">
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- END notification navbar list-->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="http://ahloman.net/wp-content/uploads/2013/06/user.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="http://ahloman.net/wp-content/uploads/2013/06/user.jpg" class="img-circle" alt="User Image">
                                <p>
                                    {{Auth::user()->name}}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{url('logout')}}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">Sign out</a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
								<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
							</span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="{{url('dashboard')}}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                    </a>
                </li>
				<?php
					if($userRole->hasRole($superadminRole) ){
				?>
                <li class="header">ADMINISTRATOR</li>
                <li class="treeview"><a href="{{url('/users')}}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                <li class="treeview"><a href="{{url('/roles')}}"><i class="fa fa-user-plus"></i> <span>Role</span></a></li>
                <li class="treeview"><a href="{{url('/permissions')}}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
                <li class="treeview"><a href="{{url('/category')}}"><i class="fa fa-key"></i> <span>Category</span></a></li>
               <?php
					}
			   ?>
			   
			   
				<?php

					
					//IF USER IS VENDOR MANAGER
					
					if($userRole->hasRole($vendorManagerRole) || $userRole->hasRole($superadminRole) ||  $userRole->hasRole($vendorRole) ){
				?>
				<li class="treeview">
				  <a href="#">
					<i class="fa fa-dashboard"></i> <span>Product</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
				  </a>
				  <ul class="treeview-menu" style="display: none;">
                      <li><a href="{{url('/product')}}"><i class="fa fa-key"></i> <span>Manage Products</span></a></li>
                      <li><a href="{{url('/product/bulkproduct')}}"><i  class="fa fa-file-excel-o"></i>Upload CSV</a></li>
                      <li><a href="{{url('/products/approvalpending')}}"><i  class="fa fa-file-excel-o"></i>Approval Pending</a></li>
                      <li><a href="{{url('/products/approval')}}"><i  class="fa fa-file-excel-o"></i>Approval</a></li>
                      <li><a href="{{url('/products/productreject')}}"><i  class="fa fa-file-excel-o"></i>Reject</a></li>
                      <li><a href="{{url('/products/storefrontpush')}}"><i  class="fa fa-file-excel-o"></i>Store Front Push</a></li>
				  </ul>
				</li>
				
				<?php
					}
				?>
				
				
				<?php

					
					//IF USER IS VENDOR MANAGER
					
					if($userRole->hasRole($headOfVendor) || $userRole->hasRole($superadminRole) || $userRole->hasRole($categoryManagerRole)  || $userRole->hasRole($vendorManagerRole) || $userRole->hasRole($operationRole) ||  $userRole->hasRole($financeRole) ){
				?>
						<li class="treeview">
						  <a href="#">
							<i class="fa fa-users"></i> <span>Vendor Management</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu" style="display: none;">
						    <?php if($userRole->hasRole($headOfVendor) || $userRole->hasRole($superadminRole) ){?>
								<li><a href="{{url('/vendor_info')}}"><i  class="fa fa-bars"></i>Vendor Application Request</a></li>
							<?php } ?>
							 <?php if($userRole->hasRole($headOfVendor) || $userRole->hasRole($categoryManagerRole)  || $userRole->hasRole($vendorManagerRole)  ){?>
								<li><a href="{{url('/vendor_application_request')}}"><i  class="fa  fa-street-view"></i>Vendor Lead Request</a></li>
							<?php } ?>
							 <?php if($userRole->hasRole($operationRole) ||  $userRole->hasRole($financeRole) ){?>
								<li><a href="{{url('/vendor_application_review')}}"><i  class="fa  fa-street-view"></i>Vendor Lead Review</a></li>
							<?php } ?>
						
							<!-- <li><a href="{{url('/vendor_info/create')}}"><i class="fa fa-user-plus"></i> <span>Add New Vendor</span></a></li> -->
						  </ul>
						</li>
				<?php
					}
				?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        @yield('content')
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class = 'AjaxisModal'>
    </div>
</div>

<script src="{{url('public/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/js/app.min.js')}}"></script>
<script src="{{url('public/js/demo.js')}}"></script>
<script> var baseURL = "{{ URL::to('/') }}"</script>
<script src = "{{url('public/js/AjaxisBootstrap.js') }}"></script>
<script src = "{{url('public/js/customA.js') }}"></script>

<script src = "{{url('public/js/chosen.jquery.js') }}"></script>

<script src="{{url('public/js/treeview.js')}}"></script>
<script src="{{url('public/js/pusher.min.js')}}"></script>
<?php /*
<script>
    // pusher log to console.
    Pusher.logToConsole = true;
    // here is pusher client side code.
    var pusher = new Pusher("{{env("PUSHER_KEY")}}", {
        encrypted: true
    });
    var channel = pusher.subscribe('test-channel');
    channel.bind('test-event', function(data) {
        // display message coming from server on dashboard Notification Navbar List.
        $('.notification-label').addClass('label-warning');
        $('.notification-menu').append(
                '<li>\
                            <a href="#">\
                                        <i class="fa fa-users text-aqua"></i> '+data.message+'\
						</a>\
			</li>'
        );
    });
</script>
*/?>

<!-- AJAX LOADER START-->

<style>
.ajax-loader-region{
                position:fixed;
                top:0;
                left:0;
                background:#ddd;
                opacity:0.8;
                z-index:9998;
                height:100%;
                width:100%;
				display:none;
				}
				
 #ajax-loader-region-content{
		  position:absolute;
		  color:black;
		  top:50%;
		  left:50%;
		  padding:15px;
		  -ms-transform: translateX(-50%) translateY(-50%);
		  -webkit-transform: translate(-50%,-50%);
		  transform: translate(-50%,-50%);
		}
</style>
<div class="ajax-loader-region">
	<div id="ajax-loader-region-content">
	{{ Html::image('img/ajax-loader.gif') }}
	</div>
</div>
<script>
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
	jQuery( document ).ajaxStart(function() {
	  jQuery('.ajax-loader-region').show();
	});

	jQuery( document ).ajaxStop(function() {
	  jQuery('.ajax-loader-region').hide();
	});


</script>

<!-- AJAX LOADER END-->

<!-- MODAL START-->
<!-- Button trigger modal -->


<!-- Modal -->
<!-- Large modal -->

<div class="modal fade custom-modal-view"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog custom-modal-dialog modal-lg" role="document">
   <div class="modal-content"> 
   <div class="modal-header"> 
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<h4 class="modal-title custom-modal-title" id="myLargeModalLabel">Large modal</h4> 
   </div> 
	<div class="modal-body custom-modal-body">  </div> 
   </div>
  </div>
</div>
<!-- MODAL END -->
<!-- Modal -->
<!-- Large modal -->

<div class="modal fade customTwo-modal-view"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog customTwo-modal-dialog modal-lg" role="document">
   <div class="modal-content"> 
   <div class="modal-header"> 
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<h4 class="modal-title customTwo-modal-title" id="myLargeModalLabel">Large modal</h4> 
   </div> 
	<div class="modal-body customTwo-modal-body">  </div> 
   </div>
  </div>
</div>
<!-- MODAL END -->

<!-- MODAL CONFIRM BOX -->


<div class="modal fade confirm"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="confirmBoxModal" >
  <div class="modal-dialog" role="document">
   <div class="modal-content"> 
   <div class="modal-header"> 
	<button type="button" class="close closeCnfButton" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<h4 class="modal-title confirm-modal-title" >Are you sure ?</h4> 
   </div> 
	<div class="modal-body confirm-modal-body">  </div> 
	 <div class="modal-footer ">
		<span class="confirm-modal-footer">
			<button type="button"  class="btn btn-primary" id="delete">Yes</button>
			<button type="button" data-dismiss="modal" class="btn closeCnfButton">No</button>
		</span>
		<span class="confirm-modal-footerWaiting" style="display:none;">
			<i class="fa fa-refresh fa-spin"></i>Please Wait...
		</span>
     </div>
   </div>
  </div>
</div>


 <script type="text/javascript">
  // MAKE TEXT FIELD WRITABLE ON MODAL
	$.fn.modal.Constructor.prototype.enforceFocus = function () {};
	
	//REFRESH MODAL ON CLOSE
	jQuery(document).ready(function(){
		jQuery(".modal").on("hidden.bs.modal", function(){
			jQuery(this).find("modal-body").html("");
				
		});
		
		
	});
	
	
 </script>
 <!-- MODAL CONFIRM BOX -->
</body>
</html>
