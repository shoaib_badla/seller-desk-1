<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{url('public/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('public/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('public/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('public/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('public/css/_all-skins.min.css')}}">

    <link rel="stylesheet" href="{{url('public/css/treeview.css')}}">

    <link rel="stylesheet" href="{{url('public/css/scpcustom.css')}}">
	<link rel="stylesheet" href="{{url('public/css/custom_style.css')}}">

    <link href="{{url('public/css/jquery.ui.autocomplete.css')}}" rel="stylesheet">
  
	<link href="{{url('public/css/ui.jqgrid-bootstrap.css')}}" rel="stylesheet">
	<link href="{{url('public/css/ui.jqgrid-bootstrap-ui.css')}}" rel="stylesheet">
	<link href="{{url('public/css/ui.jqgrid.css')}}" rel="stylesheet">
    <link href="{{url('public/css/jquery-ui-1.10.3.custom.css')}}" rel="stylesheet">
	<link href="{{url('public/css/jquery.ui.autocomplete.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{url('public/js/html5shiv.min.js')}}"></script>
    <script src="{{url('public/js/respond.min.js')}}"></script>
    <![endif]-->
	
	
	
	
	
	<!-- Compiled and minified JavaScript -->
	<script src="{{url('public/js/jquery-2.1.1.min.js')}}"></script>
    <script src="{{url('public/js/jquery.js')}}"></script>
    <script src="{{url('public/js/jquery-ui.min.js')}}"></script>

    <script src="{{url('public/js/enyo.dropzone.js')}}"></script>
    <script src="{{url('public/js/laradrop.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/custom_method.js' )}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jqGrid/i18n/grid.locale-en.js' )}}"></script>
	<script type="text/javascript" src="{{ asset('public/js/jqGrid/jquery.jqGrid.min.js' )}}"></script>
	
	
	<script type="text/javascript" src="{{ asset('vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation.js' )}}"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="">

	<?php
		//GET USER ROLE INFO
		$userRole = \App\User::findOrfail(Auth::user()->id);
		
		$roleName	=	Config::get('constants.vendorManagerRole');
		$vendorRole	=	Config::get('constants.vendorRole');
		$superadminRole	=	Config::get('constants.superadminRole');
	?>
    <header class="main-header">
        <!-- Logo -->
        <a href="{{url('home')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b>CP</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Seller Desk</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Notification Navbar List-->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label notification-label">new</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Your notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu notification-menu">
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- END notification navbar list-->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="http://ahloman.net/wp-content/uploads/2013/06/user.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="http://ahloman.net/wp-content/uploads/2013/06/user.jpg" class="img-circle" alt="User Image">
                                <p>
                                    {{Auth::user()->name}}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="{{url('logout')}}" class="btn btn-default btn-flat"
                                       onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">Sign out</a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
   
    <div class="">
        @yield('content')
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class = 'AjaxisModal'>
    </div>
</div>

<script src="{{url('public/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/js/app.min.js')}}"></script>
<script src="{{url('public/js/demo.js')}}"></script>
<script> var baseURL = "{{ URL::to('/') }}"</script>
<script src = "{{url('public/js/AjaxisBootstrap.js') }}"></script>
<script src = "{{url('public/js/customA.js') }}"></script>
<script src="{{url('public/js/treeview.js')}}"></script>
<script src="{{url('public/js/pusher.min.js')}}"></script>


<!-- AJAX LOADER START-->

<style>
.ajax-loader-region{
                position:fixed;
                top:0;
                left:0;
                background:#ddd;
                opacity:0.8;
                z-index:9998;
                height:100%;
                width:100%;
				display:none;
				}
				
 #ajax-loader-region-content{
		  position:absolute;
		  color:black;
		  top:50%;
		  left:50%;
		  padding:15px;
		  -ms-transform: translateX(-50%) translateY(-50%);
		  -webkit-transform: translate(-50%,-50%);
		  transform: translate(-50%,-50%);
		}
</style>
<div class="ajax-loader-region">
	<div id="ajax-loader-region-content">
	{{ Html::image('img/ajax-loader.gif') }}
	</div>
</div>
<script>
	jQuery( document ).ajaxStart(function() {
	  jQuery('.ajax-loader-region').show();
	});

	jQuery( document ).ajaxStop(function() {
	  jQuery('.ajax-loader-region').hide();
	});


</script>

<!-- AJAX LOADER END-->

<!-- MODAL START-->
<!-- Button trigger modal -->


<!-- Modal -->
<!-- Large modal -->

<div class="modal fade custom-modal-view"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog custom-modal-dialog modal-lg" role="document">
   <div class="modal-content"> 
   <div class="modal-header"> 
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<h4 class="modal-title custom-modal-title" id="myLargeModalLabel">Large modal</h4> 
   </div> 
	<div class="modal-body custom-modal-body">  </div> 
   </div>
  </div>
</div>
<!-- MODAL END -->

<!-- MODAL CONFIRM BOX -->


<div class="modal fade confirm"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog" role="document">
   <div class="modal-content"> 
   <div class="modal-header"> 
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	<h4 class="modal-title confirm-modal-title" >Are you sure ?</h4> 
   </div> 
	<div class="modal-body confirm-modal-body">  </div> 
	 <div class="modal-footer ">
		<span class="confirm-modal-footer">
			<button type="button"  class="btn btn-primary" id="delete">Yes</button>
			<button type="button" data-dismiss="modal" class="btn">No</button>
		</span>
		<span class="confirm-modal-footerWaiting" style="display:none;">
			<i class="fa fa-refresh fa-spin"></i>Please Wait...
		</span>
     </div>
   </div>
  </div>
</div>


 
 <!-- MODAL CONFIRM BOX -->
</body>
</html>
