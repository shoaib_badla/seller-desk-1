@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show attributes_mapping
    </h1>
    <form method = 'get' action = '{!!url("attributes_mapping")!!}'>
        <button class = 'btn blue'>attributes_mapping Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>attribute_set_id : </i></b>
                </td>
                <td>{!!$attributes_mapping->attribute_set_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>attribute_id : </i></b>
                </td>
                <td>{!!$attributes_mapping->attribute_id!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection