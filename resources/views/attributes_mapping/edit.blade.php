@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit attributes_mapping
    </h1>
    <form method = 'get' action = '{!!url("attributes_mapping")!!}'>
        <button class = 'btn blue'>attributes_mapping Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("attributes_mapping")!!}/{!!$attributes_mapping->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="attribute_set_id" name = "attribute_set_id" type="text" class="validate" value="{!!$attributes_mapping->
            attribute_set_id!!}"> 
            <label for="attribute_set_id">attribute_set_id</label>
        </div>
        <div class="input-field col s6">
            <input id="attribute_id" name = "attribute_id" type="text" class="validate" value="{!!$attributes_mapping->
            attribute_id!!}"> 
            <label for="attribute_id">attribute_id</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection