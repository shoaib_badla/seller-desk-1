@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        attributes_mapping Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("attributes_mapping")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New attributes_mapping</button>
        </form>
    </div>
    <table>
        <thead>
            <th>attribute_set_id</th>
            <th>attribute_id</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($attributes_mappings as $attributes_mapping) 
            <tr>
                <td>{!!$attributes_mapping->attribute_set_id!!}</td>
                <td>{!!$attributes_mapping->attribute_id!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/attributes_mapping/{!!$attributes_mapping->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/attributes_mapping/{!!$attributes_mapping->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/attributes_mapping/{!!$attributes_mapping->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $attributes_mappings->render() !!}

</div>
@endsection