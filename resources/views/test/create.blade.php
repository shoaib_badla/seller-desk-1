@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create test
    </h1>
    <form method = 'get' action = '{!!url("test")!!}'>
        <button class = 'btn blue'>test Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("test")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="firstname" name = "firstname" type="text" class="validate">
            <label for="firstname">firstname</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection