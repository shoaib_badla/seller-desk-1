@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show test
    </h1>
    <form method = 'get' action = '{!!url("test")!!}'>
        <button class = 'btn blue'>test Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>firstname : </i></b>
                </td>
                <td>{!!$test->firstname!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection