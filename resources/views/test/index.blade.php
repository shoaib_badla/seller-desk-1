@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        test Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("test")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New test</button>
        </form>
    </div>
    <table>
        <thead>
            <th>firstname</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($tests as $test) 
            <tr>
                <td>{!!$test->firstname!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/test/{!!$test->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/test/{!!$test->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/test/{!!$test->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $tests->render() !!}

</div>
@endsection