@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit test
    </h1>
    <form method = 'get' action = '{!!url("test")!!}'>
        <button class = 'btn blue'>test Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("test")!!}/{!!$test->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="firstname" name = "firstname" type="text" class="validate" value="{!!$test->
            firstname!!}"> 
            <label for="firstname">firstname</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection