@extends('layouts.vendormargin')
@section('title','Index')
@section('content')


<div class = 'container'  style="text-align:center">
   
	<div class='welcome-region'>
	   <h1 >
			Welcome to Seller Center
		</h1>
		
		<div class="row">
				<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
				<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
		</div>
		<div>
			<button type="button" class="btn btn-primary margin nextbtn"> Next </button>
		</div>
	</div>
	
	<div class='term-condition-region' style="display:none" >
		<h1 >
			Please read our terms and condition
		</h1>
		
		<div class="row" style="max-height:200px; overflow: scroll;">
				<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
				<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
					<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
					<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
					<p>
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
		</div>
		<div >
			<span class="btn-region">
				<button type="button" class="btn btn-default margin dontAgreeBtn"> I Don't Agree </button>
				<button type="button" class="btn btn-primary margin agreeBtn"> Agree </button>
			<span>
			<span class='form-waiting-region' style="display:none;"><i class="fa fa-refresh fa-spin"></i>Please Wait...</span>
		</div>
	</div>
</div>
<script>
	jQuery('document').ready(function(){
		
		jQuery('.nextbtn').on('click',function(){
			
			jQuery('.welcome-region').hide();
			jQuery('.term-condition-region').show();
		});
		
		jQuery('.dontAgreeBtn').on('click',function(){
			
			jQuery('.welcome-region').show();
			jQuery('.term-condition-region').hide();
		});
		
		//REDIRECT TO MARGIN SCREEN
		jQuery('.agreeBtn').on('click',function(){
			jQuery('.btn-region').hide();
			jQuery('.form-waiting-region').show();
			window.location.replace("<?php echo url('vendordetail');?>");
		});
	});
</script>
@endsection