<script>
	//ADD NEW PICKUP ADDREE
	var pickupAddreesLimit = 1;
jQuery(document).ready(function(){

	//jQuery( ".pickup-address-region:last-child" )
	//displayChildHideAndShowBtn();
	displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');
	displayChildHideAndShowBtn('tab5primary','category-level-region1','addCategory','removeCategory');
	

	
	jQuery(document).on( 'click', '.addPickupAddress', function() {
		
		//PICKUP ADDRESS LIMIT 
		if(pickupAddreesLimit >=5){
			return false;
		}
		
		var pickupAddressHtml	=	jQuery('.pickup-address-region').clone(true);
        
		pickupAddressHtml =   pickupAddressHtml.find("input:text").val("").end();
		pickupAddressHtml =   pickupAddressHtml.find("select").val("").end();
		pickupAddressHtml =   pickupAddressHtml.find("textarea").val("").end();
		pickupAddressHtml =   pickupAddressHtml.find("span.maxcharacterLimit").html("").end();
        pickupAddressHtml.appendTo('#tab4primary');
		
		
		//REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
		jQuery('#tab4primary').find("div.pickup-address-region" ).first().removeClass('pickup-address-region');
		
		//HIDE AND SHOW BUTTON
		displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');
		

		pickupAddreesLimit++;
	});
	
	//REMOVE PICKUP Address
	jQuery('.removePickupAddress').on('click',function(){
		
		jQuery(this).closest(".pickup-address-region1").remove();
		//ADD CLASS FOR MAKING CLONE
		//REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
		jQuery('#tab4primary').find("div.pickup-address-region1" ).first().addClass('pickup-address-region');
		//HIDE AND SHOW BUTTON
		displayChildHideAndShowBtn('tab4primary','pickup-address-region1','addPickupAddress','removePickupAddress');
		
		pickupAddreesLimit--;
	});
	
	//CATGEORY DD AND REMOVE FUNCTIONALITY
		//ADD NEW CATGEORY 
	jQuery(document).on( 'click', '.addCategory', function() {
	
		var pickupAddressHtml	=	jQuery('.category-level-region').clone(true);
        
		pickupAddressHtml =   pickupAddressHtml.find("input:text").val("").end();
        pickupAddressHtml.appendTo('#tab5primary');
		
		
		//REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
		jQuery('#tab5primary').find("div.category-level-region" ).first().removeClass('category-level-region');
		
		//HIDE AND SHOW BUTTON
		displayChildHideAndShowBtn('tab5primary','category-level-region1','addCategory','removeCategory');
		
	});
	
	//REMOVE CATGEORY
	jQuery('.removeCategory').on('click',function(){
		jQuery(this).closest(".category-level-region1").remove();
		//ADD CLASS FOR MAKING CLONE
		//REMOVE CLASS FROM PREVIOUS DIV FOR CLONE
		jQuery('#tab5primary').find("div.category-level-region1" ).first().addClass('category-level-region');
		//HIDE AND SHOW BUTTON
		displayChildHideAndShowBtn('tab5primary','category-level-region1','addCategory','removeCategory');
	});
});

		function validateFieldAndShowErrorMessage(fieldType,fieldName,errMessageDisplay){
			var errOccur = false;
			var errorArray = [];
			var loop = 0;
			 jQuery(fieldType+"[name^='"+fieldName+"']").each(function () {
				 var inpValue	=	jQuery.trim(jQuery(this).val());
				 jQuery(".err-region-"+fieldName+loop).remove();
				 if(inpValue==""){
					 jQuery( "<p style='color:red;' id='tab4primary' class='errRegionArea error-help-block err-region-"+fieldName+loop+"'>"+errMessageDisplay+"</p>" ).insertAfter( this );
					  jQuery(this).closest('.form-group').removeClass('has-success').addClass('has-error');
					 errorArray.push(true);
				 }else{
				    jQuery(this).closest('.form-group').removeClass('has-error').addClass('has-success'); 
					errorArray.push(false);	 
				 }
				
				loop++;
			});
			
			
			//CHECK ERROR OCCUR ON ARRAY
			jQuery(errorArray).each(function(key, errorOccur){
				if(errorOccur){
					errOccur = errorOccur;
				}
			});
			
			return errOccur;
		}


	//DISPLAY RANGE OF ACCEPTANCE DAY
	var acceptance_of_return_Region	=	jQuery('#acceptance_of_return').val();
		
	if(acceptance_of_return_Region=="Yes"){
		
		jQuery('.acceptance_of_return_rangeRegion').show();
		jQuery('.acceptance_of_return_Region').addClass('col-xs-8');
	}
	if(acceptance_of_return_Region=="No"){
				jQuery('.field.acceptance_of_return_Region').addClass('imfull');
	}	
		
//VALIDATION FORM SUBMISION PUT HERE BECAUSE OF TOKEN
	jQuery(document).ready(function(){
		
		
		
		//HIDE & SHOW RETURN ACCEPTANCE DEPEND ON OPTION
		jQuery('#acceptance_of_return').on('change',function(){
			
			var acceptance_of_return_Region	=	jQuery.trim(jQuery(this).val());
			
			if(acceptance_of_return_Region=="Yes"){
				jQuery('.field.acceptance_of_return_Region').removeClass('imfull');
				jQuery('.acceptance_of_return_rangeRegion').show();
				jQuery('.acceptance_of_return_Region').addClass('col-xs-8');
			}
			if(acceptance_of_return_Region=="No"){
				jQuery('.field.acceptance_of_return_Region').addClass('imfull');
				jQuery('.acceptance_of_return_Region').removeClass('col-xs-8');
				jQuery('.acceptance_of_return_rangeRegion').hide();
			}	
		});
		
		/**
		* FORM VALIDATION START
		*/

        jQuery("#addVendorDetailInfo").validate({
            errorElement: 'span',
            errorClass: 'help-block error-help-block',

            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length ||
                    element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
            },

            
            /*
             // Uncomment this to mark as validated non required fields
             unhighlight: function(element) {
             $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
             },
             */
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // remove the Boostrap error class from the control group
            },

            focusInvalid: false, // do not focus the last invalid input
            ignore:"ui-tabs-hide",
            rules:  {
					"vendor_name" :{
						"laravelValidation": [
								 ["Required", [], "Vendor Name is required.", true],
								  ["Max", ["80"], "Vendor Name may not be greater than 80 characters.", false],
								  ["Min", ["3"], "Vendor Name may not be lesser than 3 characters", false]
							 ]
					},
					"cnic" :{
						"laravelValidation": [
								 ["Required", [], "CNIC is required.", true],
								  ["Max", ["13"], "Please provide valid CNIC.", false],
								  ["Min", ["13"], "Please provide valid CNIC", false]
							 ]
					},
					"vendor_email" :{
						"laravelValidation": [
								 ["Required", [], "Company/Individual Name/(Registered Name)  is required.", true],
								 ["Email", [], "Company/Individual Name/(Registered Name)  is invalid.", true],
							 ]
					},
					"poc_email" :{
						"laravelValidation": [
								 ["Required", [], "POC Email is required.", true],
								 ["Email", [], "POC Email is invalid.", true],
							 ]
					},
							 
					"poc_name" :{
						"laravelValidation": [
								 ["Required", [], "POC Name is required.", true],
								  ["Max", ["80"], "POC Name may not be greater than 80 characters.", false],
								  ["Min", ["3"], "POC Name may not be lesser than 3 characters", false]
							 ]
					},					
							 
					"form_of_organization" :{
						"laravelValidation": [
								 ["Required", [], "Form Of Organization is required.", true],
							 ]
					},						 
					"poc_contact" :{
						"laravelValidation": [
								 ["Required", [], "POC Contact is required.", true],
								  ["Max", ["10"], "POC Contact may not be greater than 10 characters.", false],
								  ["Min", ["10"], "POC Contact is Invalid", false]
							 ]
					},					
												
					"country" :{
						"laravelValidation": [
								 ["Required", [], "Country is required.", true],
							 ]
					},				
							 
					"province" :{
						"laravelValidation": [
								 ["Required", [], "Province is required.", true],
							 ]
					},				 
							
					"cell_phone" :{
						"laravelValidation": [
								 ["Required", [], "Cell Number is required.", true],
								  ["Max", ["10"], "Cell Number may not be greater than 10 characters.", false],
								  ["Min", ["10"], "Cell Number is Invalid", false]
							 ]
					},					 
							
					"city" :{
						"laravelValidation": [
								 ["Required", [], "City is required.", true],
							 ]
					},					
						
					"ntn1" :{
						"laravelValidation": [
								 ["Required", [], "NTN is required.", true],
								  ["Max", ["7"], "NTN is Invalid", false],
								  ["Min", ["7"], "NTN is Invalid", false]
							 ]
					},	
					"ntn2" :{
						"laravelValidation": [
								 ["Required", [], "NTN is required.", true],
								  ["Max", ["1"], "NTN is Invalid", false],
								  ["Min", ["1"], "NTN is Invalid", false]
							 ]
					},			
					"office_phone" :{
						"laravelValidation": [
								 ["Required", [], "Office Phone is required.", true],
								  ["Max", ["10"], "Office Phone is Invalid", false],
								  ["Min", ["10"], "Office Phone is Invalid", false]
							 ]
					},				
					"company_address" :{
						"laravelValidation": [
								 ["Required", [], "Address (Company Place of Business)  is required.", true],
							 ]
					},			

					 "shop_online_name": {
						 "laravelValidation": [
							 ["Required", [], "The Shop Online Name field is required.", true],
							 ["Max", ["100"], "The Shop Online Name may not be greater than 100 characters.", false]
						 ]
					 },
					"company_owner_name": {
						 "laravelValidation": [
							 ["Required", [], "The Company Owner Name field is required.", true],
							 ["Max", ["80"], "The Company Owner Name may not be greater than 80 characters.", false]
							
						 ]
					 },
					 "authorize_person_cnic": {
						 "laravelValidation": [
							 //["Numeric", [], "The Authorize Person CNIC must be a number.", false],
							 ["Min", ["13"], "Please enter valid Authorize Person CNIC Number", false]
						 ]
					 },
					 "company_website": {
						 "laravelValidation": [
							 ["Required", [], "The Company Website field is required.", true],
							 ["Url",[],"The Company Website URL is invalid.",false],
							 ["Max", ["255"], "The Company Website may not be greater than 255 characters.", false]
						 ]
					 },
					 "payment_cycle": {
						 "laravelValidation": [
							 ["Required", [], "The Billing Cycle field is required.", true]
						 ]
					 },
					 "acceptance_of_return": {
						 "laravelValidation": [
							 ["Required", [], "The Acceptance of Return field is required.", true]
						 ]
					 },
					 "offer_exchange": {
						 "laravelValidation": [
							 ["Required", [], "The Offer Exchanges field is required.", true]
						 ]
					 },
					 "withholding_tax_dposited_by": {
						 "laravelValidation": [
							 ["Required", [], "The Witholding tax deposited by field is required.", true]
						 ]
					 },
					 "internation_shipping": {
						 "laravelValidation": [
							 ["Required", [], "The International Shipping field is required.", true]
						 ]
					 },
					 "business_model": {
						 "laravelValidation": [
							 ["Required", [], "The Business Model field is required.", true]
						 ]
					 },
					 "company_individual_registered_address": {
						 "laravelValidation": [
							 ["Required", [], "The Company / Individual Registered Addressed field is required.", true]
						 ]
					 },
					 "return_address": {
						 "laravelValidation": [
							 ["Required", [], "The Address (for returns) field is required.", true]
						 ]
					 },
					 "return_province": {
						 "laravelValidation": [
							 ["Required", [], "The Return Province field is required.", true]
						 ]
					 },
					 "return_city": {
						 "laravelValidation": [
							 ["Required", [], "The Return City field is required.", true]
						 ]
					 },
					"bank_name": {
						 "laravelValidation": [
							 ["Max", ["150"], "The Bank Name may not be greater than 150 characters.", false],
							 ["Min", ["3"], "The Bank Name may not be lesser than 3 characters.", false],
							
						 ]
					 },
					 "bank_account_title": {
						 "laravelValidation": [
							 ["Max", ["150"], "The Bank Account Title may not be greater than 150 characters.", false],
							 ["Min", ["3"], "The Bank Account Title may not be lesser than 3 characters.", false],
							
						 ]
					 },
					 "bank_account_number": {
						 "laravelValidation": [
							 ["Max", ["40"], "The Bank Account Number may not be greater than 40 characters.", false],
							 ["Min", ["8"], "The Bank Account Number may not be lesser than 8 characters.", false],
							
						 ]
					 },
					 "strn": {
						 "laravelValidation": [
							 ["Max", ["20"], "The STRN may not be greater than 20 characters.", false],
							 ["Min", ["3"], "The STRN may not be lesser than 3 characters.", false],
							
						 ]
					 },
					 "authorize_person_name": {
						 "laravelValidation": [
							 ["Max", ["150"], "The Authorize Person Name may not be greater than 150 characters.", false],
							 ["Min", ["3"], "The Authorize Person Name may not be lesser than 3 characters.", false],
							
						 ]
					 },
				 }
		 });
				 
		jQuery('#submit').on('click',function(){
		   
		   
			
			//Validate Dynamic Generated Field
			var pickup_address_Err = validateFieldAndShowErrorMessage('textarea','pickup_address','Pickup Address is required');
			var pickup_city_Err = validateFieldAndShowErrorMessage('select','pickup_city','Pickup City is required');
			var pickup_province_Err = validateFieldAndShowErrorMessage('select','pickup_province','Pickup City is required');
		
			//CHECK L3 Catgeory Is SELECTED
			var selectedL3CategoryForMargin = getSelectedL3Category();
			var L3Selected = true;
			
			//CHECK IF ALL AGREE FOR OTHER ALL CATEGRY THEN L3 is not required
			var agreeForAllOtherCatgeory			=	jQuery('#agree_for_all_other_catgeory').is(":checked");
			var agreeForAllOtherCatgeoryRegionShow	=	jQuery('.selectedCatgeoryRegion').is(":visible");
			
			
			// CHECK NO L3 SELECTED 
			if (selectedL3CategoryForMargin.length === 0 ) {
				
				//NO REGION SHOW
				if(agreeForAllOtherCatgeoryRegionShow == false){
					L3Selected = false;
					jQuery('.catgeorySelectionErr').show();
				}
				//IF NOT AGREE FOR ALL OTHER ITEM SELECT ATLEASE ONE CATEGORY AND AGREE REGION VISIBLE
				else if(agreeForAllOtherCatgeory == false && agreeForAllOtherCatgeoryRegionShow == true){
					L3Selected = false;
					jQuery('.catgeorySelectionErr').show();
				}else{
					jQuery('.catgeorySelectionErr').hide();
				}
				
			}else{console.log('hre');
				jQuery('.catgeorySelectionErr').hide();
			}
			
			var formValid = jQuery('#addVendorDetailInfo').valid();
			

			//CHECK FORM IS VALID AND NO ERROR OCCUR
			if(formValid &&  !pickup_address_Err && !pickup_city_Err && !pickup_province_Err && L3Selected ){
				jQuery('#addVendorDetailInfo').submit();
				jQuery('.jserrorregion').hide();
				
				jQuery('.btn-submit').hide();
				jQuery('.form-waiting-region').show();
				
			}else{
				jQuery('.jserrorregion').show();
				
				//ACTIVE TAB CONTAINING ERROR
				//
				//GET CLOSEST TAB WITH ERROR 
				jQuery('.error-help-block').each(function(){
					var errorText 	= jQuery(this).text();
					if(errorText != ""){
						
						var errorTabId	=	jQuery('#'+jQuery(this).attr('id')).closest('div.tab-pane').attr('id');
						
						jQuery('.'+errorTabId).click();
						 return false; 
					}
				});
				
				
				jQuery('.btn-submit').show();
				jQuery('.form-waiting-region').hide();
			}
							
		});
	});
	
	/**
	* NUMERIC INPUT
	*/

	//jQuery(".numericOnlyField").on('keypress',function (e) {
	jQuery( "body" ).delegate( ".numericOnlyField", "keypress", function(e) {
		
		jQuery(this).val(jQuery(this).val().replace(/\D/g,''));
		
		 return !(e.which != 8 && e.which != 0 &&
					(e.which < 48 || e.which > 57) && e.which != 46);
	});
	jQuery( "body" ).delegate( ".numericOnlyField", "keyup", function(e) {
		
		jQuery(this).val(jQuery(this).val().replace(/\D/g,''));
		
		 return !(e.which != 8 && e.which != 0 &&
					(e.which < 48 || e.which > 57) && e.which != 46);
	});
	


	//FIXED CNIC LENGTH
	jQuery('.maxCharacterLimit').keydown(function(e){
		
		var maxLengthAllowed = jQuery(this).attr('maxLengthAllowed');
		//LEFT AND RIGHT KEY ALLOW
		
		if(e.keyCode != 37 || e.keyCode != 39){
			
			fixedInputLength(jQuery(this),maxLengthAllowed);
		}
	});
	//FIXED CNIC LENGTH
	jQuery('.maxCharacterLimit').keyup(function(e){
		
		var maxLengthAllowed = jQuery(this).attr('maxLengthAllowed');
		//LEFT AND RIGHT KEY ALLOW
		if(e.keyCode != 37 || e.keyCode != 39){
			
			fixedInputLength(jQuery(this),maxLengthAllowed);
		}
	});
	
	
	/**
	* MAKE POC NAME SAME AS VENDOR NAME
	*/
	jQuery('#vendor_name').on('keyup',function(){
		jQuery('#poc_name').val(jQuery(this).val());
	});


	/**
	* MAKE POC CONTACT SAME AS VENDOR CONTACT
	*/
	jQuery('#cell_phone').on('keyup',function(){
		jQuery('#poc_contact').val(jQuery(this).val());
	});

	//NTN NUMBER FORMAT
	jQuery('.ntnNumberField').on('keyup',function(){
		
		//BIND '-' AFTER SEVEN DIGIT
		if(jQuery(this).val().length == 7){
			jQuery(this).val(jQuery(this).val()+'-');  
		}
		
		//LIMIT LENGTH
		var maxLength = 9;
		if(jQuery(this).val().length > maxLength){
			jQuery(this).val(jQuery(this).val().substr(0, maxLength));  
		}
	});
		
		
	//FIRST CHARACTER CAN'T BE ZERO
	jQuery('.firstCharacterCantZero').on('keypress',function(e){ 
		//DISABLE ON FIRST ATTEMO  
		if (this.value.length == 0 && e.which == 48 ){
		  return false;
		}
	   //RELACE ON EDIT MODE
		var val = jQuery(this).val();
		while (val.substring(0, 1) === '0') {   //First character is a '0'.
			val = val.substring(1);             //Trim the leading '0'
		}
	   jQuery(this).val(val);   //update input with new value
	});	

		
			/**
	* FIXED INPUT LENGTH
	*/
	function fixedInputLength(fieldName,maxLength){



		if(fieldName.val().length > maxLength){
			
			//IF CURRENT FIELD IS NTN1 THEN SWITCH FOCUS TO NTN2
			if(fieldName.attr('id')=='ntn1'){
				jQuery("#ntn2").focus();
			}

			fieldName.val(fieldName.val().substr(0, maxLength));  
		}
	}
	
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}

	
	
	/**
	* CATEGORY SELECTION
	*/
	
	//GET L2 CATEGORY BASE ON L1 CATEGORY
	jQuery(document).ready(function(){
		
		
		var loop = <?php echo isset($vendorMargin)?$vendorMargin->count():0; ?>;
		//SELECT CATEGORY FOR MARGING NEGOTIATTION
		jQuery( "body" ).delegate( ".l3CategoryForMargin", "click", function() {
			
			var catgeory_name 	= jQuery.trim(jQuery(this).find('#catgeory_name').val());
			var standard_margin = jQuery.trim(jQuery(this).find('#standard_margin').val());
			var catgeory_id 	= jQuery.trim(jQuery(this).find('#catgeory_id').val());
			
			//REMOVE SELECTED RAW
			jQuery(this).remove();
			
			loop++;
			
			//ADD JASCVRIPT CONTENT
			//var txt = 'alert("Hello");';
			var spinner = "jQuery('#spinner"+loop+"').spinner({ min: 1,max:100 }).val('"+standard_margin+"');";
			var scr = document.createElement("script");
			scr.type= "text/javascript";

			// We have to use .text for IE, .textContent for standards compliance.
			if ("textContent" in scr)
				scr.textContent = spinner;
			else
				scr.text = spinner;
						 
			var spinnerData =  "<input id='spinner"+loop+"' class='spinner numericOnlyField' name='proposed_margin[]' min='1' /> ";

			  var tdData = "<td>"+catgeory_name+"</td><td>"+standard_margin+"</td><td>"+spinnerData+"<span  class='selectedForMarginNegtation'><i style='float:right;' class='fa  fa-minus-square'></i><input type='hidden'  id='catgeory_name' value='"+catgeory_name+"' /><input type='hidden' name='systemMargin[]' id='standard_margin' value='"+standard_margin+"' /><input type='hidden' name='categoryId[]' value='"+catgeory_id+"' id='catgeory_id' /></span></td>";
			//ADD RAW
			jQuery('.margingNegotiationTable tr:last').after('<tr>'+tdData+'</tr>');
			document.getElementById('spinner'+loop).appendChild(scr);
			

		});
		
		//REMOVE SELECT NEGOTIATED FOR MARGIN
		jQuery('body').delegate( ".selectedForMarginNegtation", "click", function() {
			
			var catgeory_name 	= jQuery.trim(jQuery(this).find('#catgeory_name').val());
			var standard_margin = jQuery.trim(jQuery(this).find('#standard_margin').val());
			var catgeory_id 	= jQuery.trim(jQuery(this).find('#catgeory_id').val());
			
			// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
			l3ListOption = "<tr class='l3CategoryForMargin'><td colspan='2'>"+catgeory_name+"<input type='hidden' value='"+catgeory_name+"' id='catgeory_name'/><input type='hidden' value='"+standard_margin+"' id='standard_margin'/><input type='hidden' value='"+catgeory_id+"' id='catgeory_id'/><i style='float:right;' class='fa  fa-plus-square'></i></td></tr>";
			
			jQuery('.selectingMargingNegotiationTable tr:last').after(l3ListOption);
			
			//REMOVE RAW
			jQuery(this).closest('tr').remove();
									
		});
		

		
		
		//REMOVE OLD SELECT  NEGOTIATED FOR MARGIN
		jQuery('body').delegate( ".oldSelectedForMarginNegtation", "click", function() {
			
			//REMOVE RAW
			jQuery(this).closest('tr').remove();
									
		});
	});

	function getSelectedL3Category(){
		
		var selectedL3CategoryForMargin = [];
		jQuery('input[name^="categoryId"]').each(function() {
			selectedL3CategoryForMargin.push(jQuery(this).val());
		});
		
		return selectedL3CategoryForMargin;
	}
	
</script>