@extends('layouts.empty')
@section('title','Index')
@section('content')


<div class = 'container ' style="text-align:center">
  
	<div class="row col-md-9 ">

			<div class="row" style="max-height:500px; overflow-y:scroll;">
				<h2>
					Catgeory Standard Margin List
					
				</h2>
			
			
			<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
				
                <tr role="row">
					<th >S.No</th>
					<th >L1 Category</th>
					<th >L2 Category</th>
					<th >L3 Category</th>
					<th >Margin</th>
				</tr>
                </thead>
                <tbody>
				
				<?php
				
				 if(!empty($getL3CatgeoryWithParent)){
					 $sno = 1;
					 foreach($getL3CatgeoryWithParent as $row){
				?>
							 <tr role="row">
							  <td ><?php echo $sno;?></td>
							  <td><?php echo $row['L1_Category'];?></td>
							  <td><?php echo $row['L2_Category'];?></td>
							  <td><?php echo $row['L3_Category'];?></td>
							  <td><?php echo $row['margin'];?></td>
							
							
							</tr>
				<?php		
						$sno++;
					 }
				 }
				?>
               
			
				</tbody>
                
              </table>
			</div>
				
	</div>
</div>

@endsection