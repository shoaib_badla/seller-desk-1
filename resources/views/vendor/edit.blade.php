@extends('layouts.app')
@section('title','Index')
@section('content')


<div class = 'container'>
	<h3 class="box-title">
		Update Vendor Info
	</h3>
		
	
    <div class="row">
	<div class="alert alert-danger jserrorregion" style="display:none;">
		<ul>
				<li>Please fill all required field </li>
		</ul>
	</div>
	  {!! Form::open(array('url'=>"vendor_info/$vendor_info->id/update",'method'=>'POST','id'=>'addNewVendor', 'files'=>true)) !!}
		@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
	<div style='float:right;padding-right:100px;' >
	<!-- <button class = 'btn btn-primary btn-sm' type="submit">submit</button>-->
		<span class='btn-submit' ><a href="#" class = 'btn btn-primary btn-sm'  id="submit">Update</a></span>
		<span class='form-waiting-region' style="display:none;"><i class="fa fa-refresh fa-spin"></i>Please Wait...</span>
	<div class='err-region alert alert-danger' style='display:none;'></div>
	
	</div>
		<div class="col-md-11">
			
			 <div class="panel with-nav-tabs panel-primary">
			
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab1primary" data-toggle="tab">General Information</a></li>
						<li><a href="#tab2primary" data-toggle="tab">Financial Information</a></li>
						<li><a href="#tab4primary" data-toggle="tab">Pickup Address(s)</a></li>
						<li><a href="#tab5primary" data-toggle="tab">Category(s)</a></li>
						<li><a href="#tab3primary" data-toggle="tab">Files</a></li>
						
					</ul>
				</div>
			
				<!-- /.box-header -->
				<div class="panel-body">
					<div class="tab-content">
					
						<div class="tab-pane fade in active" id="tab1primary">
							  <div class="row">
									<div class="col-md-6">
										<div class="form-group col-md-8">
											<label for="firstname">Firstname</label>
											<input id="firstname" name = "firstname" type="text" class="form-control validate" value="{{$vendor_info->firstname}}">
										</div>
										 <div class="form-group col-md-8">
											<label for="vendor_email">Vendor Email Address</label>
											<input id="vendor_email" name = "vendor_email" type="text" class="form-control validate" value="{{$vendor_info->vendor_email}}" readonly="readonly">
											
										</div>
										
										 <div class="form-group col-md-8">
											<label for="cnic">CNIC</label>
											<input id="cnic" name = "cnic" type="text" class="form-control validate" value="{{$vendor_info->cnic}}">
										</div>


									
									
										<div class="form-group col-md-8">
											<label for="poc_email">POC Email</label>
											<input id="poc_email" name = "poc_email" type="text" class="form-control validate" value="{{$vendor_info->poc_email}}">
										</div>
										
										
									
										
										 <div class="form-group col-md-8">
											<label for="vendor_manager_assigned">Vendor Manager Assigned</label>
											<input id="vendor_manager_assigned" name = "vendor_manager_assigned" type="text" class="form-control validate" value="{{$vendor_info->vendor_manager_assigned}}">
											
										</div>
										 <div class="form-group col-md-8">
											<label for="take_returns">Take Returns</label>
											<select id="take_returns" name = "take_returns" class="form-control validate" >
												<option value="">Please Select Option</option>
												<option value="Yes" <?php echo ($vendor_info->take_returns == 'Yes') ?  "selected='selected'" :  ''; ?>>Yes</option>
												<option value="No" <?php echo ($vendor_info->take_returns == 'No') ?  "selected='selected'" :  ''; ?> >No</option>
												
											</select>
										</div>
										 <div class="form-group col-md-8">
											<label for="take_exchange">Take Exchange</label>
											<select id="take_exchange" name = "take_exchange" class="form-control validate" >
												<option value="">Please Select Option</option>
												<option value="Yes" <?php  echo ($vendor_info->take_exchange == 'Yes') ?  "selected='selected'" :  ''; ?>>Yes</option>
												<option value="No" <?php  echo ($vendor_info->take_exchange == 'No') ?  "selected='selected'" :  ''; ?>>No</option>
											</select>
										</div>
										<div class="form-group col-md-8">
											<label for="country">Country</label>
											<select id="country" name = "country" class="form-control validate" >
												<option value="">Please Select Country</option>
												<?php
													if(!empty($countryList)){
														foreach($countryList as $countryId=>$countryListRow){
												?>
															<option <?php echo ($vendor_info->country == $countryId) ?  "selected='selected'" :  ''; ?> value="<?php echo $countryId;?>"><?php echo $countryListRow;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
										 <div class="form-group col-md-8">
											<label for="trading_or_commision">Trading Or Commision</label>
											<input id="trading_or_commision" name = "trading_or_commision" type="text" class="form-control validate" value="{{$vendor_info->trading_or_commision}}">
											
										</div>
										 <div class="form-group col-md-8">
											<label for="legal_address">Legal Address</label>
											<textarea class="form-control" rows="3" id="legal_address" name = "legal_address">{{$vendor_info->legal_address}}</textarea>
										</div>
									</div>
									<!-- /.col -->
									<div class="col-md-6">
										 <div class="form-group col-md-8">
											<label for="lastname">Lastname</label>
											<input id="lastname" name = "lastname" type="text" class="form-control validate" value="{{$vendor_info->lastname}}">
											
										</div>
										<div class="form-group col-md-8">
											<label for="online_selling_name">Online Selling Name</label>
											<input id="online_selling_name" name = "online_selling_name" type="text" class="form-control validate" value="{{$vendor_info->online_selling_name}}">
										</div>
										
										<div class="form-group col-md-8">
											<label for="mode_of_fullfilment">Mode Of Fullfilment</label>
											<select id="mode_of_fullfilment" name = "mode_of_fullfilment" class="form-control validate" >
												<option value="">Please select option</option>
												<?php
													if(!empty($modeOfFullfillment)){
														foreach($modeOfFullfillment as $modeOfFullfillmentRow){
												?>
													<option <?php echo ($vendor_info->mode_of_fullfilment == $modeOfFullfillmentRow) ?  "selected='selected'" :  ''; ?> value="<?php echo $modeOfFullfillmentRow;?>"><?php echo $modeOfFullfillmentRow;?></option>
												<?php
														}
													}
												?>
											</select>
										</div>


										
										 <div class="form-group col-md-8">
											<label for="poc_contact">POC Contact</label>
											<input id="poc_contact" name = "poc_contact" type="text" class="form-control validate" value="{{$vendor_info->poc_contact}}">
											
										</div>
										
								
										 <div class="form-group col-md-8">
											<label for="payment_cycle">Payment Cycle</label>
											<input id="payment_cycle" name = "payment_cycle" type="text" class="form-control validate" value="{{$vendor_info->payment_cycle}}">
											
										</div>
										 <div class="form-group col-md-8">
											<label for="return_time">Return Time</label>
											<input id="return_time" name = "return_time" type="text" class="form-control validate" value="{{$vendor_info->return_time}}">
											
										</div>
										 <div class="form-group col-md-8">
											<label for="return_location">Return Location</label>
											<input id="return_location" name = "return_location" type="text" class="form-control validate" value="{{$vendor_info->return_location}}">
											
										</div>
										 <div class="form-group col-md-8">
											<label for="city">City</label>
											<select id="city" name = "city" class="form-control validate" >
												<option value="">Please Select City</option>
												<?php
													if(!empty($cityList)){
														foreach($cityList as $cityId=>$cityListRow){
												?>
															<option <?php echo ($vendor_info->city == $cityId) ?  "selected='selected'" :  ''; ?>  value="<?php echo $cityId;?>"><?php echo $cityListRow;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
										 <div class="form-group col-md-8">
											<label for="city">Brand(s) Offered</label>
											<select multiple=""  id="brand[]" name = "brand[]" class="form-control validate" >
												<?php
													if(!empty($brandList)){
														foreach($brandList as $brandId=>$brandName){
												?>
															<option  <?php if(in_array($brandId,$vendorBrandId)){ echo "selected='selected'";}?> value="<?php echo $brandId;?>"><?php echo $brandName;?></option>
												<?php													
														}
													}
												?>
											</select>
									
									</div>
									</div>
									<!-- /.col -->
								  </div>
				  <!-- /.row -->
							</div>
							
							 <div class="tab-pane fade" id="tab2primary">
								   <div class="row">
										<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="bank_account_number">Bank Account Number</label>
												<input id="bank_account_number" name = "bank_account_number" type="text" class="form-control validate" value="{{$vendor_info->bank_account_number}}">
											</div>
											 <div class="form-group col-md-8">
												<label for="bank_name">Bank Name</label>
												<input id="bank_name" name = "bank_name" type="text" class="form-control validate" value="{{$vendor_info->bank_name}}">
												
											</div>
											<div class="form-group col-md-8">
												<label for="ntn">NTN</label>
												<input id="ntn" name = "ntn" type="text" class="form-control validate" value="{{$vendor_info->ntn}}">
											</div>
											
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											 <div class="form-group col-md-8">
												<label for="bank_account_title">Bank Account Title</label>
												<input id="bank_account_title" name = "bank_account_title" type="text" class="form-control validate" value="{{$vendor_info->bank_account_title}}">
											</div>
										    <div class="form-group col-md-8">
												<label for="strn">STRN</label>
												<input id="strn" name = "strn" type="text" class="form-control validate" value="{{$vendor_info->strn}}">
											</div>
										</div>
									<!-- /.col -->
								  </div>
							 </div>
							 <div class="tab-pane fade" id="tab3primary">
								<div class="row">
									<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="logo">Logo</label>
													<?php
														if(!empty($vendor_info->logo)){
													?>
															<img src="<?php echo asset('uploads/vendor_logo/'.$vendor_info->logo);?>" class="img-circle" width="50"/>
													<?php												
														}
													?>
												<input type="file"  id="logo" name = "logo" >
												
												<p class="help-block">
												Allowed format (JPEG,JPG,PNG).
											
												</p>
											</div>
											
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											 <div class="form-group col-md-8">
												<label for="form_of_organization">Form Of rganization</label>
												<input type="file"  id="form_of_organization" name = "form_of_organization" >
												<p class="help-block">
													Allowed format (PDF).
													<br/>
													 <?php 
													  if(!empty($vendor_info->form_of_organization)){
														  ?>
														<a href="<?php echo asset('uploads/form_of_organization/'.$vendor_info->form_of_organization);?>" 	target ="_blank"><?php echo wordwrap($vendor_info->form_of_organization_original,50,"<br>\n");?></a>
														<?php
													  }else{ echo "No Uploaded Yet";}
													 ?>
												
												</p>
											</div>
										</div>
									<!-- /.col -->
								  </div>
							 </div>
							 <div class="tab-pane fade" id="tab4primary">
							 
								<?php
									if(!empty($vendor_pickup_address)){
										$totalAddress = count($vendor_pickup_address);
										$loop = 1;
										
										foreach($vendor_pickup_address as $vendor_pickup_addressRow){
											
										
								?>
										<div class="row  <?php echo ($totalAddress==$loop)?'pickup-address-region':'';?> pickup-address-region1 ">
											<div class="box-header with-border">
											  <div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool addPickupAddress"  style='display:none;'><i class="fa fa-plus"></i></button>
												<button type="button" class="btn btn-box-tool removePickupAddress"  style='display:none;'><i class="fa fa-remove"></i></button>
											  </div>
											</div>
											<div class="col-md-6">
													<div class="form-group col-md-8">
														<label for="pickup_address">Pickup Address</label>
														<textarea id="pickup_address[]" name = "pickup_address[]" type="text" class="form-control validate pickup_address" rows="3" ><?php echo $vendor_pickup_addressRow->pickup_address;?></textarea>
													</div>
													<div class="form-group col-md-8">
														<label for="warehouse_reporting_ffc">Warehouse Reporting FFC</label>
														<input id="warehouse_reporting_ffc[]" name = "warehouse_reporting_ffc[]" type="text" class="form-control validate"  value="<?php echo $vendor_pickup_addressRow->warehouse_reporting_ffc;?>">
													</div>
													<div class="form-group col-md-8">
														<label for="warehouse_timming">Warehouse Timming</label>
														<input id="warehouse_timming[]" name = "warehouse_timming[]" type="text" class="form-control validate"  value="<?php echo $vendor_pickup_addressRow->warehouse_timing;?>">
														
													</div>
												</div>
												<!-- /.col -->
												<div class="col-md-6">
												<div class="form-group col-md-8">
														<label for="pickup_city">Pickup City</label>
														<select id="pickup_city[]" name = "pickup_city[]" class="form-control validate" >
															<option value="">Please Select City</option>
															<?php
																if(!empty($cityList)){
																	foreach($cityList as $cityId=>$cityListRow){
															?>
																		<option <?php echo ($vendor_pickup_addressRow->pickup_city)==$cityId? "selected='selected'":'';?> value="<?php echo $cityId;?>"><?php echo $cityListRow;?></option>
															<?php													
																	}
																}
															?>
														</select>
													</div>
													<div class="form-group col-md-8">
														<label for="warehouse_reporting_poc">Warehouse Reporting POC</label>
														<input id="warehouse_reporting_poc[]" name = "warehouse_reporting_poc[]" type="text" class="form-control validate"  value="<?php echo $vendor_pickup_addressRow->warehouse_poc;?>">
													</div>
													
												</div>
											<!-- /.col -->
										  </div>

								<?php		
											$loop++;	
										}
									}else{
								?>	
								
									<div class="row pickup-address-region pickup-address-region1 ">
										<div class="box-header with-border">
										  <div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool addPickupAddress"  style='display:none;'><i class="fa fa-plus"></i></button>
											<button type="button" class="btn btn-box-tool removePickupAddress"  style='display:none;'><i class="fa fa-remove"></i></button>
										  </div>
										</div>
										<div class="col-md-6">
												<div class="form-group col-md-8">
													<label for="pickup_address">Pickup Address</label>
													<textarea id="pickup_address[]" name = "pickup_address[]" type="text" class="form-control validate pickup_address" rows="3" ></textarea>
												</div>
												<div class="form-group col-md-8">
													<label for="warehouse_reporting_ffc">Warehouse Reporting FFC</label>
													<input id="warehouse_reporting_ffc[]" name = "warehouse_reporting_ffc[]" type="text" class="form-control validate">
												</div>
												<div class="form-group col-md-8">
													<label for="warehouse_timming">Warehouse Timming</label>
													<input id="warehouse_timming[]" name = "warehouse_timming[]" type="text" class="form-control validate">
													
												</div>
											</div>
											<!-- /.col -->
											<div class="col-md-6">
											<div class="form-group col-md-8">
													<label for="pickup_city">Pickup City</label>
													<select id="pickup_city[]" name = "pickup_city[]" class="form-control validate" >
														<option value="">Please Select City</option>
														<?php
															if(!empty($cityList)){
																foreach($cityList as $cityId=>$cityListRow){
														?>
																	<option value="<?php echo $cityId;?>"><?php echo $cityListRow;?></option>
														<?php													
																}
															}
														?>
													</select>
												</div>
												<div class="form-group col-md-8">
													<label for="warehouse_reporting_poc">Warehouse Reporting POC</label>
													<input id="warehouse_reporting_poc[]" name = "warehouse_reporting_poc[]" type="text" class="form-control validate">
												</div>
												
											</div>
										<!-- /.col -->
									  </div>
								  
								  <?php } ?>
							 </div>
							 
							 <!-- CATEGORY TAB START -->
							  <div class="tab-pane fade" id="tab5primary">
							  
							  <?php
									if(!empty($vendor_product_category)){
										$totalCate = count($vendor_product_category);
										$loop = 1;
										
										foreach($vendor_product_category as $vendor_product_categoryRow){
								?>
										<div class="row <?php echo ($totalCate==$loop)?'category-level-region':'';?> category-level-region1">
											<div class="box-header with-border">
											  <div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool addCategory"  style='display:none;'><i class="fa fa-plus"></i></button>
												<button type="button" class="btn btn-box-tool removeCategory"  style='display:none;'><i class="fa fa-remove"></i></button>
											  </div>
											</div>
											<div class="col-md-6">
													<div class="form-group col-md-8">
														<label for="product_category">Product Category</label>
														<select id="product_category[]" name = "product_category[]" class="form-control validate" >
															<option value="">Please Select Category</option>
															<?php
																if(!empty($categoryList)){
																	foreach($categoryList as $category_id=>$category_name){
															?>
																		<option <?php echo ($vendor_product_categoryRow->category_id==$category_id)? "selected='selected'":'';?> value="<?php echo $category_id;?>"><?php echo $category_name;?></option>
															<?php													
																	}
																}
															?>
														</select>
													</div>
													
												</div>
												<!-- /.col -->
												<div class="col-md-6">
													 <div class="form-group col-md-8">
														<label for="proposed_commission">Proposed Commission (%)</label>
														<input id="proposed_commission[]" name = "proposed_commission[]" type="text" class="form-control validate" value="{{$vendor_product_categoryRow->proposed_commission}}">
														
													</div>
												</div>
											<!-- /.col -->
										  </div>
								<?php
										$loop++;
										}
									}else{
								?>
									<div class="row category-level-region category-level-region1">
										<div class="box-header with-border">
										  <div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool addCategory"  style='display:none;'><i class="fa fa-plus"></i></button>
											<button type="button" class="btn btn-box-tool removeCategory"  style='display:none;'><i class="fa fa-remove"></i></button>
										  </div>
										</div>
										<div class="col-md-6">
												<div class="form-group col-md-8">
													<label for="product_category">Product Category</label>
													<select id="product_category[]" name = "product_category[]" class="form-control validate" >
														<option value="">Please Select Category</option>
														<?php
															if(!empty($categoryList)){
																foreach($categoryList as $category_id=>$category_name){
														?>
																	<option value="<?php echo $category_id;?>"><?php echo $category_name;?></option>
														<?php													
																}
															}
														?>
													</select>
												</div>
												
											</div>
											<!-- /.col -->
											<div class="col-md-6">
												 <div class="form-group col-md-8">
													<label for="proposed_commission">Proposed Commission (%)</label>
													<input id="proposed_commission[]" name = "proposed_commission[]" type="text" class="form-control validate">
													
												</div>
											</div>
										<!-- /.col -->
									  </div>
								<?php								
									}
								?>
								
							 </div>
							 <!-- CATEGORY TAB END -->
				<!-- /.box-body -->
						</div>
					</div>
			
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
 <!-- Laravel Javascript Validation -->
  
<script type="text/javascript" src="{{ asset('vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation.js' )}}"></script>
@include('vendor.common_js_method');
  <?php /*{!! JsValidator::formRequest('App\Http\Requests\vendorFormRequest', '#addNewVendor'); !!}*/?>
<script>
//VALIDATION FORM SUBMISION PUT HERE BECAUSE OF TOKEN
	jQuery(document).ready(function(){
		jQuery('#submit').on('click',function(){
		   
		 
		  
			//Validate Dynamic Generated Field
			var pickup_address_Err = validateFieldAndShowErrorMessage('textarea','pickup_address','Pickup Address is required');
			var pickup_city_Err = validateFieldAndShowErrorMessage('select','pickup_city','Pickup City is required');
			var warehouse_reporting_ffc_Err = validateFieldAndShowErrorMessage('input','warehouse_reporting_ffc','Warehouse Reporting FFC is required');
			var warehouse_reporting_poc_Err = validateFieldAndShowErrorMessage('input','warehouse_reporting_poc','Warehouse Reporting POC is required');
			var warehouse_timming_Err = validateFieldAndShowErrorMessage('input','warehouse_timming','Warehouse Timming is required');
			var product_category_Err = validateFieldAndShowErrorMessage('select','product_category','Product Category is required');
			var proposed_commission_Err = validateFieldAndShowErrorMessage('input','proposed_commission','Proposed Commission is required');
		
			var formValid = jQuery('#addNewVendor').valid();
			
			var pocEmailErr = false;
			var vendorEmailErr = false;
			if(formValid){
				//VALIDATE EMAIL ADDRESS AS JS VALIDTOR NOT WORKIN GAS EXPECTED FOR EMAIL
				var poc_email = jQuery.trim(jQuery('#poc_email').val());
				jQuery('.poc-emil-rr').remove();
				if( !isValidEmailAddress( poc_email ) ) {
					 jQuery( "<p style='color:red;' class=' poc-emil-rr'>Email Address should be valid</p>" ).insertAfter( jQuery('#poc_email') );
					 jQuery('#poc_email').closest('.form-group').removeClass('has-success').addClass('has-error');
					 
					 pocEmailErr=true;
				}else{
					 jQuery('#poc_email').closest('.form-group').removeClass('has-error').addClass('has-success'); 
					 pocEmailErr = false
				}
				
				var vendor_email = jQuery.trim(jQuery('#vendor_email').val());
				jQuery('.vendor_email-rr').remove();
				if( !isValidEmailAddress( vendor_email ) ) {
					 jQuery( "<p style='color:red;' class=' vendor_email-rr'>Email Address should be valid</p>" ).insertAfter( jQuery('#vendor_email') );
					 jQuery('#vendor_email').closest('.form-group').removeClass('has-success').addClass('has-error');
					  vendorEmailErr=true;
				}else{
					 jQuery('#vendor_email').closest('.form-group').removeClass('has-error').addClass('has-success');
					  vendorEmailErr=false;					 
					  
				}
			
			}
			
			//IF VENDOR EMAIL VALIDATED
			if(!vendorEmailErr){
				var emailAlreadyExist = false;
				var vendor_id = "<?php echo $vendor_info->id;?>";
				//IF EMAIL ADDRess is valid
					jQuery.ajax({
						url: "<?php echo url('/vendor_info/checkVendorAlradyExsist');?>",
						type: "post",
						data: {
							"_token": "{{ csrf_token() }}",
							"vendor_email": vendor_email,
							"vendor_id":vendor_id
						},
						success: function(result){
							//UPDATE ATTRIBUTES VALUE
							if(result=='exsist'){
								 jQuery( "<p style='color:red;' class=' vendor_email-rr'>Vendor already exsist with provided email address!</p>" ).insertAfter( jQuery('#vendor_email') );
								 jQuery('#vendor_email').closest('.form-group').removeClass('has-success').addClass('has-error');
								 emailAlreadyExist = true;
							}else{
								 jQuery('#vendor_email').closest('.form-group').removeClass('has-error').addClass('has-success');
								
							}
							
							
							//FORM SUBMIT AFTER AJAX
							//CHECK FORM IS VALID AND NO ERROR OCCUR
							if(formValid &&  !pickup_address_Err && !pickup_city_Err && !warehouse_reporting_ffc_Err && !warehouse_reporting_poc_Err && !warehouse_timming_Err && !product_category_Err && !proposed_commission_Err  && !pocEmailErr  && !vendorEmailErr && !emailAlreadyExist ){
								jQuery('#addNewVendor').submit();
								jQuery('.jserrorregion').hide();
								jQuery('.btn-submit').hide();
								jQuery('.form-waiting-region').show();
							}else{
								jQuery('.jserrorregion').show();
								jQuery('.btn-submit').show();
								jQuery('.form-waiting-region').hide();
							}
							
						}
					});
			}
			

			
			 
			
		});
	});
</script>
@endsection