@extends('layouts.ajax')
@section('title','Index')
@section('content')


<div class="box box-primary">
           
            <!-- /.box-header -->
            <!-- form start -->
           {!! Form::open(array('url'=>'approvedVendorSubmit','method'=>'POST','id'=>'approvedVendor', 'files'=>true)) !!}
		
			<input type="hidden" name="vendor_id" value="<?php echo $vendorId;?>"/>
              <div class="box-body">
                <div class="form-group">
                  	<label for="vendormanagerAssigned">Assigned Manager</label>
					<select id="vendormanagerAssigned" name = "vendormanagerAssigned" class="form-control" >
						<option value="">Please Select Option</option>
						
						<option value="<?php echo Auth::user()->id;?>"><?php echo Auth::user()->name;?></option>
						<?php
							if(!empty($vendorManagerList)){
						?>
								<optgroup label = "Vendor Managers">
						<?php
								foreach($vendorManagerList as $manager_id => $manager_name){
						?>
							<option value="<?php echo $manager_id;?>"><?php echo $manager_name;?></option>
						<?php	
							
								}
						?>
								</optgroup>
						<?php
							}
						?>
						<?php
							if(!empty($categoryManagerList)){
						?>
								<optgroup label = "Category Managers">
						<?php
								foreach($categoryManagerList as $manager_id => $manager_name){
						?>
							<option value="<?php echo $manager_id;?>"><?php echo $manager_name;?></option>
						<?php	
							
								}
						?>
								</optgroup>
						<?php
							}
						?>
						
					</select>
                </div>
               
              
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <span class='btn-submit' ><a href="#" class = 'btn btn-primary btn-sm'  id="submit">Submit</a></span>
				<span class='form-waiting-region' style="display:none;"><i class="fa fa-refresh fa-spin"></i>Please Wait...</span>
			
				<div class='err-region alert alert-danger' style='display:none;'></div>
		
              </div>
			{!! Form::close() !!}
          </div>
<script>
//VALIDATION FORM SUBMISION PUT HERE BECAUSE OF TOKEN
	jQuery(document).ready(function(){
		jQuery('#submit').on('click',function(){
		   
		   

			var formValid = jQuery('#approvedVendor').valid();
			
			
			
			//CHECK FORM IS VALID AND NO ERROR OCCUR
			if(formValid){
				jQuery('#approvedVendor').submit();
				jQuery('.jserrorregion').hide();

				jQuery('.btn-submit').hide();
				jQuery('.form-waiting-region').show();

			}else{
				jQuery('.jserrorregion').show();

				jQuery('.btn-submit').show();
				jQuery('.form-waiting-region').hide();
			}
							
		});
		
		   jQuery("#approvedVendor").validate({
            errorElement: 'span',
            errorClass: 'help-block error-help-block',

            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length ||
                    element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
            },

            
           
             
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // remove the Boostrap error class from the control group
            },

            focusInvalid: false, // do not focus the last invalid input
            ignore:"ui-tabs-hide",
            rules:  {
					 "vendormanagerAssigned": {
						 "laravelValidation": [
							 ["Required", [], "The Assigned Manager is required.", true],
						 ]
					 }
					 
					
				 }
			 });
	});
</script>
@endsection


