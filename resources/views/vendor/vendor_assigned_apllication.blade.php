@extends('layouts.app')
@section('title','Index')
@section('content')


<div class = 'container'>
    <h1>
        Vendor Listing
    </h1>
    
	<div class="row">
	
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		
		
		@if (!empty($success))
			{{ $success }}
		@endif
			
		<!--<div style="float:right; padding-right:70px;">
			<a href="{{url('/vendor_info/create')}}" class=" btn btn-primary"><i class="fa fa-user-plus"></i> <span>Add New Vendor</span></a>
		</div>
		<div style="clear:both">&nbsp;</div> -->
		<?php
			//REJECTION LIST OPTION
			$reasonOption	=	"";
			if(!empty($getRejectionList)){
				foreach($getRejectionList as $getRejectionListRow){
					$reasonOption .= '<option value="'.$getRejectionListRow.'">'.$getRejectionListRow.'</option>';
				}
			}
			
		?>
		
		<script>

				function gridCompleteEvent(){
					
						/**
						* VENDOR DETAIL VIEW
						*/
						jQuery(".viewVendor").on('click',function(){
							//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
							jQuery('.ajax-loader-region').show();
						
							var vendorId = jQuery(this).attr('vendor-id');
							jQuery.ajax({
								url: "<?php echo url('/vendor_info/view/');?>",
								type: "post",
								data: {
									"_token": "{{ csrf_token() }}",
									"vendor_id": vendorId
									
								},
								success: function(result){
									jQuery('.custom-modal-view').modal('toggle');
									jQuery('.custom-modal-dialog').addClass('modal-lg');
									jQuery('.custom-modal-title').html('Vendor Info');//SET TITLE
									jQuery('.custom-modal-body').html(result);//SET BODY
									
									//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
									jQuery('.ajax-loader-region').hide();
								}
							});
						});
						
						
						
						
						//REMOVE VIEW ICON FROM GRID
						jQuery('#view_vendorApplicationListView').remove();
				}

				</script>
					<?php 
				echo GridRender::setGridId("vendorApplicationListView")
				  ->enableFilterToolbar()
				  ->setGridOption('url',URL::to('/vendor_application_request_data'))
				  ->setGridOption('rowNum', 15)
				  ->setGridOption('sortname','id')
				  ->setGridOption('sortorder','desc')
				  
				  ->setGridOption('viewrecords',false)
				  ->setGridOption('caption','Vendor Listing ')
				 
				  
				  ->setGridOption('width', 1100)
				  ->setGridOption('height', 350)
				  ->setGridOption('rowList', [15, 25, 50])
				  ->setGridOption('pager', "jqGridPager")
				  ->setGridOption('shrinkToFit', false)
				  //->setNavigatorOptions('navigator', array('viewtext'=>'view'))
				 // ->setNavigatorOptions('view',array('closeOnEscape'=>false))
				  ->setFilterToolbarOptions(array('autosearch'=>true))
				  ->setGridOption('postData', array('_token' => Session::token()))
				  ->setGridEvent('gridComplete', 'gridCompleteEvent') //gridCompleteEvent must be previously declared as a javascript function.
				  //->setNavigatorEvent('view', 'beforeShowForm', 'function(){alert("Before show form");}')
				 // ->setFilterToolbarEvent('beforeSearch', 'function(){alert("Before search event");}')
				 // ->addGroupHeader(array('startColumnName' => 'amount', 'numberOfColumns' => 3, 'titleText' => 'Price'))
				  ->addColumn(array('index'=>'id', 'width'=>20,'search'=>false))
				  ->addColumn(array('label'=>'Action','index'=>'action_column', 'width'=>50,'search'=>false))
				   ->addColumn(array('label'=>'Application Progress','index'=>'is_vendor_approved', 'width'=>270,'search'=>true,'stype'=>'select','searchoptions'=>array('value'=> 'all:All;0:Pending Vendor Detail;1:Vendor Onboard;2:Vendor Detail Pending for Review;4:Vendor Detail send for Correction;6:Send for Other Depart Review;7:Application Reviewed By Other Depart')))
				   ->addColumn(array('label'=>'Finance Status','index'=>'is_vendor_finance_approved', 'width'=>120,'search'=>true,'stype'=>'select','searchoptions'=>array('value'=> 'all:All;1:Approved;2:Rejected;3:Pending')))
				   ->addColumn(array('label'=>'Operation Status','index'=>'is_vendor_ops_approved', 'width'=>120,'search'=>true,'stype'=>'select','searchoptions'=>array('value'=> 'all:All;1:Approved;2:Rejected;3:Pending')))
				  ->addColumn(array('label'=>'Vendor Fullname','index'=>'vendor_name', 'width'=>100))
				  ->addColumn(array('label'=>'Vendor Email','index'=>'vendor_email', 'width'=>100))
				  ->addColumn(array('label'=>'Cell Phone','index'=>'cell_phone', 'width'=>150))
				  ->addColumn(array('label'=>'Form Of Organization','index'=>'form_of_organization', 'width'=>100))
				  ->addColumn(array('label'=>'NTN Number','index'=>'ntn_number', 'width'=>100))
				  ->addColumn(array('label'=>'Province','index'=>'province', 'width'=>100))
				  ->addColumn(array('label'=>'City','index'=>'city', 'width'=>100))
				  
				  ->renderGrid()
				?>
			
	</div>
</div>
@endsection