@extends('layouts.ajax')
@section('title','Index')
@section('content')


<div class = 'container ' style="text-align:center">
  
	<div class="row col-md-9 ">

			<div class="row">
				<h2>
					Vendor Signing Contract
				</h2>
    
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
					</p>
			
			<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
					<th >S.No</th>
					<th >Contract Name</th>
					<th >Download</th>
				</tr>
                </thead>
                <tbody>
				
				<?php
				 if(!empty($vendor_signing_contract)){
					 $sno = 1;
					 foreach($vendor_signing_contract as $row){
						
				?>
							 <tr role="row">
							  <td ><?php echo $sno;?></td>
							  <td><?php echo $row->orginal_name;?></td>
							  <td>
								 <a href="<?php echo asset('uploads/sigining_contract/'.$row->uploaded_name);?>" target="_blank"><i class="fa fa-download"></i> Download</a>
							  </td>
							
							</tr>
				<?php		
						$sno++;
					 }
				 }
				?>
               
				
				</tbody>
                
              </table>

			</div>
				
	</div>
</div>

@endsection