@extends('layouts.empty')
@section('title','Index')
@section('content')


<div class = 'container'>
		<div class="row">
			
			<?php
				//IF STATSU NEW THEN ASSSIGN CAT MANAGER
				if($vendor_info->status==Config::get('constants.vendorNewStatus')){
			?>
				<a href='javascript:void(0)' class='rejectedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-danger'><i class='fa fa-close'></i> Reject Application</small></a>
				<a href='javascript:void(0)'  class='approvedVendor' vendor-id='<?php echo $vendor_info->id; ?>'><small class='label label-success'><i class='fa  fa-user'></i> Accept & Assign Manager</small></a>
			<?php
				}elseif($vendor_info->vendor_manager_assigned==Auth::user()->id){
					//IF CM/VM&HOV is Assigned then this person cancel can reject application
					//echo $vendor_info->vendor_manager_assigned."<br/>".Auth::user()->id;
			?>
					<a href='javascript:void(0)' class='rejectedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-danger'><i class='fa fa-close'></i> Reject Application</small></a>
			<?php			
				}
			?>
		</div>
		
			<div style="clear:both;">&nbsp;</div>
		<div class="col-xs-9">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
				<?php

					$userRole = \App\User::findOrfail(Auth::user()->id);
					
					$superadminRole			=	Config::get('constants.superadminRole');
					$headOfVendor			=	Config::get('constants.headOfVendor');
					//DISPLAY ASSIGNED MANAGER FOR HOV AND SUPER ADMIN ONLYE
					if($userRole->hasRole($headOfVendor) || $userRole->hasRole($superadminRole) ){
					;
						if(!empty($vendor_info->assignedManager)){
							
						
				?>
						<div class="box-header callout callout-info" >
							<span style="float:right;">Assigned Manager : <b> <?php echo $vendor_info->assignedManager;?></b></span>
						</div>
				<?php	
						}
					}
				?>
              <table class="table table-hover">
                <tbody>
                <tr>
                  <td width="40%"><b>Company / Individual Name/(Registered Name)</b></td>
                  <td>{{$vendor_info->vendor_name}}</td>
                </tr>
                <tr>
                  <td><b>Owner's CNIC</b></td>
                  <td>{{$vendor_info->cnic}}</td>
                </tr>
                <tr>
                  <td><b>Email Address</b></td>
                  <td>{{$vendor_info->vendor_email}}</td>
                </tr>
                <tr>
                  <td><b>Cell Number</b></td>
                  <td>{{$vendor_info->cell_phone}}</td>
                </tr>
                <tr>
                  <td><b>Office Phone</b></td>
                  <td>{{$vendor_info->office_phone}}</td>
                </tr>
                <tr>
                  <td><b>Form Of Organization</b></td>
                  <td>{{$vendor_info->form_of_organization}}</td>
                </tr>
                <tr>
                  <td><b>POC Name</b></td>
                  <td>{{$vendor_info->poc_name}}</td>
                </tr>
                <tr>
                  <td><b>POC Email</b></td>
                  <td>{{$vendor_info->poc_email}}</td>
                </tr>
                <tr>
                  <td><b>POC Contact Number</b></td>
                  <td>{{$vendor_info->poc_contact}}</td>
                </tr>
                <tr>
                  <td><b>NTN Number</b></td>
                  <td>{{$vendor_info->ntn_number}}</td>
                </tr>
                <tr>
                  <td><b>Country</b></td>
                  <td>{{$vendor_info->country}}</td>
                </tr>
                <tr>
                  <td><b>Province</b></td>
                  <td>{{$vendor_info->province}}</td>
                </tr>
                <tr>
                  <td><b>City</b></td>
                  <td>{{$vendor_info->city}}</td>
                </tr>
                <tr>
                  <td><b>Address (Company Place of Business)</b></td>
                  <td>{{   App\Helper\Common::wordWrapContent($vendor_info->company_address) }}</td>
                </tr>
                <tr>
                  <td><b>Brand(s) Offered</b></td>
                  <td>{{$brandList}}</td>
                </tr>
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
	
		
</div>
	<?php
		//REJECTION LIST OPTION
		$reasonOption	=	"";
		if(!empty($getRejectionList)){
			foreach($getRejectionList as $getRejectionListRow){
				$reasonOption .= '<option value="'.$getRejectionListRow.'">'.$getRejectionListRow.'</option>';
			}
		}
		
	?>	
 <script>
	jQuery(document).ready(function(){
		
								
		/**
		* VENDOR APPROVAL POPUP
		*/
		
		jQuery(".approvedVendor").on('click',function(){
			
			//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
			jQuery('.ajax-loader-region').show();
		
			var vendorId = jQuery(this).attr('vendor-id');
			jQuery.ajax({
				url: "<?php echo url('approveVendorRequest');?>",
				type: "post",
				data: {
					"_token": "{{ csrf_token() }}",
					"vendor_id": vendorId
					
				},
				success: function(result){
					
					//REMOVE CLASS FOR DISPLAY MODAL SMALL
					jQuery('.customTwo-modal-dialog').removeClass('modal-lg');
					jQuery('.customTwo-modal-view').modal('toggle');
					jQuery('.customTwo-modal-title').html('Vendor Info');//SET TITLE
					jQuery('.customTwo-modal-body').html(result);//SET BODY
					
					//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
					jQuery('.ajax-loader-region').hide();
				}
			});
		});
						
		
		/**
		* REJECT VENDOR
		*/
		jQuery('.rejectedVendor').on('click', function(e){
			
			//UNBIND CONFIRM YES BUTTON CLICK EVENT
			jQuery('.confirm').unbind();
			
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			
			var vendorId = jQuery(this).attr('vendor-id');
			
			jQuery('.confirm-modal-footer').show();
			jQuery('.confirm-modal-footerWaiting').hide();
			
			jQuery('.confirm-modal-body').html('Are you sure you want to reject vendor application?  <div class="form-group"><label>Rejection Reason </label><select class="form-control"  multiple="multiple" id="lead_rejection_reason" name="lead_rejection_reason"><?php echo $reasonOption; ?></select><div class="error-region-rejection" style="display:none; color:red;"></div></div>');//SET BODY
			
			jQuery('.confirm').modal({ backdrop: 'static', keyboard: false }).on('click', '#delete', function (e) {
					//$form.trigger('submit');
					jQuery('.confirm-modal-footer').hide();
					jQuery('.confirm-modal-footerWaiting').show();
					
					
					//GET REJECTION REASON
					var lead_rejection_reason = jQuery.trim(jQuery('#lead_rejection_reason').val());
				
					if(lead_rejection_reason==""){
						jQuery(".error-region-rejection").html("Please provide rejection reason.");
						jQuery(".error-region-rejection").show();
						
						jQuery('.confirm-modal-footer').show();
						jQuery('.confirm-modal-footerWaiting').hide();
					}else{
						jQuery(".error-region-rejection").html("");
						jQuery(".error-region-rejection").hide();
										
							
						lead_rejection_reason = encodeURIComponent(lead_rejection_reason);
							
						//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
						jQuery('.ajax-loader-region').show();
						jQuery.ajax({
							url: "<?php echo url('rejected');?>",
							type: "post",
							data: {
								"_token": "{{ csrf_token() }}",
								"vendor_id": vendorId,
								"lead_rejection_reason":lead_rejection_reason
								
							},
							success: function(result){
								if(result=='success'){
									location.reload();
								}
								
							}
						});
					}				
			});
		});
	});
 </script>
@endsection