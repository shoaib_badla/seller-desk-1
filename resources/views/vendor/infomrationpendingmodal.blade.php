@extends('layouts.empty')
@section('title','Index')
@section('content')

	
		<div class="col-md-9">	
		
			
				<div class="row">
				
					<?php
					
						$rejectionUrl	=	url('rejected');
						$approvalUrl	=	url('/');
						//PANEL FOR ASSIGN CAT/VEND MANAGER & APPLICATION STATUS IS NOT APPROVED
						if($vendor_info->vendor_manager_assigned==Auth::user()->id && $vendor_info->is_vendor_approved!=1){
					?>
						<?php
								//NO REJECTION WILL BE DISPLAY TO VM/CM & HOV WHEN PENDING FROM OTHER DEPART & SEND FOR CORRECTION, REJECTION & SEND APPLUCATIONTO VENDOR
							if(!in_array($vendor_info->is_vendor_approved,array(4,5,6))){
								
								//CHECK APPLICATION APROVED FROM BOTH DEPART THEN CONCERN PERSON NOT ABLE TO CORRECTION, REJECTION & SEND FOR REVIEW APPLICATION
								if($vendor_info->is_vendor_finance_approved==1 && $vendor_info->is_vendor_ops_approved==1){
									
										
								}else{
						?>
									<a href='javascript:void(0)' class='rejectedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-danger'><i class='fa fa-close'></i> Reject Application</small></a> 
									<a href='javascript:void(0)' class='sendForCorrection' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-warning'><i class='fa fa-send-o'></i> Send For Correction</small></a>
									<a href='javascript:void(0)' class='sendForFutherReview' vendor-id='<?php echo $vendor_info->id;?>'><small class='label bg-blue'><i class='fa  fa-users'></i> Send Application to Operation & Finance </small></a> 
						<?php						
								}
						?>
							
						<?php
							}
						?>
						
							<?php
								//SHOW VENDOR APPLICATION WHEN ALL DEAPRT FEEDBACK COME
								if($vendor_info->is_vendor_approved==7){
										//CHECK APPLICATION APROVED FROM BOTH DEPART
										if($vendor_info->is_vendor_finance_approved==1 && $vendor_info->is_vendor_ops_approved==1){
											
											$approvalUrl	=	url('approved');
							?>
											<a href='javascript:void(0)' class='approveVendorRequest' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-success'><i class='fa  fa-check'></i> Approved Application</small></a> 
							<?php
										}
								}
							?>
						<?php
						}
					?>
					
					<?php 
							
							if(!in_array($vendor_info->is_vendor_approved,array(4,5,7))){
					?>
								<?php
									//DISPLAY FINANCE AND OPS DEPART BUTTON
									if($is_financeUser || $is_OpsUser){
										if($is_financeUser){
											$rejectionUrl	=	url('rejectedfromfinance');
											$approvalUrl	=	url('approvedfromfinance');
								?>
											<?php
												//DISPLAY REJECTION IF NOT REJECTED 
												if($vendor_info->is_vendor_finance_approved==3){
											?>
												<a href='javascript:void(0)' class='rejectedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-danger'><i class='fa fa-close'></i> Reject Application</small></a> 
												<a href='javascript:void(0)' class='approvedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-success'><i class='fa  fa-check'></i> Approved Application</small></a> 
											<?php
												}
											?>
								<?php
										}
										if($is_OpsUser){
											$rejectionUrl	=	url('rejectedfromops');
											$approvalUrl	=	url('approvedfromops');
								?>
											<?php
												//DISPLAY REJECTION IF NOT REJECTED 
												if($vendor_info->is_vendor_ops_approved==3){
											?>
												<a href='javascript:void(0)' class='rejectedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-danger'><i class='fa fa-close'></i> Reject Application</small></a> 
												<a href='javascript:void(0)' class='approvedVendor' vendor-id='<?php echo $vendor_info->id;?>'><small class='label label-success'><i class='fa fa-check'></i> Approved Application</small></a> 
											<?php
												}
											?>
								<?php
										}
											
									}
								?>
								
					<?php
							}
						
					?>
				</div>
			
			
			<div style="clear:both;">&nbsp;</div>
			 <div class="panel with-nav-tabs panel-primary">

				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab6primary"  class="tab6primary" data-toggle="tab">Basic Info</a></li>
						<li ><a href="#tab1primary"  class="tab1primary" data-toggle="tab">General Info</a></li>
						<li><a href="#tab2primary"  class="tab2primary" data-toggle="tab">Financial Info</a></li>
						
						<li><a href="#tab4primary"  class="tab4primary" data-toggle="tab">Pickup Address(s)</a></li>
						<li><a href="#tab3primary"  class="tab3primary" data-toggle="tab">Return Address</a></li>
						<li><a href="#tab5primary" class="tab5primary" data-toggle="tab">Category(s)</a></li>
						
					</ul>
				</div>
			
				<!-- /.box-header -->
				<div class="panel-body">
					<div class="tab-content">
					
						<div class="tab-pane fade in active " id="tab6primary">
							<div class="row box-body">
							
								  <table class="table table-hover">
									<tbody>
											<?php

												$userRole = \App\User::findOrfail(Auth::user()->id);
												
												$superadminRole			=	Config::get('constants.superadminRole');
												$headOfVendor			=	Config::get('constants.headOfVendor');
												//DISPLAY ASSIGNED MANAGER FOR HOV AND SUPER ADMIN ONLYE
												if($userRole->hasRole($headOfVendor) || $userRole->hasRole($superadminRole) ){
												
													if(!empty($vendor_info->assignedManager)){
														
													
											?>
									
													<tr>
													  <td width="40%"><b>Assigned Manager</b></td>
													  <td>{{$vendor_info->assignedManager}}</td>
													</tr>
													
											<?php	
													}
												}
											?>
									<tr>
									  <td width="40%"><b>Company / Individual Name/(Registered Name)</b></td>
									  <td>{{$vendor_info->vendor_name}}</td>
									</tr>
									<tr>
									  <td><b>Owner's CNIC</b></td>
									  <td>{{$vendor_info->cnic}}</td>
									</tr>
									<tr>
									  <td><b>Email Address</b></td>
									  <td>{{$vendor_info->vendor_email}}</td>
									</tr>
									<tr>
									  <td><b>Cell Number</b></td>
									  <td>{{$vendor_info->cell_phone}}</td>
									</tr>
									<tr>
									  <td><b>Office Phone</b></td>
									  <td>{{$vendor_info->office_phone}}</td>
									</tr>
									<tr>
									  <td><b>Form Of Organization</b></td>
									  <td>{{$vendor_info->form_of_organization}}</td>
									</tr>
									<tr>
									  <td><b>POC Name</b></td>
									  <td>{{$vendor_info->poc_name}}</td>
									</tr>
									<tr>
									  <td><b>POC Email</b></td>
									  <td>{{$vendor_info->poc_email}}</td>
									</tr>
									<tr>
									  <td><b>POC Contact Number</b></td>
									  <td>{{$vendor_info->poc_contact}}</td>
									</tr>
									<tr>
									  <td><b>NTN Number</b></td>
									  <td>{{$vendor_info->ntn_number}}</td>
									</tr>
									<tr>
									  <td><b>Country</b></td>
									  <td>{{$vendor_info->country}}</td>
									</tr>
									<tr>
									  <td><b>Province</b></td>
									  <td>{{$vendor_info->province}}</td>
									</tr>
									<tr>
									  <td><b>City</b></td>
									  <td>{{$vendor_info->city}}</td>
									</tr>
									<tr>
									  <td><b>Address (Company Place of Business)</b></td>
									  <td>{{   App\Helper\Common::wordWrapContent($vendor_info->company_address) }}</td>
									</tr>
									<tr>
									  <td><b>Brand(s) Offered</b></td>
									  <td>{{$brandList}}</td>
									</tr>
									
								  </tbody></table>
							  </div>
			  <!-- /.row -->
						</div>
							
						<div class="tab-pane fade " id="tab1primary">
							  <div class="row box-body">
								  <table class="table table-hover">
									<tbody>
										<tr>
										  <td width="40%"><b>Shop Online Name</b></td>
										  <td>{{$vendor_info->shop_online_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company Owner Name (as per CNIC) </b></td>
										  <td>{{$vendor_info->company_owner_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Authorized Person Name (if other than owner)</b></td>
										  <td>{{$vendor_info->authorize_person_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Authorized Person CNIC (if other than owner)</b></td>
										  <td>{{$vendor_info->authorize_person_cnic}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company Website</b></td>
										  <td>{{$vendor_info->company_website}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Billing Cycle</b></td>
										  <td>{{$vendor_info->payment_cycle}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Acceptance of Returns </b></td>
										  <td>{{$vendor_info->acceptance_of_return}}</td>
										</tr>
										<?php if($vendor_info->acceptance_of_return=='Yes'){ ?>
											<tr>
											  <td width="40%"><b>Acceptance of Returns Days </b></td>
											  <td>{{$vendor_info->acceptance_of_return_range}}</td>
											</tr>
										<?php } ?>
										
										<tr>
										  <td width="40%"><b>Offer Exchange</b></td>
										  <td>{{$vendor_info->offer_exchange}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Witholding tax deposited by</b></td>
										  <td>{{$vendor_info->withholding_tax_dposited_by}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>International Shipping</b></td>
										  <td>{{$vendor_info->internation_shipping}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Business Model</b></td>
										  <td>{{$vendor_info->business_model}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company / Individual Registered Addressed (as per NTN / CNIC)</b></td>
										  <td>{{$vendor_info->company_individual_registered_address}}</td>
										</tr>
										
										
									</tbody>
								  </table>
								
									<!-- /.col -->
								  </div>
				  <!-- /.row -->
							</div>
							
							 <div class="tab-pane fade" id="tab2primary">
								   <div class="row">
										<?php
											if(!empty($vendBankInfoArr)){
												foreach($vendBankInfoArr as $vendBankInfoRow){
													
										?>
												<table class="table table-hover">
													
													<tbody>
														<tr>
														  <td width="40%"><b>Bank Name</b></td>
														  <td><?php echo $vendBankInfoRow['bank_name'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Bank Account Title</b></td>
														   <td><?php echo $vendBankInfoRow['bank_account_title'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Bank Account Number</b></td>
														  <td><?php echo $vendBankInfoRow['bank_account_number'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>STRN</b></td>
														  <td><?php echo $vendBankInfoRow['strn'];?></td>
														</tr>
													</tbody>
												</table>
												
										<?php										
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							
							
							 <div class="tab-pane fade" id="tab3primary">
								   <div class="row">
										<?php
											if(!empty($vendorReturnAddressArr)){	
												$loop = 1;
												foreach($vendorReturnAddressArr as $vendorReturnAddressArrRow){
													
										?>
												<table class="table table-hover">
													
													<tbody>
														<tr>
														  <td width="40%"><b>Pickup Address</b></td>
														  <td><?php echo $vendorReturnAddressArrRow['return_address'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Province</b></td>
														   <td><?php echo $vendorReturnAddressArrRow['return_city'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>City</b></td>
														  <td><?php echo $vendorReturnAddressArrRow['return_province'];?></td>
														</tr>
														
													</tbody>
												</table>
												
										<?php		
													$loop++;								
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <div class="tab-pane fade" id="tab4primary">
							 
							
								<div class="row">
										<?php
											if(!empty($VendorPickupAddressArr)){	
												$loop = 1;
												foreach($VendorPickupAddressArr as $vendorPickupAddressArrRow){
													
										?>
												<table class="table table-hover">
													<thead>
														<tr style="background-color:#f9f9f9"><th colspan="2">Pickup Address : <?php echo $loop;?></th></tr>
													</thead>
													<tbody>
														<tr>
														  <td width="40%"><b>Pickup Address</b></td>
														  <td><?php echo $vendorPickupAddressArrRow['pickup_address'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Province</b></td>
														   <td><?php echo $vendorPickupAddressArrRow['pickup_city'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>City</b></td>
														  <td><?php echo $vendorPickupAddressArrRow['pickup_province'];?></td>
														</tr>
														
													</tbody>
												</table>
												
										<?php		
													$loop++;								
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <!-- CATEGORY TAB START -->
							  <div class="tab-pane fade" id="tab5primary">
								<div class="row" style="max-height:300px; overflow-y:scroll;">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th >S.No</th>
												<th >L1 Category</th>
												<th >L2 Category</th>
												<th >L3 Category</th>
												<th >Standard Margin</th>
												<th>Proposed Margin</th>
											</tr>
										</thead>
										<tbody>
										
											<?php
											 if(count($vendorMargin)>0){
												 $sno = 1;
												 foreach($vendorMargin as $row){
													
											?>
													 <tr role="row">
													  <td ><?php echo $sno;?></td>
													  <td><?php echo $row->L1_Category;?></td>
													  <td><?php echo $row->L2_Category;?></td>
													  <td><?php echo $row->L3_Category;?></td>
													  <td><?php echo $row->margin;?></td>
													  <td>
														  <?php  echo $row->proposed_commission;  ?>	

													  </td>
													<input type='hidden' name='categoryId[]' value='<?php echo $row->category_id;?>'  />
													</tr>
										<?php		
												$sno++;
											 }
										 }else{
											
										?>
											 <tr role="row"><td colspan="6">No Record Found</td></tr>
										<?php
										 }
										?>
									   
									
										</tbody>
									
								  </table>
								  </div>
								 <?php
									if($vendor_info->agree_for_all_other_catgeory){
								 ?>
									 <div class="row ">
									  <div class="col-md-8">
											<div class="checkbox">
											  <label>
												View All Other's Margins : 
											  </label><a href='javascript:void();' onClick='return false;' class='viewAllCategoryMargin'>View Margin</a>
											</div>
										</div>
									 </div>
								<?php } ?>
							 </div>
							 <!-- CATEGORY TAB END -->
				<!-- /.box-body -->
						</div>
					</div>
			
					
			</div>
		</div>
	
	<?php
		//REJECTION LIST OPTION
		$reasonOption	=	"";
		if(!empty($getRejectionList)){
			foreach($getRejectionList as $getRejectionListRow){
				$reasonOption .= '<option value="'.$getRejectionListRow.'">'.$getRejectionListRow.'</option>';
			}
		}
		
	?>	
  <script>
  
	jQuery(document).ready(function(){
		
	
		/**
		* SEND FOR CORRECTION VENDOR
		*/
		jQuery('.sendForCorrection').on('click', function(e){
			
			//UNBIND CONFIRM YES BUTTON CLICK EVENT
			jQuery('.confirm').unbind();
			
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			
			var vendorId = jQuery(this).attr('vendor-id');
			
			jQuery('.confirm-modal-footer').show();
			jQuery('.confirm-modal-footerWaiting').hide();
			
			jQuery('.confirm-modal-body').html('Are you sure you want to send vendor application for correction?  <div class="form-group"><label>Correction Reason </label><textarea class="form-control" rows="3" placeholder="Enter Correction Comment" name="correction_comment" id="correction_comment"></textarea><div class="error-region-correction" style="display:none; color:red;"></div></div>');//SET BODY
			
			jQuery('.confirm').modal({ backdrop: 'static', keyboard: false }).on('click', '#delete', function (e) {
			
					//$form.trigger('submit');
					jQuery('.confirm-modal-footer').hide();
					jQuery('.confirm-modal-footerWaiting').show();
					
					
					//GET REJECTION REASON
					var correction_comment = jQuery.trim(jQuery('#correction_comment').val());
				
					if(correction_comment==""){
						jQuery(".error-region-correction").html("Please provide correction comment.");
						jQuery(".error-region-correction").show();
						
						jQuery('.confirm-modal-footer').show();
						jQuery('.confirm-modal-footerWaiting').hide();
					}else{
						jQuery(".error-region-correction").html("");
						jQuery(".error-region-correction").hide();
										
							
						correction_comment = encodeURIComponent(correction_comment);
							
						//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
						jQuery('.ajax-loader-region').show();
						jQuery.ajax({
							url: "<?php echo url('sendForCorrection');?>",
							type: "post",
							data: {
								"_token": "{{ csrf_token() }}",
								"vendor_id": vendorId,
								"correction_comment":correction_comment
								
							},
							success: function(result){
								if(result=='success'){
									location.reload();
								}
								
							}
						});
					}				
			});
		});
		/**
		* REJECT VENDOR
		*/
		jQuery('.rejectedVendor').on('click', function(e){
			
			//UNBIND CONFIRM YES BUTTON CLICK EVENT
			jQuery('.confirm').unbind();
			
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			
			var vendorId = jQuery(this).attr('vendor-id');
			
			jQuery('.confirm-modal-footer').show();
			jQuery('.confirm-modal-footerWaiting').hide();
			
			jQuery('.confirm-modal-body').html('Are you sure you want to reject vendor application?  <div class="form-group"><label>Rejection Reason </label><select class="form-control"  multiple="multiple" id="lead_rejection_reason" name="lead_rejection_reason"><?php echo $reasonOption; ?></select><div class="error-region-rejection" style="display:none; color:red;"></div></div>');//SET BODY
			
			jQuery('.confirm').modal({ backdrop: 'static', keyboard: false }).on('click', '#delete', function (e) {
					//$form.trigger('submit');
					jQuery('.confirm-modal-footer').hide();
					jQuery('.confirm-modal-footerWaiting').show();
					
					
					//GET REJECTION REASON
					var lead_rejection_reason = jQuery.trim(jQuery('#lead_rejection_reason').val());
				
					if(lead_rejection_reason==""){
						jQuery(".error-region-rejection").html("Please provide rejection reason.");
						jQuery(".error-region-rejection").show();
						
						jQuery('.confirm-modal-footer').show();
						jQuery('.confirm-modal-footerWaiting').hide();
					}else{
						jQuery(".error-region-rejection").html("");
						jQuery(".error-region-rejection").hide();
										
							
						lead_rejection_reason = encodeURIComponent(lead_rejection_reason);
							
						//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
						jQuery('.ajax-loader-region').show();
						jQuery.ajax({
							url: "<?php echo $rejectionUrl?>",
							type: "post",
							data: {
								"_token": "{{ csrf_token() }}",
								"vendor_id": vendorId,
								"lead_rejection_reason":lead_rejection_reason
								
							},
							success: function(result){
								if(result=='success'){
									location.reload();
								}
								
							}
						});
					}				
			});
		});
		
		/**
		* APPROVE VENDOR APPLICATION
		*/
		jQuery('.approvedVendor').on('click', function(e){
			
			//UNBIND CONFIRM YES BUTTON CLICK EVENT
			jQuery('.confirm').unbind();
			
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			
			var vendorId = jQuery(this).attr('vendor-id');
			
			jQuery('.confirm-modal-footer').show();
			jQuery('.confirm-modal-footerWaiting').hide();
			
			jQuery('.confirm-modal-body').html('Are you sure you want to approve vendor application?  ');//SET BODY
			
			jQuery('.confirm').modal({ backdrop: 'static', keyboard: false }).on('click', '#delete', function (e) {
					//$form.trigger('submit');
					jQuery('.confirm-modal-footer').hide();
					jQuery('.confirm-modal-footerWaiting').show();
					
					
						//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
						jQuery('.ajax-loader-region').show();
						jQuery.ajax({
							url: "<?php echo $approvalUrl?>",
							type: "post",
							data: {
								"_token": "{{ csrf_token() }}",
								"vendor_id": vendorId
								
							},
							success: function(result){
								if(result=='success'){
									location.reload();
								}
								
							}
						});
				});				
			});
		
		
		
	//VIEW ALL OTHER MARGIN
		jQuery('.viewAllCategoryMargin').on('click',function(){
			
			 jQuery('.ajax-loader-region').show();
			 
			//GET SELECTED L3 CATEGORY ID ARRAY
			var selectedL3CategoryForMargin = getSelectedL3Category();
			selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			
			if(selectedL3CategoryForMargin==""){
				var selectedL3CategoryForMargin = [1];
				selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			}
			
			
			//GET ALL OTHER MARGIN
			jQuery.ajax({
				url: "<?php echo url('viewAllOtherCategory');?>",
				type: "post",
				data: {
					"_token": "{{ csrf_token() }}",
					"category_id": selectedL3CategoryForMargin
				},
				success: function(result){
					
					jQuery('.customTwo-modal-view').modal('toggle');
					jQuery('.customTwo-modal-dialog').addClass('modal-lg');
					jQuery('.customTwo-modal-title').html('Vendor Info');//SET TITLE
					jQuery('.customTwo-modal-body').html(result);//SET BODY
			
					//console.log(opts);
					 jQuery('.ajax-loader-region').hide();
				}
			});
			
			
		});
		
		/**
		* FINAL VENDOR APPROVED WITH CONTRACT UP
		*/
		jQuery('.approveVendorRequest').on('click',function(){
			
			jQuery('.ajax-loader-region').show();
			
			var vendorId = jQuery(this).attr('vendor-id');
			
			//GET ALL OTHER MARGIN
			jQuery.ajax({
				url: "<?php echo $approvalUrl;?>",
				type: "post",
				data: {
					"_token": "{{ csrf_token() }}",
					"vendor_id": vendorId
				},
				success: function(result){
					
					jQuery('.customTwo-modal-view').modal('toggle');
					jQuery('.customTwo-modal-dialog').addClass('modal-lg');
					jQuery('.customTwo-modal-title').html('Vendor Info');//SET TITLE
					jQuery('.customTwo-modal-body').html(result);//SET BODY
					
					jQuery('.ajax-loader-region').hide();
					//console.log(opts);
					
				}
			});
			
		});
	});
	
	/**
	* SEND TO OTHER DEPART FOR REVIEW
	*/
	jQuery('.sendForFutherReview').on('click',function(){
		
		//UNBIND CONFIRM YES BUTTON CLICK EVENT
		jQuery('.confirm').unbind();
		
		$.fn.modal.Constructor.prototype.enforceFocus = function () {};
		
		var vendorId = jQuery(this).attr('vendor-id');
		
		jQuery('.confirm-modal-footer').show();
		jQuery('.confirm-modal-footerWaiting').hide();
		
		jQuery('.confirm-modal-body').html('Are you sure you want send vendor application to Operation & Finance departments for review? ');//SET BODY
		
		jQuery('.confirm').modal({ backdrop: 'static', keyboard: false }).on('click', '#delete', function (e) {
				//$form.trigger('submit');
				jQuery('.confirm-modal-footer').hide();
				jQuery('.confirm-modal-footerWaiting').show();
				
				//SHOW LOADER B/C AJAX EVENT NOT CALL AFTER ONCOLSE IN JQGRID
				jQuery('.ajax-loader-region').show();
				jQuery.ajax({
					url: "<?php echo url('sendapplicationforreview');?>",
					type: "post",
					data: {
						"_token": "{{ csrf_token() }}",
						"vendor_id": vendorId
						
					},
					success: function(result){
						if(result=='success'){
							location.reload();
						}
						
					}
				});
		});
	});
	
	function getSelectedL3Category(){
		
		var selectedL3CategoryForMargin = [];
		jQuery('input[name^="categoryId"]').each(function() {
			selectedL3CategoryForMargin.push(jQuery(this).val());
		});
		
		return selectedL3CategoryForMargin;
	}
  </script>

@endsection