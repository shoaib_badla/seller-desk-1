@extends('layouts.ajax')
@section('title','Index')
@section('content')


<div class="box box-primary">
           
		   {!! Form::open(array('url'=>'downloadmargin','method'=>'POST','id'=>'downloadMargin','target'=>'_blank')) !!}
				<input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $vendorId;?>"/>
				 <span class='downloadMargin' ><a href="#" class = 'btn btn-primary btn-sm'  ><i class="fa fa-save"></i> Download Margin </a> </span>
			  {!! Form::close() !!}
            <!-- /.box-header -->
            <!-- form start -->
           {!! Form::open(array('url'=>'approvedSubmit','method'=>'POST','id'=>'approvedVendor', 'files'=>true)) !!}
		
			<input type="hidden" name="vendor_id" id="vendor_id" value="<?php echo $vendorId;?>"/>
              <div class="box-body">
                
               
                <div class="form-group">
				<div class='err-region alert alert-danger' style='display:none;'></div>
				
                 <label for="signing_contract">Signing Contract</label>
					<input type="file"  id="signing_contract" name = "signing_contract" >
					<p class="help-block">Allowed format (PDF).</p>
                </div>
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <span class='btn-submit' ><a href="#" class = 'btn btn-primary btn-sm'  id="submit">Submit</a></span>
				<span class='form-waiting-region' style="display:none;"><i class="fa fa-refresh fa-spin"></i>Please Wait...</span>
			
				
		
              </div>
			{!! Form::close() !!}
          </div>
<script>
//VALIDATION FORM SUBMISION PUT HERE BECAUSE OF TOKEN
	jQuery(document).ready(function(){
		jQuery('#submit').on('click',function(){
		   
		   
			jQuery('.btn-submit').hide();
			jQuery('.form-waiting-region').show();

			var formValid = jQuery('#approvedVendor').valid();
			
			
			
			//CHECK FORM IS VALID AND NO ERROR OCCUR
			if(formValid){
				
				var vendor_id 	= jQuery.trim(jQuery('#vendor_id').val());
				//CHECK MARGIN DOWNLOADED OR NOT
				jQuery.ajax({
					url: "<?php echo url('checkmargindownloaded');?>",
					type: "post",
					data: {
						"_token": "{{ csrf_token() }}",
						"vendor_id": vendor_id
					},
					success: function(result){
					
						if(result == 'success'){
							jQuery('#approvedVendor').submit();
							jQuery('.err-region').hide();

							jQuery('.btn-submit').hide();
							jQuery('.form-waiting-region').show();
						}else{
							jQuery('.err-region').html('Download Contract Annexture first before upload contract!');
							jQuery('.err-region').show();
							
							jQuery('.btn-submit').show();
							jQuery('.form-waiting-region').hide();
						}
						
					}
				});

			}else{
				

				jQuery('.btn-submit').show();
				jQuery('.form-waiting-region').hide();
			}
							
		});
		
		   jQuery("#approvedVendor").validate({
            errorElement: 'span',
            errorClass: 'help-block error-help-block',

            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length ||
                    element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.insertAfter(element.parent());
                    // else just place the validation message immediatly after the input
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // add the Bootstrap error class to the control group
            },

            
           
             
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // remove the Boostrap error class from the control group
            },

            focusInvalid: false, // do not focus the last invalid input
            ignore:"ui-tabs-hide",
            rules:  {
					  "signing_contract": {
						 "laravelValidation": [
							 ["Required", [], "The Signing Contract is required.", true],
							 ["Max", ["20000"], "The Signing Contract may not be greater than 20 MB.", true],
							 ["Mimes", ["pdf"], "The Signing Contract must be a file of type: PDF.", false]
						 ]
					 }
					 
					
				 }
			 });
			 
			 //DOWNLOAD Margin
			 jQuery('.downloadMargin').on('click',function(){
				 jQuery('#downloadMargin').submit();
			 });
	});
</script>
@endsection