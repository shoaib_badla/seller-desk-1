@extends('layouts.vendormargin')
@section('title','Index')
@section('content')

<div class = 'container'>
	<h3 class="box-title">
		Detail Information Required
	</h3>
		
	<style>
	.errorTxt{color:#dd4b39 !important;}
	</style>
    <div class="row">
	<div class="alert alert-danger jserrorregion" style="display:none;">
		<ul>
				<li>Please fill all required field </li>
		</ul>
	</div>
	  {!! Form::open(array('url'=>'vendordetail','method'=>'POST','id'=>'addVendorDetailInfo')) !!}
		@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
	<div style='float:right;padding-right:100px;' >
	<!-- <button class = 'btn btn-primary btn-sm' type="submit">submit</button>-->
		<span class='btn-submit' ><a href="#" class = 'btn btn-primary btn-sm'  id="submit">Submit</a></span>
		<span class='form-waiting-region' style="display:none;"><i class="fa fa-refresh fa-spin"></i>Please Wait...</span>
	
	<div class='err-region alert alert-danger' style='display:none;'></div>
	
	</div>
		<div class="col-md-11">
			
			 <div class="panel with-nav-tabs panel-primary">
			
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li><a href="#tab6primary"  class="tab6primary" data-toggle="tab">Basic Information</a></li>
						<li class="active"><a href="#tab1primary"  class="tab1primary" data-toggle="tab">General Information</a></li>
						<li><a href="#tab2primary"  class="tab2primary" data-toggle="tab">Financial Information</a></li>
						
						<li><a href="#tab4primary"  class="tab4primary" data-toggle="tab">Pickup Address(s)</a></li>
						<li><a href="#tab3primary"  class="tab3primary" data-toggle="tab">Return Address</a></li>
						<li><a href="#tab5primary" class="tab5primary" data-toggle="tab">Category(s)</a></li>
						
					</ul>
				</div>
			
				<!-- /.box-header -->
				<div class="panel-body">
					<div class="tab-content">
					
						<div class="tab-pane fade " id="tab6primary">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group col-md-8">
												<label for="vendor_name">Company/Individual Name/(Registered Name) <span class="errorTxt">*</span></label>
												<input id="vendor_name" name = "vendor_name" type="text" class="form-control  validate" tabindex="1" value="<?php echo $vendor_info->vendor_name;?>">
												<div for="vendor_name"></div>
											</div>
											
											<div class="form-group col-md-8">
												<label for="vendor_email">Email Address <span class="errorTxt">*</span></label>
												<input  type="text" class="form-control  validate" tabindex="3"  value="<?php echo $vendor_info->vendor_email;?>" disabled>
												<div for="vendor_email"></div>
											</div>
										
											<div class="form-group col-md-8">
												<label for="office_phone">Office Phone <span class="errorTxt">*</span></label>
												<div class="sellercenter-widthspan">
													<span class="num">+92</span>
													<input id="office_phone" name = "office_phone"  type="text" class="numericOnlyField form-control  validate maxCharacterLimit firstCharacterCantZero" value="<?php echo $vendor_info->office_phone;?>" maxLengthAllowed="10" tabindex="5" >
												</div>
												<div for="office_phone"></div>
											</div>
											
											<div class="form-group col-md-8">
												<label for="poc_name">POC Name <span class="errorTxt">*</span></label>
												<input id="poc_name" name = "poc_name" value="<?php echo $vendor_info->poc_name;?>" type="text" class="form-control  validate" tabindex="7">
												<div for="poc_name"></div>
											</div>
											
											
											 <div class="form-group col-md-8">
												<label for="poc_contact">POC Contact Number <span class="errorTxt">*</span></label>
												<div class="sellercenter-widthspan">
													<span class="num">+92</span>
													<input id="poc_contact" name = "poc_contact" type="text" class="numericOnlyField form-control  validate maxCharacterLimit firstCharacterCantZero" value="<?php echo $vendor_info->poc_contact;?>" maxLengthAllowed="10" tabindex="9">
												</div>
												<div for="poc_contact"></div>
											</div>
											
											
											
											
											
											 
											
											<div class="form-group col-md-8">
													
													<div class="seller-center-ntn">
														<label for="ntn1">NTN Number <span class="errorTxt">*</span></label>
												
															<?php  $ntnArr = explode("-",$vendor_info->ntn_number);?>
														<input id="ntn1" name = "ntn1" type="text" class="firstfield form-control  validate  numericOnlyField maxCharacterLimit" maxLengthAllowed="7"  value="<?php echo isset($ntnArr[0])?$ntnArr[0]:'';?>" tabindex="11" >
														<span class="dash">-</span>
														 <input id="ntn2" name = "ntn2" type="text" class="secondfield form-control  validate  numericOnlyField maxCharacterLimit"  maxLengthAllowed="1"  value="<?php echo isset($ntnArr[1])?$ntnArr[1]:'';?>" tabindex="12"  >
													</div>
													<div for="ntn1"></div>
													<div for="ntn2"></div>
											</div>
												
											
												
											
										
											
											 <div class="form-group col-md-8">
												<label for="brand">Brand(s) Offered</label>
												<select multiple=""  id="brand[]" name = "brand[]" class="form-control  validate" tabindex="14">
													<?php
														if(!empty($brandList)){
															foreach($brandList as $brandId=>$brandName){
													?>
																<option value="<?php echo $brandId;?>" <?php echo (in_array($brandId,$vendorBrand))?'selected="selected"':'';?> ><?php echo $brandName;?></option>
													<?php													
															}
														}
													?>
												</select>
												<div for="brand"></div>
										
											</div>
									
									
									 
									
								</div>
								<!-- /.col -->
								<div class="col-md-6">
									<div class="form-group col-md-8">
										<label for="cnic">Owner's CNIC <span class="errorTxt">*</span></label>
										<input id="cnic" name = "cnic" tabindex="2" type="text" class="form-control  validate numericOnlyField maxCharacterLimit" maxLengthAllowed="13" value="<?php echo $vendor_info->cnic;?>">
										<div for="cnic"></div>
									</div>
									<div class="form-group col-md-8">
										<label for="cell_phone">Cell Number <span class="errorTxt">*</span></label>
										<div class="sellercenter-widthspan">
											<span class="num">+92</span>
											<input id="cell_phone" name = "cell_phone" type="text" class="numericOnlyField form-control  validate maxCharacterLimit firstCharacterCantZero" value="<?php echo $vendor_info->cell_phone;?>" maxLengthAllowed="10"  tabindex="4">
										</div>
										<div for="cell_phone"></div>
									</div>
									
									 <div class="form-group col-md-8">
										<label for="form_of_organization">Form Of Organization <span class="errorTxt">*</span></label>
										<select id="form_of_organization" name = "form_of_organization"  class="form-control  validate" tabindex="6">
											<option value="">Please Select Country</option>
											<?php
												if(!empty($form_of_organization)){
													foreach($form_of_organization as $form_of_organizationId=>$form_of_organizationRow){
											?>
														<option value="<?php echo $form_of_organizationId;?>" <?php echo ($vendor_info->form_of_organization==$form_of_organizationId)?'selected="selected"':'';?>><?php echo $form_of_organizationRow;?></option>
											<?php													
													}
												}
											?>
										</select>
										<div for="form_of_organization"></div>
									</div>
									
									<div class="form-group col-md-8">
										<label for="poc_email">POC Email <span class="errorTxt">*</span></label>
										<input id="poc_email" name = "poc_email" type="text" value="<?php echo $vendor_info->poc_email;?>"   class="form-control  validate" tabindex="8">
										<div for="poc_email"></div>
									</div>
									
									
									<div class="form-group col-md-8">
										<label for="country">Country <span class="errorTxt">*</span></label>
										<select id="country" name = "country" class="form-control  validate"  tabindex="10">
											<option value="">Please Select Country</option>
											<?php
												if(!empty($countryList)){
													foreach($countryList as $countryId=>$countryListRow){
											?>
														<option value="<?php echo $countryId;?>" <?php echo ($vendor_info->country==$countryId)?'selected="selected"':'';?>><?php echo $countryListRow;?></option>
											<?php													
													}
												}
											?>
										</select>
										<div for="country"></div>
									</div>
									<div class="form-group col-md-8">
										<label for="province">Province <span class="errorTxt">*</span></label>
										<select id="province" name = "province" class="form-control  validate" tabindex="13" >
											<option value="">Please Select Province</option>
											<?php
												if(!empty($provinceList)){
													foreach($provinceList as $provinceId=>$provinceListRow){
											?>
														<option value="<?php echo $provinceId;?>" <?php echo ($vendor_info->province==$provinceId)?'selected="selected"':'';?>><?php echo $provinceListRow;?></option>
											<?php													
													}
												}
											?>
										</select>
										<div for="province"></div>
									</div>
									 <div class="form-group col-md-8">
										<label for="city">City <span class="errorTxt">*</span></label>
										<select id="city" name = "city" class="form-control  validate" tabindex="15" >
											<option value="">Please Select City</option>
											<?php
												if(!empty($cityList)){
													foreach($cityList as $cityId=>$cityListRow){
											?>
														<option value="<?php echo $cityId;?>" <?php echo ($vendor_info->city==$cityId)?'selected="selected"':'';?>><?php echo $cityListRow;?></option>
											<?php													
													}
												}
											?>
										</select>
										<div for="city"></div>
									</div>
									
									
									<div class="form-group col-md-8">
										<label for="company_address">Address (Company Place of Business) <span class="errorTxt">*</span></label>
										<textarea class="form-control maxAllowedCharacterArea" rows="3" id="company_address" name = "company_address" tabindex="17"  maxAllowedCharacter='500'><?php echo $vendor_info->company_address;?></textarea>
										<span class="maxcharacterLimit"></span>
										<div for="company_address"></div>
									</div>
							
									
								</div>
								<!-- /.col -->
							  </div>
			  <!-- /.row -->
						</div>
							
						<div class="tab-pane fade in active" id="tab1primary">
							  <div class="row">
									<div class="col-md-6">
										<div class="form-group col-md-8">
											<label for="shop_online_name">Shop Online Name <span class="errorTxt">*</span></label>
											<input id="shop_online_name" name = "shop_online_name" type="text" class="form-control validate">
										</div>
										 
										
										 <div class="form-group col-md-8">
											<label for="authorize_person_name">Authorized Person Name (if other than owner)</label>
											<input id="authorize_person_name" name = "authorize_person_name" type="text" class="form-control validate">
										</div>


										<div class="form-group col-md-8">
											<label for="company_website">Company Website <span class="errorTxt">*</span></label>
											<input id="company_website" name = "company_website" type="text" class="form-control validate">
											
										</div>
									
										
										<div class="form-group col-md-8">
											
											
											<div class="sellercenter-returns">
												<div class="field acceptance_of_return_Region">	
													<label for="acceptance_of_return">Acceptance of Returns </label>
													<select id="acceptance_of_return" name = "acceptance_of_return" class="form-control validate" >
														<option value="Yes"  >Yes</option>
														<option value="No" >No</option>
													</select>
												</div>
												<div class="field acceptance_of_return_rangeRegion" style="display:none;">
													<label for="acceptance_of_return_range">Days </label>
													<select id="acceptance_of_return_range" name = "acceptance_of_return_range" class="form-control validate" >
														<?php 
															for($i=15;$i<=31;$i++){
														?>
															<option value="<?php echo $i;?>" ><?php echo $i;?></option>
														<?php												
															}
														?>
														
													
													</select>
												</div>
											</div>
											
											
											
										</div>
										
										 <div class="form-group col-md-8">
											<label for="withholding_tax_dposited_by">Witholding tax deposited by <span class="errorTxt">*</span></label>
											
											<select id="withholding_tax_dposited_by" name = "withholding_tax_dposited_by" class="form-control validate" >
												<?php
													if(!empty($withholding_tax_dposited_by)){
														foreach($withholding_tax_dposited_by as $withholding_tax_dposited_id=>$withholding_tax_dposited_row){
															
												?>
															<option value="<?php echo $withholding_tax_dposited_id;?>"><?php echo $withholding_tax_dposited_row;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
									
										<div class="form-group col-md-8">
											<label for="business_model">Business Model <span class="errorTxt">*</span></label>
											<select id="business_model" name = "business_model" class="form-control validate" >
												<?php
													if(!empty($business_model)){
														foreach($business_model as $business_model_id=>$business_model_row){
												?>
															<option value="<?php echo $business_model_id;?>"><?php echo $business_model_row;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
										<?php /*
										<div class="form-group col-md-8">
											<label for="pickup_day">Pick Up <span class="errorTxt">*</span></label>
											<select id="pickup_day" name = "pickup_day" class="form-control validate" >
													<option value="Same Day">Same Day</option>
													<?php for($i=1;$i<=25;$i++){ ?>
															<option value="<?php echo $i;?>"><?php echo $i;?> <?php echo ($i>1)?'Days':'Day';?></option>
													<?php } ?>
											</select>
										</div>
										*/?>
										
										 
										
									</div>
									<!-- /.col -->
									<div class="col-md-6">
									
										<div class="form-group col-md-8">
											<label for="company_owner_name">Company Owner Name (as per CNIC) <span class="errorTxt">*</span></label>
											<input id="company_owner_name" name = "company_owner_name" type="text" class="form-control  validate">
											
										</div>
										<div class="form-group col-md-8">
											<label for="authorize_person_cnic">Authorized Person CNIC (if other than owner)</label>
											<input id="authorize_person_cnic" name = "authorize_person_cnic" type="text" class="form-control validate numericOnlyField maxCharacterLimit" maxLengthAllowed="13">
										</div>
										<div class="form-group col-md-8">
											<label for="payment_cycle">Payment Cycle <span class="errorTxt">*</span></label>
											<select id="payment_cycle" name = "payment_cycle" class="form-control validate" >
												<?php
													if(!empty($payment_cycle)){
														foreach($payment_cycle as $payment_cycle_row){
												?>
															<option value="<?php echo $payment_cycle_row;?>"><?php echo $payment_cycle_row;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
										
										 <div class="form-group col-md-8">
											<label for="offer_exchange">Offer Exchange <span class="errorTxt">*</span></label>
											<select id="offer_exchange" name = "offer_exchange" class="form-control validate" >
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
										 <div class="form-group col-md-8">
											<label for="internation_shipping">International Shipping <span class="errorTxt">*</span></label>
											<select id="internation_shipping" name = "internation_shipping" class="form-control validate" >
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>

										 <div class="form-group col-md-8">
											<label for="company_individual_registered_address">Company / Individual Registered Addressed (as per NTN / CNIC) <span class="errorTxt">*</span></label>
											<textarea class="form-control maxAllowedCharacterArea" rows="3" id="company_individual_registered_address" name = "company_individual_registered_address" maxAllowedCharacter='500'></textarea>
											<span class="maxcharacterLimit"></span>
										</div>
										
										
										
									</div>
									<!-- /.col -->
								  </div>
				  <!-- /.row -->
							</div>
							
							 <div class="tab-pane fade" id="tab2primary">
								   <div class="row">
										<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="bank_name">Bank Name</label>
												<input id="bank_name" name = "bank_name" type="text" class="form-control validate">
												
											</div>
											<div class="form-group col-md-8">
												<label for="bank_account_number">Bank Account Number</label>
												<input id="bank_account_number" name = "bank_account_number" type="text" class="form-control validate">
											</div>
											 
											
											
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											 <div class="form-group col-md-8">
												<label for="bank_account_title">Bank Account Title</label>
												<input id="bank_account_title" name = "bank_account_title" type="text" class="form-control validate">
											</div>
										    <div class="form-group col-md-8">
												<label for="strn">STRN</label>
												<input id="strn" name = "strn" type="text" class="form-control validate">
											</div>
										</div>
									<!-- /.col -->
								  </div>
							 </div>
							
							
							 <div class="tab-pane fade" id="tab3primary">
								   <div class="row">
										<div class="col-md-6">
											 <div class="form-group col-md-8">
												<label for="return_address">Address (for returns) <span class="errorTxt">*</span></label>
												<textarea class="form-control maxAllowedCharacterArea" rows="3" id="return_address" name = "return_address" maxAllowedCharacter="500"></textarea>
												<span class="maxcharacterLimit"></span>
											</div>
										
											 <div class="form-group col-md-8">
												<label for="return_city"> City <span class="errorTxt">*</span></label>
												<select id="return_city" name = "return_city" class="form-control validate" >
													<option value="">Please Select City</option>
													<?php
														if(!empty($cityList)){
															foreach($cityList as $cityId=>$cityListRow){
													?>
																<option value="<?php echo $cityId;?>"><?php echo $cityListRow;?></option>
													<?php													
															}
														}
													?>
												</select>
											</div>
											
											
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="return_province"> Province <span class="errorTxt">*</span></label>
												<select id="return_province" name = "return_province" class="form-control validate" >
													<option value="">Please Select Province</option>
													<?php
														if(!empty($provinceList)){
															foreach($provinceList as $provinceListId=>$provinceListRow){
													?>
																<option value="<?php echo $provinceListId;?>"><?php echo $provinceListRow;?></option>
													<?php													
															}
														}
													?>
												</select>
											</div>
										</div>
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <div class="tab-pane fade" id="tab4primary">
							 
							
								<div class="row pickup-address-region pickup-address-region1 ">
									<div class="box-header with-border">
									  <div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool addPickupAddress"  style='display:none;'><i class="fa fa-plus"></i></button>
										<button type="button" class="btn btn-box-tool removePickupAddress"  style='display:none;'><i class="fa fa-remove"></i></button>
									  </div>
									</div>
									<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="pickup_address">Pickup Address <span class="errorTxt">*</span></label>
												<textarea id="pickup_address[]" name = "pickup_address[]" type="text" class="form-control validate pickup_address maxAllowedCharacterArea" maxAllowedCharacter='500' rows="3" ></textarea>
												<span class="maxcharacterLimit"></span>
											</div>
											<div class="form-group col-md-8">
												<label for="pickup_city">City <span class="errorTxt">*</span></label>
												<select id="pickup_city[]" name = "pickup_city[]" class="form-control validate" >
													<option value="">Please Select City</option>
													<?php
														if(!empty($cityList)){
															foreach($cityList as $cityId=>$cityListRow){
													?>
																<option value="<?php echo $cityId;?>"><?php echo $cityListRow;?></option>
													<?php													
															}
														}
													?>
												</select>
											</div>
										
										</div>
										<!-- /.col -->
										<div class="col-md-6">
											<div class="form-group col-md-8">
												<label for="pickup_province">Province <span class="errorTxt">*</span></label>
												<select id="pickup_province[]" name = "pickup_province[]" class="form-control validate" >
													<option value="">Please Select Province</option>
													<?php
														if(!empty($provinceList)){
															foreach($provinceList as $provinceListId=>$provinceListRow){
													?>
																<option value="<?php echo $provinceListId;?>"><?php echo $provinceListRow;?></option>
													<?php													
															}
														}
													?>
												</select>
											</div>
											
											
										</div>
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <!-- CATEGORY TAB START -->
							  <div class="tab-pane fade" id="tab5primary">
								<div class="row">
									<div class="callout callout-danger error-help-block catgeorySelectionErr" id='tab5primary' style="display:none;">
									  <p>Please select category from 3rd Level.</p>
									</div>
									<div class="col-md-6">
										
										<div class="form-group col-md-8">
											<label for="l1_product_category">L1 Category</label>
											<select id="l1_product_category" name = "l1_product_category" class="form-control validate" >
												<option value="">Please Select Category</option>
												<?php
													if(!empty($categoryList)){
														foreach($categoryList as $category_id=>$category_name){
												?>
															<option value="<?php echo $category_id;?>"><?php echo $category_name;?></option>
												<?php													
														}
													}
												?>
											</select>
										</div>
										
									</div>
									<div class="col-md-6">
										<div class="form-group col-md-8">
											<label for="l2_product_category">L2 Category</label>
											<select id="l2_product_category" name = "l2_product_category" class="form-control validate" >
												<option value="">Please Select Category</option>
												
											</select>
										</div>
										
									</div>
									<!-- /.col -->
										
									<!-- /.col -->
								  </div>
								  <div class="row">
										<div class="col-md-4">
											<div class="box box-info  box-solid l3CatgeoryWaitingRegion"  style="display:none; " >
												<div class="box-header">
												   <h3 class="box-title">L3 Category</h3>
												</div>
												<div class="box-body">
													Loading...
												</div>
												<!-- /.box-body -->
												<!-- Loading (remove the following to stop the loading)-->
												<div class="overlay">
												  <i class="fa fa-refresh fa-spin"></i>
												</div>
											<!-- end loading -->
											</div>
											  <div class="box box-info  box-solid l3CatgeoryRegion" style="display:none; max-height:400px;  overflow-y: auto;  overflow-x: hidden;" >
													<div class="box-header ">
													  <h3 class="box-title">L3 Category</h3>
													</div>
													
													<div class="box-body">
													  <div class="table-responsive">
														<table class="table no-margin selectingMargingNegotiationTable">
														  <thead>
														  <tr>
															<th>Category</th>
															<th>Action</th>
														  </tr>
														  </thead>
														  <tbody class="l3CatgeoryList" >
														  
														  </tbody>
														</table>
													  </div>
													  <!-- /.table-responsive -->
													</div>
												</div>
									  </div>
									  
									  <div class="col-md-8">
											
											  <div class="box box-info  box-solid selectedCatgeoryRegion" style="display:none; max-height:400px;  overflow-y: auto;  overflow-x: hidden;" >
													<div class="box-header ">
													  <h3 class="box-title">Selected Category</h3>
													</div>
													
													<div class="box-body">
													  <div class="table-responsive">
														<table class="table no-margin margingNegotiationTable">
														  <thead>
														  <tr>
															<th>Category</th>
															<th>Standard Margin</th>
															<th>Proposed Margin</th>
															<th>&nbsp;</th>
														  </tr>
														  </thead>
														  <tbody>
														  
														  </tbody>
														</table>
													  </div>
													  <!-- /.table-responsive -->
													</div>
												</div>
									  </div>
								</div>
								 <div class="row selectedCatgeoryRegion" style="display:none;">
								  <div class="col-md-8">
										<div class="checkbox">
										  <label>
											<input type="checkbox" name="agree_for_all_other_catgeory" id="agree_for_all_other_catgeory" checked> I Agree for other categories with their Standard Margin 
										  </label><a href='javascript:void();' onClick='return false;' class='viewAllCategoryMargin'>View Margin</a>
										</div>
									</div>
								 </div>
							 </div>
							 <!-- CATEGORY TAB END -->
				<!-- /.box-body -->
						</div>
					</div>
			
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
 <!-- Laravel Javascript Validation -->
  

@include('vendor.common_js_method');
 <?php /* {!! $validator !!}*/?>
<script>
//RENDER AND DISPLAY CHARACTER LIMIT TEXT
//maxCharacterLimitForField('maxAllowedCharacterArea');

//GET L2 CATEGORY LIST
		jQuery('#l1_product_category').on('change',function(){
			
			var category_id 	=	jQuery.trim(jQuery(this).val());
			
			//RESET LEVEL CATEGORY
			jQuery('#l2_product_category').empty().append('<option value="">Please Select Category</option>');

			
			if(category_id!=''){
				jQuery.ajax({
						url: "<?php echo url('getChildCategory');?>",
						type: "post",
						data: {
							"_token": "{{ csrf_token() }}",
							"category_id": category_id
						},
						success: function(result){
							
							 var opts = jQuery.parseJSON(result);
							 // Parse the returned json data
							 if (!jQuery.isEmptyObject(opts))
							{
								// Use jQuery's each to iterate over the opts value
								jQuery.each(opts, function(i, d) {
									
									// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
									jQuery('#l2_product_category').append('<option value="' + i + '">' + d[0]+ '</option>');
								});
							}
							
							//console.log(opts);
							
						}
					});
			}
			
		});
		
		//GET L3 CATEGORY LIST
		jQuery('#l2_product_category').on('change',function(){
			
			var category_id 	=	jQuery.trim(jQuery(this).val());
			
			//RESET LEVEL CATEGORY
			jQuery('.l3CatgeoryWaitingRegion').show();
			jQuery('.l3CatgeoryRegion').hide();

			//ADD EMPTY CATAGEORY LIST DEAFULT
			jQuery('.l3CatgeoryList').html('<tr><td colspan="2">No Record Found</td></tr>');
			
			//GET SELECTED L3 CATEGORY ID ARRAY
			var selectedL3CategoryForMargin = getSelectedL3Category();
			//console.log(selectedL3CategoryForMargin);
			
			if(category_id!=''){
				jQuery.ajax({
						url: "<?php echo url('getChildCategory');?>",
						type: "post",
						data: {
							"_token": "{{ csrf_token() }}",
							"category_id": category_id
						},
						success: function(result){
							
							 var opts = jQuery.parseJSON(result);
							 // Parse the returned json data
							 if (!jQuery.isEmptyObject(opts))
							{
								var l3ListOption = "";
								// Use jQuery's each to iterate over the opts value
								jQuery.each(opts, function(i, d) {
									
									//CHECK IF CATEGORY NOT ALREADY SEELCTED THEN DON'T DISPLAY IT
									//console.log(jQuery.inArray( i, selectedL3CategoryForMargin ));
									 if(jQuery.inArray( i, selectedL3CategoryForMargin )>=0){
										 return true;
									 }
									// You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
									l3ListOption += "<tr class='l3CategoryForMargin'><td colspan='2'>"+d[0]+"<input type='hidden' value='"+d[0]+"' id='catgeory_name'/><input type='hidden' value='"+d[1]+"' id='standard_margin'/><input type='hidden' value='"+d[2]+"' id='catgeory_id'/><i style='float:right;' class='fa  fa-plus-square'></i></td></tr>";
								});
								
								jQuery('.l3CatgeoryList').html(l3ListOption);
							}
							
							//console.log(opts);
							
						}
					});
			}
			
			jQuery('.l3CatgeoryWaitingRegion').hide();
			jQuery('.l3CatgeoryRegion').show();
			jQuery('.selectedCatgeoryRegion').show();
			
		});
		
		//VIEW ALL OTHER MARGIN
		jQuery('.viewAllCategoryMargin').on('click',function(){
			
			//GET SELECTED L3 CATEGORY ID ARRAY
			var selectedL3CategoryForMargin = getSelectedL3Category();
			selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			
			
			if(selectedL3CategoryForMargin==""){
				var selectedL3CategoryForMargin = [1];
				selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			}
			
			//GET ALL OTHER MARGIN
			
			jQuery.ajax({
				url: "<?php echo url('viewAllOtherCategory');?>",
				type: "post",
				data: {
					"_token": "{{ csrf_token() }}",
					"category_id": selectedL3CategoryForMargin
				},
				success: function(result){
					
					jQuery('.custom-modal-view').modal('toggle');
					jQuery('.custom-modal-dialog').addClass('modal-lg');
					jQuery('.custom-modal-title').html('Vendor Info');//SET TITLE
					jQuery('.custom-modal-body').html(result);//SET BODY
			
					//console.log(opts);
					
				}
			});
			
			
			
		});
</script>
@endsection