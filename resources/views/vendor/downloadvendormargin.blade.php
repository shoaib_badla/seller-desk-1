@extends('layouts.ajax')
@section('title','Index')
@section('content')


<div class = 'container ' style="text-align:center">

	<div class="row col-md-9 ">

			<div class="row">
				<h2>
					Vendor Margin
					
				</h2>
			
			
			
			
			<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info" >
                <thead>
				
                <tr role="row">
					<th >S.No</th>
					<th >L1 Category</th>
					<th >L2 Category</th>
					<th >L3 Category</th>
					<th >Margin</th>
					<th>Proposed Margin</th>
				</tr>
                </thead>
                <tbody>
				
				<?php
				 if(!empty($vendorMarginList)){
					 $sno = 1;
					 foreach($vendorMarginList as $row){
						 
				?>
							 <tr role="row" style="page-break-inside: avoid;">
							  <td ><?php echo $sno;?></td>
							  <td><?php echo $row['L1_Category'];?></td>
							  <td><?php echo $row['L2_Category'];?></td>
							  <td><?php echo $row['L3_Category'];?></td>
							  <td><?php echo $row['margin'];?></td>
							  <td>
								  <?php echo $row['proposed_commission'];   ?>	

							  </td>
							
							</tr>
				<?php		
						$sno++;
						
					 }
				 }
				?>
               
			
				</tbody>
                
              </table>
			</div>
				
	</div>
</div>

@endsection