@extends('layouts.vendormargin')
@section('title','Index')
@section('content')

<div class = 'container'>
		<h2 style="text-align:center;">
		   Application Approval : Pending
		</h2>
		<div class="row">
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
				</p>
				
			</div>

	
		<div class="col-md-11">
			
			 <div class="panel with-nav-tabs panel-primary">
			
				<div class="panel-heading">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab6primary"  class="tab6primary" data-toggle="tab">Basic Information</a></li>
						<li ><a href="#tab1primary"  class="tab1primary" data-toggle="tab">General Information</a></li>
						<li><a href="#tab2primary"  class="tab2primary" data-toggle="tab">Financial Information</a></li>
						
						<li><a href="#tab4primary"  class="tab4primary" data-toggle="tab">Pickup Address(s)</a></li>
						<li><a href="#tab3primary"  class="tab3primary" data-toggle="tab">Return Address</a></li>
						<li><a href="#tab5primary" class="tab5primary" data-toggle="tab">Category(s)</a></li>
						
					</ul>
				</div>
			
				<!-- /.box-header -->
				<div class="panel-body">
					<div class="tab-content">
					
						<div class="tab-pane fade in active " id="tab6primary">
							<div class="row box-body">
							
								  <table class="table table-hover">
									<tbody>
									<tr>
									  <td width="40%"><b>Company / Individual Name/(Registered Name)</b></td>
									  <td>{{$vendor_info->vendor_name}}</td>
									</tr>
									<tr>
									  <td><b>Owner's CNIC</b></td>
									  <td>{{$vendor_info->cnic}}</td>
									</tr>
									<tr>
									  <td><b>Email Address</b></td>
									  <td>{{$vendor_info->vendor_email}}</td>
									</tr>
									<tr>
									  <td><b>Cell Number</b></td>
									  <td>{{$vendor_info->cell_phone}}</td>
									</tr>
									<tr>
									  <td><b>Office Phone</b></td>
									  <td>{{$vendor_info->office_phone}}</td>
									</tr>
									<tr>
									  <td><b>Form Of Organization</b></td>
									  <td>{{$vendor_info->form_of_organization}}</td>
									</tr>
									<tr>
									  <td><b>POC Name</b></td>
									  <td>{{$vendor_info->poc_name}}</td>
									</tr>
									<tr>
									  <td><b>POC Email</b></td>
									  <td>{{$vendor_info->poc_email}}</td>
									</tr>
									<tr>
									  <td><b>POC Contact Number</b></td>
									  <td>{{$vendor_info->poc_contact}}</td>
									</tr>
									<tr>
									  <td><b>NTN Number</b></td>
									  <td>{{$vendor_info->ntn_number}}</td>
									</tr>
									<tr>
									  <td><b>Country</b></td>
									  <td>{{$vendor_info->country}}</td>
									</tr>
									<tr>
									  <td><b>Province</b></td>
									  <td>{{$vendor_info->province}}</td>
									</tr>
									<tr>
									  <td><b>City</b></td>
									  <td>{{$vendor_info->city}}</td>
									</tr>
									<tr>
									  <td><b>Address (Company Place of Business)</b></td>
									  <td>{{   App\Helper\Common::wordWrapContent($vendor_info->company_address) }}</td>
									</tr>
									<tr>
									  <td><b>Brand(s) Offered</b></td>
									  <td>{{$brandList}}</td>
									</tr>
									
								  </tbody></table>
							  </div>
			  <!-- /.row -->
						</div>
							
						<div class="tab-pane fade " id="tab1primary">
							  <div class="row box-body">
								  <table class="table table-hover">
									<tbody>
										<tr>
										  <td width="40%"><b>Shop Online Name</b></td>
										  <td>{{$vendor_info->shop_online_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company Owner Name (as per CNIC) </b></td>
										  <td>{{$vendor_info->company_owner_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Authorized Person Name (if other than owner)</b></td>
										  <td>{{$vendor_info->authorize_person_name}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Authorized Person CNIC (if other than owner)</b></td>
										  <td>{{$vendor_info->authorize_person_cnic}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company Website</b></td>
										  <td>{{$vendor_info->company_website}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Billing Cycle</b></td>
										  <td>{{$vendor_info->payment_cycle}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Acceptance of Returns </b></td>
										  <td>{{$vendor_info->acceptance_of_return}}</td>
										</tr>
										<?php if($vendor_info->acceptance_of_return=='Yes'){ ?>
											<tr>
											  <td width="40%"><b>Acceptance of Returns Days </b></td>
											  <td>{{$vendor_info->acceptance_of_return_range}}</td>
											</tr>
										<?php } ?>
										
										<tr>
										  <td width="40%"><b>Offer Exchange</b></td>
										  <td>{{$vendor_info->offer_exchange}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Witholding tax deposited by</b></td>
										  <td>{{$vendor_info->withholding_tax_dposited_by}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>International Shipping</b></td>
										  <td>{{$vendor_info->internation_shipping}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Business Model</b></td>
										  <td>{{$vendor_info->business_model}}</td>
										</tr>
										<tr>
										  <td width="40%"><b>Company / Individual Registered Addressed (as per NTN / CNIC)</b></td>
										  <td>{{$vendor_info->company_individual_registered_address}}</td>
										</tr>
										<!--
										<tr>
										  <td width="40%"><b>Pick Up</b></td>
										  <td>{{$vendor_info->pickup_day}}</td>
										</tr>
										-->
									</tbody>
								  </table>
								
									<!-- /.col -->
								  </div>
				  <!-- /.row -->
							</div>
							
							 <div class="tab-pane fade" id="tab2primary">
								   <div class="row">
										<?php
											if(!empty($vendBankInfoArr)){
												foreach($vendBankInfoArr as $vendBankInfoRow){
													
										?>
												<table class="table table-hover">
													
													<tbody>
														<tr>
														  <td width="40%"><b>Bank Name</b></td>
														  <td><?php echo $vendBankInfoRow['bank_name'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Bank Account Title</b></td>
														   <td><?php echo $vendBankInfoRow['bank_account_title'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Bank Account Number</b></td>
														  <td><?php echo $vendBankInfoRow['bank_account_number'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>STRN</b></td>
														  <td><?php echo $vendBankInfoRow['strn'];?></td>
														</tr>
													</tbody>
												</table>
												
										<?php										
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							
							
							 <div class="tab-pane fade" id="tab3primary">
								   <div class="row">
										<?php
											if(!empty($vendorReturnAddressArr)){	
												$loop = 1;
												foreach($vendorReturnAddressArr as $vendorReturnAddressArrRow){
													
										?>
												<table class="table table-hover">
													
													<tbody>
														<tr>
														  <td width="40%"><b>Pickup Address</b></td>
														  <td><?php echo $vendorReturnAddressArrRow['return_address'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Province</b></td>
														   <td><?php echo $vendorReturnAddressArrRow['return_city'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>City</b></td>
														  <td><?php echo $vendorReturnAddressArrRow['return_province'];?></td>
														</tr>
														
													</tbody>
												</table>
												
										<?php		
													$loop++;								
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <div class="tab-pane fade" id="tab4primary">
							 
							
								<div class="row">
										<?php
											if(!empty($VendorPickupAddressArr)){	
												$loop = 1;
												foreach($VendorPickupAddressArr as $vendorPickupAddressArrRow){
													
										?>
												<table class="table table-hover">
													<thead>
														<tr style="background-color:#f9f9f9"><th colspan="2">Pickup Address : <?php echo $loop;?></th></tr>
													</thead>
													<tbody>
														<tr>
														  <td width="40%"><b>Pickup Address</b></td>
														  <td><?php echo $vendorPickupAddressArrRow['pickup_address'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>Province</b></td>
														   <td><?php echo $vendorPickupAddressArrRow['pickup_city'];?></td>
														</tr>
														<tr>
														  <td width="40%"><b>City</b></td>
														  <td><?php echo $vendorPickupAddressArrRow['pickup_province'];?></td>
														</tr>
														
													</tbody>
												</table>
												
										<?php		
													$loop++;								
												}
											}
										?>
									
									<!-- /.col -->
								  </div>
							 </div>
							 
							 <!-- CATEGORY TAB START -->
							  <div class="tab-pane fade" id="tab5primary">
								<div class="row">
									<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
										<thead>
											<tr role="row">
												<th >S.No</th>
												<th >L1 Category</th>
												<th >L2 Category</th>
												<th >L3 Category</th>
												<th >Standard Margin</th>
												<th>Proposed Margin</th>
											</tr>
										</thead>
										<tbody>
										
											<?php
											
											 if(count($vendorMargin)>0){
												 $sno = 1;
												 foreach($vendorMargin as $row){
													
											?>
													 <tr role="row">
													  <td ><?php echo $sno;?></td>
													  <td><?php echo $row->L1_Category;?></td>
													  <td><?php echo $row->L2_Category;?></td>
													  <td><?php echo $row->L3_Category;?></td>
													  <td><?php echo $row->margin;?></td>
													  <td>
														  <?php  echo $row->proposed_commission;  ?>	

													  </td>
													<input type='hidden' name='categoryId[]' value='<?php echo $row->category_id;?>'  />
													</tr>
										<?php		
												$sno++;
											 }
										 }else{
											
										?>
											 <tr role="row"><td colspan="6">No Record Found</td></tr>
										<?php
										 }
										?>
									   
									
										</tbody>
									
								  </table>
								  </div>
								 <?php
									if($vendor_info->agree_for_all_other_catgeory){
								 ?>
									 <div class="row ">
									  <div class="col-md-8">
											<div class="checkbox">
											  <label>
												View All Other's Margins : 
											  </label><a href='javascript:void();' onClick='return false;' class='viewAllCategoryMargin'>View Margin</a>
											</div>
										</div>
									 </div>
								<?php } ?>
							 </div>
							 <!-- CATEGORY TAB END -->
				<!-- /.box-body -->
						</div>
					</div>
			
					
			</div>
		</div>
	
		
	</div>
</div>
  <script>
	jQuery(document).ready(function(){
	//VIEW ALL OTHER MARGIN
		jQuery('.viewAllCategoryMargin').on('click',function(){
			
			//GET SELECTED L3 CATEGORY ID ARRAY
			var selectedL3CategoryForMargin = getSelectedL3Category();
			selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			
			if(selectedL3CategoryForMargin==""){
				var selectedL3CategoryForMargin = [1];
				selectedL3CategoryForMargin	=	selectedL3CategoryForMargin.join();
			}
			
			//GET ALL OTHER MARGIN
			jQuery.ajax({
				url: "<?php echo url('viewAllOtherCategory');?>",
				type: "post",
				data: {
					"_token": "{{ csrf_token() }}",
					"category_id": selectedL3CategoryForMargin
				},
				success: function(result){
					
					jQuery('.custom-modal-view').modal('toggle');
					jQuery('.custom-modal-dialog').addClass('modal-lg');
					jQuery('.custom-modal-title').html('Vendor Info');//SET TITLE
					jQuery('.custom-modal-body').html(result);//SET BODY
			
					//console.log(opts);
					
				}
			});
			
			
		});
	});
	
	function getSelectedL3Category(){
		
		var selectedL3CategoryForMargin = [];
		jQuery('input[name^="categoryId"]').each(function() {
			selectedL3CategoryForMargin.push(jQuery(this).val());
		});
		
		return selectedL3CategoryForMargin;
	}
  </script>

@endsection