@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        attribute Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("attribute")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New attribute</button>
        </form>
    </div>
    <table>
        <thead>
            <th>attribute_set_id</th>
            <th>code</th>
            <th>name</th>
            <th>type</th>
            <th>default_value</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>premission_role_id</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($attributes as $attribute) 
            <tr>
                <td>{!!$attribute->attribute_set_id!!}</td>
                <td>{!!$attribute->code!!}</td>
                <td>{!!$attribute->name!!}</td>
                <td>{!!$attribute->type!!}</td>
                <td>{!!$attribute->default_value!!}</td>
                <td>{!!$attribute->created_at!!}</td>
                <td>{!!$attribute->updated_at!!}</td>
                <td>{!!$attribute->premission_role_id!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/attribute/{!!$attribute->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/attribute/{!!$attribute->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/attribute/{!!$attribute->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $attributes->render() !!}

</div>
@endsection