@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit attribute
    </h1>
    <form method = 'get' action = '{!!url("attribute")!!}'>
        <button class = 'btn blue'>attribute Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("attribute")!!}/{!!$attribute->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="attribute_set_id" name = "attribute_set_id" type="text" class="validate" value="{!!$attribute->
            attribute_set_id!!}"> 
            <label for="attribute_set_id">attribute_set_id</label>
        </div>
        <div class="input-field col s6">
            <input id="code" name = "code" type="text" class="validate" value="{!!$attribute->
            code!!}"> 
            <label for="code">code</label>
        </div>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$attribute->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="type" name = "type" type="text" class="validate" value="{!!$attribute->
            type!!}"> 
            <label for="type">type</label>
        </div>
        <div class="input-field col s6">
            <input id="default_value" name = "default_value" type="text" class="validate" value="{!!$attribute->
            default_value!!}"> 
            <label for="default_value">default_value</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate" value="{!!$attribute->
            created_at!!}"> 
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate" value="{!!$attribute->
            updated_at!!}"> 
            <label for="updated_at">updated_at</label>
        </div>
        <div class="input-field col s6">
            <input id="premission_role_id" name = "premission_role_id" type="text" class="validate" value="{!!$attribute->
            premission_role_id!!}"> 
            <label for="premission_role_id">premission_role_id</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection