@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show attribute
    </h1>
    <form method = 'get' action = '{!!url("attribute")!!}'>
        <button class = 'btn blue'>attribute Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>attribute_set_id : </i></b>
                </td>
                <td>{!!$attribute->attribute_set_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>code : </i></b>
                </td>
                <td>{!!$attribute->code!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$attribute->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>type : </i></b>
                </td>
                <td>{!!$attribute->type!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>default_value : </i></b>
                </td>
                <td>{!!$attribute->default_value!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$attribute->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$attribute->updated_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>premission_role_id : </i></b>
                </td>
                <td>{!!$attribute->premission_role_id!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection