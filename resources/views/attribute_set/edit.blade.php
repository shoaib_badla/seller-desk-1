@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit attribute_set
    </h1>
    <form method = 'get' action = '{!!url("attribute_set")!!}'>
        <button class = 'btn blue'>attribute_set Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("attribute_set")!!}/{!!$attribute_set->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$attribute_set->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate" value="{!!$attribute_set->
            created_at!!}"> 
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate" value="{!!$attribute_set->
            updated_at!!}"> 
            <label for="updated_at">updated_at</label>
        </div>
        <div class="input-field col s6">
            <input id="user_id" name = "user_id" type="text" class="validate" value="{!!$attribute_set->
            user_id!!}"> 
            <label for="user_id">user_id</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection