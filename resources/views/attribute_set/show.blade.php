@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show attribute_set
    </h1>
    <form method = 'get' action = '{!!url("attribute_set")!!}'>
        <button class = 'btn blue'>attribute_set Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$attribute_set->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>created_at : </i></b>
                </td>
                <td>{!!$attribute_set->created_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>updated_at : </i></b>
                </td>
                <td>{!!$attribute_set->updated_at!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>user_id : </i></b>
                </td>
                <td>{!!$attribute_set->user_id!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection