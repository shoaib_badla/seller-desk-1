@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        attribute_set Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("attribute_set")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New attribute_set</button>
        </form>
    </div>
    <table>
        <thead>
            <th>name</th>
            <th>created_at</th>
            <th>updated_at</th>
            <th>user_id</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($attribute_sets as $attribute_set) 
            <tr>
                <td>{!!$attribute_set->name!!}</td>
                <td>{!!$attribute_set->created_at!!}</td>
                <td>{!!$attribute_set->updated_at!!}</td>
                <td>{!!$attribute_set->user_id!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/attribute_set/{!!$attribute_set->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/attribute_set/{!!$attribute_set->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/attribute_set/{!!$attribute_set->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $attribute_sets->render() !!}

</div>
@endsection