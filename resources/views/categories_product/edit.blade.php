@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit categories_product
    </h1>
    <form method = 'get' action = '{!!url("categories_product")!!}'>
        <button class = 'btn blue'>categories_product Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("categories_product")!!}/{!!$categories_product->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="cat_id" name = "cat_id" type="text" class="validate" value="{!!$categories_product->
            cat_id!!}"> 
            <label for="cat_id">cat_id</label>
        </div>
        <div class="input-field col s6">
            <input id="product_id" name = "product_id" type="text" class="validate" value="{!!$categories_product->
            product_id!!}"> 
            <label for="product_id">product_id</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection