@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        categories_product Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("categories_product")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New categories_product</button>
        </form>
    </div>
    <table>
        <thead>
            <th>cat_id</th>
            <th>product_id</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($categories_products as $categories_product) 
            <tr>
                <td>{!!$categories_product->cat_id!!}</td>
                <td>{!!$categories_product->product_id!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/categories_product/{!!$categories_product->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/categories_product/{!!$categories_product->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/categories_product/{!!$categories_product->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $categories_products->render() !!}

</div>
@endsection