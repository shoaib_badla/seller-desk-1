@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show categories_product
    </h1>
    <form method = 'get' action = '{!!url("categories_product")!!}'>
        <button class = 'btn blue'>categories_product Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>cat_id : </i></b>
                </td>
                <td>{!!$categories_product->cat_id!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>product_id : </i></b>
                </td>
                <td>{!!$categories_product->product_id!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection