@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit brand
    </h1>
    <form method = 'get' action = '{!!url("brand")!!}'>
        <button class = 'btn blue'>brand Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("brand")!!}/{!!$brand->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$brand->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="description" name = "description" type="text" class="validate" value="{!!$brand->
            description!!}"> 
            <label for="description">description</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection