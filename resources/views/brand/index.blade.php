@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        brand Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("brand")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New brand</button>
        </form>
    </div>
    <table>
        <thead>
            <th>name</th>
            <th>description</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($brands as $brand) 
            <tr>
                <td>{!!$brand->name!!}</td>
                <td>{!!$brand->description!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/brand/{!!$brand->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/brand/{!!$brand->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/brand/{!!$brand->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $brands->render() !!}

</div>
@endsection