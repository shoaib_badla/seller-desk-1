@extends('layouts.app')
@section('title','Create')
@section('content')
    <div class="box box-primary">
    <div class="box-header">
        <h3>Create new Category</h3>
    </div>
    <form method = 'get' action = '{!!url("category")!!}'>
        <button class = 'btn blue'>category Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("category")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate">
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="level" name = "level" type="text" class="validate">
            <label for="level">level</label>
        </div>
        <div class="input-field col s6">
            <input id="parent_id" name = "parent_id" type="text" class="validate">
            <label for="parent_id">parent_id</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate">
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate">
            <label for="updated_at">updated_at</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection