@extends('layouts.app')
@section('title','Index')
@section('content')
    {{--<div class="box box-primary">--}}
    {{--<div class="box-header">--}}
        {{--<h3>Category</h3>--}}
    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<form class = 'col s3' method = 'get' action = '{!!url("category")!!}/create'>--}}
            {{--<button class = 'btn red' type = 'submit'>Create New category</button>--}}
        {{--</form>--}}
    {{--</div>--}}
        {{--<a href="{!!url("category")!!}/create" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> New</a>--}}
    {{--<div class="box-body">--}}
    {{--<table class="table table-striped" >--}}
        {{--<head>--}}
            {{--<th>name</th>--}}
            {{--<th>level</th>--}}
            {{--<th>parent_id</th>--}}
            {{--<th>created_at</th>--}}
            {{--<th>updated_at</th>--}}
            {{--<th>actions</th>--}}
        {{--</head>--}}
        {{--<tbody>--}}
            {{--@foreach($categories as $category) --}}
            {{--<tr>--}}
                {{--<td>{!!$category->name!!}</td>--}}
                {{--<td>{!!$category->level!!}</td>--}}
                {{--<td>{!!$category->parent_id!!}</td>--}}
                {{--<td>{!!$category->created_at!!}</td>--}}
                {{--<td>{!!$category->updated_at!!}</td>--}}
                {{--<td>--}}
                    {{--<div class = 'row'>--}}
                        {{--<a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/category/{!!$category->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>--}}
                        {{--<a href = '#' class = 'viewEdit btn-floating blue' data-link = '/category/{!!$category->id!!}/edit'><i class = 'material-icons'>edit</i></a>--}}
                        {{--<a href = '#' class = 'viewShow btn-floating orange' data-link = '/category/{!!$category->id!!}'><i class = 'material-icons'>info</i></a>--}}
                    {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--@endforeach --}}
        {{--</tbody>--}}
    {{--</table>--}}
        {{--</div>--}}
    {{--{!! $categories->render() !!}--}}

{{--</div>--}}

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Manage Category TreeView</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Category List</h3>
                        <ul id="tree1">
                            @foreach($categories as $category)
                                <li>
                                    {{ $category->name }}
                                    @if(count($category->childs))
                                        @include('category.manageChild',['childs' => $category->childs])
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h3>Add New Category</h3>

                        {!!Form::open(['route'=>'add.category']) !!}

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">�</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            {!! Form::label('Name:') !!}
                            {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter Name']) !!}
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                            {!! Form::label('Category:') !!}
                            {!! Form::select('parent_id',$allCategories, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                            <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success">Add New</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection