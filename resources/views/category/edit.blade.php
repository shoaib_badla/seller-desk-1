@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit category
    </h1>
    <form method = 'get' action = '{!!url("category")!!}'>
        <button class = 'btn blue'>category Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("category")!!}/{!!$category->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$category->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="level" name = "level" type="text" class="validate" value="{!!$category->
            level!!}"> 
            <label for="level">level</label>
        </div>
        <div class="input-field col s6">
            <input id="parent_id" name = "parent_id" type="text" class="validate" value="{!!$category->
            parent_id!!}"> 
            <label for="parent_id">parent_id</label>
        </div>
        <div class="input-field col s6">
            <input id="created_at" name = "created_at" type="text" class="validate" value="{!!$category->
            created_at!!}"> 
            <label for="created_at">created_at</label>
        </div>
        <div class="input-field col s6">
            <input id="updated_at" name = "updated_at" type="text" class="validate" value="{!!$category->
            updated_at!!}"> 
            <label for="updated_at">updated_at</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection