<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Categories_products.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:49:24pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class CategoriesProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('categories_products',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('cat_id');
        
        $table->integer('product_id');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('categories_products');
    }
}
