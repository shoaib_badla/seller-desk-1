<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Product_universal_skus.
 *
 * @author  The scaffold-interface created at 2017-01-16 12:43:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ProductUniversalSkus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('product',function (Blueprint $table){

            $table->increments('id');

            $table->integer('attribute_set_id');

            $table->integer('universal_sku_id');

            $table->integer('vendor_id');

            $table->String('sku');

            $table->integer('mode_of_fullfillment');

            /**
             * Foreignkeys section
             */


            $table->timestamps();


            // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
