<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class RejectionReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('rejection_reason',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('reason');
        $table->longText('description')->nullable();
		$table->String('department')->nullable();
      
        
        
        $table->timestamps();
        
        });
        // type your addition here

		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('rejection_reason');
    }
}
