<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorInfos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('vendor_infos',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('vendor_name');
        $table->String('status')->default('new');
		$table->String('vendor_email');
        $table->String('cnic');
        $table->String('cell_phone');
        $table->String('office_phone');
        $table->integer('form_of_organization');
        $table->String('poc_name');
        $table->String('poc_email');
        $table->String('poc_contact');
        $table->String('ntn_number');
        $table->integer('country');
        $table->integer('province');
        $table->integer('city');
        $table->longText('company_address');
        
      
        
        
        $table->timestamps();
        
        });
        // type your addition here

		
		//CREATE VENDOR BRAND TABLE
		Schema::create('vendor_brand',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('vendor_id');
		$table->integer('brand_id');
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('vendor_infos');
        Schema::drop('vendor_brand');
    }
}
