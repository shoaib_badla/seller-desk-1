<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Tests.
 *
 * @author  The scaffold-interface created at 2017-01-03 06:46:41am
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Tests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('tests',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('firstname');
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('tests');
    }
}
