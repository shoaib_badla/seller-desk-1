<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Attribute_sets.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:09:57pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class AttributeSets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('attribute_sets',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->date('created_at');
        
        $table->date('updated_at');
        
        $table->integer('user_id');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('attribute_sets');
    }
}
