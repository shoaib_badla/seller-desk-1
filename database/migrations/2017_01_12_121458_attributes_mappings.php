<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Attributes_mappings.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:14:58pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class AttributesMappings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('attributes_mapping',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('attribute_set_id');
        
        $table->integer('attribute_id');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('attributes_mapping');
    }
}
