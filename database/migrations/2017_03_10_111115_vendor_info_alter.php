<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorInfoAlter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {

      Schema::table('vendor_infos',function (Blueprint $table){

			$table->String('shop_online_name')->after('company_address')->nullable();
			
			$table->String('authorize_person_name')->after('shop_online_name')->nullable();		
			$table->String('company_website')->after('authorize_person_name')->nullable();		
			$table->String('acceptance_of_return')->after('company_website')->nullable();		
			$table->String('acceptance_of_return_range')->after('acceptance_of_return')->nullable();			
			$table->integer('withholding_tax_dposited_by')->after('acceptance_of_return_range')->nullable();			
			$table->String('business_model')->after('withholding_tax_dposited_by')->nullable();			
			$table->String('pickup_day')->after('business_model')->nullable();			
			$table->String('company_owner_name')->after('pickup_day')->nullable();			
			$table->String('authorize_person_cnic')->after('company_owner_name')->nullable();			
			$table->integer('payment_cycle')->after('authorize_person_cnic')->nullable();			
			$table->String('offer_exchange')->after('payment_cycle')->nullable();			
			$table->String('internation_shipping')->after('offer_exchange')->nullable();			
			$table->String('company_individual_registered_address')->after('internation_shipping')->nullable();			
			$table->integer('agree_for_all_other_catgeory')->after('company_individual_registered_address')->default(1)->nullable();			
			$table->integer('detailed_added_by')->after('agree_for_all_other_catgeory');			
			
        });
		
		//CREATE VENDOR CATEGORY
		Schema::create('vendor_product_category',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->integer('category_id')->nullable();
			$table->integer('proposed_commission')->nullable();
			$table->integer('priority')->nullable();
        
			$table->timestamps();
        
        });
		
		//CREATE VENDOR CATEGORY
		Schema::create('vendor_pickup_address',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->longText('pickup_address')->nullable();
			$table->integer('pickup_city')->nullable();
			$table->integer('pickup_province')->nullable();
        
			$table->timestamps();
        
        });
		
		
		//CREATE VENDOR CATEGORY
		Schema::create('vendor_return_address',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->longText('return_address')->nullable();
			$table->integer('return_city')->nullable();
			$table->integer('return_province')->nullable();
        
			$table->timestamps();
        
        });
		
		//CREATE VENDOR BANK INFO
		Schema::create('vendor_bank_info',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->String('bank_name')->nullable();
			$table->String('bank_account_number')->nullable();
			$table->String('bank_account_title')->nullable();
			$table->String('strn')->nullable();
        
			$table->timestamps();
        
        });
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
		Schema::table('vendor_infos', function (Blueprint $table) {
			$table->dropColumn(['shop_online_name','authorize_person_name','company_website','acceptance_of_return','acceptance_of_return_range', 'withholding_tax_dposited_by', 'business_model','pickup_day','company_owner_name','authorize_person_cnic','payment_cycle','offer_exchange','internation_shipping','company_individual_registered_address','agree_for_all_other_catgeory','detailed_added_by']);
		});
		
		//DROP TABLE
		 Schema::drop('vendor_product_category');
		 Schema::drop('vendor_pickup_address');
		 Schema::drop('vendor_return_address');
		 Schema::drop('vendor_bank_info');
		
    }
}
