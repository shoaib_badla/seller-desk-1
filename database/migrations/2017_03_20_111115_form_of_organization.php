<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class FormOfOrganization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {


		//CREATE VENDOR SIGINING CONTRACT
		Schema::create('form_of_organization',function (Blueprint $table){

			$table->increments('id');
        
			$table->String('label');
			$table->timestamps();
        
        });
		
		//ADD DEFAULT DATA
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'Sole Proprietor',
			)
		);
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'Private Limited',
			)
		);
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'Partnership',
			)
		);
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'Individual',
			)
		);
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'NGO',
			)
		);
		DB::table('form_of_organization')->insert(
			array(
				'label' => 'Public Limited',
			)
		);
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {

		Schema::drop('form_of_organization');
		
    }
}
