<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorInfoAlterV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {

      Schema::table('vendor_application_comment_history',function (Blueprint $table){

			$table->string('user_role', 100)->change();
			
			
        });
		
		
		 Schema::table('vendor_infos',function (Blueprint $table){
			$table->longText('company_individual_registered_address')->change();
			
			
			$table->integer('magento_id')->after('id')->default(0)->nullable();
			$table->integer('is_margin_downloaded')->after('vendor_manager_assigned')->default(0)->nullable();
			$table->integer('approved_by')->after('is_vendor_ops_approved')->nullable();
			$table->dateTime('approved_at')->after('approved_by')->nullable();
        });
		
		//CREATE VENDOR SIGINING CONTRACT
		Schema::create('vendor_signing_contract',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->String('orginal_name');
			$table->String('uploaded_name');
			$table->integer('uploaded_by');
        
			$table->timestamps();
        
        });
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
		Schema::table('vendor_infos', function (Blueprint $table) {
			$table->dropColumn(['magento_id','approved_by','approved_at','is_margin_downloaded']);
		});
		
		Schema::drop('vendor_signing_contract');
		
    }
}
