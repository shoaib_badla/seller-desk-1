<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Province extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {


		//CREATE VENDOR SIGINING CONTRACT
		Schema::create('province_tbl',function (Blueprint $table){

			$table->increments('id');
        
			$table->String('name');
			$table->timestamps();
        
        });
		
		//ADD DEFAULT DATA
		DB::table('province_tbl')->insert(
			array(
				'name' => 'Sindh',
			)
		);
		DB::table('province_tbl')->insert(
			array(
				'name' => 'Punjab',
			)
		);
		DB::table('province_tbl')->insert(
			array(
				'name' => 'Balochistan',
			)
		);
		DB::table('province_tbl')->insert(
			array(
				'name' => 'KPK',
			)
		);
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {

		Schema::drop('province_tbl');
		
    }
}
