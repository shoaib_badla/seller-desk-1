<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorInfoModification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::table('vendor_infos',function (Blueprint $table){

			$table->integer('vendor_manager_assigned')->after('company_address')->nullable();
			
			$table->integer('is_vendor_approved')->after('vendor_manager_assigned')->default(0)->nullable();		
			$table->integer('is_vendor_finance_approved')->after('is_vendor_approved')->default(0)->nullable();		
			$table->integer('is_vendor_ops_approved')->after('is_vendor_finance_approved')->default(0)->nullable();		
			
			/**THIS COLOUMN IS ON FLAT TABLE BECAUSE IT'S ONE TIME ACTION*/
			$table->integer('accepted_by')->after('is_vendor_ops_approved')->nullable();
			$table->dateTime('accepted_at')->after('accepted_by')->nullable();
			$table->String('rejected_by')->after('accepted_at')->nullable();
			$table->longText('rejection_reason')->after('rejected_by')->nullable();
			$table->dateTime('rejected_at')->after('rejection_reason')->nullable();
      
		
        });
        //CREATE VENDOR APPLICATION REJECTION,ACCEPTANCE HISTORY
		Schema::create('vendor_application_comment_history',function (Blueprint $table){

			$table->increments('id');
        
			$table->integer('vendor_id');
			$table->String('status');
			$table->longText('comment')->nullable();
			$table->integer('user_id');
			$table->integer('user_role');
			$table->integer('comment_show')->default(1); #TO SHOW ON VENDOR 
        
			$table->timestamps();
        
        });
		
		//ADD IS_ACTIVE COLOUMN IN USER TABLE
		
		 Schema::table('users',function (Blueprint $table){

			$table->integer('is_active')->after('api_token')->default(1);
			
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
		
		Schema::table('vendor_infos', function (Blueprint $table) {
			$table->dropColumn(['is_vendor_approved','is_vendor_finance_approved','is_vendor_ops_approved','vendor_manager_assigned','accepted_by', 'accepted_at', 'rejected_by','rejection_reason','rejected_at']);
		});
		
		Schema::drop('vendor_application_comment_history');
    }
}
