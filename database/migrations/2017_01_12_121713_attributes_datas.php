<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Attributes_datas.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:17:13pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class AttributesDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('attributes_data',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('attribute_id');
        
        $table->String('value');
        
        $table->integer('approved_by');
        
        $table->integer('rejected_by');
        
        $table->date('approved_at');
        
        $table->date('rejected_at');
        
        $table->longText('reason');
        
        $table->date('created_at');
        
        $table->date('updated_at');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('attributes_data');
    }
}
