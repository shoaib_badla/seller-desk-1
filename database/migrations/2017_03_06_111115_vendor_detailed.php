<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Vendor_infos.
 *
 * @author  The scaffold-interface created at 2017-01-19 12:45:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class VendorDetailed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {

       //CREATE BUSINESS MODEL
		Schema::create('business_model',function (Blueprint $table){

			$table->increments('id');
        
			$table->String('label');
			$table->longText('description')->nullable();
        
			$table->timestamps();
        
        });
		
		//ADD DEFAULT DATA
		DB::table('business_model')->insert(
			array(
				'label' => 'Commision',
				'description' => 'Commision (Principal Agent relationship)'
			)
		);		
		DB::table('business_model')->insert(
			array(
				'label' => 'Purchase',
				'description' => 'Purchase (Buyer Seller Relationship)'
			)
		);
		
		//CREATING WITHHOLDING TAX DEPOISTER
		Schema::create('withholding_tax_depositer',function (Blueprint $table){

			$table->increments('id');
        
			$table->String('label');
			$table->longText('description')->nullable();
        
			$table->timestamps();
        
        });
		//ADD DEFAULT DATA
		DB::table('withholding_tax_depositer')->insert(
			array(
				'label' => 'N/A',
				'description' => 'N/A'
			)
		);		
		DB::table('withholding_tax_depositer')->insert(
			array(
				'label' => 'Vendor',
				'description' => 'Vendor'
			)
		);		
		DB::table('withholding_tax_depositer')->insert(
			array(
				'label' => 'TCS Ecom Pvt Ltd',
				'description' => 'TCS Ecom Pvt Ltd'
			)
		);
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
		Schema::drop('business_model');
		Schema::drop('withholding_tax_depositer');
    }
}
