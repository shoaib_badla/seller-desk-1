<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Categories_attributes.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:48:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class CategoriesAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('categories_attributes',function (Blueprint $table){

        $table->increments('id');
        
        $table->integer('cat_id');
        
        $table->integer('attribitute_set_id');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('categories_attributes');
    }
}
