<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Product_universal_skus.
 *
 * @author  The scaffold-interface created at 2017-01-16 12:43:15pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ProductUniversalSkus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('product_universal_skus',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('universal_sku');
        
        $table->integer('product_count');
        
        
        /**
         * Foreignkeys section
         */
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('product_universal_skus');
    }
}
