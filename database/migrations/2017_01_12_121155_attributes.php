<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Attributes.
 *
 * @author  The scaffold-interface created at 2017-01-12 12:11:55pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Attributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('attributes',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('code');
        
        $table->String('name');
        
        $table->String('type');
        
        $table->String('default_value');
        
        $table->date('created_at');
        
        $table->date('updated_at');
        
        $table->String('premission_role_id');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('attributes');
    }
}
