<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1','middleware' => 'auth:api'], function () {
    //    Route::resource('task', 'TasksController');
	Route::post('dataoffload','CustomApiController@dataoffload');
	Route::get('citylist','CustomApiController@citylist');
	Route::get('countrylist','CustomApiController@countrylist');
	Route::get('categorylist','CustomApiController@categorylist');
	Route::get('brandlist','CustomApiController@brandlist');
	Route::get('modeoffullfilement','CustomApiController@modeoffullfilement');
	Route::get('checkvendoralreadyexsist','CustomApiController@checkvendoralreadyexsist');
    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_api_routes
});


//GET TOKEN API BY USER
Route::post('getapitoken','CustomApiController@getApiToken');