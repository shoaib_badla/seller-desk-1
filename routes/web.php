<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function () {
   /* Route::get('/', function () {
        return view('home');
    });*/
	Route::get('/', '\App\Http\Controllers\HomeController@index');
	
     Route::get('/link1', function ()    {
       // Uses Auth Middleware
	   return 'tes';
    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
Route::group(['middleware' => ['web', 'auth']], function () {
//Users
Route::get('users','\App\Http\Controllers\UserController@index');
Route::get('users/edit/{user_id}', '\App\Http\Controllers\UserController@edit');
Route::post('users/update', '\App\Http\Controllers\UserController@update');
Route::get('users/create', '\App\Http\Controllers\UserController@create');
Route::post('users/store', '\App\Http\Controllers\UserController@store');
Route::get('users/delete/{user_id}', '\App\Http\Controllers\UserController@destroy');
Route::post('users/addRole', '\App\Http\Controllers\UserController@addRole');
Route::get('users/removeRole/{role}/{user_id}', '\App\Http\Controllers\UserController@revokeRole');

//Roles
Route::get('roles', '\App\Http\Controllers\RoleController@index');
Route::get('roles/edit/{role_id}', '\App\Http\Controllers\RoleController@edit');
Route::post('roles/update', '\App\Http\Controllers\RoleController@update');
Route::get('roles/create', '\App\Http\Controllers\RoleController@create');
Route::post('roles/store', '\App\Http\Controllers\RoleController@store');
    Route::post('roles/addPermission', '\App\Http\Controllers\RoleController@addPermission');
    Route::get('roles/removePermission/{permission}/{user_id}', '\App\Http\Controllers\RoleController@revokePermission');
Route::get('roles/delete/{role_id}', '\App\Http\Controllers\RoleController@destroy');



//Permissions
Route::get('permissions', '\App\Http\Controllers\PermissionController@index');
Route::get('permissions/edit/{role_id}', '\App\Http\Controllers\PermissionController@edit');
Route::post('permissions/update', '\App\Http\Controllers\PermissionController@update');
Route::get('permissions/create', '\App\Http\Controllers\PermissionController@create');
Route::post('permissions/store', '\App\Http\Controllers\PermissionController@store');
Route::get('permissions/delete/{role_id}', '\App\Http\Controllers\PermissionController@destroy');


});

Route::group(['middleware'=> 'web', 'auth'],function(){
});
//test Routes
Route::group(['middleware'=> 'web' , 'auth'],function(){
  Route::resource('test','\App\Http\Controllers\TestController');
  Route::post('test/{id}/update','\App\Http\Controllers\TestController@update');
  Route::get('test/{id}/delete','\App\Http\Controllers\TestController@destroy');
  Route::get('test/{id}/deleteMsg','\App\Http\Controllers\TestController@DeleteMsg');
});

//attribute_set Routes
Route::group(['middleware'=> 'web' , 'auth'],function(){
  Route::resource('attribute_set','\App\Http\Controllers\Attribute_setController');
  Route::post('attribute_set/{id}/update','\App\Http\Controllers\Attribute_setController@update');
  Route::get('attribute_set/{id}/delete','\App\Http\Controllers\Attribute_setController@destroy');
  Route::get('attribute_set/{id}/deleteMsg','\App\Http\Controllers\Attribute_setController@DeleteMsg');
});

//attribute Routes
Route::group(['middleware'=> 'web' , 'auth'],function(){
  Route::resource('attribute','\App\Http\Controllers\AttributeController');
  Route::post('attribute/{id}/update','\App\Http\Controllers\AttributeController@update');
  Route::get('attribute/{id}/delete','\App\Http\Controllers\AttributeController@destroy');
  Route::get('attribute/{id}/deleteMsg','\App\Http\Controllers\AttributeController@DeleteMsg');
});

//attributes_mapping Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('attributes_mapping','\App\Http\Controllers\Attributes_mappingController');
  Route::post('attributes_mapping/{id}/update','\App\Http\Controllers\Attributes_mappingController@update');
  Route::get('attributes_mapping/{id}/delete','\App\Http\Controllers\Attributes_mappingController@destroy');
  Route::get('attributes_mapping/{id}/deleteMsg','\App\Http\Controllers\Attributes_mappingController@DeleteMsg');
});

//attributes_datum Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('attributes_datum','\App\Http\Controllers\Attributes_datumController');
  Route::post('attributes_datum/{id}/update','\App\Http\Controllers\Attributes_datumController@update');
  Route::get('attributes_datum/{id}/delete','\App\Http\Controllers\Attributes_datumController@destroy');
  Route::get('attributes_datum/{id}/deleteMsg','\App\Http\Controllers\Attributes_datumController@DeleteMsg');
});

//category Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('category','\App\Http\Controllers\CategoryController');
  Route::post('add-category',['as'=>'add.category','uses'=>'CategoryController@addCategory']);
  Route::post('category/{id}/update','\App\Http\Controllers\CategoryController@update');
  Route::get('category/{id}/delete','\App\Http\Controllers\CategoryController@destroy');
  Route::get('category/{id}/deleteMsg','\App\Http\Controllers\CategoryController@DeleteMsg');
});

//categories_attribute Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('categories_attribute','\App\Http\Controllers\Categories_attributeController');
  Route::post('categories_attribute/{id}/update','\App\Http\Controllers\Categories_attributeController@update');
  Route::get('categories_attribute/{id}/delete','\App\Http\Controllers\Categories_attributeController@destroy');
  Route::get('categories_attribute/{id}/deleteMsg','\App\Http\Controllers\Categories_attributeController@DeleteMsg');
});

//categories_product Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('categories_product','\App\Http\Controllers\Categories_productController');
  Route::post('categories_product/{id}/update','\App\Http\Controllers\Categories_productController@update');
  Route::get('categories_product/{id}/delete','\App\Http\Controllers\Categories_productController@destroy');
  Route::get('categories_product/{id}/deleteMsg','\App\Http\Controllers\Categories_productController@DeleteMsg');
});


Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::post('product/attributeListByCategory','\App\Http\Controllers\ProductController@attributeListByCategory');
    Route::post('product/attributeListByattributeset','\App\Http\Controllers\ProductController@attributeListByattributeset');
    Route::post('product/attributeListByattributesetupdate','\App\Http\Controllers\ProductController@attributeListByattributesetupdate');
    Route::post('products/getalll3categories','\App\Http\Controllers\ProductController@getalll3categories');
    Route::post('products/configurableopt','\App\Http\Controllers\ProductController@getconfigurableopt');
  Route::post('product/sampleCSVDownload','\App\Http\Controllers\ProductController@sampleCSVDownload');
  Route::post('product/bulkproductSubmit','\App\Http\Controllers\ProductController@bulkproductSubmit');
  Route::resource('product/bulkproduct','\App\Http\Controllers\ProductController@bulkproduct');
  Route::resource('product','\App\Http\Controllers\ProductController');
    Route::resource('product/create','\App\Http\Controllers\ProductController@create');

    Route::get('products/approvalpending',[
        'middleware'=> ['permission:Pending Approval'],
        'uses' => '\App\Http\Controllers\ProductController@approvalpending']);
    Route::get('products/productreject',[
        'middleware'=> ['permission:Reject'],
        'uses' => '\App\Http\Controllers\ProductController@productreject']);

    Route::get('products/storefrontpush',
            ['middleware'=> ['permission:Store Front Push'],
            'uses' => '\App\Http\Controllers\ProductController@storefrontpush']);

    Route::get('products/approval',
        ['middleware'=> ['permission:Approval'],
        'uses' => '\App\Http\Controllers\ProductController@approval']);

    Route::resource('product/edit','\App\Http\Controllers\ProductController@update');
    Route::resource('product/view','\App\Http\Controllers\ProductController@view');
    Route::get('products/approvalview/{id}/{type}','\App\Http\Controllers\ProductController@approvalview');
    Route::get('products/backtoapproval/{id}','\App\Http\Controllers\ProductController@backtoapproval');
    Route::get('products/pushtomagento/{id}','\App\Http\Controllers\ProductController@pushtomagento');
    Route::get('products/rejectview/{id}','\App\Http\Controllers\ProductController@rejectview');

  //Route::get('upload', 'UploadsController@index');
    Route::post('upload/uploadFiles', '\App\Http\Controllers\ProductController@multiple_upload');
    Route::post('products/add', '\App\Http\Controllers\ProductController@add_productdata');
    Route::post('products/updatedata', '\App\Http\Controllers\ProductController@update_productdata');
    Route::post('products/addapprovereject', '\App\Http\Controllers\ProductController@addapprovereject');
    Route::post('products/revertapproval', '\App\Http\Controllers\ProductController@revertapproval');


    Route::get('searchajax',array('as'=>'searchajax','uses'=>'ProductController@autoComplete'));
    Route::resource('addmagento', '\App\Http\Controllers\ProductController@addmagento');
    Route::resource('product-grid-data', '\App\Http\Controllers\ProductController@productgriddata');
    Route::resource('approval-pending-data', '\App\Http\Controllers\ProductController@approvalpendinggriddata');
    Route::resource('product-approval-data', '\App\Http\Controllers\ProductController@productaprovalgriddata');
    Route::resource('product-reject-data', '\App\Http\Controllers\ProductController@productrejectedgriddata');
    Route::resource('storefront-push-data', '\App\Http\Controllers\ProductController@storefrontpushgriddata');
  //Route::post('categories_product/{id}/update','\App\Http\Controllers\Categories_productController@update');
 // Route::get('categories_product/{id}/delete','\App\Http\Controllers\Categories_productController@destroy');
 // Route::get('categories_product/{id}/deleteMsg','\App\Http\Controllers\Categories_productController@DeleteMsg');

    Route::get('products/containers', [
        'as' => 'product.containers',
        'uses' => '\App\Http\Controllers\ProductController@getContainers'
    ]);

    Route::get('product/store', [
        'as' => 'product.store',
        'uses' => '\App\Http\Controllers\ProductController@store'
    ]);

    Route::get('product/show', [
        'as' => 'product.imagesave',
        'uses' => '\App\Http\Controllers\ProductController@show'
    ]);
    Route::post('product/store', [
        'as' => 'product.imagesave',
        'uses' => '\App\Http\Controllers\ProductController@store'
    ]);
    Route::get('product/imageupdate/{id}', [
        'as' => 'product.imageupdate',
        'uses' => '\App\Http\Controllers\ProductController@imageupdate'
    ]);
    Route::post('product/updatestore/{id}', [
        'as' => 'product.updatestore',
        'uses' => '\App\Http\Controllers\ProductController@updatestore'
    ]);
    Route::get('product/containers/pro', [
        'as' => 'product.containers',
        'uses' => '\App\Http\Controllers\ProductController@getContainers'
    ]);

    Route::post('product/imagecreate', [
        'as' => 'product.imagecreate',
        'uses' => '\App\Http\Controllers\ProductController@imagecreate'
    ]);

    Route::delete('product/imagedestroy/{id}', [
        'as' => 'product.imagedestroy',
        'uses' => '\App\Http\Controllers\ProductController@imagedestroy'
    ]);
    Route::delete('product/updateimagedestroy/{id}', [
        'as' => 'product.updateimagedestroy',
        'uses' => '\App\Http\Controllers\ProductController@updateimagedestroy'
    ]);

    Route::post('product/move', [
        'as' => 'product.move',
        'uses' => '\App\Http\Controllers\ProductController@move'
    ]);

});

//product_universal_sku Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('product_universal_sku','\App\Http\Controllers\Product_universal_skuController');
  Route::post('product_universal_sku/{id}/update','\App\Http\Controllers\Product_universal_skuController@update');
  Route::get('product_universal_sku/{id}/delete','\App\Http\Controllers\Product_universal_skuController@destroy');
  Route::get('product_universal_sku/{id}/deleteMsg','\App\Http\Controllers\Product_universal_skuController@DeleteMsg');
});

//vendor_info Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('vendor_info','\App\Http\Controllers\Vendor_infoController');
  Route::resource('vendor-grid-data', '\App\Http\Controllers\Vendor_infoController@vendordata');
  Route::resource('vendor_application_request','\App\Http\Controllers\Vendor_infoController@vendorApplicationRequest');
  Route::resource('vendor_application_request_data', '\App\Http\Controllers\Vendor_infoController@vendorApplicationRequestData');
  Route::resource('vendor_application_review','\App\Http\Controllers\Vendor_infoController@vendorApplicationReview');
  Route::resource('vendor_application_review_data', '\App\Http\Controllers\Vendor_infoController@vendorApplicationReviewData');
  Route::post('vendor_info/checkVendorAlradyExsist','\App\Http\Controllers\Vendor_infoController@checkVendorAlradyExsist');
  Route::get('vendor_info/edit/{id}','\App\Http\Controllers\Vendor_infoController@edit');
  Route::post('vendor_info/{id}/update','\App\Http\Controllers\Vendor_infoController@update');
  Route::post('vendor_info/view','\App\Http\Controllers\Vendor_infoController@veiwBasicVendorInfo');
  Route::post('approveVendorRequest','\App\Http\Controllers\Vendor_infoController@approvedVendor');
  Route::post('approvedVendorSubmit','\App\Http\Controllers\Vendor_infoController@approvedVendorFormSubmit');
  
  Route::post('rejected','\App\Http\Controllers\Vendor_infoController@rejecteVendor');
  Route::post('rejectedfromfinance','\App\Http\Controllers\Vendor_infoController@rejectedFromFinanceDepart');
  Route::post('rejectedfromops','\App\Http\Controllers\Vendor_infoController@rejectedFromOpsDepart');
  Route::post('approved','\App\Http\Controllers\Vendor_infoController@vendorFinalApproval');
  Route::post('approvedSubmit','\App\Http\Controllers\Vendor_infoController@vendorFinalApprovalSubmit');
  Route::post('approvedfromfinance','\App\Http\Controllers\Vendor_infoController@approvedFromFinanceDepart');
  Route::post('approvedfromops','\App\Http\Controllers\Vendor_infoController@approvedFromOpsDepart');
  Route::post('holded','\App\Http\Controllers\Vendor_infoController@holdVendor');
  Route::post('leaded','\App\Http\Controllers\Vendor_infoController@leadedVendor');
  
  Route::get('vendor_info/{id}/delete','\App\Http\Controllers\Vendor_infoController@destroy');
  Route::get('vendor_info/{id}/deleteMsg','\App\Http\Controllers\Vendor_infoController@DeleteMsg');
});

//brand Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('brand','\App\Http\Controllers\BrandController');
  Route::post('brand/{id}/update','\App\Http\Controllers\BrandController@update');
  Route::get('brand/{id}/delete','\App\Http\Controllers\BrandController@destroy');
  Route::get('brand/{id}/deleteMsg','\App\Http\Controllers\BrandController@DeleteMsg');
});

//VENDOR Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('vendordetail','\App\Http\Controllers\VendorController');
  Route::get('thankyou','\App\Http\Controllers\VendorController@thankyou');
  Route::get('vendor/dashboard','\App\Http\Controllers\VendorController@dashboard');
  Route::get('applicationpending','\App\Http\Controllers\VendorController@applicationPendingScreen');
  Route::get('applicationcorrection','\App\Http\Controllers\VendorController@applicationCorrectionScreen');
  Route::post('getChildCategory','\App\Http\Controllers\VendorController@getChildCategory');
  Route::post('viewAllOtherCategory','\App\Http\Controllers\VendorController@viewAllOtherCategory');
  Route::post('vendorSigningContract','\App\Http\Controllers\VendorController@vendorSigningContract');
  Route::post('downloadmargin','\App\Http\Controllers\VendorController@downloadAgreedMargin');
 
  Route::post('sendForCorrection','\App\Http\Controllers\VendorController@sendForCorrection');
  Route::post('checkmargindownloaded','\App\Http\Controllers\VendorController@checkMarginDownloaded');
  Route::post('sendapplicationforreview','\App\Http\Controllers\VendorController@sendApplicationForReviewToOtherDepart');

  
  Route::resource('vendor-margin-grid-data', '\App\Http\Controllers\VendorController@vendormargindata');
   Route::post('vendor/margin_submit','\App\Http\Controllers\VendorController@vendormargindatasubmit');
  
});


Route::resource('addVendorFromYayvo','\App\Http\Controllers\Vendor_infoController@addVendorFromYayvo');
Route::resource('getlistofmarginmyvendor','\App\Http\Controllers\VendorController@getListOfMarginByVendorId'); //DOWNLOAD PDF MARGIN
//attribute_option_value Routes
Route::group(['middleware'=> 'web', 'auth'],function(){
  Route::resource('attribute_option_value','\App\Http\Controllers\Attribute_option_valueController');
  Route::post('attribute_option_value/{id}/update','\App\Http\Controllers\Attribute_option_valueController@update');
  Route::get('attribute_option_value/{id}/delete','\App\Http\Controllers\Attribute_option_valueController@destroy');
  Route::get('attribute_option_value/{id}/deleteMsg','\App\Http\Controllers\Attribute_option_valueController@DeleteMsg');
});
