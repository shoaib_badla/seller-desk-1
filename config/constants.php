<?php

return [
	'superadminRole' => 'superadmin',
	'headOfVendor' => 'head_of_vendor',
	'vendorRole' => 'vendor',
	'categoryManagerRole' => 'category_manager',
	'financeRole' => 'finance',
	'operationRole' => 'operation',
	'apiRole' => 'api',
	'vendorManagerRole' => 'vendor_manager',
	'magentoUrl' => 'http://scp.yayvo.com/index.php',
	'vendorAcceptedStatus' => 'Accepted',
	'vendorApprovedStatus' => 'Approved',
	'vendorRejectedStatus' => 'Rejected', #DONT' CHANGE STATUS USED IN MAGENTO INLINE QUERY
	'vendorLeadStatus' => 'Lead',
	'vendorNewStatus' => 'new',
	
	//COMMENT HISTORY STATUS
	'correctionRequired' => 'correction_required',
	'applicationSendOtherDeaprt' => 'application_send_for_review_to_departs',
	'applicationRejectedFromFinance' => 'vendor_rejected_by_finance',
	'applicationRejectedFromOps' => 'vendor_rejected_by_ops',
	'applicationApprovedFromOps' => 'vendor_approved_by_ops',
	'applicationApprovedFromFinance' => 'vendor_approved_by_finance',
	
	'cc_email_address' => 'muhammad.arshad@tcs-e.com',
     
//MAGENTO SOAP URL
	 'magentoApiUrl' => 'http://scp.yayvo.com/index.php/api/?wsdl',
	 'magentoApiUsername' => 'arshad',
	 'magentoApiPassword' => 'abc123+',
];
