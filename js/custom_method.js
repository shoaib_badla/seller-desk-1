jQuery(document).ready(function(){
	//FIXED LENGTH CHARACTER
	jQuery(".maxAllowedCharacterArea").on('keyup',function (e) {
		
			maxCharacterLimitForField(jQuery(this));
	});
	
	//GET DEFAULT VALUE IF ANY
	jQuery(".maxAllowedCharacterArea").each(function () {
		
			maxCharacterLimitForField(jQuery(this));
	});
	
});

//MAX CHARACTER LIMIT MESSAGE DISPAY
function maxCharacterLimitForField(fieldClass){
	
	var maxAllowedCharacter	= fieldClass.attr('maxAllowedCharacter');
	var currentId			= fieldClass.attr('id');
	
	fixedInputLength(fieldClass,maxAllowedCharacter);
	
	if(fieldClass.val().length > maxAllowedCharacter){
		return false;
	}
	//jQuery("#span_"+currentId).html("Remaining characters : " +(maxAllowedCharacter - jQuery(this).val().length));
	fieldClass.siblings('span.maxcharacterLimit').html("Remaining characters : " +(maxAllowedCharacter - fieldClass.val().length));
}

function displayChildHideAndShowBtn(parentDiv,childRegion,addbtnClass,removeBtnClass){
	
	var totalChildNumber = jQuery("#"+parentDiv).children().length;
	var loop = 1;
	jQuery("."+childRegion).each(function(){
		//console.log(loop);
		//console.log(totalChildNumber);
		//IF ONLY ONE ELEMENT
		if(totalChildNumber==1){
			jQuery(this).find("div.box-tools" ).show();
			jQuery('.'+addbtnClass).show();
			jQuery('.'+removeBtnClass).hide();
		}else if(loop == totalChildNumber){
			jQuery(this).find("div.box-tools" ).show();
			//DISPLAY REMOVE AND ADD ON LAST CHILD
			jQuery('.'+addbtnClass).show();
			jQuery('.'+removeBtnClass).show();
		}else{
			//console.log('mak');
			jQuery(this).find("div.box-tools" ).hide();
		}
		
		loop++;
	});
}

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
